import { Component, ViewChild } from '@angular/core';

import { Events, MenuController, Nav, Platform, AlertController } from 'ionic-angular';
import { Splashscreen } from 'ionic-native';
import { Storage } from '@ionic/storage';

import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { Dashboard2dPage } from '../pages/dashboard2d/dashboard2d';
import {SQLite} from 'ionic-native';
import { Http } from '@angular/http';
import {TranslateService, TranslatePipe} from 'ng2-translate/ng2-translate';
import { Network } from '@ionic-native/network';
import { DbService } from '../providers/db-service';
import { Push, PushToken } from '@ionic/cloud-angular';

declare var cordova;
@Component({
  templateUrl: 'app.template.html'
})
export class ConferenceApp {
  // the root nav is a child of the root app component
  // @ViewChild(Nav) gets a reference to the app's root nav
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  isLogin: boolean = false;

  //Idle
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;

  drop_config = false;

  constructor(
    public events: Events,
    public menu: MenuController,
    public platform: Platform,
    public storage: Storage,
    private http: Http,
    private translate: TranslateService,
    private network: Network,
    private database: DbService,
    public alertCtrl: AlertController,
    public push: Push
  ) {
    platform.ready().then(() => {
            let db = new SQLite();
            console.log("open database app.compontent")
            db.openDatabase({
                name: "route_db.db",
                location: "default"
            }).then(() => {
                let tableDrop = "DROP TABLE IF EXISTS config";
                let table = "CREATE TABLE IF NOT EXISTS config(id INTEGER PRIMARY KEY, urlLogin VARCHAR(50), inic_config BOOLEAN, tokenPush VARCHAR(255), ultimo INTEGER, sucxcom VARCHAR(255), inic_sync BOOLEAN)";
                // let table2 = "CREATE TABLE IF NOT EXISTS mis_clientes(codcli VARCHAR(50) PRIMARY KEY, todos BOOLEAN)";
                // let table3 = "CREATE TABLE IF NOT EXISTS productosxcliente(codpro VARCHAR(50), codcli VARCHAR(50), prio INTEGER, PRIMARY KEY (codpro, codcli))";
                // let table4 = "CREATE TABLE IF NOT EXISTS mis_planes(codpdv VARCHAR(50) PRIMARY KEY, despdv VARCHAR(50))";
                // let table5 = "CREATE TABLE IF NOT EXISTS clientesxplanes(codpdv VARCHAR(100), codcli VARCHAR(50), prio INTEGER, visitado BOOLEAN, codmot VARCHAR(4), PRIMARY KEY (codpdv, codcli))";
                //let cobranzaxdeposito_drop = "DROP TABLE IF EXISTS cobranzaxdeposito";
                //let cobranzaxdeposito_create = "CREATE TABLE IF NOT EXISTS cobranzaxdeposito(depnum VARCHAR(50), cobnum VARCHAR(50), facnum VARCHAR(50), coblin INTEGER, PRIMARY KEY(depnum, cobnum, facnum, coblin))";
              /*//////////////////////////////////////////////////facturacion testing/////////////////////////////////////////////////
              this.http.get("https://my-json-server.typicode.com/citerio/jsonapi/db").map(res => res.json()).subscribe(
                (res: any) => {
                  this.database.openDatabase().then(
                    () => {
                      for (let index = 0; index < res.tables.length; index++) {
                        this.database.deleteTable(res.tables[index].drop);
                        this.database.createTable2(res.tables[index].create);
                      }
                    });
                });
    //////////////////////////////////////////////////facturacion testing/////////////////////////////////////////////////*/
                

                if(this.drop_config) {
                  db.executeSql(tableDrop, {}).then(
                  () => {
                    db.executeSql(table, {});
                  });
                } else {
                  db.executeSql(table, {});
                }

                /*if(true) {
                  db.executeSql(cobranzaxdeposito_drop, {}).then(
                  () => {
                    db.executeSql(cobranzaxdeposito_create, {});
                  });
                }*/
                
                // db.executeSql(table2, {});
                // db.executeSql(table3, {});
                // db.executeSql(table4, {});
                // db.executeSql(table5, {});

            }, (error) => {
                console.error("Unable to open database", error);
            });

            this.initPushNotifications();
        });

    this.initTranslation();
    this.platformReady();
  }

  platformReady() {
    // Call any initial plugins when ready
    this.platform.ready().then(() => {
      cordova.plugins.certificates.trustUnsecureCerts(true);
      this.storage.get('todo').then(
        (value) => {
          if( (value !== undefined) && (value !== '') && (value != null) ){
            this.rootPage = DashboardPage;
          } else {
            this.rootPage = LoginPage;
          }
        },
        (error) => {
          this.rootPage = LoginPage;
        }).catch(() => {this.rootPage = LoginPage;});

        Splashscreen.hide();
    });
  }

  initTranslation() {
    var userLang = navigator.language.split('-')[0]; // use navigator lang if available
    userLang = /(en|es)/gi.test(userLang) ? userLang : 'en';
 
    // this language will be used as a fallback when a translation isn't found in the current language
    this.translate.setDefaultLang('en');
 
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this.translate.use(userLang);
  }

  initPushNotifications(){

    
    /*this.push.register().then((t: PushToken) => {
      return this.push.saveToken(t);
    }).then((t: PushToken) => {
      console.log('token: ' + t.token);
      this.database.openDatabase().then(
      () => {
        this.database.insertOrUpdateTokenPush(t.token).then(
          () => {
            console.log('Token saved:', t.token);
          });
      });
    });*/

     this.database.openDatabase().then(
      () => {
        this.database.insertOrUpdateTokenPush("xxxx").then(
          () => {
            console.log('Token saved:', "xxxx");
          });
      });          

    /*this.push.rx.notification()
    .subscribe((msg) => {

      if(msg.app.asleep || msg.app.closed){

        this.showMessage(msg);

      }else{

        this.showMessage(msg);

      }

      console.log('I received awesome push: ' + msg);
    });*/
  }

  showMessage(msg: any){

    this.translate.get(["common.push", "common.acept"] , null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
        title: localizedValues["common.push"],
        subTitle: msg.title,      
        message: msg.text,
        buttons: [
          {
            text: localizedValues["common.acept"],
            handler: () =>{
              console.log('aceptar');
            }
          }
        ]
      });
      alert.present();        
      
      });  

  }

}
