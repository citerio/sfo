import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from 'ionic-native';
import { AppConstants } from './app-constants';
import { AppUtils } from './app-utils';

/*
  Generated class for the DbService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DbService {

  db: SQLite = null;
  data: any;
  current_order_number: string = '';
  current_charge_number: string = '';
  current_invoice_number: string = '';
  current_invoice_ln_number: string = '';
  current_invoice_control_ln_number: string = '';
  current_deposit_number: string = '';

  constructor(
    public http: Http) {
    this.db = new SQLite();
  }



  ////////////////////////////////////////////////////common operations//////////////////////////////////
  openDatabase() {
    let db_name = '';
    db_name = 'route_db.db';
    return this.db.openDatabase({
      name: db_name,
      location: 'default' // the location field is required
    });
  }

  load() {

    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }

    // don't have the data yet
    return new Promise(resolve => {
      this.http.get('http://www.json-generator.com/api/json/get/cjKyhvbFlu?indent=2')
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });

  }

  activateFK() {

    let sql = 'PRAGMA foreign_keys = ON';
    this.db.executeSql(sql, []);

  }

  createTables() {
    let tables = [];
    let sql = '';
    tables = this.data.tables;
    for (let index = 0; index < tables.length; index++) {
      sql = tables[index].query;
      this.db.executeSql(sql, []);
    }
  }

  loadTables() {
    let data = [];
    let sql = '';
    data = this.data.data;
    for (let index = 0; index < data.length; index++) {
      sql = data[index].query;
      this.db.executeSql(sql, []);
    }
  }

  getAllProducts() {
    let sql = 'SELECT * FROM MAE1037 ORDER BY despro';
    return this.db.executeSql(sql, [])
      .then(response => {
        let productos = [];
        for (let index = 0; index < response.rows.length; index++) {
          productos.push(response.rows.item(index));
        }
        return Promise.resolve(productos);
      })
  }

  getAllClients() {
    let sql = 'SELECT * FROM MAE1016 ORDER BY descli ';
    return this.db.executeSql(sql, [])
      .then(response => {
        let clientes = [];
        for (let index = 0; index < response.rows.length; index++) {
          clientes.push(response.rows.item(index));
        }
        return Promise.resolve(clientes);
      })
  }

  getClientName(codcli: any) {

    let sql = 'SELECT * FROM MAE1016 WHERE codcli=?';
    return this.db.executeSql(sql, [codcli])
      .then(response => {

        return Promise.resolve(response.rows.item(0));

      })

  }

  getSENIATSerial() {

    let sql = 'SELECT * FROM PAR1007';
    return this.db.executeSql(sql, [])
      .then(response => {

        return Promise.resolve(response.rows.item(0));

      })

  }

  getTaxByProduct(lista: any[]) {

    let products: any[] = [];

    lista.forEach(e => {

      products.push(e.codart);

    });

    let sql = "SELECT * FROM MAE1037 WHERE codpro IN (" + products.join(',') + ")";
    return this.db.executeSql(sql, [])
      .then(response => {
        let clientes = [];
        for (let index = 0; index < response.rows.length; index++) {
          clientes.push(response.rows.item(index));
        }
        return Promise.resolve(clientes);
      })

  }

  /*getTaxByProduct(lista: any[]){    

    let products: any[] = [];

    lista.forEach(e => {
            
      products.push(e.codart.toString().trim());
      
    });

    let sql = "SELECT * FROM MAE1037";

    if(products.length > 0) {
      sql += " WHERE codpro IN (";

      for(let index = 0; index < (products.length - 1); index++){
        sql += "?, ";
      }
      sql += "?)"
    } 

    console.log('consulta sql: ' + sql);

    return this.db.executeSql(sql, [products])
    .then(response => {
      console.log('consulta bien: getTaxByProduct');
      let clientes = [];
      for (let index = 0; index < response.rows.length; index++) {
        clientes.push( response.rows.item(index) );
      }
      return Promise.resolve( clientes );
    })

  }*/

  getCompanyName(codusr: any) {

    let sql = 'SELECT * FROM REL1003 WHERE codusr=?';
    return this.db.executeSql(sql, [codusr])
      .then(response => {

        return Promise.resolve(response.rows.item(0));

      })

  }

  getPrncpy() {

    let sql = 'SELECT prncpy FROM PAR1006';
    return this.db.executeSql(sql, [])
      .then(response => {
        let prncpy = response.rows.item(0).prncpy;
        return Promise.resolve(prncpy);
      })

  }

  getGpsobl() {
    let sql = 'SELECT gpsobl FROM PAR1006';
    return this.db.executeSql(sql, [])
      .then(response => {
        let gpsobl = response.rows.item(0).gpsobl;
        return Promise.resolve(gpsobl);
      })

  }

  getProductPresentations(coduni: any) {
    let sql = 'SELECT * FROM codunv WHERE coduni=? ORDER BY descripcion';
    return this.db.executeSql(sql, [coduni])
      .then(response => {
        let presentaciones = [];
        for (let index = 0; index < response.rows.length; index++) {
          presentaciones.push(response.rows.item(index));
        }
        return Promise.resolve(presentaciones);
      })
  }

  getNotVisitedReasons() {
    let sql = 'SELECT * FROM MAE1017 WHERE tipmotInt=? OR tipmotInt=?';
    return this.db.executeSql(sql, [0, 1])
      .then(response => {
        let motivos = [];
        for (let index = 0; index < response.rows.length; index++) {
          motivos.push(response.rows.item(index));
        }
        return Promise.resolve(motivos);
      })
  }

  getRemoveLineReasons() {
    let sql = 'SELECT * FROM MAE1017 WHERE tipmotInt=?';
    return this.db.executeSql(sql, [7])
      .then(response => {
        let motivos = [];
        for (let index = 0; index < response.rows.length; index++) {
          motivos.push(response.rows.item(index));
        }
        return Promise.resolve(motivos);
      });
  }

  getRemoveOrderReasons() {
    let sql = 'SELECT * FROM MAE1017 WHERE tipmotInt=?';
    return this.db.executeSql(sql, [8])
      .then(response => {
        let motivos = [];
        for (let index = 0; index < response.rows.length; index++) {
          motivos.push(response.rows.item(index));
        }
        return Promise.resolve(motivos);
      });
  }

  updateNotVisitedReason(codpdv: any, codcli: any, feccre: any, codmot: any, visited: any) {

    let sql = 'UPDATE clientesxplanes SET codmot=?, visitado=?, sync=?, eco=eco+1 WHERE codpdv=? AND codcli=? AND feccre=?';
    return this.db.executeSql(sql, [codmot, visited, false, codpdv, codcli, feccre]);

  }

  ////////////////////////////////////////////////////common operations//////////////////////////////////




  ////////////////////////////////////////////////////orders operations//////////////////////////////////

  getClientByOrder(pednum: string) {
    let cliente: any = {};
    let sql = 'SELECT codcli FROM PED1000 WHERE pednum = ?';
    return this.db.executeSql(sql, [pednum])
      .then(response => {
        let codcli = response.rows.item(0).codcli;
        return Promise.resolve(codcli);
      });
  }

  // getProductsByClient(codcli: string, offset: number, cdcplt: string) {
  //   let object: any = { "todos": false, "productos": [] };
  //   let sql = "SELECT todos FROM mis_clientes WHERE codcli = ?";
  //   console.log("codcli: " + codcli);
  //   return this.db.executeSql(sql, [codcli])
  //     .then(response => {
  //       object.todos = response.rows.item(0).todos;
  //       sql = "SELECT mae1037.codpro, mae1037.despro, tmp1001.precio AS prepro, mae1037.codunv, pxc.prio from MAE1037 mae1037 JOIN productosxcliente pxc ON mae1037.codpro = pxc.codpro" +
  //         " JOIN TMP1001 tmp1001 ON tmp1001.codart = mae1037.codpro WHERE pxc.codcli = ? AND tmp1001.codlis = ? ORDER BY despro LIMIT ? OFFSET ?";

  //       return this.db.executeSql(sql, [codcli, cdcplt, AppConstants.SQL_LIMIT_ROW, offset])
  //         .then(response => {
  //           for (let index = 0; index < response.rows.length; index++) {
  //             object.productos.push(response.rows.item(index));
  //           }
  //           return Promise.resolve(object);
  //         })
  //     });
  // }

  getProductsByClient(codcli: string, offset: number, cdcplt: string) {
    let object: any = { "todos": false, "productos": [] };
    let sql = "SELECT todos FROM mis_clientes WHERE codcli = ?";
    console.log("codcli: " + codcli);
    return this.db.executeSql(sql, [codcli])
      .then(response => {
        object.todos = response.rows.item(0).todos;
        sql = "SELECT mae1037.codpro, mae1037.despro, tmp1001.coddiv, tmp1001.precio AS prepro, mae1037.codunv from MAE1037 mae1037" +
          " JOIN TMP1001 tmp1001 ON tmp1001.codart = mae1037.codpro WHERE tmp1001.codlis = ? ORDER BY despro LIMIT ? OFFSET ?";

        return this.db.executeSql(sql, [cdcplt, AppConstants.SQL_LIMIT_ROW, offset])
          .then(response => {
            for (let index = 0; index < response.rows.length; index++) {
              object.productos.push(response.rows.item(index));
            }
            return Promise.resolve(object);
          })
      });
  }

  getProductsByClientByFilter(codcli: string, offset: number, filter: string, cdcplt: string) {
    let object: any = { "todos": false, "productos": [] };
    let sql = "SELECT todos FROM mis_clientes WHERE codcli = ?";
    console.log("codcli: " + codcli);
    return this.db.executeSql(sql, [codcli])
      .then(response => {
        object.todos = response.rows.item(0).todos;
        sql = "SELECT mae1037.codpro, mae1037.despro, tmp1001.coddiv, tmp1001.precio AS prepro, mae1037.codunv from MAE1037 mae1037" +
          " JOIN TMP1001 tmp1001 ON tmp1001.codart = mae1037.codpro WHERE tmp1001.codlis = ? AND (mae1037.despro LIKE '%" + filter + "%' OR mae1037.codpro LIKE '%" + filter + "%')  ORDER BY despro LIMIT ? OFFSET ?";

        return this.db.executeSql(sql, [cdcplt, AppConstants.SQL_LIMIT_ROW, offset])
          .then(response => {
            for (let index = 0; index < response.rows.length; index++) {
              object.productos.push(response.rows.item(index));
            }
            return Promise.resolve(object);
          })
      });
  }

  getProductPriceByCode(codlis: string, codart: string) {
    let sql = 'SELECT precio FROM TMP1001 WHERE codlis = ? AND codart = ?';
    console.log('codlis: ' + codlis + ' codart: ' + codart);
    return this.db.executeSql(sql, [codlis, codart])
      .then(response => {
        let precio = response.rows.item(0).precio;
        return Promise.resolve(precio);
      });
  }

  getCdcpltFromClient(codcli: string) {
    let sql = 'SELECT cdcplt FROM MAE1016 WHERE codcli = ?';
    return this.db.executeSql(sql, [codcli])
      .then(response => {
        let cdcplt = response.rows.item(0).cdcplt;
        return Promise.resolve(cdcplt);
      });
  }

  getAllClientsProducts(lista: any[], offset: number, cdcplt: string) {
    let sql = "SELECT mae1037.codpro, mae1037.despro, tmp1001.precio AS prepro, mae1037.codunv FROM MAE1037 mae1037 JOIN TMP1001 tmp1001 ON mae1037.codpro = tmp1001.codart";

    if (lista.length > 0) {
      sql += " WHERE codpro NOT IN (";

      for (let index = 0; index < (lista.length - 1); index++) {
        sql += "?, ";
      }
      sql += "?)"
      sql += " AND tmp1001.codlis = ?"
    } else {
      sql += " WHERE tmp1001.codlis = ?";
    }
    sql += " ORDER BY despro";
    sql += " LIMIT ? OFFSET ?";

    console.log('consulta sql: ' + sql);
    lista.push(cdcplt);
    lista.push(AppConstants.SQL_LIMIT_ROW);
    lista.push(offset);
    return this.db.executeSql(sql, lista).then(
      response => {
        console.log('consulta bien: getAllClientsProducts');
        let productos: any[] = [];
        for (let i = 0; i < response.rows.length; i++) {
          productos.push(response.rows.item(i));
        }
        return Promise.resolve(productos);
      });
  }

  getAllClientsProductsByfilter(lista: any[], offset: number, filter: string, cdcplt: string) {
    let sql = "SELECT mae1037.codpro, mae1037.despro, tmp1001.precio AS prepro, mae1037.codunv FROM MAE1037 JOIN TMP1001 tmp1001 ON mae1037.codpro = tmp1001.codart";

    sql += " WHERE";
    if (lista.length > 0) {
      sql += " codpro NOT IN (";

      for (let index = 0; index < (lista.length - 1); index++) {
        sql += "?, ";
      }
      sql += "?) AND"
    }

    sql += " tmp1001.codlis = ? AND despro LIKE '%" + filter + "%'";

    sql += " ORDER BY despro";
    sql += " LIMIT ? OFFSET ?";

    lista.push(cdcplt);
    lista.push(AppConstants.SQL_LIMIT_ROW);
    lista.push(offset);
    return this.db.executeSql(sql, lista).then(
      response => {
        let productos: any[] = [];
        for (let i = 0; i < response.rows.length; i++) {
          productos.push(response.rows.item(i));
        }
        return Promise.resolve(productos);
      });
  }

  getOldProductsLength(lista: any[]) {
    let sql = "SELECT count(codpro) AS total FROM MAE1037";

    if (lista.length > 0) {
      sql += " WHERE codpro NOT IN (";

      for (let index = 0; index < (lista.length - 1); index++) {
        sql += "?, ";
      }
      sql += "?)";
    }

    return this.db.executeSql(sql, lista).then(
      response => {
        console.log('consulta bien: getOldProductsLength');
        return Promise.resolve(response.rows.item(0).total);
      });
  }

  getAllClientsOffset(offset: number) {
    let sql = "SELECT * FROM MAE1016 ORDER BY descli LIMIT ? OFFSET ?";

    return this.db.executeSql(sql, [AppConstants.SQL_LIMIT_ROW, offset]).then(
      response => {
        console.log('consulta bien: getAllClientsOffset');
        let clients: any[] = [];
        for (let i = 0; i < response.rows.length; i++) {
          clients.push(response.rows.item(i));
        }
        return Promise.resolve(clients);
      });
  }

  getAllClientsOffsetCharging(offset: number) {
    let sql = "SELECT * FROM MAE1016 WHERE ((SELECT COUNT(*) FROM FACTURA_COBRAR WHERE MAE1016.codcli = FACTURA_COBRAR.codcli) >= 1) ORDER BY descli LIMIT ? OFFSET ?";

    return this.db.executeSql(sql, [AppConstants.SQL_LIMIT_ROW, offset]).then(
      response => {
        console.log('consulta bien: getAllClientsOffset');
        let clients: any[] = [];
        for (let i = 0; i < response.rows.length; i++) {
          clients.push(response.rows.item(i));
        }
        return Promise.resolve(clients);
      });
  }

  getAllClientsOffsetInvoicing(offset: number) {
    let sql = "SELECT * FROM MAE1016 WHERE ((SELECT COUNT(*) FROM FAC1001 WHERE MAE1016.codcli = FAC1001.codcli) >= 1) ORDER BY descli LIMIT ? OFFSET ?";

    return this.db.executeSql(sql, [AppConstants.SQL_LIMIT_ROW, offset]).then(
      response => {
        console.log('consulta bien: getAllClientsOffset');
        let clients: any[] = [];
        for (let i = 0; i < response.rows.length; i++) {
          clients.push(response.rows.item(i));
        }
        return Promise.resolve(clients);
      });
  }

  getAllClientsOffsetByfilter(offset: number, filter: string) {
    let sql = "SELECT * FROM MAE1016 WHERE descli LIKE '%" + filter + "%' ORDER BY descli LIMIT ? OFFSET ?";

    return this.db.executeSql(sql, [AppConstants.SQL_LIMIT_ROW, offset]).then(
      response => {
        let clients: any[] = [];
        for (let i = 0; i < response.rows.length; i++) {
          clients.push(response.rows.item(i));
        }
        return Promise.resolve(clients);
      });
  }

  getOrders(pedfas: any, pedsta: any) {
    let today = AppUtils.getTodayInitSqliteFormat();
    console.log('today: ' + today);
    let sql = "SELECT PED1000.codcli, PED1000.pedfas, PED1000.pedsta, PED1000.pednum, PED1000.fechcr, PED1000.geopcr, MAE1016.descli FROM PED1000 JOIN MAE1016 ON PED1000.codcli = MAE1016.codcli WHERE pedfas=? AND pedsta=? AND fechcr >= DATETIME('" + today + "') ORDER BY fechcr";
    return this.db.executeSql(sql, [pedfas, pedsta])
      .then(response => {
        let pedidos = [];
        for (let index = 0; index < response.rows.length; index++) {
          pedidos.push(response.rows.item(index));
        }
        return Promise.resolve(pedidos);
      });
  }

  getOrdersToSync() {
    let sql = "SELECT PED1000.codcli, PED1000.pedfas, PED1000.pedsta, PED1000.pednum, PED1000.fechcr, PED1000.geopcr, PED1000.codmot, PED1000.succom FROM PED1000 WHERE (pedfas=? AND pedsta=?) OR (pedfas=? AND pedsta=?) ORDER BY fechcr";
    return this.db.executeSql(sql, [6, 1, 5, 3])
      .then(response => {
        let pedidos = [];
        for (let index = 0; index < response.rows.length; index++) {
          pedidos.push(response.rows.item(index));
        }
        return Promise.resolve(pedidos);
      });
  }

  /*getInvoices(pedfas:any, pedsta:any){
    let sql = 'SELECT FAC1000.codcli, FAC1000.pedfas, FAC1000.pedsta, FAC1000.pednum, FAC1000.fechcr, MAE1016.descli FROM FAC1000 JOIN MAE1016 ON FAC1000.codcli = MAE1016.codcli WHERE pedfas=? AND pedsta=? ORDER BY fechcr';
    return this.db.executeSql(sql, [pedfas,pedsta])
    .then(response => {
      let pedidos = [];
      for (let index = 0; index < response.rows.length; index++) {
        pedidos.push( response.rows.item(index) );
      }
      return Promise.resolve( pedidos );
    })
  }*/

  setOrdenToSync(orderid: string) {
    let sql = 'UPDATE PED1000 SET pedfas=?, pedsta=? WHERE pednum=?';
    return this.db.executeSql(sql, [4, 1, orderid])
      .then(response => {
        return Promise.resolve();
      })
  }

  getSucxcomp() {
    let sql = "SELECT * FROM REL1003";

    return this.db.executeSql(sql, []).then(
      response => {
        let sucxcom: any[] = [];
        for (let i = 0; i < response.rows.length; i++) {
          sucxcom.push(response.rows.item(i));
        }
        return Promise.resolve(sucxcom);
      });
  }

  getclientesListos() {
    // let sql = 'SELECT * FROM clientesxplanes';
    let sql = 'SELECT * FROM clientesxplanes WHERE ((visitado IS ?) OR (visitado IS ?)) AND (sync IS NOT ?)';
    return this.db.executeSql(sql, [false, true, true])//'false', 'true', 'true'
      .then(response => {
        let clientesListos = [];
        console.log('Clientes listos: ' + response.rows.length);
        for (let index = 0; index < response.rows.length; index++) {
          clientesListos.push(response.rows.item(index));
        }
        return Promise.resolve(clientesListos);
      });
  }

  getOpenOrders() {
    let sql = 'SELECT * FROM PED1000 WHERE pedfas=? OR pedfas=? ORDER BY fechcr ';
    return this.db.executeSql(sql, [1, 2])
      .then(response => {
        let open_pedidos = [];
        for (let index = 0; index < response.rows.length; index++) {
          open_pedidos.push(response.rows.item(index));
        }
        return Promise.resolve(open_pedidos);
      })
  }

  getOrder(pednum: any, pedfas: any, pedsta: any) {
    let sql = 'SELECT * FROM PED1001 WHERE pednum=? AND pedfas=? AND pedsta=? ORDER BY fechcr';
    return this.db.executeSql(sql, [pednum, pedfas, pedsta])
      .then(response => {
        let pedido = [];
        for (let index = 0; index < response.rows.length; index++) {
          pedido.push(response.rows.item(index));
        }
        return Promise.resolve(pedido);
      })
  }

  getOrderToSync(pednum: any) {
    let sql = 'SELECT * FROM PED1001 WHERE pednum=? ORDER BY fechcr';
    return this.db.executeSql(sql, [pednum])
      .then(response => {
        let pedido = [];
        for (let index = 0; index < response.rows.length; index++) {
          pedido.push(response.rows.item(index));
        }
        return Promise.resolve(pedido);
      })
  }

  getMonped() {
    let sql = 'SELECT monped FROM PAR1006';
    return this.db.executeSql(sql, [])
      .then(response => {
        return Promise.resolve(response.rows.item(0).monped);
      })
  }

  createOrder(cliente: any, geopcr: string, ispdv: boolean) {


    let serie_pedido = '';
    let ultimo_numero_pedido = '';
    let numero_final = '';

    let codpdv = '';
    if (cliente.codpdv) {
      codpdv = cliente.codpdv;
    }

    return this.getOrderSeriesNum().then((serie) => {
      serie_pedido = serie;
      return this.getSeriesLastNum(serie_pedido).then((ultimo_numero) => {
        ultimo_numero_pedido = ultimo_numero;
        numero_final = serie_pedido + (ultimo_numero + 1);
        this.current_order_number = numero_final;
        this.updateLastNumber(serie_pedido);
        let datetime = new Date().toLocaleString();
        if (ispdv) {
          this.updateVisitPlanDates(codpdv, datetime, cliente.feccre);
        }
        return this.getSelectedSucxcomp().then((sucxcom) => {
          let sql = 'INSERT INTO PED1000(pednum, codcli, fechcr, pedfas, pedsta, geopcr, ispdv, codpdv, succom) VALUES(?,?,?,?,?,?,?,?,?)';
          return this.db.executeSql(sql, [numero_final, cliente.codcli, AppUtils.getSqliteDateTimeFormat(datetime), 1, 1, geopcr, ispdv, codpdv, sucxcom]);
        });
      });
    });
  }



  getChargesOfClient(codcli: string, cobfas: number, cobsta: number) {
    let today = AppUtils.getTodayInitSqliteFormat();
    let sql = "SELECT * FROM cobranza WHERE codcli=? AND cobfas=? AND cobsta=? AND fechcr >= DATETIME('" + today + "')";

    return this.db.executeSql(sql, [codcli, cobfas, cobsta])
      .then(response => {
        let charges = [];
        for (let index = 0; index < response.rows.length; index++) {
          charges.push(response.rows.item(index));
        }
        return Promise.resolve(charges);
      });
  }

  getAllPrintedCharges(cobfas: number, cobsta: number) {
    let sql = "SELECT * FROM cobranza WHERE cobfas=? AND cobsta=?";

    return this.db.executeSql(sql, [cobfas, cobsta])
      .then(response => {
        let charges = [];
        for (let index = 0; index < response.rows.length; index++) {
          charges.push(response.rows.item(index));
        }
        return Promise.resolve(charges);
      });
  }

  /*createCharge(charge: any, invoices: any[]){
    let promises = [];
    let query: string;

    let serie_cobranza = '';
    let ultimo_numero_cobranza = '';
    let numero_final = '';

    let bank = null;
    let reference = '';
    let check_a_number = '';
    let check_number = '';
    let document_number = '';
    let retention_number = 0;

    if(charge.payment_method == 1 || charge.payment_method == 3) {
      reference = charge.reference;
      bank = charge.bank_list;
    } else {
      if(charge.payment_method == 2) {
        bank = charge.bank_list;
        check_a_number = charge.check_a_number;
        check_number = charge.check_number;
      }
    }

    if(charge.cntesp == true) {
      document_number = charge.document_number;
      retention_number = charge.retention_number;
    }

    return this.getChargeSeriesNum().then((serie) => {
      serie_cobranza = serie;
      return this.getChargeSeriesLastNum(serie_cobranza).then((ultimo_numero) => {
        ultimo_numero_cobranza = ultimo_numero;
        numero_final = serie_cobranza + (ultimo_numero + 1);
        this.current_charge_number = numero_final;
        this.updateLastChargeNumber(serie_cobranza);
        let datetime = new Date().toLocaleString();
        let sql = 'INSERT INTO cobranza(cobnum, codcli, montco, metpag, codban, ctachq, numchq, divisa, numcom, monret, cobfas, cobsta, fechcr) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)';
        promises.push(this.db.executeSql(sql, [numero_final, charge.codcli, charge.charge_amount, charge.payment_method, bank, check_a_number, check_number, charge.divisa, document_number, retention_number, 1, 1, AppUtils.getSqliteDateTimeFormat(datetime)]));
        
        query = 'INSERT INTO facturaxcobranza(cobnum, facnum) VALUES (?,?)';
        for(let i = 0; i < invoices.length; i++) {
          promises.push(this.db.executeSql(query, [numero_final, invoices[i].facnum]));
        }

        return Promise.all(promises);
      });
    });
  }*/

  createCharge(charge: any, invoices: any[]) {
    let promises = [];
    let query: string;

    let serie_cobranza = '';
    let ultimo_numero_cobranza = '';
    let numero_final = '';

    let bank = '';
    let reference = '';
    let check_a_number = '';
    let check_number = '';
    let document_number = '';
    let retention_number = 0;

    /*if (charge.metpag == 1 || charge.metpag == 3) {
      reference = charge.referc;
      bank = charge.codban;
    } else {
      if (charge.metpag == 2) {
        bank = charge.codban;
        check_a_number = charge.ctachq;
        check_number = charge.numchq;
      }
    }

    if (charge.cntesp == true) {
      document_number = charge.numcom;
      retention_number = charge.monret;
    }*/

    return this.getChargeSeriesNum().then((serie) => {
      serie_cobranza = serie;
      return this.getChargeSeriesLastNum(serie_cobranza).then((ultimo_numero) => {

        console.log('ultimo in par1006: ' + ultimo_numero);

        return this.getLastChargeHead().then((ultimo_numero_tabla) => {

          console.log('ultimo in cobranza: ' + ultimo_numero_tabla);

          if (Number(ultimo_numero) < Number(ultimo_numero_tabla)) {
            ultimo_numero = ultimo_numero_tabla;
          }

          //ultimo_numero_cobranza = ultimo_numero;
          numero_final = serie_cobranza + (ultimo_numero + 1);
          this.current_charge_number = numero_final;
          this.updateLastChargeNumber(serie_cobranza);
          let datetime = new Date().toLocaleString();
          let sql = 'INSERT INTO cobranza(cobnum, codcli, montco, metpag, codban, referc, ctachq, numchq, divisa, numcom, monret, cobfas, cobsta, fechcr) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
          //promises.push(this.db.executeSql(sql, [numero_final, charge.codcli, charge.montco, charge.metpag, bank, charge.referc, check_a_number, check_number, charge.divisa, document_number, retention_number, 1, 1, AppUtils.getSqliteDateTimeFormat(datetime)]));
          //return Promise.all(promises);
          return this.db.executeSql(sql, [numero_final, charge.codcli, charge.montco, charge.metpag, bank, charge.referc, check_a_number, check_number, charge.divisa, document_number, retention_number, 1, 1, AppUtils.getSqliteDateTimeFormat(datetime)]);

        });
      });
    });
  }

  getLastChargeHead() {
    let sql = 'SELECT cobnum FROM cobranza WHERE ROWID IN ( SELECT max( ROWID ) FROM cobranza )';
    return this.db.executeSql(sql, [])
      .then(response => {
        let num = '0';
        if (response.rows.length > 0) {
          let cobnum: String = response.rows.item(0).cobnum;
          num = cobnum.substr(6);
        }
        console.log('Cobnum in table: ' + num);
        return Promise.resolve(Number(num));
      })
  }

  updateChargePhaseStatus(cobnum: any, fase: any, status: any, charge: any) {

    let sql = "UPDATE cobranza SET montco=?, metpag=?, codban=coalesce(?,'.'), referc=coalesce(?,'.'), ctachq=coalesce(?,'.'), numchq=coalesce(?,'.'), divisa=?, numcom=coalesce(?,'.'), monret=?, cobfas=?, cobsta=? WHERE cobnum=?";
    return this.db.executeSql(sql, [charge.montco, charge.metpag, charge.codban, charge.referc, charge.ctachq, charge.numchq, charge.divisa, charge.numcom, charge.monret, fase, status, cobnum]);

  }

  createChargeInvoices(cobnum: any, invoices: any[]) {
    let promises = [];
    let query: string;

    query = 'INSERT INTO facturaxcobranza(cobnum, facnum) VALUES (?,?)';
    for (let i = 0; i < invoices.length; i++) {
      promises.push(this.db.executeSql(query, [cobnum, invoices[i].facnum]));
    }

    return Promise.all(promises);
  }

  getAmountCharged(cobnum: any, facnum: any) {

    let sql = 'SELECT * FROM facturaxcobranza WHERE cobnum=? AND facnum=?';
    return this.db.executeSql(sql, [cobnum, facnum])
      .then(response => {
        return Promise.resolve(response.rows.item(0).montco);
      })
  }

  createChargexInvoice(charge: any) {
    let query: string;

    this.getChargesxInvoiceLinesNum(charge.cobnum, charge.facnum).then((numero) => {

      let numero_linea = (numero * 10) + 10;
      let datetime = new Date().toLocaleString();
      query = 'INSERT INTO facturaxcobranza(cobnum, facnum, coblin, montco, metpag, codban, referc, ctachq, numchq, divisa, fechcr) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
      return this.db.executeSql(query, [charge.cobnum, charge.facnum, numero_linea, charge.montco, charge.metpag, charge.codban, charge.referc, charge.ctachq, charge.numchq, charge.divisa, datetime]);

    })

  }

  /*createChargesxInvoicesRenewed(charge: any, cobnum:any, invoices: any[]){
    let promises = [];
    let query: string;

    query = 'INSERT INTO facturaxcobranza(cobnum, facnum, coblin, montco, metpag, codban, referc, ctachq, numchq, divisa, fechcr) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
    for (let i = 0; i < invoices.length; i++) {
      let header = invoices[i].header;

      //let charges = invoices[i].charges;
      let numero_linea = 0;
      invoices.forEach(c => {

        numero_linea = numero_linea + 10;
        console.log("numero coblin: " + numero_linea);        
        let datetime = new Date().toLocaleString();          
        promises.push(this.db.executeSql(query, [cobnum, c.facnum, numero_linea, c.salfac, charge.metpag, charge.codban, charge.referc, charge.ctachq, charge.numchq, charge.divisa, datetime]));           
  
                
      });
      
    }

    return Promise.all(promises); 
  }*/

  // createChargesxInvoicesRenewed(charge: any, cobnum: any, invoices: any[]) {
  //   let promises = [];
  //   let query: string;

  //   query = 'INSERT INTO facturaxcobranza(cobnum, facnum, coblin, montco, metpag, codban, referc, ctachq, numchq, divisa, fechcr, tinv) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)';


  //   //let charges = invoices[i].charges;
  //   let remaining = charge.montco;
  //   let numero_linea = 0;
  //   invoices.forEach(c => {
  //     let charged = 0;
  //     if (Boolean(charge.cntesp)) {
  //       let bi = c.salfac - c.ivafac;
  //       let tax = c.ivafac;
  //       let mt = tax * 0.75;
  //       tax = tax - mt;
  //       bi = bi + tax;
  //       if (remaining >= bi) {
  //         charged = bi;
  //         remaining = remaining - bi;
  //       } else {
  //         charged = remaining;
  //         remaining = remaining - remaining;
  //       }
  //     } else {
  //       if (remaining >= c.salfac) {
  //         charged = c.salfac;
  //         remaining = remaining - c.salfac;
  //       } else {
  //         charged = remaining;
  //         remaining = remaining - remaining;
  //       }
  //     }

  //     this.getChargesxInvoiceLinesNum(cobnum, c.facnum).then((numero) => {

  //       numero_linea = (numero * 10) + 10;
  //       console.log("numero coblin: " + numero_linea);
  //       let datetime = new Date().toLocaleString();
  //       promises.push(this.db.executeSql(query, [cobnum, c.facnum, numero_linea, charged, charge.metpag, charge.codban, charge.referc, charge.ctachq, charge.numchq, charge.divisa, datetime, c.ttyp]));

  //     })      

  //   });

  //   return Promise.all(promises);
  // }


  // createChargesxInvoicesRenewed(charge: any, cobnum: any, invoices: any[]) {
  //   let promises = [];
  //   let query: string;

  //   query = 'INSERT INTO facturaxcobranza(cobnum, facnum, clicob, coblin, montco, metpag, codban, referc, ctachq, numchq, divisa, fechcr, tinv) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)';


  //   //let charges = invoices[i].charges;
  //   let remaining = charge.montco;
  //   let numero_linea = 0;


  //   invoices.forEach(c => {

  //     this.getExchangeRate(c.coddiv, charge.divisa)
  //       .then((tasa) => {

  //         let charged = 0;
  //         if (Boolean(charge.cntesp)) {
  //           let bi = c.salfac - c.ivafac;
  //           let tax = c.ivafac;
  //           let mt = tax * 0.75;
  //           tax = tax - mt;
  //           bi = bi + tax;
  //           let exchange_bi = bi * tasa;
  //           if (remaining >= exchange_bi) {
  //             charged = exchange_bi / tasa;
  //             remaining = remaining - exchange_bi;
  //           } else {
  //             charged = remaining / tasa;
  //             remaining = remaining - remaining;
  //           }
  //         } else {
  //           let exchange_bi = c.salfac * tasa;
  //           if (remaining >= exchange_bi) {
  //             charged = exchange_bi / tasa;
  //             remaining = remaining - exchange_bi;
  //           } else {
  //             charged = remaining / tasa;
  //             remaining = remaining - remaining;
  //           }
  //         }

  //         numero_linea = numero_linea + 10;
  //         console.log("numero coblin: " + numero_linea);
  //         let datetime = new Date().toLocaleString();
  //         promises.push(this.db.executeSql(query, [cobnum, c.facnum, c.clicob, numero_linea, charged, charge.metpag, charge.codban, charge.referc, charge.ctachq, charge.numchq, c.coddiv, datetime, c.ttyp]));


  //       })
  //       .catch(() => {
  //         console.log('Error obteniendo la tasa de cambio de la factura ');
  //       })

  //   });

  //   return Promise.all(promises);
  // }

  createChargesxInvoicesRenewed(charge: any, cobnum: any, invoices: any[], date_1_3: any) {
    let promises = [];
    let promises_rates = [];
    let query: string;

    query = "INSERT INTO facturaxcobranza(cobnum, facnum, clicob, coblin, montco, montcon, monret, metpag, codban, referc, ctachq, numchq, divisa, fechcr, tinv) VALUES (?,?,?,?,?,?,?,?,coalesce(?,'.'),coalesce(?,'.'),coalesce(?,'.'),coalesce(?,'.'),?,?,?)";
    let datetime = new Date().toLocaleString();
    if (charge.metpag == 1 || charge.metpag == 3) {
      datetime = date_1_3;
    }

    //let charges = invoices[i].charges;
    let remaining = charge.montco;
    let numero_linea = 0;


    for (let index = 0; index < invoices.length; index++) {
      const c = invoices[index];
      promises_rates.push(

        this.getExchangeRate(c.coddiv, charge.divisa)
          .then((tasa) => {            

            let charged = 0;
            let charged_exchanged = 0;
            let charged_retained = 0;
            if (Boolean(charge.cntesp)) {
              let bi = c.salfac - c.ivafac;
              let tax = c.ivafac;
              let mt = tax * 0.75;              
              tax = tax - mt;
              bi = bi + tax;
              let exchange_bi = (bi - c.salcob) * tasa;
              /////////////////iva > 0///////////////////
              if(c.ivafac > 0 && remaining >= c.ivafac && mt > c.ivaret){
                charged_retained = mt - c.ivaret;
              }
              /////////////////iva > 0///////////////////
              if (remaining >= exchange_bi) {
                charged = exchange_bi / tasa;
                charged_exchanged = exchange_bi;
                remaining = remaining - exchange_bi;                
              } else {
                charged = remaining / tasa;
                charged_exchanged = remaining;
                remaining = remaining - remaining;
              }
              
            } else {
              let exchange_bi = (c.salfac - c.salcob) * tasa;
              if (remaining >= exchange_bi) {
                charged = exchange_bi / tasa;
                charged_exchanged = exchange_bi;
                remaining = remaining - exchange_bi;
              } else {
                charged = remaining / tasa;
                charged_exchanged = remaining;
                remaining = remaining - remaining;
              }
            }

            numero_linea = numero_linea + 10;
            console.log("numero coblin: " + numero_linea);
            
            promises.push(this.db.executeSql(query, [cobnum, c.facnum, c.clicob, numero_linea, charged, charged_exchanged, charged_retained, charge.metpag, charge.codban, charge.referc, charge.ctachq, charge.numchq, c.coddiv, datetime, c.ttyp]));


          })
          .catch(() => {
            console.log('Error obteniendo la tasa de cambio de la factura ');
          })

      )


    }

    return Promise.all(promises_rates)
      .then(() => {

        return Promise.all(promises);

      });


  }

  getExchangeRate(divisa: any, divisa_b: any) {

    let sql = 'SELECT * FROM mae1049 WHERE coddiv=? AND coddiv_b=?';
    return this.db.executeSql(sql, [divisa, divisa_b])
      .then(response => {
        return Promise.resolve(response.rows.item(0).tasa);
      })
  }

  createChargesxInvoices(cobnum: any, invoices: any[]) {
    let promises = [];
    let query: string;

    query = 'INSERT INTO facturaxcobranza(cobnum, facnum, coblin, montco, metpag, codban, referc, ctachq, numchq, divisa, fechcr) VALUES (?,?,?,?,?,?,?,?,?,?,?)';
    for (let i = 0; i < invoices.length; i++) {
      let header = invoices[i].header;

      let charges = invoices[i].charges;
      let numero_linea = 0;
      charges.forEach(c => {

        numero_linea = numero_linea + 10;
        console.log("numero coblin: " + numero_linea);
        let datetime = new Date().toLocaleString();
        promises.push(this.db.executeSql(query, [c.cobnum, c.facnum, numero_linea, c.montco, c.metpag, c.codban, c.referc, c.ctachq, c.numchq, c.divisa, datetime]));


      });

    }

    return Promise.all(promises);
  }

  /*getChargesxInvoiceLinesNum(cobnum: any, facnum: any){

    let sql = 'SELECT MAX(coblin) as line FROM facturaxcobranza WHERE cobnum=? AND facnum=?';
    return this.db.executeSql(sql, [cobnum, facnum])
    .then(response => {      
      return Promise.resolve( response.rows.item(0).line );
    })

  }*/

  getChargesxInvoiceLinesNum(cobnum: any, facnum: any) {

    let sql = 'SELECT COUNT (*) as total FROM facturaxcobranza WHERE cobnum=? AND facnum=?';
    return this.db.executeSql(sql, [cobnum, facnum])
      .then(response => {
        return Promise.resolve(response.rows.item(0).total);
      })

  }

  deleteChargesxInvoices(cobnum: any) {
    let query: string;
    query = 'DELETE FROM facturaxcobranza WHERE cobnum=?';
    return this.db.executeSql(query, [cobnum]);

  }

  deleteChargexInvoice(charge: any) {
    let query: string;
    query = 'DELETE FROM facturaxcobranza WHERE cobnum=? AND facnum=? AND coblin=?';
    return this.db.executeSql(query, [charge.cobnum, charge.facnum, charge.coblin]);

  }

  /*updateChargexInvoice(charge:any){

    let sql = 'UPDATE facturaxcobranza SET montco=?, metpag=?, codban=?, referc=?, ctachq=?, numchq=?, divisa=?, numcom=?, monret=?, cobfas=?, cobsta=? WHERE cobnum=?';
    return this.db.executeSql(sql, [charge.montco, charge.metpag, charge.codban, charge.referc, charge.ctachq, charge.numchq, charge.divisa, charge.numcom, charge.monret, fase, status, cobnum]);

  }*/


  deleteChargeInvoices(cobnum: any) {

    let sql = 'DELETE FROM facturaxcobranza WHERE cobnum=?';
    return this.db.executeSql(sql, [cobnum]);

  }

  /*createChargeRetentions(cobnum:any, retentions: any[]){
    let promises = [];
    let query: string;

    query = 'INSERT INTO retencionxcobranza(compnum, cobnum, montcom) VALUES (?,?,?)';
    for(let i = 0; i < retentions.length; i++) {
        promises.push(this.db.executeSql(query, [retentions[i].compnum, cobnum, retentions[i].montcom]));
      }

    return Promise.all(promises); 
  }*/

  createChargeRetentions(cobnum: any, retention: any) {
    let promises = [];
    let query: string;

    query = 'INSERT INTO retencionxcobranza(compnum, cobnum, montcom) VALUES (?,?,?)';
    promises.push(this.db.executeSql(query, [retention.compnum, retention.cobnum, retention.montcom]));

    return Promise.all(promises);
  }

  deleteChargeRetentions(cobnum: any) {

    let sql = 'DELETE FROM retencionxcobranza WHERE cobnum=?';
    return this.db.executeSql(sql, [cobnum]);

  }

  getCurrentChargeNumber() {

    return this.current_charge_number;

  }

  getChargeInvoices2() {
    let sql = 'SELECT * FROM facturaxcobranza';
    return this.db.executeSql(sql, [])
      .then(response => {
        let facturas = [];
        for (let index = 0; index < response.rows.length; index++) {
          facturas.push(response.rows.item(index).facnum);
        }
        return Promise.resolve(facturas);
      })
  }

  getChargeInvoices(cobnum: any) {
    let sql = 'SELECT * FROM facturaxcobranza WHERE cobnum=?';
    return this.db.executeSql(sql, [cobnum])
      .then(response => {
        let facturas = [];
        for (let index = 0; index < response.rows.length; index++) {
          facturas.push(response.rows.item(index).facnum);
        }
        return Promise.resolve(facturas);
      })
  }

  getChargeInvoicesAllFields(cobnum: any) {
    let sql = 'SELECT * FROM facturaxcobranza WHERE cobnum=?';
    return this.db.executeSql(sql, [cobnum])
      .then(response => {
        let facturas = [];
        for (let index = 0; index < response.rows.length; index++) {
          facturas.push(response.rows.item(index));
        }
        return Promise.resolve(facturas);
      })
  }

  getChargeDate_1_3(cobnum: any) {
    let sql = 'SELECT fechcr FROM facturaxcobranza WHERE cobnum=?';
    return this.db.executeSql(sql, [cobnum])
      .then(response => {
        //console.log('Cobnum in table: ' + num);
        return Promise.resolve(response.rows.item(0).fechcr);
      })
  }

  getInvoicesXcharge(facturas: any[]) {    

    let sql = "SELECT * FROM factura_cobrar WHERE facnum IN (" + facturas.join(',') + ")";
    return this.db.executeSql(sql, [])
      .then(response => {
        let clientes = [];
        for (let index = 0; index < response.rows.length; index++) {
          clientes.push(response.rows.item(index));
        }
        return Promise.resolve(clientes);
      })

  }

  saveCashControl(charge: any, denomination: any[]) {
    let promises = [];
    let query: string;
    query = 'INSERT INTO efectivoentregado (cobnum, cant_1, cant_2, cant_5, cant_10, cant_20, cant_50, cant_100, cant_200, cant_500) VALUES (?,?,?,?,?,?,?,?,?,?)';
    promises.push(this.db.executeSql(query, [charge.cobnum, denomination[0].quantity, denomination[1].quantity, denomination[2].quantity, denomination[3].quantity, denomination[4].quantity, denomination[5].quantity, denomination[6].quantity, denomination[7].quantity, denomination[8].quantity]));
    return Promise.all(promises);
  }

  deleteCashControlxCharge(cobnum: any) {
    let query: string;
    query = 'DELETE FROM efectivoentregado WHERE cobnum=?';
    return this.db.executeSql(query, [cobnum]);

  }

  updateInvoicesChargedAmount(charge: any, invoices: any[]) {
    let promises = [];
    let promises_charges = [];

    if (Boolean(charge.cntesp)) {

      for (let i = 0; i < invoices.length; i++) {

        promises_charges.push(
          this.getAllChargesXInvoices(invoices[i].facnum)
          .then((charges) => {          

            let charged = 0;
            let tax_chargable = invoices[i].ivafac * 0.25;
            let tax_charged = 0;
            let charged_retained = 0;
            charges.forEach(c => {
              charged = charged + c.montco;
              charged_retained = charged_retained + c.monret;
            });
            if (charged >= tax_chargable) {
              tax_charged = tax_chargable;
            } else {
              tax_charged = charged;
            }
            let sql = 'UPDATE FACTURA_COBRAR SET salcob=?, ivacob=?, ivaret=? WHERE facnum=?';
            promises.push(this.db.executeSql(sql, [charged, tax_charged, charged_retained, invoices[i].facnum]));

          })
          .catch(() => alert("error on updating invoices charged amount"))
        )        

      }

    } else {

      for (let i = 0; i < invoices.length; i++) {

        promises_charges.push(
          this.getAllChargesXInvoices(invoices[i].facnum)
          .then((charges) => {

            let charged = 0;
            let tax_chargable = invoices[i].ivafac;
            let tax_charged = 0;
            charges.forEach(c => {
              charged = charged + c.montco;
            });
            if (charged >= tax_chargable) {
              tax_charged = tax_chargable;
            } else {
              tax_charged = charged;
            }
            let sql = 'UPDATE FACTURA_COBRAR SET salcob=?, ivacob=? WHERE facnum=?';
            promises.push(this.db.executeSql(sql, [charged, tax_charged, invoices[i].facnum]));

          })
          .catch(() => alert("error on updating invoices charged amount"))
        )       

      }
    }

    return Promise.all(promises_charges)
      .then(() => {

        return Promise.all(promises);

      });    

  }

  getAllChargesXInvoices(facnum: any) {
    let sql = 'SELECT * FROM facturaxcobranza WHERE facnum=?';
    return this.db.executeSql(sql, [facnum])
      .then(response => {
        let facturas = [];
        for (let index = 0; index < response.rows.length; index++) {
          facturas.push(response.rows.item(index));
        }
        return Promise.resolve(facturas);
      })
  }

  /*getInvoiceCharge(facnum: any) {
    let sql = 'SELECT * FROM facturaxcobranza WHERE facnum=?';
    return this.db.executeSql(sql, [facnum])
      .then(response => {
        return Promise.resolve(response.rows.item(0));
      })
  }*/

  getInvoiceCharge(facnum:any, cobnum: any) {
    let sql = 'SELECT * FROM facturaxcobranza WHERE facnum=? AND cobnum=?';
    return this.db.executeSql(sql, [facnum, cobnum])
      .then(response => {
        let cobranzas = [];
        for (let index = 0; index < response.rows.length; index++) {
          cobranzas.push(response.rows.item(index));
        }
        return Promise.resolve(cobranzas);
      })
  }

  /*getChargeRetentions(cobnum:any, pedfas:any, pedsta:any){
    let sql = 'SELECT * FROM retencionxcobranza WHERE cobnum=?';
    return this.db.executeSql(sql, [cobnum])
    .then(response => {
      let retenciones = [];
      for (let index = 0; index < response.rows.length; index++) {
        retenciones.push( response.rows.item(index) );
      }
      return Promise.resolve( retenciones );
    })
  }*/

  getChargeRetentions(cobnum: any, pedfas: any, pedsta: any) {
    let sql = 'SELECT * FROM retencionxcobranza WHERE cobnum=?';
    return this.db.executeSql(sql, [cobnum])
      .then(response => {
        return Promise.resolve(response.rows.item(0));
      })
  }

  invoiceAlreadyCharged(facnum: any) {
    let sql = 'SELECT * FROM facturaxcobranza WHERE facnum=?';
    return this.db.executeSql(sql, [facnum])
      .then(response => {
        let facturas = [];
        for (let index = 0; index < response.rows.length; index++) {
          facturas.push(response.rows.item(index));
        }
        return Promise.resolve(facturas);
      })
  }

  createInvoiceToCharge(invoice: any) {
    let promises = [];
    let query: string;

    query = 'INSERT INTO factura_cobrar (facnum, codcli, fecfac, diasve, salfac, salcob, ivafac, ivacob) VALUES (?,?,?,?,?,?,?,?)';
    promises.push(this.db.executeSql(query, [invoice.nfact, invoice.codcli, invoice.fechcr, invoice.diasve, invoice.salfac, invoice.salcob, (invoice.salfac * 16) / 100, 0]));
    return Promise.all(promises);
  }

  getInvoicesToCharge2(codcli: any) {
    let sql = 'SELECT * FROM factura_cobrar WHERE codcli=?';
    return this.db.executeSql(sql, [codcli])
      .then(response => {
        let facturas = [];
        for (let index = 0; index < response.rows.length; index++) {
          facturas.push(response.rows.item(index));
        }
        return Promise.resolve(facturas);
      })
  }

  getInvoicesToCharge(codcli: any) {
    let sql = "SELECT * FROM factura_cobrar WHERE codcli=? AND (salfac > salcob OR salfac = salcob) ORDER BY (SELECT substr(reversed_date, 7,4) || '-' || substr(reversed_date, 4, 2)|| '-' || substr(reversed_date, 1, 2) AS proper_date FROM (SELECT fecfac AS reversed_date))";
    return this.db.executeSql(sql, [codcli])
      .then(response => {
        let facturas = [];
        for (let index = 0; index < response.rows.length; index++) {
          facturas.push(response.rows.item(index));
        }
        return Promise.resolve(facturas);
      })
  }

  /*getInvoicesToCharge(codcli: any, cobnum: any) {
    let sql = 'SELECT * FROM factura_cobrar WHERE codcli=? AND ((( SELECT COUNT(*) FROM facturaxcobranza WHERE facturaxcobranza.facnum = factura_cobrar.facnum ) = 0) || (( SELECT COUNT(*) FROM facturaxcobranza WHERE facturaxcobranza.cobnum = ? AND facturaxcobranza.facnum = factura_cobrar.facnum) > 0))';
    return this.db.executeSql(sql, [codcli, cobnum])
      .then(response => {
        let facturas = [];
        for (let index = 0; index < response.rows.length; index++) {
          facturas.push(response.rows.item(index));
        }
        return Promise.resolve(facturas);
      })
  }*/

  getAccountStatement(codcli: any) {
    let sql = 'SELECT * FROM ESTADOCUENTA WHERE codcli=?';
    return this.db.executeSql(sql, [codcli])
      .then(response => {
        let account_statement = response.rows.item(0);
        return Promise.resolve(account_statement);
      })

  }


  /**************************************DEPOSITS***********************************************/

  createDeposit(deposit: any) {
    let promises = [];
    let query: string;
    let serie_cobranza = '';
    let ultimo_numero_cobranza = '';
    let numero_final = '';

    return this.getDepositSeriesNum().then((serie) => {
      serie_cobranza = serie;
      return this.getDepositSeriesLastNum(serie_cobranza).then((ultimo_numero) => {
        ultimo_numero_cobranza = ultimo_numero;
        numero_final = serie_cobranza + (ultimo_numero + 1);
        this.current_deposit_number = numero_final;
        this.updateLastDepositNumber(serie_cobranza);
        let datetime = new Date().toLocaleString();
        let sql = 'INSERT INTO deposito(depnum, codcli, codban, efeche, numref, depfas, depsta, depfec) VALUES(?,?,?,?,?,?,?,?)';
        promises.push(this.db.executeSql(sql, [numero_final, deposit.codcli, deposit.codban, deposit.efeche, deposit.numref, 1, 1, AppUtils.getSqliteDateTimeFormat(datetime)]));
        return Promise.all(promises);
      });
    });
  }

  getDeposits(codcli: any, depfas: number, depsta: number) {
    let today = AppUtils.getTodayInitSqliteFormat();
    let sql = "SELECT * FROM deposito WHERE codcli=? AND depfas=? AND depsta=? AND depfec >= DATETIME('" + today + "')";

    return this.db.executeSql(sql, [codcli, depfas, depsta])
      .then(response => {
        let deposits = [];
        for (let index = 0; index < response.rows.length; index++) {
          deposits.push(response.rows.item(index));
        }
        return Promise.resolve(deposits);
      });
  }

  getAllPrintedDeposits(depfas: number, depsta: number) {
    let sql = "SELECT * FROM deposito WHERE depfas=? AND depsta=?";

    return this.db.executeSql(sql, [depfas, depsta])
      .then(response => {
        let deposits = [];
        for (let index = 0; index < response.rows.length; index++) {
          deposits.push(response.rows.item(index));
        }
        return Promise.resolve(deposits);
      });
  }


  updateDepositPhaseStatus(depnum: any, fase: any, status: any, deposit: any) {

    let sql = 'UPDATE deposito SET codban=?, efeche=?, numref=?, depfas=?, depsta=? WHERE depnum=?';
    return this.db.executeSql(sql, [deposit.codban, deposit.efeche, deposit.numref, fase, status, depnum]);

  }

  deleteDepositCharges(depnum: any) {

    let sql = 'DELETE FROM cobranzaxdeposito WHERE depnum=?';
    return this.db.executeSql(sql, [depnum]);

  }

  createDepositCharges(depnum: any, charges: any[]) {
    let promises = [];
    let query: string;

    query = 'INSERT INTO cobranzaxdeposito(depnum, cobnum, facnum, coblin) VALUES (?,?,?,?)';
    for (let i = 0; i < charges.length; i++) {
      promises.push(this.db.executeSql(query, [depnum, charges[i].cobnum, charges[i].facnum, charges[i].coblin]));
    }

    return Promise.all(promises);
  }

  getCurrentDepositNumber() {

    return this.current_deposit_number;

  }

  getDepositCharges(depnum: any, pedfas: any, pedsta: any) {
    let sql = 'SELECT * FROM cobranzaxdeposito WHERE depnum=?';
    return this.db.executeSql(sql, [depnum])
      .then(response => {
        let cobranzas = [];
        for (let index = 0; index < response.rows.length; index++) {
          cobranzas.push(response.rows.item(index).cobnum);
        }
        return Promise.resolve(cobranzas);
      })
  }

  getAllDepositCharges() {
    let sql = 'SELECT * FROM cobranzaxdeposito';
    return this.db.executeSql(sql, [])
      .then(response => {
        let cobranzas = [];
        for (let index = 0; index < response.rows.length; index++) {
          cobranzas.push(response.rows.item(index).cobnum);
        }
        return Promise.resolve(cobranzas);
      })
  }

  /*getChargeDeposit(cobnum: any){
    let sql = 'SELECT * FROM cobranzaxdeposito WHERE cobnum=?';
    return this.db.executeSql(sql, [cobnum])
    .then(response => {
      return Promise.resolve( response.rows.item(0).depnum );
    })
  }*/

  getChargeDeposit(cobnum: any, facnum: any, coblin: any) {
    let sql = 'SELECT * FROM cobranzaxdeposito WHERE cobnum=? AND facnum=? AND coblin=?';
    return this.db.executeSql(sql, [cobnum, facnum, coblin])
      .then(response => {
        return Promise.resolve(response.rows.item(0));
      })
  }

  /**************************************DEPOSITS***********************************************/


  createOrderLine(pedido: any, act_solo_cant: boolean) {

    let numero_linea;

    return this.productAlreadyAdded(pedido.pednum, pedido.codart, pedido.coduni).then((response) => {

      if (response.rows.length > 0) {

        //this.updateOrderPhaseStatus(pedido.pednum, 2, 1);
        let datetime = new Date().toLocaleString();
        let numero_linea = response.rows.item(0).pedlin;
        let sql = '';

        if (act_solo_cant) {

          sql = 'UPDATE PED1001 SET pedcan=?, pedprt=?, fechcr=?, pedfas=?, pedsta=?, coduni=? WHERE pednum=? AND pedlin=? AND codart=? AND coduni=?';

        } else {

          ///articulo eliminado y que es insertado nuevamente
          if (response.rows.item(0).pedfas == 5) {

            sql = 'UPDATE PED1001 SET pedcan=?, pedprt=?, fechcr=?, pedfas=?, pedsta=?, coduni=? WHERE pednum=? AND pedlin=? AND codart=? AND coduni=?';

          } else {///articulo existente a actualizar

            sql = 'UPDATE PED1001 SET pedcan=pedcan + ?, pedprt=pedprt + ?, fechcr=?, pedfas=?, pedsta=?, coduni=? WHERE pednum=? AND pedlin=? AND codart=? AND coduni=?';

          }


        }

        this.getOrderPhaseStatus(pedido.pednum).then((ped) => {

          return this.db.executeSql(sql, [pedido.pedcan, pedido.pedprt, datetime, ped.pedfas, ped.pedsta, pedido.coduni, pedido.pednum, numero_linea, pedido.codart, pedido.coduni]);

        });


      }

      if (response.rows.length == 0) {

        this.getOrderLinesNum(pedido.pednum).then((numero) => {

          numero_linea = (numero * 10) + 10;
          this.updateOrderPhaseStatus(pedido.pednum, 2, 1);
          let datetime = new Date().toLocaleString();
          let sql = 'INSERT INTO PED1001(pednum, pedlin, codart, descar, coduni, pedcan, pedpru, pedprt, fechcr, geopcr, geopel, pedfas, pedsta) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)';
          return this.db.executeSql(sql, [pedido.pednum, numero_linea, pedido.codart, pedido.descar, pedido.coduni, pedido.pedcan, pedido.pedpru, pedido.pedprt, datetime, '', '', 2, 1]);

        })

      }

    })



  }

  getOrderSeriesNum() {

    let sql = 'SELECT * FROM MAE1040';
    return this.db.executeSql(sql, [])
      .then(response => {
        let serie = response.rows.item(0).codser;
        return Promise.resolve(serie);
      })

  }

  getChargeSeriesNum() {

    let sql = 'SELECT * FROM MAE1040';
    return this.db.executeSql(sql, [])
      .then(response => {
        let serie = response.rows.item(0).codser;
        return Promise.resolve(serie);
      })

  }

  getDepositCreationDate(depnum: any) {

    let sql = 'SELECT depfec FROM deposito WHERE depnum=?';
    return this.db.executeSql(sql, [depnum])
      .then(response => {

        return Promise.resolve(response.rows.item(0).depfec);

      })

  }

  getDepositSeriesNum() {

    let sql = 'SELECT * FROM deposit_serie';
    return this.db.executeSql(sql, [])
      .then(response => {
        let serie = response.rows.item(0).codser;
        return Promise.resolve(serie);
      })

  }

  getSeriesLastNum(series: any) {

    let sql = 'SELECT ultimo FROM PAR1006 WHERE serie=?';
    return this.db.executeSql(sql, [series])
      .then(response => {
        let ultimo_numero = response.rows.item(0).ultimo;
        return Promise.resolve(ultimo_numero);
      })

  }

  getChargeSeriesLastNum(series: any) {

    let sql = 'SELECT ultimo_charge FROM PAR1006 WHERE serie=?';
    return this.db.executeSql(sql, [series])
      .then(response => {
        let ultimo_numero = response.rows.item(0).ultimo_charge;
        return Promise.resolve(ultimo_numero);
      })

  }

  getDepositSeriesLastNum(series: any) {

    let sql = 'SELECT ultimo_deposit FROM PAR1006 WHERE serie=?';
    return this.db.executeSql(sql, [series])
      .then(response => {
        let ultimo_numero = response.rows.item(0).ultimo_deposit;
        return Promise.resolve(ultimo_numero);
      })

  }

  updateLastNumber(series: any) {

    let sql = 'UPDATE PAR1006 SET ultimo = ultimo + 1 WHERE serie=?';
    return this.db.executeSql(sql, [series]);

  }

  updateLastChargeNumber(series: any) {

    let sql = 'UPDATE PAR1006 SET ultimo_charge = ultimo_charge + 1 WHERE serie=?';
    return this.db.executeSql(sql, [series]);

  }

  updateLastDepositNumber(series: any) {

    let sql = 'UPDATE PAR1006 SET ultimo_deposit = ultimo_deposit + 1 WHERE serie=?';
    return this.db.executeSql(sql, [series]);

  }

  getOrderLinesNum(pednum: any) {

    let sql = 'SELECT * FROM PED1001 WHERE pednum=?';
    return this.db.executeSql(sql, [pednum])
      .then(response => {
        return Promise.resolve(response.rows.length);
      })

  }

  updateOrderPhaseStatus(pednum: any, fase: any, status: any) {

    let sql = 'UPDATE PED1000 SET pedfas=?, pedsta=? WHERE pednum=?';
    return this.db.executeSql(sql, [fase, status, pednum]);

  }

  updateOrderLinePhaseStatus(pednum: any, pedlin: any, fase: any, status: any) {

    let sql = 'UPDATE PED1001 SET pedfas=?, pedsta=? WHERE pednum=? AND pedlin=?';
    return this.db.executeSql(sql, [fase, status, pednum, pedlin]);

  }


  updateOrderLinesPhaseStatus(pednum: any, fase: any, status: any) {

    let sql = 'UPDATE PED1001 SET pedfas=?, pedsta=? WHERE pednum=? AND pedfas !=?';
    return this.db.executeSql(sql, [fase, status, pednum, 5]);

  }

  getCurrentOrderNumber() {

    return this.current_order_number;

  }

  productAlreadyAdded(pednum: any, codart: any, coduni: any) {

    let sql = 'SELECT * FROM PED1001 WHERE pednum=? AND codart=? AND coduni=?';
    return this.db.executeSql(sql, [pednum, codart, coduni])
      .then(response => {

        return Promise.resolve(response);

      })

  }

  getCodeDescription(codart: any, coduni: any) {
    let sql = 'SELECT * FROM MAE1037 WHERE codpro=?';
    return this.db.executeSql(sql, [codart])
      .then(response => {
        let presentaciones = [];
        presentaciones = JSON.parse(response.rows.item(0).codunv);
        let found = false;
        let index = 0;

        while (!found && index < presentaciones.length) {

          if (presentaciones[index].coduni == coduni) {

            found = true;
            return Promise.resolve(presentaciones[index]);

          }

          index++;

        }


      })
  }

  clientAlreadyHasOrders(codcli: any) {

    let today = AppUtils.getTodayInitSqliteFormat();
    let sql = "SELECT * FROM PED1000 WHERE codcli=? AND fechcr >= DATETIME('" + today + "')";
    return this.db.executeSql(sql, [codcli])
      .then(response => {

        return Promise.resolve(response);

      })

  }

  getOrderCreationDate(pednum: any) {

    let sql = 'SELECT fechcr FROM PED1000 WHERE pednum=?';
    return this.db.executeSql(sql, [pednum])
      .then(response => {

        return Promise.resolve(response.rows.item(0).fechcr);

      })

  }

  getChargeCreationDate(cobnum: any) {

    let sql = 'SELECT fechcr FROM cobranza WHERE cobnum=?';
    return this.db.executeSql(sql, [cobnum])
      .then(response => {

        return Promise.resolve(response.rows.item(0).fechcr);

      })

  }

  updateRemoveOrderReason(pednum: string, codmot: string) {

    let sql = 'UPDATE PED1000 SET codmot=? WHERE pednum=?';
    return this.db.executeSql(sql, [codmot, pednum]);

  }

  updateRemoveLineReason(pednum: string, pedlin: any, codmot: string) {

    let sql = 'UPDATE PED1001 SET codmot=? WHERE pednum=? AND pedlin=?';
    return this.db.executeSql(sql, [codmot, pednum, pedlin]);

  }

  getBankDescByCode(codban: any) {
    let sql = 'SELECT desban FROM MAE1042 WHERE codban=?';
    return this.db.executeSql(sql, [codban])
      .then(response => {
        let desban = response.rows.item(0).desban;
        return Promise.resolve(desban);
      })

  }

  getBankDescAndCtaByCode(codban: string) {
    let sql = 'SELECT desban, numcue FROM MAE1042 WHERE codban=?';
    return this.db.executeSql(sql, [codban])
      .then(response => {
        return Promise.resolve(response.rows.item(0));
      })

  }

  getDealerBankByCode(coddis: string) {
    let sql = 'SELECT codban FROM REL1020 WHERE coddis=?';
    return this.db.executeSql(sql, [coddis])
      .then(response => {
        return Promise.resolve(response.rows.item(0).codban);
      })

  }

  getOrderPhaseStatus(pednum: any) {

    let sql = 'SELECT pedfas, pedsta FROM PED1000 WHERE pednum=?';
    return this.db.executeSql(sql, [pednum])
      .then(response => {

        return Promise.resolve(response.rows.item(0));

      })

  }

  ////////////////////////////////////////////////////orders operations//////////////////////////////////



  ////////////////////////////////////////////////////billing operations//////////////////////////////////

  getInvoice(pednum: any, pedfas: any, pedsta: any, codcli: any) {
    let sql = 'SELECT * FROM FAC1001 WHERE orno=? AND clifac=? AND pedfas=? AND pedsta=? ORDER BY fechcr';
    return this.db.executeSql(sql, [pednum, codcli, pedfas, pedsta])
      .then(response => {
        let pedido = [];
        for (let index = 0; index < response.rows.length; index++) {
          pedido.push(response.rows.item(index));
        }
        return Promise.resolve(pedido);
      })
  }

  updateInvoiceLine(pedido: any) {

    let sql = '';
    sql = 'UPDATE FAC1001 SET pedcan=?, pedprt=?, fechcr=?, pedfas=?, pedsta=?, coduni=? WHERE facnum=? AND pedlin=? AND codart=? AND coduni=?';
    return this.db.executeSql(sql, [pedido.pedcan, pedido.pedprt, pedido.fechcr, 1, 1, pedido.coduni, pedido.pednum, pedido.pedlin, pedido.codart, pedido.coduni]);

  }

  getBanks() {
    let sql = 'SELECT * FROM MAE1042';
    return this.db.executeSql(sql, [])
      .then(response => {
        let bancos = [];
        for (let index = 0; index < response.rows.length; index++) {
          bancos.push(response.rows.item(index));
        }
        return Promise.resolve(bancos);
      });
  }

  getBanksNEW() {
    let sql = 'SELECT * FROM MAE1048';
    return this.db.executeSql(sql, [])
      .then(response => {
        let bancos = [];
        for (let index = 0; index < response.rows.length; index++) {
          bancos.push(response.rows.item(index));
        }
        return Promise.resolve(bancos);
      });
  }



  updateInvoicePhaseStatus(pednum: any, ordtra: any, codcli: any, fase: any, status: any) {

    let sql = 'UPDATE FAC1000 SET pedfas=?, pedsta=? WHERE ordpes=? AND ordtra=? AND clifac=?';
    return this.db.executeSql(sql, [fase, status, pednum, ordtra, codcli]);

  }

  updateInvoiceLinesPhaseStatus(pednum: any, ordtra: any, codcli: any, fase: any, status: any) {

    let sql = 'UPDATE FAC1001 SET pedfas=?, pedsta=? WHERE orno=? AND ordtra=? AND clifac=? AND pedfas !=?';
    return this.db.executeSql(sql, [fase, status, pednum, ordtra, codcli, 5]);

  }

  getInvoices(codcli: any, pedfas: any, pedsta: any) {

    let today = AppUtils.getTodayInitSqliteFormat();
    let sql = "SELECT * FROM FAC1000 WHERE codcli=? AND pedfas=? AND pedsta=?";
    return this.db.executeSql(sql, [codcli, pedfas, pedsta])
      .then(response => {

        let facturas = [];
        for (let index = 0; index < response.rows.length; index++) {
          facturas.push(response.rows.item(index));
        }
        return Promise.resolve(facturas);



      })

  }

  createInvoice(cliente: any, geopcr: string, ispdv: boolean) {


    let serie_pedido = '';
    let ultimo_numero_pedido = '';
    let numero_final = '';

    let codpdv = '';
    if (cliente.codpdv) {
      codpdv = cliente.codpdv;
    }

    return this.getInvoiceSeriesNum().then((serie) => {
      serie_pedido = serie;
      return this.getInvoiceSeriesLastNum(serie_pedido).then((ultimo_numero) => {
        ultimo_numero_pedido = ultimo_numero;
        numero_final = serie_pedido + (ultimo_numero + 1);
        //this.current_invoice_number = numero_final;
        //this.updateInvoiceLastNumber(serie_pedido);
        let datetime = new Date().toLocaleString();
        this.current_invoice_number = datetime;
        if (ispdv) {
          this.updateVisitPlanDates(codpdv, datetime, cliente.feccre);//corregir esto
        }
        let sql = 'INSERT INTO FAC1000(ordpes, ordtra, codcli, fechcr, geopcr, pedfas, pedsta, succom) VALUES(?,?,?,?,?,?,?,?)';

        return this.getSelectedSucxcomp().then((sucxcom) => {
          return this.db.executeSql(sql, ['M' + datetime, 'M' + datetime, cliente.codcli, AppUtils.getSqliteDateTimeFormat(datetime), geopcr, 2, 1, sucxcom]);
        });
      });
    });
  }


  createInvoiceLine(pedido: any, act_solo_cant: boolean) {

    let numero_linea;

    return this.invoiceProductAlreadyAdded(pedido.orno, pedido.ordtra, pedido.codcli, pedido.codart, pedido.coduni).then((response) => {

      if (response.rows.length > 0) {

        //this.updateInvoicePhaseStatus(pedido.pednum, 2, 1);
        let datetime = new Date().toLocaleString();
        let numero_linea = response.rows.item(0).pedlin;
        let sql = '';

        if (act_solo_cant) {

          sql = 'UPDATE FAC1001 SET cancar=?, pedprt=?, fechcr=?, pedfas=?, pedsta=?, coduni=? WHERE orno=? AND ordtra=? AND clifac=? AND pedlin=? AND codart=? AND coduni=?';

        } else {

          ///articulo eliminado y que es insertado nuevamente
          if (response.rows.item(0).pedfas == 5) {

            sql = 'UPDATE FAC1001 SET cancar=?, pedprt=?, fechcr=?, pedfas=?, pedsta=?, coduni=? WHERE orno=? AND ordtra=? AND clifac=? AND pedlin=? AND codart=? AND coduni=?';

          } else {///articulo existente a actualizar

            sql = 'UPDATE FAC1001 SET cancar=cancar + ?, pedprt=pedprt + ?, fechcr=?, pedfas=?, pedsta=?, coduni=? WHERE orno=? AND ordtra=? AND clifac=? AND pedlin=? AND codart=? AND coduni=?';

          }


        }

        this.getInvoicePhaseStatus(pedido.orno, pedido.ordtra, pedido.codcli).then((ped) => {

          return this.db.executeSql(sql, [pedido.cancar, pedido.pedprt, datetime, ped.pedfas, ped.pedsta, pedido.coduni, pedido.orno, pedido.ordtra, pedido.codcli, numero_linea, pedido.codart, pedido.coduni]);

        });


      }

      if (response.rows.length == 0) {

        this.getInvoiceLinesNum(pedido.orno, pedido.ordtra, pedido.codcli).then((numero) => {

          numero_linea = (numero * 10) + 10;
          this.updateInvoicePhaseStatus(pedido.orno, pedido.ordtra, pedido.codcli, 2, 1);
          let datetime = new Date().toLocaleString();
          let sql = 'INSERT INTO FAC1001(orno, codcli, clifac, conpag, ordtra, pedlin, codart, descar, coduni, pedcan, pedpru, pedprt, fechcr, geopcr, geopel, pedfas, pedsta) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
          return this.db.executeSql(sql, [pedido.orno, pedido.codcli, pedido.clifac, pedido.conpag, pedido.ordtra, numero_linea, pedido.codart, pedido.descar, pedido.coduni, pedido.cancar, pedido.pedpru, pedido.pedprt, datetime, '', '', 2, 1]);

        })

      }

    })



  }

  getClientByInvoice(pednum: string) {
    let cliente: any = {};
    let sql = 'SELECT codcli FROM FAC1000 WHERE facnum = ?';
    return this.db.executeSql(sql, [pednum])
      .then(response => {
        let codcli = response.rows.item(0).codcli;
        return Promise.resolve(codcli);
      });
  }

  getBadge() {
    let sql = 'SELECT divisa FROM PAR1006';
    return this.db.executeSql(sql, [])
      .then(response => {
        return Promise.resolve(response.rows.item(0).divisa);
      })
  }

  getCurrencies() {
    let sql = 'SELECT * FROM MAE1046';
    return this.db.executeSql(sql, [])
      .then(response => {
        let divisas = [];
        for (let index = 0; index < response.rows.length; index++) {
          divisas.push(response.rows.item(index));
        }
        return Promise.resolve(divisas);
      });
  }

  getCash(cobnum: any) {
    let sql = 'SELECT * FROM efectivoentregado WHERE cobnum=?';
    return this.db.executeSql(sql, [cobnum])
      .then(response => {
        let cashies = [];
        for (let index = 0; index < response.rows.length; index++) {
          cashies.push(response.rows.item(index));
        }
        return Promise.resolve(cashies);
      });
  }

  updateRemoveLineReasonFromAnOrder(pednum: string, codmot: string) {
    let sql = 'UPDATE PED1001 SET codmot=? WHERE pednum=?';
    return this.db.executeSql(sql, [codmot, pednum]);
  }

  invoiceProductAlreadyAdded(pednum: any, ordtra: any, codcli: any, codart: any, coduni: any) {

    let sql = 'SELECT * FROM FAC1001 WHERE orno=? AND ordtra=? AND clifac=? AND codart=? AND coduni=?';
    return this.db.executeSql(sql, [pednum, ordtra, codcli, codart, coduni])
      .then(response => {

        return Promise.resolve(response);

      })

  }

  getInvoiceLinesNum(pednum: any, ordtra: any, codcli: any) {

    let sql = 'SELECT * FROM FAC1001 WHERE orno=? AND ordtra=? AND codcli=?';
    return this.db.executeSql(sql, [pednum, ordtra, codcli])
      .then(response => {
        return Promise.resolve(response.rows.length);
      })

  }

  getInvoiceCreationDate(pednum: any) {

    let sql = 'SELECT fechcr FROM FAC1000 WHERE facnum=?';
    return this.db.executeSql(sql, [pednum])
      .then(response => {

        return Promise.resolve(response.rows.item(0).fechcr);

      })

  }

  getSelectedSucxcomp() {
    let sql = 'SELECT sucxcom FROM config WHERE id=?';
    return this.db.executeSql(sql, [1])
      .then(response => {
        return Promise.resolve(response.rows.item(0).sucxcom);
      },
        error => {
          return Promise.reject(error);
        });
  }

  getRepUser() {
    let sql = 'SELECT * FROM repreusuariologin';
    return this.db.executeSql(sql, [])
      .then(response => {
        return Promise.resolve(response.rows.item(0));
      },
        error => {
          return Promise.reject(error);
        });
  }

  getDescSucxcomp(codusr, codsuc, codpro) {
    let sql = 'SELECT * FROM REL1003 WHERE codusr=? AND codpro=? AND codsuc=?';
    return this.db.executeSql(sql, [codusr, codpro, codsuc])
      .then(response => {
        return Promise.resolve(response.rows.item(0));
      },
        error => {
          return Promise.reject(error);
        });
  }

  // getIdSucxcom() {
  //   let sql = 'SELECT despro, dessuc FROM REL1003 WHERE codusr=? AND codpro=? AND codsuc=?';
  //   return this.db.executeSql(sql, [codusr, codpro, codsuc])
  //   .then(response => {
  //     return Promise.resolve( response.rows.item(0) );
  //   },
  //   error => {
  //     return Promise.reject(error);
  //   });
  // }

  getInvoiceSeriesNum() {
    let sql = 'SELECT * FROM MAE1040';
    return this.db.executeSql(sql, [])
      .then(response => {
        let serie = response.rows.item(1).codser;
        return Promise.resolve(serie);
      });
  }

  getInvoiceSeriesLastNum(series: any) {

    let sql = 'SELECT ultimo FROM PAR1007 WHERE serie=?';
    return this.db.executeSql(sql, [series])
      .then(response => {
        let ultimo_numero = response.rows.item(0).ultimo;
        return Promise.resolve(ultimo_numero);
      })

  }

  getInvoiceSeriesLastNumLN(series: any) {

    let sql = 'SELECT nfact, ultimo FROM PAR1007 WHERE serie=?';
    return this.db.executeSql(sql, [series])
      .then(response => {
        let ultimo_numero = response.rows.item(0);
        return Promise.resolve(ultimo_numero);
      })

  }

  updateInvoiceLastNumber(series: any) {

    let sql = 'UPDATE PAR1007 SET ultimo = ultimo + 1 WHERE serie=?';
    return this.db.executeSql(sql, [series]);

  }

  updateInvoiceLastNumberLN(series: any) {

    let sql = 'UPDATE PAR1007 SET ultimo = ultimo + 1, nfact = nfact + 1 WHERE serie=?';
    return this.db.executeSql(sql, [series]);

  }

  updateSeniatControlNumberLN(series: any) {

    let sql = 'UPDATE PAR1007 SET ultimo = ? WHERE serie=?';
    return this.db.executeSql(sql, [series]);

  }

  getCurrentInvoiceNumber() {

    return this.current_invoice_number;

  }

  updateInvoiceLinePhaseStatus(pednum: any, pedlin: any, fase: any, status: any) {

    let sql = 'UPDATE FAC1001 SET pedfas=?, pedsta=? WHERE facnum=? AND pedlin=?';
    return this.db.executeSql(sql, [fase, status, pednum, pedlin]);

  }

  updateRemoveInvoiceReason(pednum: string, codmot: string) {

    let sql = 'UPDATE FAC1000 SET codmot=? WHERE facnum=?';
    return this.db.executeSql(sql, [codmot, pednum]);

  }

  updateRemoveInvoiceLineReason(pednum: string, pedlin: any, codmot: string) {

    let sql = 'UPDATE FAC1001 SET codmot=? WHERE facnum=? AND pedlin=?';
    return this.db.executeSql(sql, [codmot, pednum, pedlin]);

  }

  getInvoicePhaseStatus(pednum: any, ordtra: any, codcli: any) {

    let sql = 'SELECT pedfas, pedsta FROM FAC1000 WHERE ordpes=? AND ordtra=? AND clifac=?';
    return this.db.executeSql(sql, [pednum, ordtra, codcli])
      .then(response => {

        return Promise.resolve(response.rows.item(0));

      })

  }

  updateSelectedsucxcom(sucxcom) {

    let sql = 'UPDATE config SET sucxcom=? WHERE id=?';
    return this.db.executeSql(sql, [sucxcom, 1]);

  }

  updateInvoiceNumber(facnum: any, num: any) {

    let sql = 'UPDATE FAC1000 SET facnum=? WHERE facnum=?';
    return this.db.executeSql(sql, [num, facnum]);

  }

  updateInvoiceLinesNumber(facnum: any, num: any) {

    let sql = 'UPDATE FAC1001 SET facnum=? WHERE facnum=?';
    return this.db.executeSql(sql, [num, facnum]);

  }

  /*TODO add fields*/
  createInvoiceLN(cliente: any) {


    let serie_pedido = '';
    let ultimo_numero_pedido = '';
    let numero_final = '';
    let numero_control = '';
    let numero_factura = '';

    let codpdv = '';
    /*if(cliente.codpdv){
      codpdv = cliente.codpdv;
    }*/

    return this.getInvoiceSeriesNum().then((serie) => {
      serie_pedido = serie;
      return this.getInvoiceSeriesLastNumLN(serie_pedido).then((ultimo_numero) => {
        numero_control = ultimo_numero.ultimo;
        numero_factura = ultimo_numero.nfact;
        numero_final = serie_pedido + (ultimo_numero + 1);
        this.current_invoice_ln_number = numero_factura;
        this.current_invoice_control_ln_number = numero_control;
        this.updateInvoiceLastNumberLN(serie_pedido);
        let datetime = new Date().toLocaleString();
        //facnum = ordpes
        /*if(ispdv){
          this.updateVisitPlanDates(codpdv, datetime, cliente.feccre);//corregir esto
        }*/
        let sql = 'INSERT INTO FAC1002(facnum, nfact, codcli, clifac, fechcr, codven, refe, cond, cred, numdoc, codrep, incs, serie, numcon, numcom, facfas, facsta) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

        return this.getSelectedSucxcomp().then((sucxcom) => {
          console.log('sucxcom: ' + sucxcom);
          if (sucxcom != '' && sucxcom != null) {
            console.log('no es nula');
            let ids = sucxcom.split(" ");

            return this.getRepUser().then((rep) => {
              console.log('rep: ' + rep);
              if (rep != '' && rep != null) {
                console.log('no es nula');

                return this.db.executeSql(sql, [cliente.ordpes, numero_factura, cliente.codcli, cliente.clifac, AppUtils.getSqliteDateTimeFormat(datetime), cliente.codven, "Factura", "Pendiente", "Contado", "", rep.codrep, 0, serie_pedido, numero_control, ids[2], 3, 1]);
              }
            });

            //return this.db.executeSql(sql, [cliente.ordpes, numero_factura, cliente.codcli, AppUtils.getSqliteDateTimeFormat(datetime), "", "Factura", "Pendiente", "Contado", "", "", 0, serie_pedido, numero_control, ids[2], 3, 1]);

          }
        });
      });
    });
  }

  /*TODO add fields*/
  createInvoiceLinesLN(facnum: any, contnum: any, lines: any[]) {
    let promises = [];
    let query: string;

    return this.getSelectedSucxcomp().then((sucxcom) => {
      console.log('sucxcom: ' + sucxcom);
      if (sucxcom != '' && sucxcom != null) {
        console.log('no es nula');
        let ids = sucxcom.split(" ");

        return this.getRepUser().then((rep) => {
          console.log('rep: ' + rep);
          if (rep != '' && rep != null) {
            console.log('no es nula');

            query = 'INSERT INTO FAC1003 (facnum, pedlin, codart, descar, coduni, canfac, descue, precio, prtotal, codven, codrep, numcon, numcom, facfas, facsta) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            for (let i = 0; i < lines.length; i++) {
              promises.push(this.db.executeSql(query, [facnum, lines[i].pedlin, lines[i].codart, lines[i].descar, lines[i].coduni, lines[i].cancar, 0, lines[i].pedpru, lines[i].pedprt, lines[i].codven, rep.codrep, contnum, ids[2], 3, 1]));
            }

            return Promise.all(promises);
          }
        });
        // query = 'INSERT INTO FAC1003 (facnum, pedlin, codart, descar, coduni, canfac, descue, precio, prtotal, codven, codrep, numcon, numcom, facfas, facsta) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
        // for (let i = 0; i < lines.length; i++) {
        //   promises.push(this.db.executeSql(query, [facnum, lines[i].pedlin, lines[i].codart, lines[i].descar, lines[i].coduni, lines[i].cancar, 0, lines[i].pedpru, lines[i].pedprt, "rep.codrep", "", contnum, ids[2], 3, 1]));
        // }

        // return Promise.all(promises);
      }
    });

  }

  updateInvoiceLN(facnum: any, numdoc: any) {

    let sql = 'UPDATE FAC1002 SET numdoc=? WHERE facnum=?';
    return this.db.executeSql(sql, [numdoc, facnum]);

  }

  getCurrentLNInvoiceNumber() {

    return this.current_invoice_ln_number;

  }

  getCurrentLNInvoiceControlNumber() {

    return this.current_invoice_control_ln_number;

  }

  getInvoicesLNToSync() {
    let sql = "SELECT * FROM FAC1002 WHERE (facfas=? AND facsta=?) OR (facfas=? AND facsta=?) ORDER BY fechcr";
    return this.db.executeSql(sql, [9, 1, 5, 3])
      .then(response => {
        let pedidos = [];
        for (let index = 0; index < response.rows.length; index++) {
          pedidos.push(response.rows.item(index));
        }
        return Promise.resolve(pedidos);
      });
  }

  getChargesToSync() {
    let sql = "SELECT * FROM cobranza WHERE (cobfas=? AND cobsta=?) OR (cobfas=? AND cobsta=?) ORDER BY fechcr";
    return this.db.executeSql(sql, [6, 1, 5, 3])
      .then(response => {
        let pedidos = [];
        for (let index = 0; index < response.rows.length; index++) {
          pedidos.push(response.rows.item(index));
        }
        return Promise.resolve(pedidos);
      });
  }

  getChargeToSync(cobnum: any) {
    let sql = 'SELECT * FROM facturaxcobranza WHERE cobnum=? ORDER BY fechcr';
    return this.db.executeSql(sql, [cobnum])
      .then(response => {
        let pedido = [];
        for (let index = 0; index < response.rows.length; index++) {
          pedido.push(response.rows.item(index));
        }
        return Promise.resolve(pedido);
      })
  }

  getInvoiceLNToSync(facnum: any) {
    let sql = 'SELECT * FROM FAC1003 WHERE numcon=? ORDER BY fechcr';
    return this.db.executeSql(sql, [facnum])
      .then(response => {
        let pedido = [];
        for (let index = 0; index < response.rows.length; index++) {
          pedido.push(response.rows.item(index));
        }
        return Promise.resolve(pedido);
      })
  }

  getInvoicesLN(codcli: any, facfas: any, facsta: any) {

    let today = AppUtils.getTodayInitSqliteFormat();
    let sql = "SELECT * FROM FAC1002 WHERE codcli=? AND facfas=? AND facsta=? AND fechcr >= DATETIME('" + today + "')";
    return this.db.executeSql(sql, [codcli, facfas, facsta])
      .then(response => {

        let facturas = [];
        for (let index = 0; index < response.rows.length; index++) {
          facturas.push(response.rows.item(index));
        }
        return Promise.resolve(facturas);

      })

  }

  getInvoiceLN(facnum: any, facfas: any, facsta: any) {
    let sql = 'SELECT * FROM FAC1003 WHERE numcon=? AND facfas=? AND facsta=? ORDER BY fechcr';
    return this.db.executeSql(sql, [facnum, facfas, facsta])
      .then(response => {
        let pedido = [];
        for (let index = 0; index < response.rows.length; index++) {
          pedido.push(response.rows.item(index));
        }
        return Promise.resolve(pedido);
      })
  }

  updateInvoiceLNPhaseStatus(facnum: any, fase: any, status: any) {

    let sql = 'UPDATE FAC1002 SET facfas=?, facsta=? WHERE nfact=?';
    return this.db.executeSql(sql, [fase, status, facnum]);

  }

  updateInvoiceLinesLNPhaseStatus(facnum: any, fase: any, status: any) {

    let sql = 'UPDATE FAC1003 SET facfas=?, facsta=? WHERE numcon=?';
    return this.db.executeSql(sql, [fase, status, facnum]);

  }

  getInvoiceLNToCharge(facnum: any) {
    let sql = 'SELECT * FROM FAC1002 WHERE nfact=?';
    return this.db.executeSql(sql, [facnum])
      .then(response => {
        return Promise.resolve(response.rows.item(0));
      })
  }

  ////////////////////////////////////////////////////billing operations//////////////////////////////////





  ////////////////////////////////////////////////////visit plans operations//////////////////////////////////
  updateVisitPlanDates(codpdv: any, date: string, feccre: any) {
    let sql = "SELECT fechin FROM mis_planes WHERE codpdv = ? AND feccre=?";
    this.db.executeSql(sql, [codpdv, feccre]).then(
      (response) => {
        if (response.rows.item(0).fechin === null) {
          sql = "UPDATE mis_planes SET fechin = ?, fechfi = ?, sync = ?, eco=eco+1 WHERE codpdv = ? AND feccre=?";
          this.db.executeSql(sql, [date, date, false, codpdv, feccre]);
        } else {
          sql = "UPDATE mis_planes SET fechfi = ?, sync = ?, eco=eco+1 WHERE codpdv=? AND feccre=?";
          this.db.executeSql(sql, [date, false, codpdv, feccre]);
        }
      });
  }

  updateVisitPlanSync(codpdv: any, eco: number, feccre: string, sync: Boolean) {
    let sql = 'UPDATE mis_planes SET sync=? WHERE codpdv=? AND feccre=? AND eco=?';
    return this.db.executeSql(sql, [sync, codpdv, feccre, eco]);
  }

  getPlanesSimple() {
    // let sql = "SELECT * FROM mis_planes";
    let sql = "SELECT * FROM mis_planes WHERE sync IS ?";

    return this.db.executeSql(sql, [false])
      .then(response => {
        let planes = [];
        for (let index = 0; index < response.rows.length; index++) {
          planes.push(response.rows.item(index));
        }
        return Promise.resolve(planes);
      });
  }

  updateClientexplanSync(codpdv: any, codcli: any, sync: any, eco: number, feccre: any) {
    let sql = 'UPDATE clientesxplanes SET sync=? WHERE codpdv=? AND codcli=? AND eco=? AND feccre=?';
    return this.db.executeSql(sql, [sync, codpdv, codcli, eco, feccre]);
  }
  ////////////////////////////////////////////////////visit plans operations//////////////////////////////////




  /////////////////////////////////////////////////unknown/////////////////////////////////////////////////////

  createTable() {
    let sql = 'CREATE TABLE IF NOT EXISTS pedidos(id INTEGER PRIMARY KEY AUTOINCREMENT, title VARCHAR(50), amount INTEGER)';
    return this.db.executeSql(sql, []);
  }

  getAll() {
    let sql = 'SELECT * FROM pedidos';
    return this.db.executeSql(sql, [])
      .then(response => {
        let tasks = [];
        for (let index = 0; index < response.rows.length; index++) {
          tasks.push(response.rows.item(index));
        }
        return Promise.resolve(tasks);
      })
  }

  create(pedido: any) {
    let sql = 'INSERT INTO pedidos(title, amount) VALUES(?,?)';
    return this.db.executeSql(sql, [pedido.title, pedido.amount]);
  }

  update(pedido: any) {
    let sql = 'UPDATE pedidos SET title=?, amount=? WHERE id=?';
    return this.db.executeSql(sql, [pedido.title, pedido.completed, pedido.id]);
  }

  delete(pedido: any) {
    let sql = 'DELETE FROM pedidos WHERE id=?';
    return this.db.executeSql(sql, [pedido.id]);
  }

  //Yeisser

  getUrlServ() {
    let sql = 'select urlLogin from config';
    return this.db.executeSql(sql, [])
      .then(response => {
        console.log('consulto el dato: ');
        if ((response.rows.length > 0) && (response.rows.item(0).urlLogin != '')) {
          console.log('true');
          return Promise.resolve(response.rows.item(0).urlLogin);
        } else {
          console.log('false');
          return Promise.reject('Elemento vacio');
        }
      },
        error => {
          console.log('entro en error');
          return Promise.reject(error);
        });
  }

  getUrlServComp() {
    let sql = 'select urlLogin from config';
    return this.db.executeSql(sql, [])
      .then(response => {
        if ((response.rows.length > 0) && (response.rows.item(0).urlLogin != '')) {
          return Promise.resolve("https://" + response.rows.item(0).urlLogin + "/RouteWeb");
        } else {
          return Promise.reject('Elemento vacio');
        }
      },
        error => {
          console.log('entro en error');
          return Promise.reject(error);
        })
  }

  deleteElem() {
    let sql = 'delete from config';
    return this.db.executeSql(sql, [])
      .then(response => {
        console.log('dato borrado');
      })
  }


  createTable2(query: string) {
    return this.db.executeSql(query, [])
      .then(response => {
        console.log('consulta ejecutada: ' + query);
      })
  }

  deleteTable(query: string) {
    return this.db.executeSql(query, [])
      .then(response => {
        console.log('consulta ejecutada: ' + query);
      })
  }

  insertOrUpdateUrlServ(url: string) {
    let sql_insert = "INSERT INTO config(id, urlLogin, inic_config, inic_sync) VALUES (?, ?, ?, ?)";
    let sql_update = "UPDATE config set urlLogin = ? where id = ?";
    let sql_rev = "select id from config where id = ?";

    return this.db.executeSql(sql_rev, [1])
      .then(response => {
        if (response.rows.length == 1) {
          return this.db.executeSql(sql_update, [url, 1]);
        } else {
          return this.db.executeSql(sql_insert, [1, url, false, false]);
        }
      });

    // return this.db.executeSql(sql, []);
  }

  insertOrUpdateTokenPush(token: string) {
    let sql_insert = "INSERT INTO config(id, urlLogin, inic_config, tokenPush, inic_sync) VALUES (?, ?, ?, ?, ?)";
    let sql_update = "UPDATE config set tokenPush = ? where id = ?";
    let sql_rev = "select id from config where id = ?";

    return this.db.executeSql(sql_rev, [1])
      .then(response => {
        if (response.rows.length == 1) {
          return this.db.executeSql(sql_update, [token, 1]);
        } else {
          return this.db.executeSql(sql_insert, [1, '', false, token, false]);
        }
      });
  }

  getFieldOfTable(tableName: string, fieldName: string, cond?: string, params?: any[]) {
    let query = "SELECT " + fieldName + " FROM " + tableName;

    if (cond) {
      query += " WHERE " + cond;
    } else {
      params = [];
    }

    console.log("consulta a ejecutar: " + query);
    return this.db.executeSql(query, params)
      .then(response => {
        if (response.rows.length > 0) {
          console.log("longitud correcta");
          return Promise.resolve(response.rows.item(0)[fieldName]);
        } else {
          console.log("longitud incorrecta");
          return Promise.reject('Conjunto Vacio');
        }
      },
        error => {
          console.log('entro en error');
          return Promise.reject(error);
        });
  }

  updateInic_config(id: number, value: boolean) {
    let sql = 'UPDATE config SET inic_config=? WHERE id=?';
    return this.db.executeSql(sql, [value, id])
      .then(response => {
        return Promise.resolve();
      });
  }

  updateInic_sync(id: number, value: boolean) {
    let sql = 'UPDATE config SET inic_sync=? WHERE id=?';
    return this.db.executeSql(sql, [value, id])
      .then(response => {
        return Promise.resolve();
      });
  }

  // insertDatos(data: any[]) {
  //   let batch : any[] = [];

  //     for (let index = 0; index < data.length; index++) {
  //       let query = data[index].query + " VALUES(";
  //       let params = data[index].params;



  //       for(let i = 0; i < (params.length - 1); i++) {
  //         query += "?, ";
  //       }
  //       query += "?)";

  //       let values = data[index].values;
  //       for(let iv = 0; iv < values.length; iv++) {
  //         let data: any[] = [];
  //         for(let i = 0; i < (params.length); i++) {
  //           data.push(values[iv][params[i]])
  //         }


  //       }
  //       batch.push([query,data])

  //     }


  //       this.db.sqlBatch(batch).then(
  //         (res) => {
  //           console.log('consulta ejecutada: ');
  //           //console.log('query: '+ query);
  //           //console.log('values: '+ values[iv]);
  //         },
  //         (error) => {
  //           console.log('consulta erronea: ');
  //           //console.log('query: '+ query);
  //           //console.log('values: '+ JSON.stringify(values[iv]));
  //         });
  //   }

  insertDatos2(data: any[]) {
    let promises: any[] = [];
    for (let index = 0; index < data.length; index++) {
      let query = data[index].query + " VALUES(";
      let params = data[index].params;

      for (let i = 0; i < (params.length - 1); i++) {
        query += "?, ";
      }
      query += "?)";

      let values = data[index].values;
      for (let iv = 0; iv < values.length; iv++) {
        let data: any[] = [];
        for (let i = 0; i < (params.length); i++) {
          data.push(values[iv][params[i]])
        }

        promises.push(this.db.executeSql(query, data).then(
          (res) => {
            console.log('consulta ejecutada: ');
            console.log('query: ' + query);
            console.log('values: ' + values[iv]);
          },
          (error) => {
            console.log('consulta erronea: ');
            console.log('query: ' + query);
            console.log('values: ' + JSON.stringify(values[iv]));
          })
        );
      }
    }

    return Promise.all(promises);

  }

  insertDatos(data: any[]) {
    let promises: any[] = [];
    for (let index = 0; index < data.length; index++) {
      let query = data[index].query + " VALUES(";
      let params = data[index].params;

      for (let i = 0; i < (params.length - 1); i++) {
        query += "?, ";
      }
      query += "?)";

      let values = data[index].values;
      for (let iv = 0; iv < values.length; iv++) {
        let data: any[] = [];
        for (let i = 0; i < (params.length); i++) {
          data.push(values[iv][params[i]]);
        }

        promises.push(this.db.executeSql(query, data).then(
          res => {
            //console.log("consulta ejecutada: " + query + " data: " + data.toString());
          },
          error => {
            //console.log("consulta erronea: " + query);
          }
        ));
        // this.db.executeSql(query, data).then(
        //   (res) => {
        //     console.log('consulta ejecutada: ');
        //     console.log('query: '+ query);
        //     console.log('values: '+ values[iv]);
        //   },
        //   (error) => {
        //     console.log('consulta erronea: ');
        //     console.log('query: '+ query);
        //     console.log('values: '+ JSON.stringify(values[iv]));
        //   });
      }
    }

    return Promise.all(promises);
  }

  insertAsoc(data: any[]) {
    // console.log("longitud de asociaciones: " +data.length);
    // for(let index = 0; index < data.length; index++){
    //   let sql = "INSERT INTO mis_clientes(codcli, todos) VALUES (?, ?)";
    //   this.db.executeSql(sql, [data[index].codcli, data[index].todos]).then(
    //     (res) => {
    //       console.log('cliente insertado: ');
    //       console.log(data[index].codcli);

    //       let lista: any[] = data[index].productos;

    //       for(let i = 0; i < lista.length; i++) {
    //         let query = "INSERT INTO productosxcliente(codpro, codcli, prio) VALUES (?, ?, ?)";
    //         this.db.executeSql(query, [lista[i].codpro, data[index].codcli, lista[i].prio]);
    //       }
    //     },
    //     (error) => {
    //       console.log('insert erroneo de cliente: '+ data[index].codcli);
    //       console.log('el error es: '+ error);
    //     });
    // }
    let promises: any[] = [];

    for (let index = 0; index < data.length; index++) {
      let sql = "INSERT INTO mis_clientes(codcli, todos) VALUES (?, ?)";
      promises.push(this.db.executeSql(sql, [data[index].codcli, data[index].todos]).then(
        (res) => {
          //console.log("consulta ejecutada: " + sql);
        },
        (error) => {
          //console.log("consulta erronea: " + sql);
        }));

      let lista: any[] = data[index].productos;

      for (let i = 0; i < lista.length; i++) {
        let query = "INSERT INTO productosxcliente(codpro, codcli, prio) VALUES (?, ?, ?)";
        promises.push(this.db.executeSql(query, [lista[i].codpro, data[index].codcli, lista[i].prio]).then(
          (res) => {
            //console.log("consulta ejecutada: " + query);
          },
          (error) => {
            //console.log("consulta erronea: " + query);
          }));
      }
    }

    return Promise.all(promises);
  }

  droptables() {
    let promises: any[] = [];

    let querys: string[] = [];

    querys.push("DELETE FROM MAE1016");
    querys.push("DELETE FROM MAE1037");
    querys.push("DELETE FROM MAE1040");
    querys.push("DELETE FROM MAE1049");
    querys.push("DELETE FROM MAE1046");
    querys.push("DELETE FROM productosxcliente");
    querys.push("DELETE FROM ESTADOCUENTA");
    querys.push("DELETE FROM mis_planes");
    querys.push("DELETE FROM mis_clientes");
    querys.push("DELETE FROM factura_cobrar");
    querys.push("DELETE FROM facturaxcobranza");
    querys.push("DELETE FROM PAR1006");
    querys.push("DELETE FROM PAR1007");
    querys.push("DELETE FROM TMP1001");


    querys.forEach(
      (elem) => {
        promises.push(this.db.executeSql(elem, [])
          .then(() => { console.log("consulta ejecutada exitosamente: " + elem); })
        );
      });

    return Promise.all(promises);
  }

  deleteClientsOrProductTable(opt: number) {
    let query;

    switch (opt) {
      case 0:
        query = "DELETE FROM MAE1016";
        break;
      case 1:
        query = "DELETE FROM MAE1037";
        break;
    }

    return this.db.executeSql(query, []);
  }

  insertPlanes(data: any[]) {
    let promises: any[] = [];

    for (let index = 0; index < data.length; index++) {
      let sql = "INSERT INTO mis_planes(codpdv, despdv, feccre, eco) VALUES (?, ?, ?, ?)";
      promises.push(this.db.executeSql(sql, [data[index].codpdv, data[index].despdv, data[index].feccre, 0]).then(
        (res) => {
          //console.log("consulta ejecutada: " + sql);
        },
        (error) => {
          //console.log("consulta erronea: " + sql);
          Promise.reject(error)
        }));

      let lista: any[] = data[index].clientes;

      for (let i = 0; i < lista.length; i++) {
        let query = "INSERT INTO clientesxplanes(codpdv, codcli, prio, feccre, eco) VALUES (?, ?, ?, ?, ?)";
        promises.push(this.db.executeSql(query, [data[index].codpdv, lista[i].codcli, lista[i].prio, data[index].feccre, 0]).then(
          (res) => {
            //console.log("consulta ejecutada: " + query);
          },
          (error) => {
            //console.log("consulta erronea: " + query);
            Promise.reject(error)
          }));
      }
    }

    return Promise.all(promises);
  }



  getPlanes() {

    let planes: any[] = [];
    let promises: any[] = [];
    let dateStr;

    dateStr = AppUtils.getTodaySpain();

    console.log("fecha actual: " + dateStr);

    let sql = "SELECT * FROM mis_planes WHERE feccre=?";

    return this.db.executeSql(sql, [dateStr])
      .then(planes_cab => {

        for (let index = 0; index < planes_cab.rows.length; index++) {
          let plan: any = {
            codpdv: '',
            despdv: '',
            clientes: []
          };

          plan.codpdv = planes_cab.rows.item(index).codpdv;
          plan.despdv = planes_cab.rows.item(index).despdv;

          let query = "SELECT cxp.codpdv, cxp.codcli, cxp.prio, cxp.visitado, cxp.codmot, cxp.feccre, mae1016.descli, mae1016.coogeo FROM clientesxplanes cxp JOIN MAE1016 mae1016 ON cxp.codcli = mae1016.codcli WHERE cxp.codpdv = ? AND cxp.feccre=? ORDER BY cxp.prio";
          promises.push(
            this.db.executeSql(query, [plan.codpdv, dateStr])
              .then(clientes => {
                for (let i = 0; i < clientes.rows.length; i++) {
                  plan.clientes.push(clientes.rows.item(i));
                }
                planes.push(plan);
              })
          );
        }
        return Promise.all(promises).then(
          () => {
            return Promise.resolve(planes);
          });
      });
  }

  getPlanClient(codcli: any) {
    let sql = "SELECT * FROM clientesxplanes WHERE codcli=?";

    return this.db.executeSql(sql, [codcli])
      .then(response => {
        let clientes = [];
        for (let index = 0; index < response.rows.length; index++) {
          clientes.push(response.rows.item(index));
        }
        return Promise.resolve(clientes);
      })
  }

  getAsocClients() {
    let sql = "SELECT mae1016.codcli, mae1016.descli, mae1016.coogeo, mc.todos FROM MAE1016 mae1016 JOIN mis_clientes mc ON mae1016.codcli = mc.codcli ORDER BY mae1016.descli";

    return this.db.executeSql(sql, [])
      .then(response => {
        let clientes = [];
        for (let index = 0; index < response.rows.length; index++) {
          clientes.push(response.rows.item(index));
        }
        return Promise.resolve(clientes);
      })
  }

  OrdersWhSync() {
    let datetime = new Date();


    let sql = 'select * from PED1000 WHERE pedfas=? AND pedsta=?';
    return this.db.executeSql(sql, [6, 1])
      .then(response => {
        if (response.rows.length > 0) {
          return Promise.resolve(true);
        } else {
          return Promise.resolve(false);
        }
      },
        error => {
          console.log('entro en error OrdersWhSync');
          return Promise.reject(error);
        })
  }

  cxpWhSync() {
    let datetime = new Date();
    let sql = 'select * from clientesxplanes WHERE sync IS ?';
    return this.db.executeSql(sql, [false])
      .then(response => {
        if (response.rows.length > 0) {
          return Promise.resolve(true);
        } else {
          return Promise.resolve(false);
        }
      },
        error => {
          console.log('entro en error cxpWhSync');
          return Promise.reject(error);
        })
  }

  vpWhSync() {
    let datetime = new Date();
    let sql = 'select * from mis_planes WHERE sync IS ?';
    return this.db.executeSql(sql, [false])
      .then(response => {
        if (response.rows.length > 0) {
          return Promise.resolve(true);
        } else {
          return Promise.resolve(false);
        }
      },
        error => {
          console.log('entro en error cxpWhSync');
          return Promise.reject(error);
        })
  }

  deleteSyncOrders() {
    let promises: any[] = [];

    // let today = AppUtils.getTodayInit();

    let today = AppUtils.getTodayInitSqliteFormat();

    let del_cab = "DELETE FROM PED1000 WHERE pedfas=? AND pedsta=? AND fechcr < DATETIME('" + today + "')";
    let del_lin = "DELETE FROM PED1001 WHERE pedfas=? AND pedsta=? AND fechcr < DATETIME('" + today + "')";

    console.log("ejecutar: " + del_cab);
    promises.push(this.db.executeSql(del_cab, [4, 3]).then(
      () => {
        console.log('Exito eliminando ordenes');
      },
      error => {
        console.log('Error eliminando ordenes');
      }
    ));
    promises.push(this.db.executeSql(del_lin, [4, 1]));

    return Promise.all(promises);
  }

  deleteSyncClientesxPlanes() {
    let sql = "DELETE FROM clientesxplanes WHERE sync IS ? AND feccre < DATE('" + AppUtils.getTodaySpain() + "')";
    return this.db.executeSql(sql, [true]).then(
      () => {
        console.log('Exito eliminando clientes por planes');
      },
      error => {
        console.log('Error eliminando clientes por planes');
      });
  }

  deleteSyncPlanes() {
    let sql = "DELETE FROM mis_planes WHERE sync IS ?";
    return this.db.executeSql(sql, [true]).then(
      () => {
        console.log('Exito eliminando clientes por planes');
      },
      error => {
        console.log('Error eliminando clientes por planes');
      });
  }

  getPhotosByClient(codcli: any) {

    let sql = 'SELECT * FROM FOT1000 WHERE codcli=?';
    return this.db.executeSql(sql, [codcli])
      .then(response => {

        let fotos = [];
        for (let index = 0; index < response.rows.length; index++) {
          fotos.push(response.rows.item(index));
        }
        return Promise.resolve(fotos);


      })

  }

  insertPhotoByClient(codcli: any, destino: any) {
    let sql = 'INSERT INTO FOT1000(codcli, destino) VALUES(?,?)';
    return this.db.executeSql(sql, [codcli, destino]);
  }

  deletePhotoByClient(codcli: any, id: any) {
    let sql = 'DELETE FROM FOT1000 WHERE fotnum=? AND codcli=?';
    return this.db.executeSql(sql, [id, codcli]);
  }


}
