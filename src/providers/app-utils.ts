export class AppUtils {
    public static convertDateFormat(src: string): string {
        let date: Date = new Date(src);
        let monthStr: string;
        let dateStr: string;
        let hourStr: string;
        let minuteStr: string;
        let secondStr: string;
        if(date.getMonth() < 9){
            monthStr = ""+"0"+(date.getMonth() + 1);
        } else {
            monthStr = (date.getMonth() + 1).toString();
        }
        if(date.getDate() < 10){
            dateStr = ""+"0"+date.getDate();
        } else {
            dateStr = date.getDate().toString();
        }
        if(date.getHours() < 10){
            hourStr = ""+"0"+date.getHours();
        } else {
            hourStr = date.getHours().toString();
        }
        if(date.getMinutes() < 10){
            minuteStr = ""+"0"+date.getMinutes();
        } else {
            minuteStr = date.getMinutes().toString();
        }
        if(date.getSeconds() < 10){
            secondStr = ""+"0"+date.getSeconds();
        } else {
            secondStr = date.getSeconds().toString();
        }
        
        return(dateStr + "-" + monthStr + "-" + date.getFullYear() + " " + hourStr + ":" + minuteStr);
        // return(monthStr + "-" + dateStr + "-" + date.getFullYear() + " " + hourStr + ":" + minuteStr + ":" + secondStr);

    }

    public static getTodayInit() {
        let date: Date = new Date();
        
        return((date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + ", 12:00:00 AM");
    }

    public static getTodayInitSqliteFormat() {
        let date: Date = new Date();
        let monthStr: string;
        let dateStr: string;
        let hourStr: string;
        let minuteStr: string;
        let secondStr: string;
        if(date.getMonth() < 9){
            monthStr = ""+"0"+(date.getMonth() + 1);
        } else {
            monthStr = (date.getMonth() + 1).toString();
        }
        if(date.getDate() < 10){
            dateStr = ""+"0"+date.getDate();
        } else {
            dateStr = date.getDate().toString();
        }
        
        return( date.getFullYear() + "-" + monthStr + "-" + dateStr + " 00:00:00");
    }

    public static getTodaySpain() {
        let date: Date = new Date();
        let monthStr: string;
        let dateStr: string;
        let hourStr: string;
        let minuteStr: string;
        let secondStr: string;
        if(date.getMonth() < 9){
            monthStr = ""+"0"+(date.getMonth() + 1);
        } else {
            monthStr = (date.getMonth() + 1).toString();
        }
        if(date.getDate() < 10){
            dateStr = ""+"0"+date.getDate();
        } else {
            dateStr = date.getDate().toString();
        }
        
        return(dateStr + "/" + monthStr + "/" + date.getFullYear());
    }

    public static getSqliteDateTimeFormat(src: string) {
        let date: Date = new Date(src);
        let monthStr: string;
        let dateStr: string;
        let hourStr: string;
        let minuteStr: string;
        let secondStr: string;
        if(date.getMonth() < 9){
            monthStr = ""+"0"+(date.getMonth() + 1);
        } else {
            monthStr = (date.getMonth() + 1).toString();
        }
        if(date.getDate() < 10){
            dateStr = ""+"0"+date.getDate();
        } else {
            dateStr = date.getDate().toString();
        }
        if(date.getHours() < 10){
            hourStr = ""+"0"+date.getHours();
        } else {
            hourStr = date.getHours().toString();
        }
        if(date.getMinutes() < 10){
            minuteStr = ""+"0"+date.getMinutes();
        } else {
            minuteStr = date.getMinutes().toString();
        }
        if(date.getSeconds() < 10){
            secondStr = ""+"0"+date.getSeconds();
        } else {
            secondStr = date.getSeconds().toString();
        }
        
        return(date.getFullYear() + "-" + monthStr + "-" + dateStr + " " + hourStr + ":" + minuteStr + ":" + secondStr);
    }

    public static distance(lat1, lon1, lat2, lon2) {
        var p = 0.017453292519943295;    // Math.PI / 180
        var c = Math.cos;
        var a = 0.5 - c((lat2 - lat1) * p)/2 + 
                c(lat1 * p) * c(lat2 * p) * 
                (1 - c((lon2 - lon1) * p))/2;

        return (12742 * Math.asin(Math.sqrt(a)) ) * 1000; // 2 * R; R = 6371 km
    }
}