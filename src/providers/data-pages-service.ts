import { Injectable } from '@angular/core';
import { Observable }     from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/map';

/*
  Generated class for the DataPages provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DataPagesService {

  // public nombre: string;

  constructor() {
    console.log('Hello DataPages Provider');
  }

  private mask = new BehaviorSubject<string>(""); // true is your initial value
  fixed$ = this.mask.asObservable();

  public setMask(value: string) {
    this.mask.next(value);
    console.log('isFixed changed', value);
  }

  public getMask():BehaviorSubject<string> {
    return this.mask;
  }
}
