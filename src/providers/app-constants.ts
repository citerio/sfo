export class AppConstants {
   public static URL_LOGIN = '/mlogin';
   public static URL_CONFIG = "/api/config";//
   public static URL_LOGOUT = "/api/logout/";//
   public static URL_CREATE_TABLES = "/api/createdb/";//
   public static URL_GET_DATA = "/api/dbdata/";//
   public static URL_SEND_ORDER = "/api/PED1001/batch/create/";//
   public static URL_SEND_INVOICE = "/api/FAC1003/batch/create/";//
   public static URL_SEND_CHARGE = "/api/COB1000/batch/create/";//
   public static URL_SEND_CLIE_PLAN = "/api/MAE1043/detail_update/";//
   public static URL_SEND_PLAN = "/api/REL1012/visita/update/";//
   public static URL_GET_ESTADO_CUENTA = "/api/cobrocliente/";//
   public static URL_GET_CLIENTS = "/api/MAE1016/";
   public static URL_GET_PRODUCTS = "/api/MAE1037/";
   public static URL_GET_CLIENTS_ONLINE = "/api/buscaClientes?filtro=";

   public static TIMEOUT_REQUEST_API = 90000;
   public static SQL_LIMIT_ROW = 50;
}