import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/map';

/*
  Generated class for the DbChangesService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DbChangesService {

  constructor(public http: Http) {
    console.log('Hello DbChangesService Provider');
  }

  private sync = new BehaviorSubject<boolean>(false); // true is your initial value
  fixed$ = this.sync.asObservable();

  public setSync(value: boolean) {
    this.sync.next(value);
    console.log('isFixed changed', value);
  }

  public getSync():BehaviorSubject<boolean> {
    return this.sync;
  }
}
