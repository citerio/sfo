import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { DbService } from './db-service';
import { GenericRestService } from './generic-rest-service';
import { AppConstants } from './app-constants';
import { AppUtils } from './app-utils';

// import { MomentModule } from 'angular2-moment';
import { Observable } from 'rxjs/Observable';
import { DbChangesService } from './db-changes-service';

import 'rxjs/add/operator/map';
// import * as moment from 'moment';
// import 'moment/locale/pt-br';
/*
  Generated class for the SendDataService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class SendDataService {


  connection_watcher: any;

  constructor(
    public http: Http,
    public network: Network,
    public database: DbService,
    public genericRestService: GenericRestService,
    public dbChangesService: DbChangesService
  ) {
    console.log('Hello SendDataService Provider');
  }

  initSyncWhenOnline(syntim: number) {

    if (syntim == undefined) {
      syntim = 40;
    }

    console.log('Init sync called');
    this.connection_watcher = Observable.interval(syntim * 1000).subscribe((t) => { this.sendDataWhenOnline(); });

  }

  sendDataWhenOnline() {
    // watch network for a connection

    console.log('sendDataWhenOnline sync called');

    if (this.network.type !== 'none') {


      this.database.openDatabase().then(
        () => {
          /////////////////////////////////////////////////////sending orders//////////////////////////////////////////////////////////////
          this.database.getOrdersToSync()
            .then(printed_pedidos => {
              printed_pedidos.forEach(
                (item) => {
                  this.database.getOrderToSync(item.pednum).then(
                    (detalles: any) => {
                      let pedido = {
                        cabecera: item,
                        detalles: detalles
                      };

                      pedido.cabecera.fechcr = AppUtils.convertDateFormat(pedido.cabecera.fechcr);

                      pedido.cabecera.codsuc = { codsuc: "" };

                      console.log('cabecera pednum: ' + pedido.cabecera.pednum);
                      console.log('cabecera codcli: ' + pedido.cabecera.codcli);
                      console.log('cabecera fechcr: ' + pedido.cabecera.fechcr);
                      console.log('cabecera fechel: ' + pedido.cabecera.fechel);
                      console.log('cabecera fechcl: ' + pedido.cabecera.fechcl);
                      console.log('cabecera geopcr: ' + pedido.cabecera.geopcr);
                      console.log('cabecera geopcl: ' + pedido.cabecera.geopcl);
                      console.log('cabecera geopel: ' + pedido.cabecera.geopel);
                      console.log('cabecera pedfas: ' + pedido.cabecera.pedfas);
                      console.log('cabecera pedsta: ' + pedido.cabecera.pedsta);
                      console.log('cabecera pederr: ' + pedido.cabecera.pederr);
                      console.log('cabecera codpdv: ' + pedido.cabecera.codpdv);
                      console.log('cabecera ispdv: ' + pedido.cabecera.ispdv);
                      console.log('cabecera codmot: ' + pedido.cabecera.codmot);
                      console.log('cabecera succom: ' + JSON.stringify(pedido.cabecera));
                      console.log('cabecera succom codusr: ' + pedido.cabecera.succom.codusr);
                      console.log('cabecera succom codsuc: ' + pedido.cabecera.succom.codsuc);
                      console.log('cabecera succom codpro: ' + pedido.cabecera.succom.codpro);
                      console.log('cabecera succom dessuc: ' + pedido.cabecera.succom.dessuc);
                      console.log('cabecera succom despro: ' + pedido.cabecera.succom.despro);

                      for (let det of pedido.detalles) {
                        det.pednum = { pednum: pedido.cabecera.pednum };
                        det.fechcr = AppUtils.convertDateFormat(det.fechcr);

                        console.log('detalle pedlin: ' + det.pedlin);
                        console.log('detalle pednum: ' + det.pednum);
                        console.log('detalle codart: ' + det.codart);
                        console.log('detalle descar: ' + det.descar);
                        console.log('detalle coduni: ' + det.coduni);
                        console.log('detalle pedcan: ' + det.pedcan);
                        console.log('detalle pedpru: ' + det.pedpru);
                        console.log('detalle pedprt: ' + det.pedprt);
                        console.log('detalle fechcr: ' + det.fechcr);
                        console.log('detalle fechcl: ' + det.fechcl);
                        console.log('detalle geopcr: ' + det.geopcr);
                        console.log('detalle geopel: ' + det.geopel);
                        console.log('detalle pedfas: ' + det.pedfas);
                        console.log('detalle pedsta: ' + det.pedsta);
                        console.log('detalle pederr: ' + det.pederr);
                        console.log('detalle codmot: ' + det.codmot);
                      }

                      ////original code
                      this.genericRestService.sendToUrl(AppConstants.URL_SEND_ORDER, pedido).then(
                        (res: any) => {
                          if (res.status == 202) {
                            console.log('Pedido sincronizado correctamente en el server');
                            this.database.updateOrderPhaseStatus(item.pednum, 4, 1).then(
                              (data) => {
                                this.database.updateOrderLinesPhaseStatus(item.pednum, 4, 1).then(
                                  (data) => {
                                    console.log('Pedido sincronizado correctamente en el cliente');
                                    this.dbChangesService.setSync(true);
                                  },
                                  error => {
                                    console.log('Pedido sincronizado erroneamente en el cliente');
                                  }).catch((error) => { })
                              }).catch((error) => { console.log('Pedido sincronizado erroneamente en el cliente'); });
                          } else {
                            console.log('Pedido sincronizado erroneamente en el server. status: ' + res.status);
                            console.log('response para pedidos: ' + res._body);
                          }
                        },
                        (error: any) => {
                          console.log('Pedido sincronizado erroneamente en el server: ' + error);
                        });
                    },
                    (error: any) => {
                      console.log('Detalles obtenidos erroneamente');
                    });
                });
            },
              error => { });

          /////////////////////////////////////////////////////sending LN invoices//////////////////////////////////////////////////////////////
          this.database.getInvoicesLNToSync()
            .then(printed_pedidos => {
              printed_pedidos.forEach(
                (item) => {
                  this.database.getInvoiceLNToSync(item.numcon).then(
                    (detalles: any) => {
                      let pedido = {
                        cabecera: item,
                        detalles: detalles
                      };

                      pedido.cabecera.fechcr = AppUtils.convertDateFormat(pedido.cabecera.fechcr);

                      console.log('factura ln cabecera: ' + JSON.stringify(pedido.cabecera));

                      for (let det of pedido.detalles) {
                        det.facnum = { facnum: pedido.cabecera.facnum };
                        det.fechcr = AppUtils.convertDateFormat(det.fechcr);

                        console.log('factura ln linea: ' + JSON.stringify(det));

                      }

                      /////////original code
                      this.genericRestService.sendToUrl(AppConstants.URL_SEND_INVOICE, pedido).then(
                        (res: any) => {
                          if (res.status == 202) {
                            console.log('Factura llego correctamente en el server');
                            this.database.updateInvoiceLNPhaseStatus(item.nfact, 4, 1).then(
                              (data) => {
                                this.database.updateInvoiceLinesLNPhaseStatus(item.numcon, 4, 1).then(
                                  (data) => {
                                    console.log('Factura sincronizado correctamente en el cliente');
                                    this.dbChangesService.setSync(true);
                                  },
                                  error => {
                                    console.log('Factura sincronizado erroneamente en el cliente');
                                  }).catch((error) => { })
                              }).catch((error) => { console.log('Factura sincronizado erroneamente en el cliente'); });
                          } else {
                            console.log('Factura sincronizado erroneamente en el server. status: ' + res.status);
                            console.log('response para facturas: ' + res._body);
                          }
                        },
                        (error: any) => {
                          console.log('Factura sincronizado erroneamente en el server: ' + error);
                        });
                    },
                    (error: any) => {
                      console.log('Detalles Facturas obtenidos erroneamente');
                    });
                });
            },
              error => { });



          //////////////////////////////////////////////////////////////////////////cobranzas////////////////////////////////////////////////////////

          this.database.getChargesToSync()
            .then(printed_cobranzas => {
              printed_cobranzas.forEach(
                (item) => {
                  this.database.getChargeToSync(item.cobnum).then(
                    (detalles: any) => {

                      this.database.getCash(item.cobnum)
                        .then(
                          (cashies: any) => {
                            let cobranza = {
                              cabecera: item,
                              detalles: detalles,
                              desglose: cashies
                            };

                            cobranza.cabecera.fechcr = AppUtils.convertDateFormat(cobranza.cabecera.fechcr);

                            console.log('cobranza ln cabecera: ' + JSON.stringify(cobranza.cabecera));

                            for (let det of cobranza.detalles) {
                              det.cobnum = { cobnum: cobranza.cabecera.cobnum };
                              det.fechcr = AppUtils.convertDateFormat(det.fechcr);

                              console.log('cobranza ln linea: ' + JSON.stringify(det));

                            }

                            /////////original code
                            this.genericRestService.sendToUrl(AppConstants.URL_SEND_CHARGE, cobranza).then(
                              (res: any) => {
                                if (res.status == 202) {
                                  console.log('Cobranza llego correctamente en el server');
                                  this.database.updateChargePhaseStatus(item.cobnum, 4, 1, item).then(
                                    (data) => {
                                      console.log('Cobranza sincronizado correctamente en el cliente');
                                      this.dbChangesService.setSync(true);
                                    }).catch((error) => { console.log('Factura sincronizado erroneamente en el cliente'); });
                                } else {
                                  console.log('Cobranza sincronizado erroneamente en el server. status: ' + res.status);
                                  console.log('response para cobranzas: ' + res._body);
                                }
                              },
                              (error: any) => {
                                console.log('Cobranzas sincronizado erroneamente en el server: ' + error);
                              });


                          })
                        .catch((error) => { console.log('Error on fetching cash associated to charge'); });

                    },
                    (error: any) => {
                      console.log('Detalles Cobranzas obtenidos erroneamente');
                    });
                });
            },
              error => { });
          //////////////////////////////////////////////////////////////////////////cobranzas////////////////////////////////////////////////////////


          //////////////////////////////////////////////////////////////////////////depositos////////////////////////////////////////////////////////
          /*this.database.getAllPrintedDeposits(6, 1).then(
            (deposits) => {
              deposits.forEach((d) => {
                this.database.updateDepositPhaseStatus(d.depnum, 4, 1, d)
                  .then((data) => {
                    this.dbChangesService.setSync(true);
                  })
                  .catch((error) => { });
              })
            },
            error => {
              alert('Error obteniendo los depositos impresos');
            });*/
          //////////////////////////////////////////////////////////////////////////depositos////////////////////////////////////////////////////////


          this.database.getclientesListos().then(
            (clientesListos) => {
              console.log('Lista de clientes listos obtenidos. Longitud: ' + clientesListos.length);
              clientesListos.forEach(
                (cliente) => {
                  let clienListo = {
                    codpdv: cliente.codpdv,
                    codcli: cliente.codcli,
                    visitado: cliente.visitado,
                    codmot: cliente.codmot,
                    feccre: cliente.feccre,
                    eco: cliente.eco,
                    version: 1.0
                  }

                  this.genericRestService.sendToUrl(AppConstants.URL_SEND_CLIE_PLAN, clienListo).then(
                    (res: any) => {
                      console.log('Response para cliente por plan: ' + res._body);
                      if (res.status == 202) {
                        try {
                          let body = JSON.parse(res._body);
                          console.log('clientexplan sincronizado correctamente en el server');
                          this.database.updateClientexplanSync(clienListo.codpdv, clienListo.codcli, true, body.eco, body.feccre)
                            .then((data) => {
                              console.log('clientexplan sincronizado correctamente en el cliente');
                            })
                            .catch((error) => { console.log('clientexplan sincronizado erroneamente en el cliente'); });
                        } catch (e) {
                          console.log('clientexplan sincronizado erroneamente en el cliente. Error en tryCatch');
                        }
                      } else {
                        console.log('clientexplan sincronizado erroneamente en el server ');
                      }
                    },
                    (error: any) => {
                      console.log('clientexplan sincronizado erroneamente en el server: ' + error);
                    });
                });
            },
            error => { });

          this.database.getPlanesSimple().then(
            (planes) => {
              planes.forEach(
                (plan) => {
                  let planListo = {
                    codpdv: plan.codpdv,
                    fechin: plan.fechin,
                    fechfi: plan.fechfi,
                    feccre: plan.feccre,
                    eco: plan.eco,
                    version: 1.0
                  }

                  planListo.fechin = AppUtils.convertDateFormat(planListo.fechin);
                  planListo.fechfi = AppUtils.convertDateFormat(planListo.fechfi);

                  this.genericRestService.sendToUrl(AppConstants.URL_SEND_PLAN, planListo).then(
                    (res: any) => {
                      console.log('Response para planes simples: ' + res._body);
                      if (res.status == 202) {
                        let body = JSON.parse(res._body);
                        console.log('Plan sincronizado correctamente en el server');
                        this.database.updateVisitPlanSync(planListo.codpdv, body.eco, body.feccre, true)
                          .then((data) => {
                            console.log('Plan sincronizado correctamente en el cliente');
                          })
                          .catch((error) => { console.log('Plan sincronizado erroneamente en el cliente'); });
                      } else {
                        console.log('Plan sincronizado erroneamente en el server: ');
                      }
                    },
                    (error: any) => {
                      console.log('Plan sincronizado erroneamente en el server: ' + error);
                    });
                });
            },
            error => { });
        });
    }
  }


  finishSyncWhenOnline() {

    console.log('finish sync called');
    this.connection_watcher.unsubscribe();

  }


}
