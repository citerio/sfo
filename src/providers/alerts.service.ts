import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { AlertController } from 'ionic-angular';
import {TranslateService} from 'ng2-translate/ng2-translate';
import 'rxjs/add/operator/map';


/*
  Generated class for the Alerts provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AlertsService {

  constructor(
    public http: Http,
    public alertCtrl: AlertController,
    private translate: TranslateService
  ) {
    console.log('Hello Alerts Provider');
  }

  showAlertError(errorMsg: string) {
    this.translate.get(["common.error", "common.acept"], null).subscribe(
      (localizedValues) => {
        let alert = this.alertCtrl.create({
          title: localizedValues["common.error"],
          message: '',
          buttons: [localizedValues["common.acept"]]
        });
        alert.setMessage(errorMsg);
        alert.present();
      });
  }

  showAlert(msg: string) {
    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {
        let alert = this.alertCtrl.create({
          message: '',
          buttons: [localizedValues["common.acept"]]
        });
        alert.setMessage(msg);
        alert.present();
      });
  }

}
