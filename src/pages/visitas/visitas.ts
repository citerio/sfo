import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { CestaPage } from '../cesta/cesta';
import { PedidoMainPage } from "../pedido-main/pedido-main";
import { Diagnostic } from '@ionic-native/diagnostic';
import {TranslateService} from 'ng2-translate/ng2-translate';
import { Geolocation } from '@ionic-native/geolocation';
import { AlertsService } from '../../providers/alerts.service';
import { Storage } from '@ionic/storage';

import { AppUtils } from '../../providers/app-utils';
/*
  Generated class for the Pedido page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-visitas',
  templateUrl: 'visitas.html'
})
export class VisitasPage {

  // clientes:any[] = [];
  // clientes_copia:any[] = [];
  filtro:string = '';
  searching: any = false;
  motivos: any[] = [];
  planes: any[] = [];


  constructor(
    public navCtrl: NavController,
    public view: ViewController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private diagnostic: Diagnostic,
    private translate: TranslateService,
    public geolocation: Geolocation,
    public database: DbService,
    public loadingCtrl: LoadingController,
    public alertsServ: AlertsService,
    private storage: Storage) {}

  
  ionViewDidLoad() {
    // this.getAllClients();
    //this.getAsocClients();

    this.getNotVisitedReasons();

    this.getPlanes();

  }

  // getAllClients(){
  //   this.database.openDatabase().then(
  //     () => {
  //       this.database.getAllClients()
  //       .then(clientes => {
  //         this.clientes_copia = clientes;      
  //         this.clientes = this.clientes_copia;
  //         //this.setFilteredItems();
  //       })
  //     });
    
  // }

  // getAsocClients(){
  //   this.database.openDatabase().then(
  //     () => {
  //       this.database.getAsocClients()
  //       .then(clientes => {
  //         this.clientes_copia = clientes;      
  //         this.clientes = this.clientes_copia;
  //         //this.setFilteredItems();
  //       })
  //     });
    
  // }


  getNotVisitedReasons(){
    this.database.openDatabase().then(
      () => {
        this.database.getNotVisitedReasons()
        .then(reasons => {
          this.motivos = reasons;
        })
      });
  }    

  getPlanes(){
    this.database.openDatabase().then(
      () => {
        this.database.getPlanes()
        .then(planes => {
          this.planes = planes;
          console.log('Planes: ' + planes.length);
          //this.setFilteredItems();
        })
      });

  }

  // BuscarCliente(){
    
  //   /*return this.clientes.filter((cliente) => {
  //     if (cliente.codigo.indexOf(data.value) >= 0) {
  //       return true;
  //     }
  //     if (cliente.nombre.indexOf(data.value) >= 0) {
  //       return true;
  //     }      
  //     return false;
  //   })*/

  //   return this.clientes.filter(item => item.codcli.includes(this.filtro) ||
  //       item.descli.includes(this.filtro)
  //   )

  // }

  /*setFilteredItems() {

      this.searching = true;
      this.clientes = this.filterItems(this.filtro);
 
  }*/ 

  // filterItems(ev: any){

  //     this.clientes = this.clientes_copia;

  //     let val = ev.target.value;

  //   // if the value is an empty string don't filter the items
  //     if (val && val.trim() != '') {
  //       this.clientes = this.clientes.filter((item) => {
  //         return (item.descli.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.codcli.includes(val));
  //       })
  //     }    
 
  // }

  goToCesta(cliente: any){
    this.diagnostic.isLocationEnabled().then(
      (setting) => {
          if(setting == true) {
            this.translate.get(["get_coord", "errors.not_in_peri_gps_orde"], null).subscribe(
              (localizedValues) => {
                let loading = this.loadingCtrl.create({
                  content: localizedValues["get_coord"]
                });
                loading.present();
                
                this.geolocation.getCurrentPosition({timeout: 4000}).then(
                  (position) => {
                    let geopcr = position.coords.latitude.toString() + "," + position.coords.longitude.toString();
                    this.database.getGpsobl()
                    .then((gpsobl) => {                      
                      console.log('gpsobl: ' + gpsobl);
                      /////////////gps temporary
                      gpsobl = 'false'
                      /////////////gps temporary
                      if(gpsobl == 'true') {
                        this.storage.get('gpsper')
                        .then((gpsper) => {
                          //latitud y longitud
                          let latcli = cliente.coogeo.split(",")[0];
                          let loncli = cliente.coogeo.split(",")[1];
                          console.log('geopcr: ' + geopcr);
                          console.log('latcli: ' + latcli + " loncli: " + loncli);
                          console.log('gpsper: ' + gpsper);
                          console.log('distance: ' + AppUtils.distance(position.coords.latitude, position.coords.longitude, latcli, loncli));
                          if(AppUtils.distance(position.coords.latitude, position.coords.longitude, latcli, loncli) <= gpsper) {
                            this.database.openDatabase().then(
                              () => {
                                this.database.createOrder(cliente, geopcr, true)
                                .then((data) => {                          
                                  this.navCtrl.push(CestaPage, {cliente:cliente});
                                  this.navCtrl.remove(this.view.index);
                                  this.navCtrl.insert(this.view.index, PedidoMainPage);
                                  this.updateVisitedStatus(cliente);
                                });
                              });
                            loading.dismiss();
                          } else {
                            loading.dismiss().then(
                              () => {
                                this.alertsServ.showAlertError(localizedValues["errors.not_in_peri_gps_orde"]);
                              }
                            );
                          }
                        });
                      } else {
                        this.database.openDatabase().then(
                          () => {
                            this.database.createOrder(cliente, geopcr, true)
                            .then((data) => {                          
                              this.navCtrl.push(CestaPage, {cliente:cliente});
                              this.navCtrl.remove(this.view.index);
                              this.navCtrl.insert(this.view.index, PedidoMainPage);
                              this.updateVisitedStatus(cliente);
                            });
                          });
                        loading.dismiss();
                      }
                    });
                  }).catch((error) => {
                    console.log('no geopcr: '+ error);
                    this.database.getGpsobl()
                    .then((gpsobl) => {
                      if(gpsobl == 'true') {
                        loading.dismiss().then(
                          () => {
                            this.alertsServ.showAlertError('No se pudo obtener sus coordenada GPS. Revise su red o configuración GPS');
                          }
                        );
                      } else {
                        this.database.openDatabase().then(
                          () => {
                            this.database.createOrder(cliente, '', true)
                            .then((data) => {                          
                              this.navCtrl.push(CestaPage, {cliente:cliente});
                              this.navCtrl.remove(this.view.index);
                              this.navCtrl.insert(this.view.index, PedidoMainPage);
                              this.updateVisitedStatus(cliente);
                            });
                          });
                        loading.dismiss();
                      }
                    });
                  });
              });
          } else {
            this.database.openDatabase().then(
              () => {
                this.database.getGpsobl()
                .then((gpsobl) => {
                  if(gpsobl == 'true') {
                    console.log('gpsobl: ' + true);
                    this.showMessageTurnOnGPS();
                  } else {
                    console.log('gpsobl: ' + false);
                    this.showConfirmTurnOnGPS(cliente);
                  }
                });
              });
            // this.showAlert();
          }
      },
      (error) => {
        console.log('Entro en error buscando gps setting');
      });

    // this.database.openDatabase().then(
    //   () => {
    //     this.database.createOrder(cliente)
    //     .then((data) => {
    //       this.navCtrl.push(CestaPage, {cliente:cliente});
    //       //this.navCtrl.remove(this.view.index);
        
    //     });
    //   });
  }
//response.rows.item(0).pednum
  checkClientAlreadyHasOrders(cliente: any){    
    this.database.openDatabase().then(
      () => {
        this.database.clientAlreadyHasOrders(cliente.codcli)
        .then((response) => {            
          
          if(response.rows.length > 0){
            
            this.openConfirmationForReviewOrCreate(cliente);

          }else{

            this.openConfirmationForCreate(cliente);

          }

        
        });
      });
  }

  showMessageTurnOnGPS() {
    this.translate.get(["gps_off", "gps_proc_acti", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["gps_proc_acti"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  showConfirmTurnOnGPS(cliente: any) {
    this.translate.get(["gps_off", "acti_gps", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["acti_gps"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.database.openDatabase().then(
                  () => {
                    this.database.createOrder(cliente, '', true)
                    .then((data) => {
                      this.navCtrl.push(CestaPage, {cliente:cliente});
                      this.navCtrl.remove(this.view.index);
                    });
                  });
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  openConfirmationForReviewOrCreate(cliente:any){

    this.translate.get(["common.confirm", "visits.create", "visits.review", "visits.already_orders"], null).subscribe(
      (localizedValues) => {
        
          let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["visits.already_orders"],
          buttons: [
            {
              text: localizedValues["visits.create"],
              handler: () =>{
                this.goToCesta(cliente);
                console.log('cancelar');
              }
            },
            {
              text: localizedValues["visits.review"],
              handler: ()=>{
                this.navCtrl.push(PedidoMainPage);
                //this.closeOrder();
              }
            }
          ]
        });
        alert.present();

      });


    
  }

  openConfirmationForCreate(cliente:any){

    this.translate.get(["common.confirm", "visits.create_order", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {
        
          let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["visits.create_order"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: ()=>{
              }
            },        
            {
              text: localizedValues["common.acept"],
              handler: () =>{
                this.goToCesta(cliente);
                console.log('aceptar');
              }
            }
            
          ]
        });
        alert.present();

      });
    
  }

  openNotVisitedConfirmation(cliente: any){

    
    this.translate.get(["visits.not_visited", "common.choose_reason", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["visits.not_visited"]);
        alert.setMessage(localizedValues["common.choose_reason"]);

        this.motivos.forEach((s) => {

            alert.addInput({
            type: 'radio',
            label: s.desmot,
            value: s.codmot,
            checked: false
          });

        });    

        alert.addButton(localizedValues["common.cancel"]);

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if(data !== ''){          
              
              this.updateNotVisitedReason(cliente, data);     
              

            }
            
          }
        });

        alert.present();
        
          
      });
  }

  updateNotVisitedReason(cliente: any, codmot: any){

    this.translate.get(["visits.marked_nv", "visits.error_nv"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
        () => {
          this.database.updateNotVisitedReason(cliente.codpdv, cliente.codcli, cliente.feccre, codmot, false)
          .then(() => {
            this.getPlanes();
            this.showMessage(localizedValues["visits.marked_nv"]);         
          })
          .catch(() => {
            this.showMessage(localizedValues["visits.error_nv"]);
          });
        });
        
          
      });    

  }

  showMessage(msg: any){

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({      
        message: msg,
        buttons: [
          {
            text: localizedValues["common.acept"],
            handler: () =>{
              console.log('aceptar');
            }
          }
        ]
      });
      alert.present();        
          
      });   

  }

  updateVisitedStatus(cliente: any){

    this.database.openDatabase().then(
      () => {
        this.database.updateNotVisitedReason(cliente.codpdv, cliente.codcli, cliente.feccre, "", true)
        .then(() => {          
          //this.showMessage("Cliente marcado como NO VISITADO");         
        })
        .catch(() => {
          //this.showMessage("Ocurrio un error al marcar cliente como NO VISITADO");
        });
      });

  }



}
