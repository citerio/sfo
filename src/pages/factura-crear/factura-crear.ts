import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ModalController, Platform, ViewController } from 'ionic-angular';
import { DbService } from "../../providers/db-service";
import { TranslateService } from "ng2-translate";
import { AgregarproductoPage } from "../agregarproducto/agregarproducto";
import { DetallarproductoPage } from "../detallarproducto/detallarproducto";
import { PedidoMainPage } from "../pedido-main/pedido-main";
import { DetallarproductoFacturaPage } from "../detallarproducto-factura/detallarproducto-factura";
import { Storage } from '@ionic/storage';

/*
  Generated class for the FacturaCrear page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-factura-crear',
  templateUrl: 'factura-crear.html'
})
export class FacturaCrearPage {

  codigo_cliente:string = '';
  nombre_cliente:string = '';
  cliente: any = {};
  productos_escogidos:any[] = [];
  suma_total:any = {'monto':0};
  importe_total:number = 0;
  numero_actual_pedido:string = '';
  numero_actual_factura_LN:string = '';
  deshabilitar_cerrar:boolean = false;
  invoice:string='';
  loader:any;
  order_cr_date='';
  printer_name = '';
  printed_copy = false;
  monped = 'false';
  motivos: any[] = [];
  serie_factura: any;
  nombre_usuario: any = '';

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams, public loadingCtrl: LoadingController, public database: DbService, private modalCtrl: ModalController, public platform: Platform, public view: ViewController, private translate: TranslateService, private storage: Storage) {

    this.platformReady();
    this.getMonped();

    if(this.navParams.data){
      

      // if(this.navParams.data.cliente.codcli){

      //   this.codigo_cliente = this.navParams.data.cliente.codcli;

      //   this.database.openDatabase().then(
      //   () => {
      //     this.database.getClientName(this.navParams.data.cliente.codcli)
      //     .then((nombre) => {this.nombre_cliente = nombre;})
      //   });

      //   this.numero_actual_pedido = this.navParams.data.cliente.pednum;
      //   //this.getOrder(this.numero_actual_pedido);  

      // }

      if(this.navParams.data.cliente.codcli){

        this.codigo_cliente = this.navParams.data.cliente.codcli;

        if(this.navParams.data.cliente.facnum){
          this.numero_actual_pedido = this.navParams.data.cliente.facnum;
          this.order_cr_date = this.navParams.data.cliente.fechcr;
        } else {
          this.numero_actual_pedido = this.database.getCurrentInvoiceNumber();
          this.database.getInvoiceCreationDate(this.numero_actual_pedido)
          .then((fecha) => {this.order_cr_date = fecha;});
        }

        this.database.getClientName(this.navParams.data.cliente.codcli)
          .then((nombre) => {
            this.cliente = nombre;
            this.nombre_cliente = this.cliente.descli
          });

        this.database.getInvoiceSeriesNum()
          .then((serie)=>{
            
            this.serie_factura = serie;

          });
        
        //this.getOrder(this.numero_actual_pedido);

      }           

    }

  }

  ionViewDidLoad() {
    this.getRemoveLineReasons();
  }

  getUserName() {
    this.storage.get('nombre')
      .then((name) => this.nombre_usuario = name)
      .catch((error) => alert("error on getting user name"))
  }

  ionViewWillEnter(){
    this.getInvoice(this.numero_actual_pedido);
  }

  getMonped(){
    this.database.openDatabase().then(
      () => {
        this.database.getMonped()
        .then(monped => {
          this.monped = monped;
        })
        .catch((error) => {
          alert('ocurrio un error: '+ JSON.stringify(error));
        });
      });
  }

  getInvoice(pednum: any){
    this.database.openDatabase().then(
        () => {
          this.database.getInvoice(pednum, 2, 1, this.codigo_cliente)
          .then(pedido => {      
            this.productos_escogidos = pedido;
            if(this.productos_escogidos.length == 0){
              this.deshabilitar_cerrar = true;
            }else{
              this.deshabilitar_cerrar = false;
            }
            this.orderTotalSum();
          })
          .catch((error) => {

            alert('ocurrio un error: '+ JSON.stringify(error));}

          )
        });
    
  }  

  getRemoveLineReasons(){
    this.database.openDatabase().then(
      () => {
        this.database.getRemoveLineReasons()
        .then(reasons => {
          this.motivos = reasons;
        })
      });
  } 

  openInventory(ev: any){

    this.navCtrl.push(AgregarproductoPage, {productos_escogidos:this.productos_escogidos, suma_total:this.suma_total, numero_actual_pedido:this.numero_actual_pedido, from:2});

    /*let modal = this.modalCtrl.create(AgregarproductoPage, {
      productos_escogidos: this.productos_escogidos,
      suma_total: this.suma_total,
      numero_actual_pedido: this.numero_actual_pedido
  
    });

    modal.present({
      ev: ev
    });*/

  }

  orderTotalSum(){

    this.importe_total = 0;
    this.productos_escogidos.forEach((s) => 

        {          
          this.importe_total = this.importe_total + s.pedprt;
        }
    );

  }

  editProduct(producto: any){

    this.navCtrl.push(DetallarproductoFacturaPage, {numero_actual_pedido:this.numero_actual_pedido, producto_elegido: producto, codigo_ordtra: this.numero_actual_pedido, codigo_cliente: this.codigo_cliente});

  }

  openDeleteConfirmation(producto: any){

    this.translate.get(["basket.delete_item", "common.choose_reason", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["basket.delete_item"]);
        alert.setMessage(localizedValues["common.choose_reason"]);

        this.motivos.forEach((s) => {

            alert.addInput({
            type: 'radio',
            label: s.desmot,
            value: s.codmot,
            checked: false
          });

        });    

        alert.addButton(localizedValues["common.cancel"]);

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if(data !== ''){          
              
              this.deleteProduct(producto, data);
              

            }
            
          }
        });

        alert.present();
      });

    // this.translate.get(["common.confirm", "basket.delete_confirm", "common.cancel", "common.acept"], null).subscribe(
    //   (localizedValues) => {

    //     let alert = this.alertCtrl.create({
    //       title: localizedValues["common.confirm"],
    //       message: localizedValues["basket.delete_confirm"],
    //       buttons: [
    //         {
    //           text: localizedValues["common.cancel"],
    //           handler: () =>{
    //             console.log('cancelar');
    //           }
    //         },
    //         {
    //           text: localizedValues["common.acept"],
    //           handler: ()=>{
    //             this.deleteProduct(producto);
    //           }
    //         }
    //       ]
    //     });
    //     alert.present();
    //   });
  }

  
  deleteProduct(producto: any, data: string){

    this.translate.get(["basket.success_deleted", "basket.failure_deleted"], null).subscribe(
      (localizedValues) => {
        this.database.openDatabase().then(
        () => {
          this.database.updateInvoiceLinePhaseStatus(producto.facnum, producto.pedlin, 5, 3).then(
            (data) => {
              this.database.updateRemoveInvoiceLineReason(producto.facnum, producto.pedlin, data).then(
                () => {
                  this.showMessage(localizedValues["basket.success_deleted"]);
                  this.getInvoice(this.numero_actual_pedido);
                })
                .catch(() => { this.showMessage(localizedValues["basket.failure_deleted"]); })
            })
          .catch((error) => { this.showMessage(localizedValues["basket.failure_deleted"]); });
        });
      });
  }

  closeOrder(){

    this.translate.get(["billing.success_closed", "billing.failure_closed"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
        () => {
          this.database.updateInvoicePhaseStatus(this.numero_actual_pedido, this.numero_actual_pedido, this.codigo_cliente, 3, 1)
          .then((data) => { 
            this.database.updateInvoiceLinesPhaseStatus(this.numero_actual_pedido, this.numero_actual_pedido, this.codigo_cliente, 3, 1)
            .then((data) => { 
              this.deshabilitar_cerrar = true;
              this.createInvoiceLN();        
            } )
            .catch((error) => { this.showMessage(localizedValues["billing.failure_closed"]); })           
          })
          .catch((error) => { this.showMessage(localizedValues["billing.failure_closed"]); });
        });
          
      });    

  }

  showMessage(msg: any){

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({      
        message: msg,
        buttons: [
          {
            text: localizedValues["common.acept"],
            handler: () =>{
              console.log('aceptar');
            }
          }
        ]
      });
      alert.present();        
          
      });   

  }

  showMessageforPrinting(msg: any){


    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

          let alert = this.alertCtrl.create({      
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () =>{
                this.openPrintingConfirmation();
              }
            }
          ]
        });
        alert.present();   
          
      });    

  }

  openPrintingConfirmation(){

    this.translate.get(["common.acept", "common.cancel", "common.confirm", "billing.print_confirm"], null).subscribe(
      (localizedValues) => {

            let alert = this.alertCtrl.create({
            title: localizedValues["common.confirm"],
            message: localizedValues["billing.print_confirm"],
            buttons: [
              {
                text: localizedValues["common.cancel"],
                handler: () =>{
                  this.navCtrl.pop();
                }
              },
              {
                text: localizedValues["common.acept"],
                handler: ()=>{
                  this.printOrderSelectPrinter();
                }
              }
            ]
          });
          alert.present();  
          
      });
    
  }

  openCloseConfirmation(){

    this.translate.get(["common.acept", "common.cancel", "common.confirm", "billing.close_confirm"], null).subscribe(
      (localizedValues) => {

            let alert = this.alertCtrl.create({
            title: localizedValues["common.confirm"],
            message: localizedValues["billing.close_confirm"],
            buttons: [
              {
                text: localizedValues["common.cancel"],
                handler: () =>{
                  console.log('cancelar');
                }
              },
              {
                text: localizedValues["common.acept"],
                handler: ()=>{
                  this.closeOrder();
                }
              }
            ]
          });
          alert.present();
          
      });
    
  }

  openPaymentConfirmation(){

    this.translate.get(["common.yes", "common.no", "common.cancel", "common.confirm", "billing.payment_select", "billing.success_closed"], null).subscribe(
      (localizedValues) => {

            let alert = this.alertCtrl.create({
            title: localizedValues["common.confirm"],
            message: localizedValues["billing.payment_select"],
            buttons: [
              /*{
                text: localizedValues["common.cancel"],
                handler: () =>{
                  console.log('cancelar');
                }
              },*/
              {
                text: localizedValues["common.yes"],
                handler: ()=>{
                  this.showMessageforPrinting(localizedValues["billing.success_closed"]);
                }
              },
              {
                text: localizedValues["common.no"],
                handler: ()=>{
                  this.createInvoiceToCharge();
                }
              }
            ]
          });
          alert.present();
          
      });
    
  }

  showMessageOnPrintingSuccessOrFailure(msg: any){

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

          let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () =>{
                this.navCtrl.pop();
              }
            }
          ]
        });
        alert.present();  
          
      });     
  }

  showMessageOnPrintingSuccess(){


    this.translate.get(["common.acept", "billing.success_printed"], null).subscribe(
      (localizedValues) => {

          console.log('Entro en mi metodo');
          let alert = this.alertCtrl.create({
            message: localizedValues["billing.success_printed"],
            buttons: [
              {
                text: localizedValues["common.acept"],
                handler: () => {
                  if(this.printed_copy === true){
                    this.navCtrl.pop();
                  } else {
                    this.database.openDatabase().then(
                    () => {
                      this.database.getPrncpy().then(
                        (print_copy) => {
                          if(print_copy == 'true'){
                            console.log('prncpy: ' + true);
                            this.showMessagePrintCopy();
                          } else {
                            console.log('prncpy: ' + false);
                            this.navCtrl.pop();
                          }
                        },
                        (error) => {
                        });
                    });
                  }
                }
              }
            ]
          });
          alert.present();
          
      });


    
  }

  showMessagePrintCopy(){


    this.translate.get(["common.acept", "common.cancel", "common.confirm", "basket.print_copy_confirm"], null).subscribe(
      (localizedValues) => {

          this.printed_copy = true;
          let alert = this.alertCtrl.create({
            title: localizedValues["common.confirm"],
            message: localizedValues["basket.print_copy_confirm"],
            buttons: [
              {
                text: localizedValues["common.cancel"],
                handler: () => {
                  this.navCtrl.pop();
                }
              },
              {
                text: localizedValues["common.acept"],
                handler: () => {
                  this.presentLoading(this.printer_name);
                }
              }
            ]
          });
          alert.present();
          
      });
    
  }

  /////////////////////////////////////printing operations//////////////////////////////////////////////////////////
  printOrderSelectPrinter(){

      let context = this;

      this.translate.get(["basket.error_printers_not_found"], null).subscribe(
      (localizedValues) => {

          (<any>window).BTPrinter.list(function(data){
          //alert("Success Listing");
          context.showPrintersList(data);
          //alert(data); //list of printer in data array
          },function(err){
              //alert("Error Listing");
              context.showMessage(localizedValues["basket.error_printers_not_found"]);
          });     
          
      });

      //this.testing(); 
      //alert('imgData ' + this.imgData);

       

  }

  printOrderPrinting(printer_name:any){

    let context = this;

    this.translate.get(["basket.error_printer_disconnection", "basket.error_printing_document", "basket.error_printer_connection"], null).subscribe(
      (localizedValues) => {

         (<any>window).BTPrinter.connect(function(data){
            //alert("Success Connected");
            //alert(data);
            context.invoice = context.setInvoice();
            //this.printing();
            (<any>window).BTPrinter.printText(function(data){
                //alert("Success printing");
                context.loader.dismiss();
                context.PrintInvoice();

                (<any>window).BTPrinter.disconnect(function(data){
                    //alert("Success disconnected");
                    //alert(data)
                    },function(err){
                      context.showMessageOnPrintingSuccessOrFailure(localizedValues["basket.error_printer_disconnection"]);                  
                      //alert(err)
                    }, printer_name);        
                },function(err){
                    context.loader.dismiss();
                    context.showMessageOnPrintingSuccessOrFailure(localizedValues["basket.error_printing_document"]);                   
            }, context.invoice);
          },function(err){
            //alert("Error");
            context.loader.dismiss();
            context.showMessageOnPrintingSuccessOrFailure(localizedValues["basket.error_printer_connection"]);        
            //alert(err)
          }, printer_name);  
          
      });    

  }

  syncOrder(){

  }

  showPrintersList(printers:any[]){


    this.translate.get(["common.cancel", "common.acept", "basket.choose_bluetooth_printer"], null).subscribe(
      (localizedValues) => {

          let alert = this.alertCtrl.create();
          alert.setTitle(localizedValues["basket.choose_bluetooth_printer"]);

          printers.forEach((s) => {

              alert.addInput({
              type: 'radio',
              label: s,
              value: s,
              checked: false
            });

          });    
          
          alert.addButton({
            text: localizedValues["common.cancel"],
            handler: data => {
              
              this.navCtrl.pop();
              
            }
          });

          alert.addButton({
            text: localizedValues["common.acept"],
            handler: data => {
              if(data !== ''){
                
                this.printer_name = data;
                this.presentLoading(data);          
                

              }
              
            }
          });

          alert.present();    
          
    });   


  }  

  setInvoice(){


    let header = 'Av. Eugenio Mendoza Edif. Protinal, frente al stadium Jose\r\nBernardo Perez Valencia - Estado Carabobo Telefonos(0241)6138423\r\n';
            
    let order_number = 'Serie ' + this.serie_factura + '   Factura ' + this.numero_actual_pedido + '     Fecha: ' + this.order_cr_date + '\r\n';

    let client_info = 'Cliente: ' + this.cliente.descli.toUpperCase() + '\r\n' + 'RIF: ' + this.cliente.facrif.toUpperCase() + '\r\n' + 'Direccion: ' + this.cliente.facdir.toUpperCase() + '\r\n';

    let seller_info = 'Vendedor: ' + '\r\n' + 'Preventista: ' + this.nombre_usuario + '\r\n' + 'Condicion de pago: Contado' + '      Vence: ' + this.order_cr_date + '\r\n\n';

    let order_table_header = 'Nombre Producto      Cant   Und   Prec.Unit    Monto';

    //let order_table_body:string;

    let invoice = header + order_number + client_info + seller_info + order_table_header;



    this.productos_escogidos.forEach((s) => {
      invoice += "\r\n";
      let space: number = 0;
      //article description
      let descar = s.descar.length;
      if (descar >= 23) {
        invoice += s.descar.substring(0, 23).toUpperCase();
        invoice += " ";
      } else {
        invoice += s.descar.toUpperCase();
        space = 24 - descar;
        for (let i = 1; i <= space; i++) {
          invoice += " ";
        }
      }

      //article pedcan      
      let pedcan = s.pedcan.toString().length;
      space = 8 - pedcan;
      for (let i = 1; i <= space; i++) {
        invoice += " ";
      }
      invoice += s.pedcan;
      invoice += "    ";

      //article coduni    
      invoice += s.coduni;  
      let coduni = s.coduni.toString().length;
      space = 6 - coduni;
      for (let i = 1; i <= space; i++) {
        invoice += " ";
      }      

      //article preuprnt
      let preuprnt = 0;
      if (this.monped == 'true') {
        preuprnt = s.pedpru;
      }

      space = 10 - preuprnt.toString().length;

      for (let i = 1; i <= space; i++) {
        invoice += " ";
      }

      invoice += preuprnt;
      invoice += "    ";


      //article pretprnt
      let pretprnt = 0;
      if (this.monped == 'true') {
        pretprnt = s.pedprt;
      }

      space = 5 - pretprnt.toString().length;
      // nesp = 10 - s.pedprt.toString().length;

      for (let i = 1; i <= space; i++) {
        invoice += " ";
      }

      invoice += pretprnt;

    });

    let subtotal_order = '\r\n' + '                    Sub total:' + 0.00;

    let total_order = '\t\t\t\t\t\t' + this.importe_total + '\n';

    let no_tax = '\r\nMon. Exento:      ' + 0.00;

    let taxable = '\r\nBASE IMPONIBLE:      ' + 0.00 + ' IVA ' + 0.00;

    let total = '\r\n                    Total:      ' + 0.00;

    let footer_1 = '\r\n\n\nNO SE ACEPTAN DEVOLUCIONES\r\n * Este comprobante va sin enmiendas\r\n PRODUCTO ESPECIAL A SOLICITUD DEL CLIENTE\r\n Firma del Cliente o...............................\r\n RBF: AGRO2-000003942-8 \r\n Cestas que contienen el producto vendido son de Proagro C.A. \n\n\n'

    let footer_2 = '\r\nSaldo Inicial    Reintegrado    Consignado    Saldo Final';

    let footer_3 = '\r\n0.0    0.0    0.0    0.0';

    //let invoice = header + order_number + client_info + seller_info + order_table_header + order_table_body + subtotal_order + total_order + footer;

    // let invoice = table;

    invoice += subtotal_order + total_order + no_tax + taxable + total + footer_1 + footer_2 + footer_3;
    return invoice;

  }

  //TODO remover el error del catch, es para uso interno de la app solo
  PrintInvoice(){

    this.translate.get(["billing.error_printing_invoice"], null).subscribe(
      (localizedValues) => {

         this.database.openDatabase().then(
          () => {
            this.database.updateInvoiceLNPhaseStatus(this.numero_actual_factura_LN, 6, 1)
            .then((data) => { 
              this.database.updateInvoiceLinesLNPhaseStatus(this.numero_actual_factura_LN, 6, 1)
              .then((data) => { 
                this.showMessageOnPrintingSuccess();                
              } )
              .catch((error) => { this.showMessage(localizedValues["billing.error_printing_invoice"]); })           
            })
            .catch((error) => { this.showMessage(localizedValues["billing.error_printing_invoice"]); });
          }); 
          
      });    
    

  }

  presentLoading(data) {

    this.translate.get(["common.wait"], null).subscribe(
      (localizedValues) => {

          this.loader = this.loadingCtrl.create({
            content: localizedValues["common.wait"],
            spinner: "bubbles"
          });

          this.loader.present()
          .then(() => {this.printOrderPrinting(data);});
          
      });     
    
  }

  createInvoiceLN(){

    this.translate.get(["billing.success_closed", "billing.failure_closed"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
          () => {
            this.database.createInvoiceLN(this.navParams.data.cliente)
            .then((data) => {
              this.numero_actual_factura_LN = this.database.getCurrentLNInvoiceNumber();
              this.database.createInvoiceLinesLN(this.numero_actual_factura_LN, this.database.getCurrentLNInvoiceControlNumber(), this.productos_escogidos)
                  .then((data) => {
                    this.openPaymentConfirmation();                    
                  } )
                  .catch((error) => { this.showMessage(localizedValues["billing.failure_closed"]); })
            })
            .catch((error) => { this.showMessage(localizedValues["billing.failure_closed"]); });
          });       
          
      });    

  }

  createInvoiceToCharge(){

    this.translate.get(["billing.success_closed", "billing.failure_closed"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
          () => {
            this.database.getInvoiceLNToCharge(this.numero_actual_factura_LN)
            .then((invoice_LN) => {
              invoice_LN.diasve = 0;
              invoice_LN.salfac =  this.importe_total;          
              this.database.createInvoiceToCharge(invoice_LN)
                  .then((data) => {
                    this.showMessageforPrinting(localizedValues["billing.success_closed"]);                    
                  } )
                  .catch((error) => { this.showMessage(localizedValues["billing.failure_closed"]); })
            })
            .catch((error) => { this.showMessage(localizedValues["billing.failure_closed"]); });
        });             
          
      });       

  }

  platformReady() {
    // Call any initial plugins when ready
    this.platform.ready().then(() => {   

     /*this.platform.registerBackButtonAction(() => {
       
       this.navCtrl.push(PedidoMainPage);
       //this.navCtrl.remove(this.view.index);
          
      });*/
    });
  }

}
