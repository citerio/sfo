import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { PedidoPage } from '../pedido/pedido';
import { CestaPage } from '../cesta/cesta';
import { CestaClosedPage } from '../cesta-closed/cesta-closed';
import { CestaSyncedPrintedPage } from "../cesta-synced-printed/cesta-synced-printed";

import { DbChangesService } from '../../providers/db-changes-service';
import { TranslateService } from "ng2-translate";
/*
  Generated class for the PedidoMain page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-pedido-main',
  templateUrl: 'pedido-main.html'
})
export class PedidoMainPage {

  open_pedidos:any[] = [];
  created_pedidos:any[] = [];
  closed_pedidos:any[] = [];
  synced_pedidos:any[] = [];
  printed_pedidos:any[] = [];
  fase:string = 'open';
  server_connection_watcher:any;
  motivos: any[] = [];

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public database: DbService,
    public dbChangesService: DbChangesService,
    private translate: TranslateService
    ) {
    this.dbChangesService.getSync().subscribe(
      (res: boolean) => {
        console.log('exito recibiendo dato: '+ res);
        if(res === true) {
          console.log('exito recibiendo dato: '+ res);
          this.getSyncedOrders();
          this.getPrintedOrders();
        }
      },
      (error: any) => {
        console.log('error recibiendo dato: '+ error);
      });
    
  }

  ionViewDidLoad() {
    this.getRemoveOrderReasons();

  }

  getOpenOrders(){
    this.database.openDatabase().then(
      () => {
        this.database.getOrders(1, 1)
        .then(created_pedidos => {
          this.created_pedidos = created_pedidos;
          //this.setFilteredItems();
        })
      });

      this.database.openDatabase().then(
      () => {
        this.database.getOrders(2, 1)
        .then(processing_pedidos => {
          this.open_pedidos = this.created_pedidos.concat(processing_pedidos);
          //this.setFilteredItems();
        })
      });
    
  }

  getClosedOrders(){
    this.database.openDatabase().then(
      () => {
        this.database.getOrders(3, 1)
        .then(closed_pedidos => {
          this.closed_pedidos = closed_pedidos;
          //this.setFilteredItems();
        })
      });
  }

  getSyncedOrders(){
    this.database.openDatabase().then(
      () => {
        this.database.getOrders(4, 1)
        .then(synced_pedidos => {
          this.synced_pedidos = synced_pedidos;
          //this.setFilteredItems();
        })
      });
    
  }

  getPrintedOrders(){
    this.database.openDatabase().then(
      () => {
        this.database.getOrders(6, 1)
        .then(printed_pedidos => {
          this.printed_pedidos = printed_pedidos;
          //this.setFilteredItems();
        })
      });
    
  }

  /*ionViewDidLoad() {
    this.getOpenOrders();
  }*/

  ionViewWillEnter(){
    this.getOpenOrders();
    this.getClosedOrders();
    this.getSyncedOrders();
    this.getPrintedOrders();
  }

  goToCesta(cliente: any){

    this.navCtrl.push(CestaPage, {cliente:cliente});

  }

  goToCestaClosed(cliente: any){
    this.navCtrl.push(CestaClosedPage, {cliente:cliente});

  }

  goToCestaSynced(cliente: any){

    this.navCtrl.push(CestaSyncedPrintedPage, {cliente:cliente, synced_or_printed:4});

  }

  goToCestaPrinted(cliente: any){

    this.navCtrl.push(CestaSyncedPrintedPage, {cliente:cliente, synced_or_printed:6});

  }

  goToClientsList(){

    this.navCtrl.push(PedidoPage);

  }

  openRemoveOrderConfirmation(pedido: any){
    this.translate.get(["main_order.delete_order", "common.choose_reason", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["main_order.delete_order"]);
        alert.setMessage(localizedValues["common.choose_reason"]);

        this.motivos.forEach((s) => {
            console.log('s.codmot: ' + s.codmot);
            alert.addInput({
            type: 'radio',
            label: s.desmot,
            value: s.codmot,
            checked: false
          });

        });    

        alert.addButton(localizedValues["common.cancel"]);

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if(data !== ''){          
              console.log('data: ' + data)
              this.deleteProduct(pedido, data);
              

            }
            
          }
        });

        alert.present();
      });
  }

  getRemoveOrderReasons(){
    this.database.openDatabase().then(
      () => {
        this.database.getRemoveOrderReasons()
        .then(reasons => {
          this.motivos = reasons;
        })
      });
  } 
  
  deleteProduct(pedido: any, codmot: string){

    this.translate.get(["main_order.success_deleted", "main_order.failure_deleted"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
        () => {
          this.database.updateOrderPhaseStatus(pedido.pednum, 5, 3)
          .then((data) => {
            this.database.updateOrderLinesPhaseStatus(pedido.pednum, 5, 3)
            .then(() => {
              this.database.updateRemoveOrderReason(pedido.pednum, codmot).then(
                () => {
                  this.database.updateRemoveLineReasonFromAnOrder(pedido.pednum, codmot).then(
                    () => {
                      this.showMessage(localizedValues["main_order.success_deleted"]);

                      switch(pedido.pedfas) {
                        case 1:
                        case 2:
                          this.getOpenOrders();
                          break;
                        case 3:
                          this.getClosedOrders();
                          break;
                        case 4:
                          this.getSyncedOrders();
                          break;
                        case 6:
                          this.getPrintedOrders();
                          break;
                      }
                    }).catch((error) => { this.showMessage(localizedValues["main_order.failure_deleted"]); });
                }).catch((error) => { this.showMessage(localizedValues["main_order.failure_deleted"]); })
            })
            .catch((error) => { this.showMessage(localizedValues["main_order.failure_deleted"]); })
          })
          .catch((error) => { this.showMessage(localizedValues["main_order.failure_deleted"]); });
        });
          
          
      });    
    

  }

  showMessage(msg: any){

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({      
        message: msg,
        buttons: [
          {
            text: localizedValues["common.acept"],
            handler: () =>{
              console.log('aceptar');
            }
          }
        ]
      });
      alert.present();        
          
      });   

  } 




}
