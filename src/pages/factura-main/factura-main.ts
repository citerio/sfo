import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { Diagnostic } from "@ionic-native/diagnostic";
import { TranslateService } from 'ng2-translate';
import { AlertsService } from '../../providers/alerts.service';
import { FacturaPage } from '../factura/factura';
import { Geolocation } from '@ionic-native/geolocation';
import { AppUtils } from '../../providers/app-utils';
import { FacturaCrearPage } from '../factura-crear/factura-crear';
import { Storage } from '@ionic/storage';
import { DbChangesService } from '../../providers/db-changes-service';
import { FacturaClosedPage } from '../factura-closed/factura-closed';
import { FacturaSyncedPrintedPage } from '../factura-synced-printed/factura-synced-printed';

/*
  Generated class for the FacturaMain page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-factura-main',
  templateUrl: 'factura-main.html'
})
export class FacturaMainPage {

  open_facturas:any[] = [];
  closed_facturas:any[] = []
  synced_facturas:any[] = [];
  printed_not_sync_facturas:any[] = [];
  printed_facturas:any[] = [];
  fase:string = 'open';
  server_connection_watcher:any;
  motivos: any[] = [];
  cliente:any;  
  

  constructor(
    public navCtrl: NavController,
    public view: ViewController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public database: DbService,
    private diagnostic: Diagnostic,
    private translate: TranslateService,    
    public loadingCtrl: LoadingController,
    public geolocation: Geolocation,
    public alertsServ: AlertsService,
    public dbChangesService: DbChangesService,
    private storage: Storage) {

      if(this.navParams.data){

        /*if(this.navParams.data.facturas){      
  
          this.getInvoices(this.navParams.data.facturas);
          //this.getOrder(this.numero_actual_pedido);  
  
        }*/
  
        if(this.navParams.data.cliente){      
  
          this.cliente = this.navParams.data.cliente;
          //this.getOrder(this.numero_actual_pedido);  
  
        }         
  
      }

      this.dbChangesService.getSync().subscribe(
        (res: boolean) => {
          console.log('exito recibiendo dato en factura main: '+ res);
          if(res === true) {
            console.log('exito recibiendo dato en factura main: '+ res);
            this.getSyncedInvoices();
            this.getPrintedInvoices();
          }
        },
        (error: any) => {
          console.log('error recibiendo dato en factura main: '+ error);
        });

        this.getRemoveOrderReasons();

    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FacturaMainPage');
  }

  ionViewWillEnter(){
    this.getOpenInvoices();
    this.getClosedInvoices();
    this.getSyncedInvoices();
    this.getPrintedInvoices();
  }

  getOpenInvoices(){

    this.database.openDatabase().then(
      () => {
        this.database.getInvoices(this.cliente.codcli, 2, 1)
        .then(open_facturas => {
          this.open_facturas = open_facturas;
          //this.setFilteredItems();
        })
      });

  }

  getClosedInvoices(){

    this.database.openDatabase().then(
      () => {
        this.database.getInvoices(this.cliente.codcli, 3, 1)
        .then(closed_facturas => {
          this.closed_facturas = closed_facturas;
          //this.setFilteredItems();
        })
      });

  }

  getPrintedInvoices(){

    this.database.openDatabase().then(
      () => {
        this.database.getInvoicesLN(this.cliente.codcli, 6, 1)
        .then(printed_not_sync_facturas => {
          this.printed_not_sync_facturas = printed_not_sync_facturas;
          //this.setFilteredItems();
        })
      });

      this.database.openDatabase().then(
        () => {
          this.database.getInvoicesLN(this.cliente.codcli, 9, 1)
          .then(printed_sync_facturas => {
            this.printed_facturas = this.printed_not_sync_facturas.concat(printed_sync_facturas);
            //this.setFilteredItems();
          })
        });

  }

  getSyncedInvoices(){

    this.database.openDatabase().then(
      () => {
        this.database.getInvoicesLN(this.cliente.codcli, 4, 1)
        .then(synced_facturas => {
          this.synced_facturas = synced_facturas;
          //this.setFilteredItems();
        })
      });

  }

  goToFactura(factura: any){

    this.navCtrl.push(FacturaPage, {cliente:factura});  

  }

  goToFacturaCrear(){
    this.diagnostic.isLocationEnabled().then(
      (setting) => {
          if(setting == true) {
            this.translate.get(["get_coord", "errors.not_in_peri_gps_invo"], null).subscribe(
              (localizedValues) => {
                let loading = this.loadingCtrl.create({
                  content: localizedValues["get_coord"]
                });
                loading.present();
                
                this.geolocation.getCurrentPosition({timeout: 4000}).then(
                  (position) => {
                    let geopcr = position.coords.latitude.toString() + "," + position.coords.longitude.toString();
                    this.database.getGpsobl()
                    .then((gpsobl) => {
                      console.log('gpsobl: ' + gpsobl);
                      /////////////gps temporary
                      gpsobl = 'false'
                      /////////////gps temporary
                      if(gpsobl == 'true') {
                        this.storage.get('gpsper')
                        .then((gpsper) => {
                          let latcli = this.cliente.coogeo.split(",")[0];
                          let loncli = this.cliente.coogeo.split(",")[1];
                          console.log('geopcr: ' + geopcr);
                          console.log('latcli: ' + latcli + " loncli: " + loncli);
                          console.log('gpsper: ' + gpsper);
                          console.log('distance: ' + AppUtils.distance(position.coords.latitude, position.coords.longitude, latcli, loncli));
                          if(AppUtils.distance(position.coords.latitude, position.coords.longitude, latcli, loncli) <= gpsper) {
                            console.log('geopcr: ' + geopcr);
                            this.database.openDatabase().then(
                              () => {
                                this.database.createInvoice(this.cliente, geopcr, false)
                                .then((data) => {
                                  this.navCtrl.push(FacturaCrearPage, {cliente:this.cliente});                          
                                });
                              });
                            loading.dismiss();
                          } else {
                            loading.dismiss().then(
                              () => {
                                this.alertsServ.showAlertError(localizedValues["errors.not_in_peri_gps_invo"]);
                              }
                            );
                          }
                        });
                      } else {
                        console.log('geopcr: ' + geopcr);
                        this.database.openDatabase().then(
                          () => {
                            this.database.createInvoice(this.cliente, geopcr, false)
                            .then((data) => {
                              this.navCtrl.push(FacturaCrearPage, {cliente:this.cliente});                          
                            });
                          });
                        loading.dismiss();
                      }
                    });
                  }).catch((error) => {
                    console.log('no geopcr: '+ error);
                    this.database.getGpsobl()
                    .then((gpsobl) => {
                      if(gpsobl == 'true') {
                        loading.dismiss().then(
                          () => {
                            this.alertsServ.showAlertError('No se pudo obtener sus coordenada GPS. Revise su red o configuración GPS');
                          }
                        );
                      } else {
                        this.database.openDatabase().then(
                          () => {
                            this.database.createInvoice(this.cliente, '', false)
                            .then((data) => {
                              this.navCtrl.push(FacturaCrearPage, {cliente:this.cliente});                          
                            });
                          });
                        loading.dismiss();
                      }
                    });
                  });
              });
          } else {
            this.database.openDatabase().then(
              () => {
                this.database.getGpsobl()
                .then((gpsobl) => {
                  if(gpsobl == 'true') {
                    console.log('gpsobl: ' + true);
                    this.showMessageTurnOnGPS();
                  } else {
                    console.log('gpsobl: ' + false);
                    this.showConfirmTurnOnGPS(this.cliente);
                  }
                });
              });
            // this.showAlert();
          }
      },
      (error) => {
        console.log('Entro en error buscando gps setting');
      });

    
  }

  goToFacturaClosed(cliente: any){
    this.navCtrl.push(FacturaClosedPage, {cliente:cliente});

  }

  goToFacturaSynced(cliente: any){

    this.navCtrl.push(FacturaSyncedPrintedPage, {cliente:cliente, synced_or_printed:4});

  }

  goToFacturaPrinted(cliente: any){

    this.navCtrl.push(FacturaSyncedPrintedPage, {cliente:cliente, synced_or_printed:6});

  }

  showMessageTurnOnGPS() {
    this.translate.get(["gps_off", "gps_proc_acti", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["gps_proc_acti"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  showConfirmTurnOnGPS(cliente: any) {
    this.translate.get(["gps_off", "acti_gps", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["acti_gps"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.database.openDatabase().then(
                  () => {
                    this.database.createInvoice(cliente, '', false)
                    .then((data) => {
                      this.navCtrl.push(FacturaCrearPage, {cliente:cliente});                      
                    });
                  });
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  showAlert() {
    this.translate.get(["gps_off", "gps_proc_acti", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["gps_proc_acti"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  ///////////////////////////////////eliminacion factura///////////////////////////////////
  openRemoveOrderConfirmation(pedido: any){
    this.translate.get(["billing.delete_invoice", "common.choose_reason", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["billing.delete_invoice"]);
        alert.setMessage(localizedValues["common.choose_reason"]);

        this.motivos.forEach((s) => {
            console.log('s.codmot: ' + s.codmot);
            alert.addInput({
            type: 'radio',
            label: s.desmot,
            value: s.codmot,
            checked: false
          });

        });    

        alert.addButton(localizedValues["common.cancel"]);

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if(data !== ''){          
              console.log('data: ' + data)
              this.deleteProduct(pedido, data);
              

            }
            
          }
        });

        alert.present();
      });
  }

  getRemoveOrderReasons(){
    this.database.openDatabase().then(
      () => {
        this.database.getRemoveOrderReasons()
        .then(reasons => {
          this.motivos = reasons;
        })
      });
  } 
  
  deleteProduct(pedido: any, codmot: string){

    this.translate.get(["billing.success_deleted", "billing.failure_deleted"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
        () => {
          this.database.updateInvoicePhaseStatus(pedido.orno, pedido.ordtra, pedido.codcli, 5, 3)
          .then((data) => {
            this.database.updateInvoiceLinesPhaseStatus(pedido.orno, pedido.ordtra, pedido.codcli, 5, 3)
            .then(() => {
              this.showMessage(localizedValues["billing.success_deleted"]);              
              switch(pedido.pedfas) {
                case 1:
                case 2:
                  this.getOpenInvoices();
                  break;
                case 3:
                  this.getClosedInvoices();
                  break;
                case 4:
                  this.getSyncedInvoices();
                  break;
                case 6:
                  this.getPrintedInvoices();
                  break;
              }
            })
            .catch((error) => { this.showMessage(localizedValues["billing.failure_deleted"]); })
          })
          .catch((error) => { this.showMessage(localizedValues["billing.failure_deleted"]); });
        });
          
          
      });    
    

  }

  showMessage(msg: any){

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({      
        message: msg,
        buttons: [
          {
            text: localizedValues["common.acept"],
            handler: () =>{
              console.log('aceptar');
            }
          }
        ]
      });
      alert.present();        
          
      });   

  } 


  openCancelConfirmation(factura: any){
    this.translate.get(["common.confirm", "billing.cancel_confirm", "common.choose_reason", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["common.confirm"]);
        alert.setMessage(localizedValues["billing.cancel_confirm"]);        

        this.motivos.forEach((s) => {
          console.log('s.codmot: ' + s.codmot);
          alert.addInput({
          type: 'radio',
          label: s.desmot,
          value: s.codmot,
          checked: false
        });

      });    

        alert.addButton(localizedValues["common.cancel"]);

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if(data !== ''){          
              console.log('data: ' + data)
              this.cancelInvoice(factura, data);
              

            }
            
          }
        });

        alert.present();
      });
  }

  //8 anular  
  cancelInvoice(invoice: any, data: string){

    this.translate.get(["billing.success_cancelled", "billing.failure_cancelled"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
        () => {
          this.database.updateInvoiceLNPhaseStatus(invoice.nfact, 8, 1)
          .then((data) => {
            this.database.updateInvoiceLinesLNPhaseStatus(invoice.numcon, 8, 1)
            .then(() => {
              this.showMessage(localizedValues["billing.success_cancelled"]);              
              switch(invoice.facfas) {
                case 1:
                case 2:
                  this.getOpenInvoices();
                  break;
                case 3:
                  this.getClosedInvoices();
                  break;
                case 4:
                  this.getSyncedInvoices();
                  break;
                case 6:
                  this.getPrintedInvoices();
                  break;
              }
            })
            .catch((error) => { this.showMessage(localizedValues["billing.failure_cancelled"]); })
          })
          .catch((error) => { this.showMessage(localizedValues["billing.failure_cancelled"]); });
        });
          
          
      });  
    
  }


  openSyncConfirmation(factura: any){
    this.translate.get(["common.confirm", "billing.sync_confirm", "common.choose_reason", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["common.confirm"]);
        alert.setMessage(localizedValues["billing.sync_confirm"]); 

        alert.addButton(localizedValues["common.cancel"]);

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if(data !== ''){          
              console.log('data: ' + data)
              this.syncInvoice(factura, data);
              

            }
            
          }
        });

        alert.present();
      });
  }

  //9 ready to sync  
  syncInvoice(invoice: any, data: string){

    this.translate.get(["billing.success_sync", "billing.failure_sync"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
        () => {
          this.database.updateInvoiceLNPhaseStatus(invoice.nfact, 9, 1)
          .then((data) => {
            this.database.updateInvoiceLinesLNPhaseStatus(invoice.numcon, 9, 1)
            .then(() => {
              this.showMessage(localizedValues["billing.success_sync"]);              
              switch(invoice.facfas) {
                case 1:
                case 2:
                  this.getOpenInvoices();
                  break;
                case 3:
                  this.getClosedInvoices();
                  break;
                case 4:
                  this.getSyncedInvoices();
                  break;
                case 6:
                  this.getPrintedInvoices();
                  break;
              }
            })
            .catch((error) => { this.showMessage(localizedValues["billing.failure_sync"]); })
          })
          .catch((error) => { this.showMessage(localizedValues["billing.failure_sync"]); });
        });
          
          
      });  
    
  }

}
