import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { TranslateService } from 'ng2-translate';
import { AlertsService } from '../../providers/alerts.service';
import { CobranzaProcesarPage } from '../cobranza-procesar/cobranza-procesar';
import { AppUtils } from '../../providers/app-utils';

/*
  Generated class for the ComprobanteRetencion page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-comprobante-retencion',
  templateUrl: 'comprobante-retencion.html'
})
export class ComprobanteRetencionPage {

  invoices:any[] = [];
  comprobantes:any[] = [];  
  charge:any={};
  selected_bank:string = '';
  retained_amount:any;
  retentions_already_charged:any[]=[];
  indexes:any[]=[];
  year_month: any;
  masks: any;
  retention_comp:any={};
  compnum:any;
  montcom:number = 0;
  montcom_ui:number = 0;
  invoicesxcharge: any[] = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public database: DbService,
    public view: ViewController,
    private translate: TranslateService,
    private alertServ: AlertsService) {

      if(this.navParams.data.invoices){

        this.invoices = this.navParams.data.invoices;
        //this.calculateRetentionAmount();
  
      }
  
      if(this.navParams.data.charge){
  
        this.charge = this.navParams.data.charge;
        this.getInvoicesXChargeUI(this.charge.cobnum); 
      }
      
      if(this.navParams.data.indexes){
  
        this.indexes = this.navParams.data.indexes;
        this.indexes.push(this.view.index);             
  
      }

      let date: Date = new Date();

      this.year_month = date.getFullYear().toString() + (("0" + (date.getMonth() + 1)).slice(-2));

      this.setUpMask();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComprobanteRetencionPage');    

  }

  getInvoicesXChargeUI(cobnum: any) {
    this.database.getChargeInvoicesAllFields(cobnum).then(
      (invoices) => {
        this.invoicesxcharge = invoices;
        this.invoicesxcharge.forEach((i)=>{
          this.montcom = this.montcom + i.monret;
        })
        this.montcom_ui = Number(this.montcom.toFixed(2));
        this.getRetentionsAlreadyCharged();
      },
      error => {
        console.log('Error obteniendo las facturas ya asociadas a cobranza');

      });
  }

  createComprobante() {

    //let comprobante = {'compnum' : '', 'cobnum' : this.charge.cobnum, 'montcomp' : this.charge.monret};
    //this.retention_comp = comprobante; 
    this.compnum = '';
    //this.montcom = this.charge.monret.toFixed(2);

  }

  confirmCharge(){
    this.database.deleteChargeRetentions(this.charge.cobnum).then(
      (res) => {
        //this.alertServ.showAlert('La cobranza fue almacenada con exito');
        this.retention_comp = {};
        this.retention_comp.compnum = this.compnum;
        this.retention_comp.montcom = this.montcom;
        this.retention_comp.cobnum = this.charge.cobnum;
        this.database.createChargeRetentions(this.charge.cobnum, this.retention_comp).then(
          (res) => {
            //this.alertServ.showAlert('La cobranza fue almacenada con exito');
            this.charge.monret = this.montcom;
            this.navCtrl.push(CobranzaProcesarPage, {invoices:this.invoices, charge:this.charge, indexes:this.indexes}); 
          },
          (error) => {
            this.alertServ.showAlertError('Las comprobantes de la cobranza no fueron insertadas ');
        }); 
      },
      (error) => {
        this.alertServ.showAlertError('Las comprobantes de la cobranza no fueron insertadas ');
    });        
  }

  goBack() {
    this.navCtrl.pop();
  }

  getRetentionsAlreadyCharged() {
    this.database.getChargeRetentions(this.charge.cobnum, 1, 1).then(
      (retenciones) => {
        this.retention_comp = retenciones;
        if(typeof this.retention_comp != 'undefined'){
          this.compnum = this.retention_comp.compnum;
          //this.montcom = this.charge.monret.toFixed(2);          
        }else {
          this.createComprobante();
        }             
      },
      error => {
        alert('Error obteniendo las retenciones ya asociadas a cobranza');
      });
  }

  deleteRetention(comprobante: any){
    let index = this.comprobantes.indexOf(comprobante);
    if(index > -1){
      this.comprobantes.splice(index, 1);
    }
  }

  /*calculateRetentionAmount(){

    this.invoices.forEach(invoice => {

      //bi base imponible
      //tax iva
      //mt monto retenido

      let bi = ((invoice.header.salfac * 100) / 116);
      let tax = (bi * 16) / 100;
      let mt = tax * 0.75;
      tax = tax - mt;
      bi = bi + tax;
      this.retained_amount += mt;
      
    });


  }*/

  calculateRetentionAmount(){

    this.invoices.forEach(invoice => {

      //bi base imponible
      //tax iva
      //mt monto retenido

      let bi = ((invoice.salfac * 100) / 116);
      let tax = (bi * 16) / 100;
      let mt = tax * 0.75;
      tax = tax - mt;
      bi = bi + tax;
      this.retained_amount += mt;
      
    });


  }

  setUpMask(){

    this.masks = {

      compnum:[/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]
      
    };


  }


}
