import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { DbService } from "../../providers/db-service";
import { Diagnostic } from "@ionic-native/diagnostic";
import { TranslateService } from "ng2-translate";
import { CobranzaMainPage } from "../cobranza-main/cobranza-main";
import { GenericRestService } from "../../providers/generic-rest-service";
import {AppConstants} from '../../providers/app-constants';

import { AlertsService } from '../../providers/alerts.service';

/*
  Generated class for the Cobranzas page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cobranzas',
  templateUrl: 'cobranzas.html'
})
export class CobranzasPage {
  clientes_vista:any[] = [];
  filter: string = "";
  offset: number = 0;

  constructor(
    public navCtrl: NavController,
    public view: ViewController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public database: DbService,
    private diagnostic: Diagnostic,
    private translate: TranslateService,   
    public loadingCtrl: LoadingController,
    public genericRestService: GenericRestService,
    private alertServ: AlertsService) {}
  

  ionViewDidLoad() {
    // this.getAllClients();
    this.getAllClientsOffset();
  }

  getAllClientsOffset(){
    this.database.openDatabase().then(
      () => {
        this.database.getAllClientsOffsetCharging(this.offset)
        .then(response => {
          for(let i = 0; i < response.length; i++){
            this.clientes_vista.push(response[i]);
          }
        });
      });
  }

  // getAllClients(){
  //    this.database.openDatabase().then(
  //     () => {
  //       this.database.getAllClients()
  //       .then(clientes => {
  //         this.clientes_copia = clientes;      
  //         this.clientes = this.clientes_copia;
  //         //this.setFilteredItems();
  //       })
  //     });
    
  // }

  // filterItems(ev: any) {

  //     this.clientes = this.clientes_copia;
  //     let val = ev.target.value;

  //   // if the value is an empty string don't filter the items
  //     if (val && val.trim() != '') {
  //       this.clientes = this.clientes.filter((item) => {
  //         return (item.descli.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.codcli.includes(val));
  //       })
  //     }
 
  // }

  filterItems(ev: any){
    this.filter = ev.target.value;
    if (this.filter) {
      if(this.filter.trim() != '') {
        this.clientes_vista = [];
        this.database.openDatabase().then(
          () => {
            this.database.getAllClientsOffsetByfilter(this.clientes_vista.length, this.filter)
            .then(response => {
              for(let i = 0; i < response.length; i++){
                this.clientes_vista.push(response[i]);
              }
            });
          });
      } else {
        this.clientes_vista = [];
        this.database.openDatabase().then(
          () => {
            this.database.getAllClientsOffsetCharging(this.clientes_vista.length)
            .then(response => {
              for(let i = 0; i < response.length; i++){
                this.clientes_vista.push(response[i]);
              }
            });
          });
      }
    } else {
      this.clientes_vista = [];
      this.database.openDatabase().then(
        () => {
          this.database.getAllClientsOffsetCharging(this.clientes_vista.length)
          .then(response => {
            for(let i = 0; i < response.length; i++){
              this.clientes_vista.push(response[i]);
            }
          });
        });
    }
  }

  showMessage(msg: any){

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({      
        message: msg,
        buttons: [
          {
            text: localizedValues["common.acept"],
            handler: () =>{
              console.log('aceptar');
            }
          }
        ]
      });
      alert.present();        
          
      });   

  }

  ////////////////////////////////////////////////////COBRANZAS/////////////////////////////////////////////////////
  /*checkClientCharges(cliente: any){
     this.translate.get(["charges.get_client_info", "errors.serv_error"], null).subscribe(
      (localizedValues) => {
        let loading = this.loadingCtrl.create({
          content: localizedValues["charges.get_client_info"],
          spinner: "bubbles"
        });
        loading.present();
        this.genericRestService.sendToUrl(AppConstants.URL_GET_ESTADO_CUENTA, cliente.codcli).then(
          (res: any) => {
            if(res.status == 202) {
              let estcli = JSON.parse(res._body);

              loading.dismiss().then(
                ()=>{
                  this.navCtrl.push(CobranzaMainPage, {cliente:cliente, estcli: estcli});
                });
            } else {
              loading.dismiss().then(
                ()=> {
                  let msg: string;
                  if( (res.headers !== undefined) && (res.headers.get('sfo_error') !== undefined) ){
                    msg = res.headers.get('sfo_error');
                  } else {
                    msg = localizedValues["errors.serv_error"];
                  }
                  this.alertServ.showAlertError(msg);
                });
            }
          },
          (err) => {
            loading.dismiss().then(
              ()=> {

                // let msg: string;
                // console.log(err._body);
                // if(err.status === 0){
                //   msg = localizedValues["errors.serv_not_resp"];
                // } else {
                //   if( (err.headers !== undefined) && (err.headers.get('sfo_error') !== undefined) ){
                //     msg = err.headers.get('sfo_error');
                //   } else {
                //     msg = localizedValues["errors.serv_error"];
                //   }
                // }

                // this.alertServ.showAlertError(msg);
                alert("error obteniendo estado de cuenta: "+ err);
              });
          });
      });
  }*/

  checkClientCharges(cliente: any){
    let estcli = {codcli: cliente.codcli, cntesp: false, limcre: 0, facven: 0, totven: 0, facnve: 0, totnve: 0, credis: 0, cobran: 0};
    this.navCtrl.push(CobranzaMainPage, {cliente:cliente, estcli: estcli});             
 }
  
  doInfinite(infiniteScroll) {
    this.database.openDatabase().then(
      () => {
        if(this.filter == null || this.filter == "") {
          this.database.getAllClientsOffsetCharging(this.clientes_vista.length)
          .then(response => {
            for(let i = 0; i < response.length; i++){
              this.clientes_vista.push(response[i]);
            }

            infiniteScroll.complete();
            // if(this.productos_vista.length >= this.allProdLength){
            //   infiniteScroll.enable(false);
            // }
          });
        } else {
          this.database.getAllClientsOffsetByfilter(this.clientes_vista.length, this.filter)
          .then(response => {
            for(let i = 0; i < response.length; i++){
              this.clientes_vista.push(response[i]);
            }

            infiniteScroll.complete();
            // if(this.productos_vista.length >= this.allProdLength){
            //   infiniteScroll.enable(false);
            // }
          });
        }
      });
  }
}
