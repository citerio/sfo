import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, MenuController, Nav, Platform, LoadingController, AlertController } from 'ionic-angular';

import { Splashscreen } from 'ionic-native';

import { TabsPage } from '../tabs/tabs';
import { PedidosPage } from '../pedidos/pedidos';
import { FacturacionPage } from '../facturacion/facturacion';
import { CobranzasPage } from '../cobranzas/cobranzas';
import { AdministracionPage } from '../administracion/administracion';
import { HomePage } from '../home/home';
import { AgregarproductoPage } from '../agregarproducto/agregarproducto';
import { PedidoMainPage } from '../pedido-main/pedido-main';
import { VisitasPage } from '../visitas/visitas';
import { DataNoSyncPage } from '../data-no-sync/data-no-sync';
import { GenericRestService } from '../../providers/generic-rest-service';
import { Storage } from '@ionic/storage';

import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Http, Response, Headers } from '@angular/http';
import { LoginPage } from '../login/login';

import { TranslateService } from 'ng2-translate/ng2-translate';
import { DbService } from '../../providers/db-service';
import { DataPagesService } from '../../providers/data-pages-service';
import { SendDataService } from '../../providers/send-data-service';
import { AlertsService } from '../../providers/alerts.service';

import { AppConstants } from '../../providers/app-constants';

declare var cordova;
/*
  Generated class for the Dashboard page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

export interface PageInterface {
  title?: string;
  component: any;
  icon: string;
  logsOut?: boolean;
  index?: number;
  tabComponent?: any;
  visible?: boolean;
}

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {
  @ViewChild(Nav) nav: Nav;

  nombre: string;
  mask: string;
  showedAlert: boolean;
  confirmAlert: any;
  //Idle
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
  timeOutExec: boolean = false;
  params: any;
  selectedSucursal: string;

  sucursales: any[];
  show_sucursales: boolean = false;
  suc_icon: string = "arrow-dropdown";


  actiSync = false;
  actiLogout = true;

  home: PageInterface = { component: HomePage, tabComponent: HomePage, icon: 'home', visible: true };
  dataNoSync: PageInterface = { component: DataNoSyncPage, tabComponent: DataNoSyncPage, icon: 'home', visible: true };
  appPages: PageInterface[] = [
    { component: AdministracionPage, tabComponent: AdministracionPage, index: 4, icon: 'settings', visible: false },
    { component: VisitasPage, tabComponent: VisitasPage, index: 6, icon: 'calendar', visible: false },
    { component: PedidoMainPage, tabComponent: PedidoMainPage, icon: 'cart', visible: false },
    { component: FacturacionPage, tabComponent: FacturacionPage, icon: 'paper', visible: false },
    { component: CobranzasPage, tabComponent: CobranzasPage, icon: 'cash', visible: false }
  ];

  rootPage: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController,
    public platform: Platform,
    private idle: Idle,
    private genericRestService: GenericRestService,
    private http: Http,
    private storage: Storage,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private translate: TranslateService,
    private database: DbService,
    private dataPagesService: DataPagesService,
    private alertServ: AlertsService,
    private sendDataService: SendDataService) {


    this.getSucursales();
    this.platformReady();
    this.enableMenu(true);

    /////////////////////////////////sucursales//////////////////////////

    /* this.sucursales = [
             {title: 'Los Sauces', dayCode: 1, checked: false},
             {title: 'Santa Ana', dayCode: 2, checked: false},
             {title: 'San Diego', dayCode: 3, checked: false},
             {title: 'Santa Rosa', dayCode: 4, checked: false},
             {title: 'Tocuyito', dayCode: 5, checked: false},
             {title: 'Mariara', dayCode: 6, checked: false},
             {title: 'San Joaquin', dayCode: 0, checked: false}
         ];*/

    /////////////////////////////////sucursales//////////////////////////



    this.rootPage = HomePage;

  }

  ionViewDidLoad() {
    // sets an idle timeout of 5 seconds, for testing purposes.
    // this.sendDataService.initSyncWhenOnline();
    // this.sendDataService.sendDataWhenOnline();

    this.idle.setIdle(3000);
    // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    this.idle.setTimeout(3000);
    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    // this.idle.onTimeout.map(res => res).share()
    /*this.idle.onTimeout.map(res => res).share().subscribe(() => {
      console.log('Entro en el timeout function');
      if(!this.timedOut) {
        this.doLogout();
      }
      console.log('Timed out!');
      this.idleState = 'Timed out!';
      this.timedOut = true;
      
    });*/

    this.idle.onTimeout.subscribe(() => {
      console.log('Entro en el timeout function');
      if (!this.timedOut) {
        this.doLogout();
      }
      console.log('Timed out!');
      this.idleState = 'Timed out!';
      this.timedOut = true;

    });

    this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    this.idle.onTimeoutWarning.subscribe((countdown: any) => this.idleState = 'You will time out in ' + countdown + ' seconds!');

    this.reset();
  }

  showAlertError(errorMsg: string) {
    this.translate.get(["common.error", "common.acept"], null).subscribe(
      (localizedValues) => {
        let alert = this.alertCtrl.create({
          title: localizedValues["error"],
          subTitle: '',
          buttons: [localizedValues["acept"]]
        });
        alert.setSubTitle(errorMsg);
        alert.present();
      });
  }

  doLogout() {
    this.database.openDatabase().then(
      () => {
        this.database.getUrlServComp().then(
          (res: string) => {
            this.storage.get('todo').then(
              (value) => {
                // this.todo = value;
                this.storage.set('todo', '');
                this.storage.get('username').then(
                  (username) => {
                    let url = res + AppConstants.URL_LOGOUT + username;
                    let headers = new Headers();
                    headers.append('Authorization-Route', 'route:' + value);

                    console.log('url de logout:' + url);
                    this.http.get(url, { headers: headers }).map((res: Response) => res)./*share().*/subscribe(
                      (data: any) => {
                        console.log('Logout exitoso');
                      },
                      (error: any) => {
                        console.log('Logout erroneo: ' + error);
                      });
                  });
              });
          },
          (error: any) => {
            console.log('error leyendo el dato: '); console.log(error);
          });
      });
    this.sendDataService.finishSyncWhenOnline();

    this.navCtrl.setRoot(LoginPage, { lang: this.translate.currentLang });
  }

  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  enableMenu(loggedIn: boolean) {
    this.menu.enable(loggedIn, 'loggedInMenu');
  }

  openPage(page: PageInterface) {
    // the nav component was found using @ViewChild(Nav)
    // reset the nav to remove previous pages and only have this page
    // we wouldn't want the back button to show in this scenario

    if (!this.isPressed(page)) {
      this.nav.push(page.component);
    }

    // if (page.index) {
    //   this.nav.push(page.component/*, { tabIndex: page.index }*/);
    // } else {
    //   this.nav.push(page.component).catch(() => {
    //     console.log("Didn't set nav root");
    //   });
    // }
  }

  platformReady() {
    // Call any initial plugins when ready
    this.platform.ready().then(() => {
      cordova.plugins.certificates.trustUnsecureCerts(true);
      if ((this.navParams.data) && (this.navParams.data.fromLogin)) {
        this.translate.get("get_config", null).subscribe(
          (localizedValue) => {
            let loading = this.loadingCtrl.create({
              content: localizedValue,
              spinner: "bubbles"
            });
            loading.present();
            console.log('Vengo del login');
            this.storage.get('username').then(
              (username) => {
                console.log('Obtengo el username:' + username);
                this.genericRestService.getFromUrl(AppConstants.URL_CONFIG + "/" + username).then(
                  (res: any) => {
                    if (res.status == 202) {
                      let conf = JSON.parse(res._body);
                      this.mask = conf.mascara;

                      this.storage.set('mask', this.mask);
                      let lang;

                      if (conf.lang != undefined) {
                        lang = conf.lang;
                      } else {
                        lang = this.translate.currentLang;
                      }

                      this.storage.set('lang', lang);
                      this.nombre = conf.nombre;
                      this.storage.set('nombre', this.nombre);

                      this.storage.set('gpsper', conf.gpsper);

                      let syntim;

                      if (conf.syntim != undefined) {
                        syntim = conf.syntim;
                      } else {
                        syntim = 40;
                      }

                      this.storage.set('syntim', syntim);
                      this.sendDataService.initSyncWhenOnline(syntim);

                      // this.params = {'nombre': conf.nombre}

                      this.dataPagesService.setMask(conf.mascara);

                      this.actiSync = (this.mask[0] === '1') ? true : false;

                      for (var i = 2; i < this.mask.length; i++) {
                        if (this.mask[i] === '1') {
                          this.appPages[i - 2].visible = true;
                        }
                      }

                      this.translate.use(lang).subscribe(
                        acept => {
                          console.log('Lenguaje seteado');
                          this.translate.get(["home.menu_opt", "management.menu_opt",
                            "take_orders.menu_opt", "orders.menu_opt", "visits.menu_opt", "dataNoSync.menu_opt", "charges.menu_opt", "billing.menu_opt"], null).subscribe(
                              (localizedValues) => {
                                this.home.title = localizedValues["home.menu_opt"];
                                this.dataNoSync.title = localizedValues["dataNoSync.menu_opt"];
                                this.appPages[0].title = localizedValues["management.menu_opt"];
                                this.appPages[1].title = localizedValues["visits.menu_opt"];
                                this.appPages[2].title = localizedValues["take_orders.menu_opt"];
                                this.appPages[3].title = localizedValues["billing.menu_opt"];
                                this.appPages[4].title = localizedValues["charges.menu_opt"];
                                /*this.appPages[4].title = localizedValues["management.menu_opt"];
                                this.appPages[5].title = localizedValues["add_products.menu_opt"];
                                this.appPages[6].title = localizedValues["take_orders.menu_opt"];*/

                                // this.rootPage = HomePage;
                                loading.dismiss();
                              });
                        });
                    } else {
                      this.translate.get(["errors.serv_error"], null).subscribe(
                        (localizedValues) => {

                          for (var i = 0; i < this.appPages.length; i++) {
                            this.appPages[i].visible = false;
                          }

                          // this.rootPage = HomePage;
                          loading.dismiss().then(
                            () => {
                              let msg: string;
                              if ((res.headers !== undefined) && (res.headers.get('sfo_error') !== undefined)) {
                                msg = res.headers.get('sfo_error');
                              } else {
                                msg = localizedValues["errors.serv_error"];
                              }

                              this.showAlertError(msg);
                            });
                        });
                    }
                  }, (err: any) => {
                    this.translate.get(["errors.serv_not_resp", "errors.serv_error"], null).subscribe(
                      (localizedValues) => {

                        for (var i = 0; i < this.appPages.length; i++) {
                          this.appPages[i].visible = false;
                        }

                        // this.rootPage = HomePage;
                        loading.dismiss().then(
                          (res) => {
                            let msg: string;
                            console.log(err._body);
                            if (err.status === 0) {
                              msg = localizedValues["errors.serv_not_resp"];
                            } else {
                              if ((err.headers !== undefined) && (err.headers.get('sfo_error') !== undefined)) {
                                msg = err.headers.get('sfo_error');
                              } else {
                                msg = localizedValues["errors.serv_error"];
                              }
                            }

                            this.showAlertError(msg);
                          });
                      });
                  });
              });
          });
      } else {
        console.log('Entro en el else');
        this.storage.get('nombre').then(
          (nombre) => {
            this.nombre = nombre;
          });

        this.storage.get('mask').then(
          (mask) => {
            this.actiSync = (mask[0] === '1') ? true : false;
            for (var i = 2; i < mask.length; i++) {
              if (mask[i] === '1') {
                this.appPages[i - 2].visible = true;
              }
            }
            this.dataPagesService.setMask(mask);
          });

        this.storage.get('lang').then(
          (lang) => {
            this.translate.use(lang).subscribe(
              acept => {
                this.translate.get(["home.menu_opt", "management.menu_opt",
                  "take_orders.menu_opt", "take_orders.menu_opt", "visits.menu_opt", "charges.menu_opt", "billing.menu_opt"], null).subscribe(
                    (localizedValues) => {
                      this.home.title = localizedValues["home.menu_opt"];
                      this.dataNoSync.title = localizedValues["dataNoSync.menu_opt"];
                      this.appPages[0].title = localizedValues["management.menu_opt"];
                      this.appPages[1].title = localizedValues["visits.menu_opt"];
                      this.appPages[2].title = localizedValues["take_orders.menu_opt"];
                      this.appPages[3].title = localizedValues["billing.menu_opt"];
                      this.appPages[4].title = localizedValues["charges.menu_opt"];
                    });
              });
          });

        this.storage.get("syntim").then(
          (syntim) => {
            this.sendDataService.initSyncWhenOnline(syntim);
          });
      }
      // Splashscreen.hide();

      this.getSelectedSucxcomp();

      this.showedAlert = false;

      // Confirm exit
      this.platform.registerBackButtonAction(() => {
        if (this.nav.length() == 1) {
          if (!this.showedAlert) {
            this.confirmExitApp();
          } else {
            this.showedAlert = false;
            this.confirmAlert.dismiss();
          }
        } else {
          if (this.nav.last().component == HomePage) {
            this.rootPage = HomePage;
          } else {
            this.nav.pop();
          }
        }
      });
    });
  }


  // platformReady() {
  //   // Call any initial plugins when ready
  //   this.platform.ready().then(() => {
  //     cordova.plugins.certificates.trustUnsecureCerts(true);
  //     if( (this.navParams.data) && (this.navParams.data.fromLogin) ) {
  //       this.translate.get("get_config", null).subscribe(
  //         (localizedValue) => {
  //           let loading = this.loadingCtrl.create({
  //             content: localizedValue,
  //             spinner: "bubbles"
  //           });
  //           loading.present();
  //           console.log('Vengo del login');
  //           this.storage.get('username').then(
  //             (username) => {
  //               console.log('Obtengo el username:' + username);
  //               this.mask = "1111111";

  //                     this.storage.set('mask', this.mask);
  //                     let lang = this.translate.currentLang;                      

  //                     this.storage.set('lang', lang);
  //                     this.nombre = "Yeisser Cortez";
  //                     this.storage.set('nombre', this.nombre);

  //                     this.storage.set('gpsper', true);

  //                     let syntim = 40;                      

  //                     this.storage.set('syntim', syntim);
  //                     this.sendDataService.initSyncWhenOnline(syntim);

  //                     // this.params = {'nombre': conf.nombre}

  //                     this.dataPagesService.setMask(this.mask);

  //                     this.actiSync = (this.mask[0] === '1') ? true : false;

  //                     for (var i = 2; i < this.mask.length; i++) {
  //                       if(this.mask[i] === '1') {
  //                         this.appPages[i-2].visible = true;
  //                       }
  //                     }

  //                     this.translate.use(lang).subscribe(
  //                       acept => {
  //                         console.log('Lenguaje seteado');
  //                         this.translate.get(["home.menu_opt", "management.menu_opt",
  //                                             "take_orders.menu_opt", "orders.menu_opt","visits.menu_opt", "dataNoSync.menu_opt", "charges.menu_opt", "billing.menu_opt"], null).subscribe(
  //                           (localizedValues) => {
  //                             this.home.title = localizedValues["home.menu_opt"];
  //                             this.dataNoSync.title = localizedValues["dataNoSync.menu_opt"];
  //                             this.appPages[0].title = localizedValues["management.menu_opt"];
  //                             this.appPages[1].title = localizedValues["visits.menu_opt"];
  //                             this.appPages[2].title = localizedValues["take_orders.menu_opt"];
  //                             this.appPages[3].title = localizedValues["billing.menu_opt"];
  //                             this.appPages[4].title = localizedValues["charges.menu_opt"];
  //                             /*this.appPages[4].title = localizedValues["management.menu_opt"];
  //                             this.appPages[5].title = localizedValues["add_products.menu_opt"];
  //                             this.appPages[6].title = localizedValues["take_orders.menu_opt"];*/

  //                             // this.rootPage = HomePage;
  //                             loading.dismiss();
  //                           });
  //                       });

  //             });
  //         });
  //     } else {
  //       console.log('Entro en el else');
  //       this.storage.get('nombre').then(
  //         (nombre) => {
  //           this.nombre = nombre;
  //         });

  //       this.storage.get('mask').then(
  //         (mask) => {
  //           this.actiSync = (mask[0] === '1') ? true : false;
  //           for (var i = 2; i < mask.length; i++) {
  //             if(mask[i] === '1') {
  //               this.appPages[i-2].visible = true;
  //             }
  //           }
  //           this.dataPagesService.setMask(mask);
  //         });

  //       this.storage.get('lang').then(
  //         (lang) => {
  //           this.translate.use(lang).subscribe(
  //             acept => {
  //               this.translate.get(["home.menu_opt", "management.menu_opt",
  //                                   "take_orders.menu_opt", "take_orders.menu_opt","visits.menu_opt", "charges.menu_opt", "billing.menu_opt"], null).subscribe(
  //                 (localizedValues) => {
  //                   this.home.title = localizedValues["home.menu_opt"];
  //                   this.dataNoSync.title = localizedValues["dataNoSync.menu_opt"];
  //                   this.appPages[0].title = localizedValues["management.menu_opt"];
  //                   this.appPages[1].title = localizedValues["visits.menu_opt"];
  //                   this.appPages[2].title = localizedValues["take_orders.menu_opt"];
  //                   this.appPages[3].title = localizedValues["billing.menu_opt"];
  //                   this.appPages[4].title = localizedValues["charges.menu_opt"];
  //                 });
  //             });
  //         });

  //       this.storage.get("syntim").then(
  //         (syntim) => {
  //           this.sendDataService.initSyncWhenOnline(syntim);
  //         });
  //     }
  //     // Splashscreen.hide();

  //     this.getSelectedSucxcomp();

  //     this.showedAlert = false;

  //     // Confirm exit
  //     this.platform.registerBackButtonAction(() => {
  //         if (this.nav.length() == 1) {
  //             if (!this.showedAlert) {
  //                 this.confirmExitApp();
  //             } else {
  //                 this.showedAlert = false;
  //                 this.confirmAlert.dismiss();
  //             }
  //         } else {
  //           if(this.nav.last().component == HomePage){
  //             this.rootPage = HomePage;
  //           } else {
  //             this.nav.pop();
  //           }
  //         }
  //     });
  //   });
  // }


  getSelectedSucxcomp() {
    this.database.openDatabase().then(
      () => {
        this.database.getSelectedSucxcomp().then(
          (res: string) => {
            console.log('sucxcom: ' + res);
            if (res != '' && res != null) {
              console.log('no es nula');
              let ids = res.split(" ");
              this.database.getDescSucxcomp(ids[0], ids[1], ids[2]).then(
                (sucursal) => {
                  console.log('caigo aqui: ' + sucursal.dessuc);
                  this.selectedSucursal = sucursal;
                },
                error => {
                  console.log(error);
                });
            }
          });
      });
  }

  getSucursales() {
    this.database.openDatabase().then(
      () => {
        this.database.getSucxcomp().then(
          (sucxcomp) => {
            this.sucursales = [];
            this.sucursales = sucxcomp;
          }
        );
      }
    );
  }

  changeSucursal(opt: any) {
    this.database.updateSelectedsucxcom(opt.codusr + ' ' + opt.codsuc + ' ' + opt.codpro).then(
      () => {
        this.getSelectedSucxcomp();
      }
    );
  }

  confirmExitApp() {
    this.showedAlert = true;
    this.translate.get(["common.cancel", "common.acept", "common.sure_close", "common.exit"], null).subscribe(
      (localizedValues) => {
        this.confirmAlert = this.alertCtrl.create({
          title: localizedValues["common.exit"],
          message: localizedValues["common.sure_close"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.showedAlert = false;
                return;
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.doLogout();
              }
            }
          ]
        });
        this.confirmAlert.present();
      });
  }

  isActive(page: PageInterface) {
    let childNav = this.nav.getActiveChildNav();

    // Tabs are a special case because they have their own navigation
    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }

    if (this.nav.getActive() && this.nav.getActive().component === page.tabComponent) {
      return 'grey_light';
    }
    return;
  }

  isPressed(page: PageInterface): boolean {
    // let childNav = this.nav.getActiveChildNav();

    // Tabs are a special case because they have their own navigation
    // if (childNav) {
    //   if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
    //     return true;
    //   }
    //   return false;
    // }

    if (this.nav.getActive() && this.nav.getActive().component === page.component) {
      return true;
    }
    return false;
  }

  showAlertConfirm() {
    this.translate.get(["common.get_data", "common.sure_sync", "common.acept", "common.cancel", "common.confirm"], null).subscribe(
      (localizedValues) => {
        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["common.sure_sync"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.sincronizar();
              }
            }
          ]
        });
        alert.present();
      });

  }

  sincronizar() {

    /*//////////////////////////////////////////////////facturacion testing/////////////////////////////////////////////////
    this.http.get("https://my-json-server.typicode.com/citerio/jsonapi/db").map(res => res.json()).subscribe(
      (res: any) => {
        this.database.openDatabase().then(
        () => {
          let prom: Promise<any>  =  new Promise((resolve, reject) => {
                          return this.database.droptables().then(
                            () => {
                              this.database.insertDatos(res.data).then(
                                ()=>{resolve()},
                                error => {reject()});
                            },
                            error => {reject()});
                        });
        });
      });
      //////////////////////////////////////////////////facturacion testing/////////////////////////////////////////////////*/


    this.translate.get(["common.get_data", "common.inserting_data", "common.sync_succ", "errors.sync_fail", "errors.serv_not_resp", "errors.serv_error", "errors.not_init_config"], null).subscribe(
      (localizedValues) => {
        this.database.openDatabase().then(
          () => {
            this.database.getFieldOfTable("config", "inic_config", "id = ?", [1]).then(
              (data) => {
                if (data == 'true') {
                  let loading = this.loadingCtrl.create({
                    content: localizedValues["common.get_data"],
                    spinner: "bubbles"
                  });
                  loading.present();


                  this.genericRestService.getFromUrl(AppConstants.URL_GET_DATA).then(
                    (res: any) => {
                      if (res.status == 202) {
                        loading.setContent(localizedValues["common.inserting_data"]);
                        console.log('Devuelto: ' + res._body);

                        // try{

                        // } catch() {

                        // }
                        let body = JSON.parse(res._body);
                        let promises: any[] = [];

                        // this.database.insertDatos(body.data);
                        // this.database.insertAsoc(body.asociaciones);
                        // loading.dismiss().then(
                        //   (res) => {
                        //     this.alertServ.showAlert("Datos obtenidos con exito");
                        //   });

                        let prom: Promise<any> = new Promise((resolve, reject) => {
                          return this.database.droptables().then(
                            () => {
                              this.database.insertDatos(body.data).then(
                                () => { resolve() },
                                error => { reject() });
                            },
                            error => { reject() });
                        });

                        promises.push(prom);
                        //promises.push(this.database.insertDatos(body.data));
                        promises.push(this.database.insertAsoc(body.asociaciones));
                        promises.push(this.database.insertPlanes(body.planes));

                        Promise.all(promises).then(
                          (response) => {
                            loading.dismiss().then(
                              (res) => {
                                this.database.updateInic_sync(1, true);
                                this.showAlertSuccesSync(localizedValues["common.sync_succ"]);
                              });
                          },
                          (error) => {
                            loading.dismiss().then(
                              () => {
                                this.alertServ.showAlert(localizedValues["errors.sync_fail"]);
                              });

                          })
                          .catch(
                            () => {
                              loading.dismiss().then(
                                () => {
                                  this.alertServ.showAlert(localizedValues["errors.sync_fail"]);
                                });
                            });

                      } else {
                        loading.dismiss().then(
                          () => {
                            let msg;
                            msg = localizedValues["errors.serv_error"];
                            if ((res.headers != undefined) && (res.headers.get('sfo_error') != undefined)) {
                              msg = res.headers.get('sfo_error');
                            }

                            this.alertServ.showAlertError(msg);
                          });
                      }
                    },
                    (err: any) => {
                      loading.dismiss().then(
                        (res) => {
                          let msg;
                          if ((err.status === 0) || (err.status === undefined)) {
                            msg = localizedValues["errors.serv_not_resp"];
                          } else {
                            msg = localizedValues["errors.serv_error"];
                            if ((err.headers != undefined) && (err.headers.get('sfo_error') != undefined)) {
                              msg = err.headers.get('sfo_error');
                            }
                          }

                          this.alertServ.showAlertError(msg);
                        });
                    });
                } else {
                  this.alertServ.showAlertError(localizedValues["errors.not_init_config"]);
                }
              });
          });
      });
  }



  // sincronizar(){

  //   /*//////////////////////////////////////////////////facturacion testing/////////////////////////////////////////////////
  //   this.http.get("https://my-json-server.typicode.com/citerio/jsonapi/db").map(res => res.json()).subscribe(
  //     (res: any) => {
  //       this.database.openDatabase().then(
  //       () => {
  //         let prom: Promise<any>  =  new Promise((resolve, reject) => {
  //                         return this.database.droptables().then(
  //                           () => {
  //                             this.database.insertDatos(res.data).then(
  //                               ()=>{resolve()},
  //                               error => {reject()});
  //                           },
  //                           error => {reject()});
  //                       });
  //       });
  //     });
  //     //////////////////////////////////////////////////facturacion testing/////////////////////////////////////////////////*/


  //   this.translate.get(["common.get_data", "common.inserting_data", "common.sync_succ", "errors.sync_fail", "errors.serv_not_resp", "errors.serv_error", "errors.not_init_config"], null).subscribe(
  //     (localizedValues) => {
  //       this.database.openDatabase().then(
  //         () => {
  //           this.database.getFieldOfTable("config", "inic_config", "id = ?", [1]).then(
  //             (data) => {
  //               if(data == 'true') {
  //                 let loading = this.loadingCtrl.create({
  //                   content: localizedValues["common.get_data"],
  //                   spinner: "bubbles"
  //                 });
  //                 loading.present();

  //                 setTimeout(()=>{ 
  //                   loading.dismiss().then(
  //                     (res) => {                        
  //                       this.showAlertSuccesSync(localizedValues["common.sync_succ"]);
  //                     }); 

  //                 }, 20000);

  //               } else {
  //                 this.alertServ.showAlertError(localizedValues["errors.not_init_config"]);
  //               }
  //             });
  //         });
  //     });
  // }



  showAlertSuccesSync(msg: string) {
    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {
        let alert = this.alertCtrl.create({
          message: '',
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: data => {
                this.database.getSelectedSucxcomp().then(
                  (sucxcom) => {
                    if (sucxcom == null) {
                      this.openSucursales();
                    }
                  });
              }
            }
          ]
        });
        alert.setMessage(msg);
        alert.present();
      });
  }

  decideDelSyncData() {
    this.translate.get(["common.del_sync_succ", "common.del_data", "errors.inner_error", "errors.not_init_config"], null).subscribe(
      (localizedValues) => {
        this.database.openDatabase().then(
          () => {
            this.database.getFieldOfTable("config", "inic_config", "id = ?", [1]).then(
              (data) => {
                if (data == 'true') {
                  this.showAlertConfirmDel();
                } else {
                  this.alertServ.showAlertError(localizedValues["errors.not_init_config"]);
                }
              },
              error => {
                this.alertServ.showAlertError(localizedValues["errors.inner_error"]);
                console.log("Error en consulta de inicConfig: " + error);
              });
          });
      });
  }

  delSyncData() {
    this.translate.get(["common.del_sync_succ", "common.del_data"], null).subscribe(
      (localizedValues) => {
        let loading = this.loadingCtrl.create({
          content: localizedValues["common.del_data"],
          spinner: "bubbles"
        });
        loading.present();

        let promises: Promise<any>[] = [];

        promises.push(this.database.deleteSyncOrders());
        promises.push(this.database.deleteSyncClientesxPlanes());

        Promise.all(promises).then(
          () => {
            loading.dismiss().then(
              () => {
                this.alertServ.showAlert(localizedValues["common.del_sync_succ"]);
              });
          },
          error => {
            loading.dismiss().then(
              () => {
                this.showAlertWarning();
              });
          });
      });
  }

  showAlertConfirmDel() {
    this.translate.get(["common.sure_del_sync", "common.acept", "common.cancel", "common.confirm"], null).subscribe(
      (localizedValues) => {
        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["common.sure_del_sync"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.delSyncData();
              }
            }
          ]
        });
        alert.present();
      });
  }

  showAlertWarning() {
    this.translate.get(["common.exit", "common.retry", "common.warning", "errors.not_del_some_data"], null).subscribe(
      (localizedValues) => {
        let alert = this.alertCtrl.create({
          title: localizedValues["common.warning"],
          message: localizedValues["errors.not_del_some_data"],
          buttons: [
            {
              text: localizedValues["common.exit"],
              handler: () => {
              }
            },
            {
              text: localizedValues["common.retry"],
              handler: () => {
                this.delSyncData();
              }
            }
          ]
        });
        alert.present();
      });
  }

  openSucursales() {
    this.translate.get(["common.cancel", "common.acept", "common.choose_branch_office"], null).subscribe(
      (localizedValues) => {
        this.database.getSucxcomp().then(
          (sucxcomp) => {
            let alert = this.alertCtrl.create({
              enableBackdropDismiss: false
            });
            alert.setTitle(localizedValues["common.choose_branch_office"]);

            sucxcomp.forEach((s) => {
              alert.addInput({
                type: 'radio',
                label: s.dessuc + '-' + s.despro,
                value: s.codusr + ' ' + s.codsuc + ' ' + s.codpro,
                checked: false
              });
            });

            alert.addButton(localizedValues["common.cancel"]);

            alert.addButton({
              text: localizedValues["common.acept"],
              handler: data => {
                if (data !== '') {
                  console.log('selected: ' + data);
                  this.database.updateSelectedsucxcom(data).then(
                    () => {
                      this.getSucursales();
                      this.getSelectedSucxcomp();
                    }
                  );
                }
              }
            });
            alert.present();
          });
      });
  }


  showSucursales() {

    if (this.show_sucursales) {

      this.show_sucursales = false;
      this.suc_icon = "arrow-dropdown";


    } else {

      this.show_sucursales = true;
      this.suc_icon = "arrow-dropup";

    }

    //this.show_sucursales = !this.show_sucursales; 

  }

  // changeSucursal(sucursal:any){

  //   this.selectedSucursal = sucursal.title;

  // }


}
