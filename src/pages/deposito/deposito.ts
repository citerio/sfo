import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, App, ViewController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { DbChangesService } from '../../providers/db-changes-service';
import { TranslateService } from 'ng2-translate';
import { AlertsService } from '../../providers/alerts.service';
import { CobranzaProcesarPage } from '../cobranza-procesar/cobranza-procesar';
import { ComprobanteRetencionPage } from '../comprobante-retencion/comprobante-retencion';
import { DepositoProcesarPage } from '../deposito-procesar/deposito-procesar';

/*
  Generated class for the Deposito page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-deposito',
  templateUrl: 'deposito.html'
})
export class DepositoPage {

  selected_invoices: any[] = [];
  invoices: any[] = [];
  indexes: any[] = [];
  invoices_already_charged: any[] = [];
  type: string = 'statement';
  deshabilitar_cobrar: boolean = true;
  charge: any = {};
  show_banks_list: any = false;
  show_reference: any = false;
  show_check_a_number: any = false;
  show_check_number: any = false;
  segment_title: string;
  account_state: any = {};
  bank_list: any[] = [];
  cobfas: string = 'open';
  invoices_amount: any = 0;
  invoice_selected: any = false;
  numero_actual_cobranza: string = '';

  /**********************cobranzas****************************/
  all_charges: any[] = [];
  charges: any[] = [];
  available_charges: any[] = [];
  printed_cobranzas: any[] = [];
  filtered_cobranzas: any[] = [];
  deposit: any = {};
  bank_account_number: string = '';
  cash_and_check: boolean = true;
  numero_actual_deposito: string = '';
  cod_cli: string = '';
  charges_already_deposited: any[] = [];
  selected_charges: any[] = [];
  /**********************cobranzas****************************/

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public app: App,
    public view: ViewController,
    public navParams: NavParams,
    public database: DbService,
    public dbChangesService: DbChangesService,
    private translate: TranslateService,
    private alertServ: AlertsService) {

    if (this.navParams.data.deposit) {

      this.deposit = this.navParams.data.deposit;

      if (this.deposit.codcli) {

        this.cod_cli = this.deposit.codcli;
        this.init();

      }

      ///charge already created
      if (this.deposit.depnum) {
        this.numero_actual_deposito = this.deposit.depnum;
      } else {//charge just created
        this.numero_actual_deposito = this.database.getCurrentDepositNumber();
        this.database.getDepositCreationDate(this.numero_actual_deposito)
          .then((fecha) => {this.deposit.depfec = fecha;});
        this.deposit.depnum = this.numero_actual_deposito;
      }

    }


    this.indexes.push(this.view.index);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CobranzaPage');
  }

  /*initExisting(){

    this.loadBanks();
    this.getInvoicesAlreadyCharged();
    this.changePaymentMethod();

  }*/

  init2() {

    this.invoices.forEach(
      (i) => {
        i.seleccionada = false;
      }
    )

    console.log('ionViewDidLoad CobranzaPage');
    this.charge.metpag = 0;
    this.charge.monret = 0;
    this.account_state.cntesp = true;

    this.database.getBadge().then(
      (badge: string) => {
        console.log('divisa: ' + badge);
        this.charge.divisa = badge;
      });

    this.loadBanks();

  }

  loadBanks() {

    this.database.getBanks().then(
      (banks) => {
        banks.forEach(
          (element) => {
            console.log('codban:' + element.codban);
            console.log('desban:' + element.desban);
          });
        this.bank_list = banks;
      });

  }

  changeAccountNumber(codban: any) {

    this.database.getBankDescAndCtaByCode(codban).then(
      (bank) => {
        this.bank_account_number = bank.numcue;
      });

  }

  showMessage(msg: any) {

    this.translate.get(["common.acept", "common.cancel"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                console.log('cancelar');
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
                this.goToDepositoProcesar();
              }
            }
          ]
        });
        alert.present();

      });

  }

  showMessageSpecialTaxPayer(msg: any) {

    this.translate.get(["common.acept", "common.cancel"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                console.log('cancelar');
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
                this.goToComprobanteRetencion();
              }
            }
          ]
        });
        alert.present();

      });

  }

  changeInvoiceChecked(invoice: any) {


    this.translate.get(["charges.invoice_already_charged"], null).subscribe(
      (localizedValues) => {

        if (this.account_state.cntesp == true) {

          if (invoice.seleccionada) {

            console.log("factura seleccionada true changeInvoiceChecked");


            this.database.invoiceAlreadyCharged(invoice.facnum).then(
              (invoices) => {

                let bi = ((invoice.salfac * 100) / 112);
                let tax = (bi * 12) / 100;
                let mt = tax * 0.75;
                tax = tax - mt;
                bi = bi + tax;
                this.invoices_amount = this.invoices_amount + bi;
                this.charge.monret = this.charge.monret + mt;
                console.log("valor de cobnum: " + invoices[0].cobnum);
                console.log("valor de numero actual cobranza: " + this.numero_actual_cobranza);
                if (invoices.length > 0) {

                  if (invoices[0].cobnum != this.numero_actual_cobranza) {
                    invoice.seleccionada = false;
                    this.alertServ.showAlertError(localizedValues["charges.invoice_already_charged"]);
                  }

                }
              },
              error => {
                alert('Error consultando la factura ya asociada a cobranza');
              });



          } else {

            console.log("factura seleccionada false changeInvoiceChecked");

            let bi = ((invoice.salfac * 100) / 112);
            let tax = (bi * 12) / 100;
            let mt = tax * 0.75;
            tax = tax - mt;
            bi = bi + tax;
            this.invoices_amount = this.invoices_amount - bi;
            this.charge.monret = this.charge.monret - mt;

          }


        } else {

          if (invoice.seleccionada) {

            this.database.invoiceAlreadyCharged(invoice.facnum).then(
              (invoices) => {

                this.invoices_amount = this.invoices_amount + invoice.salfac;

                if (invoices.length > 0) {

                  if (invoices[0].cobnum != this.numero_actual_cobranza) {
                    invoice.seleccionada = false;
                    this.alertServ.showAlertError(localizedValues["charges.invoice_already_charged"]);
                  }

                }

              },
              error => {
                alert('Error consultando la factura ya asociada a cobranza');
              });



          } else {

            this.invoices_amount = this.invoices_amount - invoice.salfac;

          }

        }

      });






  }

  changePaymentMethod() {

    if (this.charge.metpag == 1 || this.charge.metpag == 3) {

      this.show_banks_list = true;
      this.show_reference = true;

      this.show_check_a_number = false;
      this.show_check_number = false;

    } else if (this.charge.metpag == 2) {
      this.show_banks_list = true;
      this.show_check_a_number = true;
      this.show_check_number = true;

      this.show_reference = false;
    } else {

      this.show_banks_list = false;
      this.show_check_a_number = false;
      this.show_check_number = false;
      this.show_reference = false;


    }

  }

  makeCharge() {
    this.translate.get(["errors.amount_mand", "errors.refer_mand", "errors.check_a_numb_mand", "errors.check_numb_mand", "errors.doc_numb_mand", "errors.rete_numb_mand", "errors.invoi_balan_less", "errors.bank_mand", "charges.no_selected_invoices", "charges.no_equal_amounts"], null).subscribe(
      (localizedValues) => {
        if (this.charge.montco == '' || this.charge.montco == null) {
          this.alertServ.showAlertError(localizedValues["errors.amount_mand"]);
        } else {
          let show_msg = false;
          switch (this.charge.metpag) {
            case '3':
            case '1':
              if (this.charge.codban == '' || this.charge.codban == null) {
                this.alertServ.showAlertError(localizedValues["errors.bank_mand"]);
                show_msg = true;
              } else {
                if (this.charge.referc == '' || this.charge.referc == null) {
                  this.alertServ.showAlertError(localizedValues["errors.refer_mand"]);
                  show_msg = true;
                }
              }
              break;
            case '2':
              if (this.charge.codban == '' || this.charge.codban == null) {
                this.alertServ.showAlertError(localizedValues["errors.bank_mand"]);
                show_msg = true;
              } else {
                if (this.charge.ctachq == '' || this.charge.ctachq == null) {
                  this.alertServ.showAlertError(localizedValues["errors.check_a_numb_mand"]);
                  show_msg = true;
                } else {
                  if (this.charge.numchq == '' || this.charge.numchq == null) {
                    this.alertServ.showAlertError(localizedValues["errors.check_numb_mand"]);
                    show_msg = true;
                  }
                }
              }
              break;
          }
          if (show_msg == false) {
            this.getSelectedInvoices();
            if (this.account_state.cntesp == true) {
              /*if(this.charge.document_number == '' || this.charge.document_number == null) {
                this.alertServ.showAlertError(localizedValues["errors.doc_numb_mand"]);
              } else {
                if(this.charge.retention_number == '' || this.charge.retention_number == null) {
                  this.alertServ.showAlertError(localizedValues["errors.rete_numb_mand"]);
                } else {
                  if(this.isAmountInvoiceGE()) {
                    this.alertServ.showAlertError(localizedValues["charges.no_equal_amounts"]);
                  } else {
                    this.charge.cntesp = this.account_state.cntesp;
                    this.charge.codcli = this.account_state.codcli;
                    this.navCtrl.push(CobranzaProcesarPage, {invoices:this.selected_invoices, charge:this.charge});
                  }
                }
              }*/

              if (this.isAmountInvoiceGE()) {
                this.showMessageSpecialTaxPayer(localizedValues["charges.no_equal_amounts"]);
              } else {
                this.goToComprobanteRetencion();
              }

            } else {
              /*if(!this.isAmountInvoiceGE()) {
                this.alertServ.showAlertError(localizedValues["errors.invoi_balan_less"]);
              } else {
                if(this.selected_invoices.length == 0){
                  this.showMessage(localizedValues["charges.no_selected_invoices"]);
                }          
                this.charge.cntesp = this.account_state.cntesp;
                this.charge.codcli = this.account_state.codcli;
                this.navCtrl.push(CobranzaProcesarPage, {invoices:this.selected_invoices, charge:this.charge});
              }*/
              if (this.selected_invoices.length == 0) {
                this.showMessage(localizedValues["charges.no_selected_invoices"]);
              } else {
                this.goToCobranzaProcesar();
              }

            }
          }
        }
      });
  }

  isAmountInvoiceGE() {

    return (this.invoices_amount != this.charge.montco);

  }

  getSelectedInvoices() {
    this.selected_invoices = [];
    this.invoices.forEach((i) => {
      if (i.seleccionada) {
        this.selected_invoices.push(i);
      }
    });
  }

  goToCobranzaProcesar() {

    this.charge.cntesp = this.account_state.cntesp;
    //this.charge.codcli = this.account_state.codcli;
    this.database.updateChargePhaseStatus(this.numero_actual_cobranza, 1, 1, this.charge).then(
      (res) => {
        this.database.deleteChargeInvoices(this.numero_actual_cobranza).then(
          (res) => {
            //this.alertServ.showAlert('La cobranza fue almacenada con exito');
            this.database.createChargeInvoices(this.numero_actual_cobranza, this.selected_invoices).then(
              (res) => {
                //this.alertServ.showAlert('La cobranza fue almacenada con exito');                        
                this.navCtrl.push(CobranzaProcesarPage, { invoices: this.selected_invoices, charge: this.charge, indexes: this.indexes });
              },
              (error) => {
                this.alertServ.showAlertError('Las facturas de la cobranza no fueron insertadas ');
              });
          },
          (error) => {
            this.alertServ.showAlertError('Las facturas de la cobranza no fueron removidas ');
          });

      },
      (error) => {
        this.alertServ.showAlertError('La cobranza cabecera no se actualizo ');
      });

  }

  goToComprobanteRetencion() {

    this.charge.cntesp = this.account_state.cntesp;
    //this.charge.codcli = this.account_state.codcli;
    this.database.updateChargePhaseStatus(this.numero_actual_cobranza, 1, 1, this.charge).then(
      (res) => {
        this.database.deleteChargeInvoices(this.numero_actual_cobranza).then(
          (res) => {
            //this.alertServ.showAlert('La cobranza fue almacenada con exito');
            this.database.createChargeInvoices(this.numero_actual_cobranza, this.selected_invoices).then(
              (res) => {
                //this.alertServ.showAlert('La cobranza fue almacenada con exito');                          
                this.navCtrl.push(ComprobanteRetencionPage, { invoices: this.selected_invoices, charge: this.charge, indexes: this.indexes });
              },
              (error) => {
                this.alertServ.showAlertError('Las facturas de la cobranza no fueron insertadas ');
              });
          },
          (error) => {
            this.alertServ.showAlertError('Las facturas de la cobranza no fueron removidas ');
          });
      },
      (error) => {
        this.alertServ.showAlertError('La cobranza cabecera no se actualizo ');
      });

  }

  /*getInvoicesAlreadyCharged() {
    this.database.getChargeInvoices(this.numero_actual_cobranza, 1, 1).then(
      (invoices) => {
        this.invoices_already_charged = invoices;
        this.checkInvoices();
      },
      error => {
        alert('Error obteniendo las facturas ya asociadas a cobranza');
      });
  }*/

  checkInvoices() {

    this.charge.monret = 0;

    /*this.invoices_already_charged.forEach(
      (f) => {
        console.log("factura asociada a cobranza: " + f.facnum);
      }
    )*/

    this.invoices.forEach(
      (i) => {
        if (this.invoices_already_charged.indexOf(i.facnum) > -1) {
          i.seleccionada = true;
          console.log("factura seleccionada true");
          //this.invoices_amount = this.invoices_amount + i.salfac;
        } else {
          i.seleccionada = false;
          console.log("factura seleccionada false");
        }
      }
    )

    //this.invoices_amount = this.invoices_amount - this.charge.monret;

  }

  /**********************cobranzas****************************/
  init() {

    this.loadBanks();
    this.changeAccountNumber(this.deposit.codban);
    this.getPrintedCharges();

  }

  getAllDepositCharges() {
    this.database.getAllDepositCharges().then(
      (charges) => {
        this.all_charges = charges;
        //this.checkCharges();
      },
      error => {
        alert('Error obteniendo todos los depositos');
      });
  }

  /*getChargesFiltered() {

    this.printed_cobranzas.forEach(
      (i) => {
        this.database.getChargeInvoices(i.cobnum).then(
          (invoices) => {
            this.filtered_cobranzas.concat(invoices);
            console.log('facturas asociadas a cobranzas obtenidas');
            //this.checkInvoices();
          },
          error => {
            console.log('Error obteniendo las facturas ya asociadas a cobranza');
          });
      })

    console.log('filtered cobranzas size: ' + this.filtered_cobranzas.length);
    this.checkCharges();
    

  }*/

  getChargesFiltered() {

    for (let index = 0; index < this.printed_cobranzas.length; index++) {
      this.database.getChargeInvoices(this.printed_cobranzas[index].cobnum).then(
        (invoices) => {
         
          this.filtered_cobranzas = this.filtered_cobranzas.concat(invoices);
          console.log('cobranzas asociadas a facturas obtenidas');
          console.log('charges size: ' + invoices.length);
          if (index == this.printed_cobranzas.length - 1) {
            console.log('filtered cobranzas size: ' + this.filtered_cobranzas.length);
            this.checkCharges();           
          }
          //this.checkInvoices();
        },
        error => {
          console.log('Error obteniendo las cobranzas ya asociadas a factura');
        });
      
    }    

  }

  checkCharges() {

    for (let index = 0; index < this.filtered_cobranzas.length; index++) {
      const i = this.filtered_cobranzas[index];
      this.database.getChargeDeposit(i.cobnum, i.facnum, i.coblin).then(
        (deposit) => {
          if (deposit != null) {
            if (this.numero_actual_deposito == deposit.depnum) {               
              this.available_charges.push(i);
            }
          } else {              
            this.available_charges.push(i);
            console.log("cobranza no asociada a deposito");
          }
          if (index == this.filtered_cobranzas.length - 1) {
            console.log('available charges size: ' + this.available_charges.length);
            this.changeCashAndCheck();           
          }
        },
        error => {
          alert('Error obteniendo el depnum de la cobranza');
        });
    }  

  }

  /*checkCharges(){      

    this.printed_cobranzas.forEach(
      (i) => {
        if (this.all_charges.indexOf(i.cobnum) > -1) {
          this.database.getChargeDeposit(i.cobnum).then(
            (depnum) => {
              if(this.numero_actual_deposito == depnum){
                this.available_charges.push(i);
              }              
            },
            error => {
              alert('Error obteniendo el depnum de la cobranza');
            });          
          console.log("cobranza asociada a deposito");
          //this.invoices_amount = this.invoices_amount + i.salfac;
        } else {
          this.available_charges.push(i);
          console.log("cobranza no asociada a deposito");
        }         
      }
    )
    
    this.changeCashAndCheck();
        
  }*/

  getPrintedCharges() {
    this.database.getChargesOfClient(this.cod_cli, 6, 1).then(
      (charges) => {
        this.printed_cobranzas = charges;
        this.getChargesFiltered();
      },
      error => {
        alert('Error obteniendo las cobranzas impresas');
      });
  }

  changeCashAndCheck() {

    if (this.cash_and_check) {

      let temp = [];

      this.available_charges.forEach((c) => {
        if (c.metpag == 0 || c.metpag == 2) {
          c.seleccionada = true;
          temp.push(c);
        }
      }
      )

      this.charges = temp;

    } else {

      let temp = [];

      this.available_charges.forEach((c) => {
        if (c.metpag == 1 || c.metpag == 3) {
          c.seleccionada = true;
          temp.push(c);
        }
      }
      )

      this.charges = temp;

    }

  }

  getSelectedCharges() {
    this.selected_charges = [];
    this.charges.forEach((i) => {
      if (i.seleccionada) {
        this.selected_charges.push(i);
      }
    });
  }

  makeDeposit() {

    this.translate.get(["errors.amount_mand", "errors.refer_mand", "charges.has_charges", "errors.check_a_numb_mand", "errors.check_numb_mand", "errors.doc_numb_mand", "errors.rete_numb_mand", "errors.invoi_balan_less", "errors.bank_mand", "charges.no_selected_invoices", "charges.no_equal_amounts"], null).subscribe(
      (localizedValues) => {
        this.getSelectedCharges();

        if (!this.cash_and_check) {
          if (this.deposit.numref == '' || this.deposit.numref == null) {
            this.alertServ.showAlertError(localizedValues["errors.refer_mand"]);
          } else {
            if (this.selected_charges.length > 0) {
              this.showMessage(localizedValues["charges.has_charges"]);
            } else {
              this.goToDepositoProcesar();
            }
          }
        } else {
          this.goToDepositoProcesar();
        }

      });
  }

  showMessageforDeposit(msg: any) {

    this.translate.get(["common.acept", "common.cancel"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                console.log('cancelar');
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
                this.goToDepositoProcesar();
              }
            }
          ]
        });
        alert.present();

      });

  }

  goToDepositoProcesar() {

    this.database.updateDepositPhaseStatus(this.numero_actual_deposito, 1, 1, this.deposit).then(
      (res) => {
        this.database.deleteDepositCharges(this.numero_actual_deposito).then(
          (res) => {
            //this.alertServ.showAlert('La cobranza fue almacenada con exito');
            this.database.createDepositCharges(this.numero_actual_deposito, this.selected_charges).then(
              (res) => {
                //this.alertServ.showAlert('La cobranza fue almacenada con exito');                        
                this.navCtrl.push(DepositoProcesarPage, { charges: this.selected_charges, deposit: this.deposit, indexes: this.indexes, bank_account_number: this.bank_account_number });
              },
              (error) => {
                this.alertServ.showAlertError('Las facturas de la cobranza no fueron insertadas ');
              });
          },
          (error) => {
            this.alertServ.showAlertError('Las facturas de la cobranza no fueron removidas ');
          });

      },
      (error) => {
        this.alertServ.showAlertError('La cobranza cabecera no se actualizo ');
      });

  }

  /**********************cobranzas****************************/



}
