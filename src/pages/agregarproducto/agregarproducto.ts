import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { DetallarproductoPage } from '../detallarproducto/detallarproducto';
import { AlertsService } from '../../providers/alerts.service';
import { AppConstants } from '../../providers/app-constants';
import { DetallarproductoFacturaPage } from "../detallarproducto-factura/detallarproducto-factura";

/*
  Generated class for the Agregarproducto page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-agregarproducto',
  templateUrl: 'agregarproducto.html'

})

export class AgregarproductoPage {
  productos_prio: any[] = [];
  codigo_ordtra:string = '';
  codigo_cliente:string = '';
  productos: any[] = [];
  productos_vista: any[] = [];
  productos_copia: any[] = [];
  productos_escogidos: any[] = [];
  descripcion_producto: string = '';
  producto_elegido: any = '';
  quantity: number;
  suma_total: any = {};
  numero_actual_pedido: string = '';
  todos: boolean;
  cont: number = 0;
  offset: number = 0;
  lista_cod_prio: any[] = [];
  //allProdLength:number;
  filter: string = "";
  from: number;
  codcli: string;
  cdcplt: string;
  divisa: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public view: ViewController,
    public database: DbService,
    private modalCtrl: ModalController,
    private alertServ: AlertsService
  ) {

    if (this.navParams.data) {
      this.productos_escogidos = this.navParams.data.productos_escogidos;
      this.suma_total = this.navParams.data.suma_total;
      this.numero_actual_pedido = this.navParams.data.numero_actual_pedido;
      this.from = this.navParams.data.from;
      this.codigo_ordtra = this.navParams.data.codigo_ordtra;
      this.codigo_cliente = this.navParams.data.codigo_cliente;
    }

    // this.getAllProducts();

    if (this.from == 1) {

      this.getAllClientsProductsOrder();

    }

    if (this.from == 2) {

      this.getAllClientsProductsInvoice();

    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FacturaPage');
    this.getCurrency();
  }

  getCurrency(){
    this.database.getBadge().then(
      (badge: string) => {
        console.log('divisa: ' + badge);
        this.divisa = badge;
      });
  }

  getAllProducts() {
    this.database.openDatabase().then(
      () => {
        this.database.getAllProducts()
          .then(productos => {
            this.productos_copia = productos;
            this.productos = this.productos_copia;
          })
      });
  }

  getAllClientsProductsOrder() {
    this.database.openDatabase().then(
      () => {
        this.database.getClientByOrder(this.numero_actual_pedido)
          .then(codcli => {
            this.codcli = codcli;
            console.log('codcli:' + codcli);
            this.database.getCdcpltFromClient(codcli)
              .then(cdcplt => {
                console.log('cdcplt:' + cdcplt);
                this.cdcplt = cdcplt;
                this.database.getProductsByClient(codcli, this.offset, cdcplt)
                  .then(object => {
                    this.productos_prio = object.productos;
                    this.productos_copia = object.productos;
                    if (object.todos == 'true') {
                      console.log('todos');
                      for (let index = 0; index < object.productos.length; index++) {
                        this.lista_cod_prio.push(object.productos[index].codpro);
                      }

                      // this.database.getOldProductsLength(this.lista_cod_prio)
                      // .then(response => {
                      //   this.allProdLength = response;
                      // });

                      this.database.getAllClientsProducts(this.lista_cod_prio.slice(0), this.offset, cdcplt)
                        .then(response => {
                          // this.productos_copia = response;
                          // this.productos = this.productos_copia;

                          // let aux = this.cont + 4000;

                          for (let i = 0; i < response.length; i++) {
                            this.productos_vista.push(response[i]);
                          }
                        });
                    } else {

                      this.productos_vista = [];
                      this.database.openDatabase().then(
                        () => {
                          this.database.getAllClientsProducts(this.lista_cod_prio.slice(0), this.productos_vista.length, cdcplt)
                            .then(response => {

                              for (let i = 0; i < response.length; i++) {
                                this.productos_vista.push(response[i]);
                              }
                            });
                        });

                    }
                  });
              });
          });
      });
  }

  getAllClientsProductsInvoice() {
    this.database.openDatabase().then(
      () => {
        this.database.getClientByInvoice(this.numero_actual_pedido)
          .then(codcli => {
            this.codcli = codcli;
            this.database.getCdcpltFromClient(codcli)
              .then(
                cdcplt => {
                  this.cdcplt = cdcplt;
                  this.database.getProductsByClient(codcli, this.offset, cdcplt)
                    .then(object => {
                      this.productos_prio = object.productos;
                      if (object.todos == 'true') {
                        console.log('todos');
                        for (let index = 0; index < object.productos.length; index++) {
                          this.lista_cod_prio.push(object.productos[index].codpro);
                        }

                        // this.database.getOldProductsLength(this.lista_cod_prio)
                        // .then(response => {
                        //   this.allProdLength = response;
                        // });

                        this.database.getAllClientsProducts(this.lista_cod_prio.slice(0), this.offset, cdcplt)
                          .then(response => {
                            // this.productos_copia = response;
                            // this.productos = this.productos_copia;

                            // let aux = this.cont + 4000;

                            for (let i = 0; i < response.length; i++) {
                              this.productos_vista.push(response[i]);
                            }
                          });
                      }
                    });
                }
              );
          });
      });
  }

  // doInfinite(infiniteScroll) {
  //   console.log('Begin async operation');

  //   this.database.openDatabase().then(
  //     () => {
  //       if (this.filter == null || this.filter == "") {
  //         this.database.getAllClientsProducts(this.lista_cod_prio.slice(0), this.productos_vista.length, this.cdcplt)
  //           .then(response => {
  //             for (let i = 0; i < response.length; i++) {
  //               this.productos_vista.push(response[i]);
  //             }

  //             infiniteScroll.complete();
  //             // if(this.productos_vista.length >= this.allProdLength){
  //             //   infiniteScroll.enable(false);
  //             // }
  //           });
  //       } else {
  //         this.database.getAllClientsProductsByfilter(this.lista_cod_prio.slice(0), this.productos_vista.length, this.filter, this.cdcplt)
  //           .then(response => {
  //             for (let i = 0; i < response.length; i++) {
  //               this.productos_vista.push(response[i]);
  //             }

  //             infiniteScroll.complete();
  //             // if(this.productos_vista.length >= this.allProdLength){
  //             //   infiniteScroll.enable(false);
  //             // }
  //           });
  //       }
  //     });
  // }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    this.database.openDatabase().then(
      () => {
        if (this.filter == null || this.filter == "") {

          this.database.getProductsByClient(this.codcli, this.productos_prio.length, this.cdcplt)
            .then(object => {
              this.productos_prio = this.productos_prio.concat(object.productos);
              //this.productos_copia = object.productos;
              infiniteScroll.complete();
            });

        } else {

          this.database.getProductsByClientByFilter(this.codcli, this.productos_prio.length, this.filter, this.cdcplt)
            .then(object => {
              this.productos_prio = this.productos_prio.concat(object.productos);
              //this.productos_copia = object.productos;
              infiniteScroll.complete();
            });
        }
      });
  }

  filterItems_test(ev: any) {

    this.productos_prio = this.productos_copia;

    this.filter = ev.target.value;

    // if the value is an empty string don't filter the items
    if (this.filter && this.filter.trim() != '') {
      this.productos_prio = this.productos_prio.filter((item) => {
        return (item.despro.toLowerCase().indexOf(this.filter.toLowerCase()) > -1 || item.codpro.includes(this.filter));
      })
    }
  }

  // filterItems(ev: any){
  //   this.filter = ev.target.value;
  //   if (this.filter) {
  //     if(this.filter.trim() != '') {
  //       this.productos_vista = [];
  //       this.database.openDatabase().then(
  //         () => {
  //           this.database.getAllClientsProductsByfilter(this.lista_cod_prio.slice(0), this.productos_vista.length, this.filter, this.cdcplt)
  //           .then(response => {

  //             for(let i = 0; i < response.length; i++){
  //               this.productos_vista.push(response[i]);
  //             }
  //           });
  //         });
  //     } else {
  //       this.productos_vista = [];
  //       this.database.openDatabase().then(
  //         () => {
  //           this.database.getAllClientsProducts(this.lista_cod_prio.slice(0), this.productos_vista.length, this.cdcplt)
  //           .then(response => {

  //             for(let i = 0; i < response.length; i++){
  //               this.productos_vista.push(response[i]);
  //             }
  //           });
  //         });
  //     }
  //   } else {
  //     this.productos_vista = [];
  //       this.database.openDatabase().then(
  //         () => {
  //           this.database.getAllClientsProducts(this.lista_cod_prio.slice(0), this.productos_vista.length, this.cdcplt)
  //           .then(response => {

  //             for(let i = 0; i < response.length; i++){
  //               this.productos_vista.push(response[i]);
  //             }
  //           });
  //         });
  //   }
  // }

  filterItems(ev: any) {
    this.filter = ev.target.value;
    if (this.filter) {
      if (this.filter.trim() != '') {
        this.productos_prio = [];
        this.database.getProductsByClientByFilter(this.codcli, this.productos_prio.length, this.filter, this.cdcplt)
          .then(object => {
            this.productos_prio = object.productos;
            //this.productos_copia = object.productos;              
          });
      } else {
        this.productos_prio = [];
        this.database.getProductsByClient(this.codcli, this.productos_prio.length, this.cdcplt)
        .then(object => {
          this.productos_prio = object.productos;
          //this.productos_copia = object.productos;          
        });
      }
    } else {
      this.productos_prio = [];
        this.database.getProductsByClient(this.codcli, this.productos_prio.length, this.cdcplt)
        .then(object => {
          this.productos_prio = object.productos;
          //this.productos_copia = object.productos;          
        });
    }
  }


  itemSelected(event: any, producto: any) {

    this.producto_elegido = producto;

    ////order////

    // this.database.openDatabase().then(
    //   () => {
    //     this.database.getCdcpltFromClient(this.codcli).then(
    //       (cdcplt: string) => {
    //         this.database.getProductPriceByCode(cdcplt, this.producto_elegido.codpro).then(
    //           (precio) => {
    // this.producto_elegido.prepro = precio;
    if (this.from == 1) {
      this.navCtrl.push(DetallarproductoPage, { productos_escogidos: this.productos_escogidos, suma_total: this.suma_total, numero_actual_pedido: this.numero_actual_pedido, agregar_index: this.view.index, producto_elegido: this.producto_elegido });
    }
    ////invoice////
    if (this.from == 2) {
      this.navCtrl.push(DetallarproductoFacturaPage, { productos_escogidos: this.productos_escogidos, suma_total: this.suma_total, numero_actual_pedido: this.numero_actual_pedido, agregar_index: this.view.index, producto_elegido: this.producto_elegido, codigo_ordtra: this.codigo_ordtra, codigo_cliente: this.codigo_cliente });
    }
    //         }
    //       );
    //     }
    //   );
    // });
  }

  dismissPopUp() {

    this.view.dismiss();

  }






}
