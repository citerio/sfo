import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController } from 'ionic-angular';
import { DbService } from "../../providers/db-service";
import { TranslateService } from "ng2-translate";

/*
  Generated class for the DetallarproductoFactura page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-detallarproducto-factura',
  templateUrl: 'detallarproducto-factura.html'
})
export class DetallarproductoFacturaPage {

  quantity:number;
  presentacion:string;
  codigo_ordtra:string = '';
  codigo_cliente:string = '';
  nombre_producto:string='';
  codigo_producto:string='';
  precio_producto:number;
  linea_producto:number;
  coduni_producto:string='';
  presentaciones:any[] = [];
  producto_elegido:any;
  productos_escogidos:any[] = [];
  suma_total:any = {};
  numero_actual_pedido:string = '';
  deshabilitar_presentacion:boolean = false;
  
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public view: ViewController, public database: DbService, private translate: TranslateService) {

    if(this.navParams.data){
      
      this.productos_escogidos = this.navParams.data.productos_escogidos;
      this.producto_elegido = this.navParams.data.producto_elegido;
      this.codigo_ordtra = this.navParams.data.codigo_ordtra;
      this.codigo_cliente = this.navParams.data.codigo_cliente;
      
      if(this.producto_elegido.despro && this.producto_elegido.codpro && (this.producto_elegido.prepro != null)){

        this.nombre_producto = this.producto_elegido.despro;
        this.codigo_producto = this.producto_elegido.codpro;

        this.precio_producto = this.producto_elegido.prepro;
        this.presentaciones = JSON.parse(this.producto_elegido.codunv);
        //alert('PRESENTACIONES: '+ JSON.stringify(this.presentaciones));
        //this.presentaciones.push(this.producto_elegido.codunv);         
        this.presentacion = this.presentaciones[0].coduni;

      }

      if(this.producto_elegido.codart && this.producto_elegido.descar && typeof this.producto_elegido.pedpru !== 'undefined'){

        this.nombre_producto = this.producto_elegido.descar;
        this.codigo_producto = this.producto_elegido.codart;
        this.precio_producto = this.producto_elegido.pedpru;
        this.quantity = this.producto_elegido.cancar;
        this.getCodeDescription(this.codigo_producto, this.producto_elegido.coduni);
        //this.getProductPresentations(this.producto_elegido.coduni);

        this.deshabilitar_presentacion = true;

      } 

      this.suma_total = this.navParams.data.suma_total;
      this.numero_actual_pedido = this.navParams.data.numero_actual_pedido;

    }

  }

  addToBasket(){

    if(this.producto_elegido){

      if(this.quantity && this.presentacion){

        let precio_unitario = this.precio_producto;
        let precio_total = this.quantity * precio_unitario;

        let producto_final = {'orno':this.numero_actual_pedido, 'codcli':this.codigo_cliente, 'ordtra':this.codigo_ordtra, 'pedlin':this.linea_producto, 'codart':this.codigo_producto, 'descar':this.nombre_producto, 'coduni': this.presentacion, 'cancar':this.quantity, 'pedpru':this.precio_producto, 'pedprt':precio_total};
        
        this.database.openDatabase().then(
          () => {
            this.database.createInvoiceLine(producto_final, this.deshabilitar_presentacion)
            .then((data) => {
              //this.suma_total.monto = this.suma_total.monto + precio_total;
              //this.getOrder(this.numero_actual_pedido);

              if(this.navParams.data.agregar_index){

                  this.navCtrl.remove(this.navParams.data.agregar_index);

              }              
              
              this.navCtrl.pop();        
              //this.dismissPopUp();
              }
            )
            .catch((error) => {

              alert('ocurrio un error al insertar: '+ JSON.stringify(error));}

            )
          });
        
        

      }else{

        this.showMessage('Ingrese cantidad y presentacion');

      }

    }

  }

  showMessage(msg: any){

    let alert = this.alertCtrl.create({      
      message: msg,
      buttons: [
        {
          text: 'Aceptar',
          handler: () =>{
            console.log('aceptar');
          }
        }
      ]
    });
    alert.present();

  }

  getCodeDescription(codart: any, coduni: any){
    this.database.openDatabase().then(
      () => {
        this.database.getCodeDescription(codart, coduni)
        .then(presentacion => {
          this.presentaciones.push(presentacion);     
          this.presentacion = this.presentaciones[0].coduni;
        })
        .catch((error) => {

              alert('ocurrio un error al insertar: '+ JSON.stringify(error));}

        )
      });
    
  }

}
