import { Component, ViewChild } from '@angular/core';
import { App, NavController, NavParams, AlertController, Content } from 'ionic-angular';
import { DbService } from "../../providers/db-service";
import { DbChangesService } from "../../providers/db-changes-service";
import { TranslateService } from "ng2-translate";
import { CobranzaProcesarPage } from "../cobranza-procesar/cobranza-procesar";
import { AlertsService } from "../../providers/alerts.service";
import { ComprobanteRetencionPage } from '../comprobante-retencion/comprobante-retencion';
import { CobranzaPage } from '../cobranza/cobranza';
import { DepositoPage } from '../deposito/deposito';
import { CrearCobranzaPage } from '../crear-cobranza/crear-cobranza';
import { CobranzaClosedPage } from '../cobranza-closed/cobranza-closed';
import { CobranzaSyncedPrintedPage } from '../cobranza-synced-printed/cobranza-synced-printed';


/*
  Generated class for the CobranzaMain page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cobranza-main',
  templateUrl: 'cobranza-main.html'
})
export class CobranzaMainPage {
  @ViewChild(Content) content: Content;
  submitted = false;
  /**********************cobranzas****************************/
  open_cobranzas: any[] = [];
  closed_cobranzas: any[] = [];
  synced_cobranzas: any[] = [];
  printed_cobranzas: any[] = [];
  codigo_cliente: string = '';

  selected_invoices: any[] = [];
  invoices: any[] = [];
  type: string = 'statement';
  deshabilitar_cobrar: boolean = true;
  charge: any = {};
  show_banks_list: any = false;
  show_reference: any = false;
  show_check_a_number: any = false;
  show_check_number: any = false;
  segment_title: string;
  account_state: any;
  bank_list: any[] = [];
  cobfas: string = 'open';
  invoices_amount: any = 0;
  invoice_selected: any = false;
  motivos: any[] = [];
  /**********************cobranzas****************************/

  /**********************depositos****************************/
  open_depositos: any[] = [];
  closed_depositos: any[] = [];
  synced_depositos: any[] = [];
  printed_depositos: any[] = [];
  deposit: any = {};
  depfas: string = 'open';
  fase_depositos: string = 'open';
  /**********************depositos****************************/

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public app: App,
    public navParams: NavParams,
    public database: DbService,
    public dbChangesService: DbChangesService,
    private translate: TranslateService,
    private alertServ: AlertsService) {


    this.dbChangesService.getSync().subscribe(
      (res: boolean) => {
        console.log('exito recibiendo dato: ' + res);
        if (res === true) {
          console.log('exito recibiendo dato: ' + res);
          this.getSyncedCharges();
          this.getPrintedCharges();
          //this.getSyncedDeposits();
          //this.getPrintedDeposits();
        }
      },
      (error: any) => {
        console.log('error recibiendo dato: ' + error);
      });



    this.account_state = this.navParams.get('estcli');

    this.init();
  }

  init() {

    if (this.navParams.data.cliente.codcli) {

      this.codigo_cliente = this.navParams.data.cliente.codcli;
      this.getAccounStatement();

    }


    /*this.invoices = this.account_state.lisfac
    this.invoices.forEach(
      (i) => {
        i.seleccionada = false;
      }
    )*/

    this.translate.get(["charges.account_statement"], null).subscribe(
      (localizedValues) => {

        if (this.type == "statement") {

          this.segment_title = localizedValues["charges.account_statement"];

        }

      });

    console.log('ionViewDidLoad CobranzaMainPage');
    this.charge.metpag = 0;
    this.charge.monret = 0;
    this.charge.montco = 0;
    this.account_state.cntesp = true;
    this.charge.cntesp = true;
    this.charge.codcli = this.codigo_cliente;
    this.deposit.codcli = this.codigo_cliente;
    //this.getSelectedSucxcomp();
    this.deposit.efeche = true;
    this.deposit.numref = "";
    // this.open_cobranzas.push({'id':'COB4564', 'monto':'450.000,00', 'metodo':'efectivo', 'fecha':'7/12/2017, 4:30:30 PM'});
    // this.open_cobranzas.push({'id':'COB4570', 'monto':'566.000,00', 'metodo':'cheque', 'fecha':'7/12/2017, 4:30:30 PM'});
    // this.open_cobranzas.push({'id':'COB4800', 'monto':'1.000.000,00', 'metodo':'efectivo', 'fecha':'7/12/2017, 4:30:30 PM'});
    // this.invoices.push({'numero':'FAC4570', 'fecha':'7/12/2017, 4:30:30 PM', 'dias_vencimiento':'60', 'saldo':'650.000,00', 'seleccionada':false });
    // this.invoices.push({'numero':'FAC6800', 'fecha':'7/12/2017, 4:30:30 PM', 'dias_vencimiento':'30', 'saldo':'75.000,00', 'seleccionada':false });
    // this.invoices.push({'numero':'FAC9570', 'fecha':'7/12/2017, 4:30:30 PM', 'dias_vencimiento':'90', 'saldo':'650.000,00', 'seleccionada':false  });
    this.database.getBadge().then(
      (badge: string) => {
        console.log('divisa: ' + badge);
        this.charge.divisa = badge;
      });

    this.database.getBanks().then(
      (banks) => {
        banks.forEach(
          (element) => {
            console.log('codban:' + element.codban);
            console.log('desban:' + element.desban);
          });
        this.bank_list = banks;
      });

    this.getRemoveOrderReasons();

  }

  ionViewWillEnter() {
    this.getOpenCharges();
    this.getClosedCharges();
    this.getPrintedCharges();
    this.getSyncedCharges();
    //this.getOpenDeposits();
    //this.getClosedDeposits();
    //this.getPrintedDeposits();
    //this.getSyncedDeposits();
  }

  /**********************cobranzas****************************/
  getOpenCharges() {
    this.database.getChargesOfClient(this.codigo_cliente, 1, 1).then(
      (charges) => {
        this.open_cobranzas = charges;
      },
      error => {
        alert('Error obteniendo las cobranzas abiertas');
      });
  }

  getClosedCharges() {
    this.database.getChargesOfClient(this.codigo_cliente, 3, 1).then(
      (charges) => {
        this.closed_cobranzas = charges;
      },
      error => {
        alert('Error obteniendo las cobranzas cerradas');
      });
  }

  getPrintedCharges() {
    this.database.getChargesOfClient(this.codigo_cliente, 6, 1).then(
      (charges) => {
        this.printed_cobranzas = charges;
      },
      error => {
        alert('Error obteniendo las cobranzas impresas');
      });
  }

  getSyncedCharges() {
    this.database.getChargesOfClient(this.codigo_cliente, 4, 1).then(
      (charges) => {
        this.synced_cobranzas = charges;
      },
      error => {
        alert('Error obteniendo las cobranzas sincronizadas');
      });
  }
  /**********************cobranzas****************************/

  /**********************depositos****************************/
  getOpenDeposits() {
    this.database.getDeposits(this.codigo_cliente, 1, 1).then(
      (deposits) => {
        this.open_depositos = deposits;
      },
      error => {
        alert('Error obteniendo los depositos abiertas');
      });
  }

  getClosedDeposits() {
    this.database.getDeposits(this.codigo_cliente, 3, 1).then(
      (deposits) => {
        this.closed_depositos = deposits;
      },
      error => {
        alert('Error obteniendo los depositos cerrados');
      });
  }

  getPrintedDeposits() {
    this.database.getDeposits(this.codigo_cliente, 6, 1).then(
      (deposits) => {
        this.printed_depositos = deposits;
      },
      error => {
        alert('Error obteniendo los depositos impresos');
      });
  }

  getSyncedDeposits() {
    this.database.getDeposits(this.codigo_cliente, 4, 1).then(
      (deposits) => {
        this.synced_depositos = deposits;
      },
      error => {
        alert('Error obteniendo los depositos sincronizados');
      });
  }

  goToDeposit() {
    this.database.createDeposit(this.deposit).then(
      (res) => {
        //this.alertServ.showAlert('La cobranza fue almacenada con exito');
        this.navCtrl.push(DepositoPage, { deposit: this.deposit });
      },
      (error) => {
        this.alertServ.showAlertError('El deposito no pudo ser creada');
      });
  }

  goToExistingDeposit(deposito: any) {

    deposito.codcli = this.account_state.codcli;
    this.navCtrl.push(DepositoPage, { deposit: deposito });

  }

  getSelectedSucxcomp() {
    this.database.openDatabase().then(
      () => {
        this.database.getSelectedSucxcomp().then(
          (res: string) => {
            console.log('sucxcom: ' + res);
            if (res != '' && res != null) {
              console.log('no es nula');
              let ids = res.split(" ");
              this.database.getDealerBankByCode(ids[1]).then(
                (codban) => {
                  this.deposit.codban = codban;
                }
              )
                .catch((error) => {
                  this.alertServ.showAlertError('error obteniendo el codigo de banco-distribuidora');
                })
            }
          });
      });
  }
  openRemoveDepositConfirmation(deposit: any) {
    this.translate.get(["deposits.delete_deposit", "common.choose_reason", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["deposits.delete_deposit"]);
        alert.setMessage(localizedValues["common.choose_reason"]);

        this.motivos.forEach((s) => {
          console.log('s.codmot: ' + s.codmot);
          alert.addInput({
            type: 'radio',
            label: s.desmot,
            value: s.codmot,
            checked: false
          });

        });

        alert.addButton(localizedValues["common.cancel"]);

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if (data !== '') {
              console.log('data: ' + data);
              this.deleteDeposit(deposit, data);


            }

          }
        });

        alert.present();
      });
  }

  deleteDeposit(deposit: any, codmot: string) {

    this.translate.get(["deposits.success_deleted", "deposits.failure_deleted"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
          () => {
            this.database.updateDepositPhaseStatus(deposit.depnum, 5, 3, deposit)
              .then((data) => {

                this.database.deleteDepositCharges(deposit.depnum).then(
                  (res) => {

                    this.showMessage(localizedValues["deposits.success_deleted"]);

                    switch (deposit.depfas) {
                      case 1:
                        this.getOpenDeposits();
                        break;
                      case 2:
                        this.getOpenDeposits();
                        break;
                      case 3:
                        this.getClosedDeposits();
                        break;
                      case 4:
                        this.getSyncedDeposits();
                        break;
                      case 6:
                        this.getPrintedDeposits();
                        break;
                    }

                  },
                  (error) => {
                    this.alertServ.showAlertError('Las facturas de la cobranza no fueron removidas ');
                  });

              })
              .catch((error) => { this.showMessage(localizedValues["deposits.failure_deleted"]); });
          });

      });

  }


  /**********************depositos****************************/

  openDeleteConfirmation(pedido: any) {

    this.translate.get(["common.confirm", "main_order.delete_confirm", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["main_order.delete_confirm"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                console.log('cancelar');
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                //this.deleteProduct(pedido);
              }
            }
          ]
        });
        alert.present();


      });




  }


  showMessage(msg: any) {

    this.translate.get(["common.acept", "common.cancel"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                console.log('cancelar');
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
              }
            }
          ]
        });
        alert.present();

      });

  }

  showMessageSpecialTaxPayer(msg: any) {

    this.translate.get(["common.acept", "common.cancel"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                console.log('cancelar');
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
                this.goToComprobanteRetencion();
              }
            }
          ]
        });
        alert.present();

      });

  }

  /*changeInvoiceChecked(invoice: any){

    if(this.account_state.cntesp == true){

      if(invoice.seleccionada){

        //bi base imponible
        //tax iva
        //mt monto retenido

        let bi = ((invoice.salfac * 100) / 112);
        let tax = (bi * 12) / 100;
        let mt = tax * 0.75;
        tax = tax - mt;
        bi = bi + tax;
        this.invoices_amount = this.invoices_amount + bi;
        this.charge.retention_amount = this.charge.retention_amount + mt;
  
      }else{
  
        let bi = ((invoice.salfac * 100) / 112);
        let tax = (bi * 12) / 100;
        let mt = tax * 0.75;
        tax = tax - mt;
        bi = bi + tax;
        this.invoices_amount = this.invoices_amount - bi;
        this.charge.retention_amount = this.charge.retention_amount - mt;
  
      }


    }else {

      if(invoice.seleccionada){

        this.invoices_amount = this.invoices_amount + invoice.salfac;
  
      }else{
  
        this.invoices_amount = this.invoices_amount - invoice.salfac;
  
      }     

    }    

  }

  /*changeInvoiceChecked(){

    let counter = 0;

    this.account_state.lisfac.forEach((i)=>{

      if(i.seleccionada){

        counter++;

      }

    })

    if(counter > 0){

      this.deshabilitar_cobrar = false;

    }else{

      this.deshabilitar_cobrar = true;


    }

  }*/

  /*changePaymentMethod(){

    if(this.charge.payment_method == 1 || this.charge.payment_method == 3){

     this.show_banks_list = true;
     this.show_reference =  true;

     this.show_check_a_number =  false;
     this.show_check_number = false;

    }else if(this.charge.payment_method == 2){
      this.show_banks_list = true;
      this.show_check_a_number =  true;
      this.show_check_number = true;   

      this.show_reference =  false;
    }else{

      this.show_banks_list = false;
      this.show_check_a_number =  false;
      this.show_check_number = false;
      this.show_reference =  false;


    }

  }*/

  makeCharge() {
    this.translate.get(["errors.amount_mand", "errors.refer_mand", "errors.check_a_numb_mand", "errors.check_numb_mand", "errors.doc_numb_mand", "errors.rete_numb_mand", "errors.invoi_balan_less", "errors.bank_mand", "charges.no_selected_invoices", "charges.no_equal_amounts"], null).subscribe(
      (localizedValues) => {
        if (this.charge.charge_amount == '' || this.charge.charge_amount == null) {
          this.alertServ.showAlertError(localizedValues["errors.amount_mand"]);
        } else {
          let show_msg = false;
          switch (this.charge.payment_method) {
            case '3':
            case '1':
              if (this.charge.bank_list == '' || this.charge.bank_list == null) {
                this.alertServ.showAlertError(localizedValues["errors.bank_mand"]);
                show_msg = true;
              } else {
                if (this.charge.reference == '' || this.charge.reference == null) {
                  this.alertServ.showAlertError(localizedValues["errors.refer_mand"]);
                  show_msg = true;
                }
              }
              break;
            case '2':
              if (this.charge.bank_list == '' || this.charge.bank_list == null) {
                this.alertServ.showAlertError(localizedValues["errors.bank_mand"]);
                show_msg = true;
              } else {
                if (this.charge.check_a_number == '' || this.charge.check_a_number == null) {
                  this.alertServ.showAlertError(localizedValues["errors.check_a_numb_mand"]);
                  show_msg = true;
                } else {
                  if (this.charge.check_number == '' || this.charge.check_number == null) {
                    this.alertServ.showAlertError(localizedValues["errors.check_numb_mand"]);
                    show_msg = true;
                  }
                }
              }
              break;
          }
          if (show_msg == false) {
            this.getSelectedInvoices();
            if (this.account_state.cntesp == true) {
              /*if(this.charge.document_number == '' || this.charge.document_number == null) {
                this.alertServ.showAlertError(localizedValues["errors.doc_numb_mand"]);
              } else {
                if(this.charge.retention_number == '' || this.charge.retention_number == null) {
                  this.alertServ.showAlertError(localizedValues["errors.rete_numb_mand"]);
                } else {
                  if(this.isAmountInvoiceGE()) {
                    this.alertServ.showAlertError(localizedValues["charges.no_equal_amounts"]);
                  } else {
                    this.charge.cntesp = this.account_state.cntesp;
                    this.charge.codcli = this.account_state.codcli;
                    this.navCtrl.push(CobranzaProcesarPage, {invoices:this.selected_invoices, charge:this.charge});
                  }
                }
              }*/

              if (this.isAmountInvoiceGE()) {
                this.showMessageSpecialTaxPayer(localizedValues["charges.no_equal_amounts"]);
              } else {
                this.goToComprobanteRetencion();
              }

            } else {
              /*if(!this.isAmountInvoiceGE()) {
                this.alertServ.showAlertError(localizedValues["errors.invoi_balan_less"]);
              } else {
                if(this.selected_invoices.length == 0){
                  this.showMessage(localizedValues["charges.no_selected_invoices"]);
                }          
                this.charge.cntesp = this.account_state.cntesp;
                this.charge.codcli = this.account_state.codcli;
                this.navCtrl.push(CobranzaProcesarPage, {invoices:this.selected_invoices, charge:this.charge});
              }*/
              if (this.selected_invoices.length == 0) {
                this.showMessage(localizedValues["charges.no_selected_invoices"]);
              } else {
                this.goToCobranzaProcesar();
              }

            }
          }
        }
      });
  }

  goToCharge() {
    let charge: any = {};    
    charge.metpag = 0; 
    charge.monret = 0;
    charge.montco = 0;
    charge.cntesp = true;
    charge.codcli = this.codigo_cliente;
    charge.divisa = this.charge.divisa;
    //charge.ctachq = '.';
    //charge.numchq = '.';
    //charge.numcom = '.';
    //charge.referc = '.';
    //charge.codban = '.';
    this.database.createCharge(charge, this.invoices).then(
      (res) => {
        //this.alertServ.showAlert('La cobranza fue almacenada con exito');
        this.navCtrl.push(CobranzaPage, { invoices: this.invoices, charge: charge, account_state: this.account_state });
      },
      (error) => {
        this.alertServ.showAlertError('La cobranza no pudo ser creada');
      });
  }

  goToCharges() {
    this.database.createCharge(this.charge, this.invoices).then(
      (res) => {
        //this.alertServ.showAlert('La cobranza fue almacenada con exito');
        this.navCtrl.push(CrearCobranzaPage, { invoices: this.invoices, charge: this.charge, account_state: this.account_state });
      },
      (error) => {
        this.alertServ.showAlertError('La cobranza no pudo ser creada');
      });
  }

  goToExistingCharge(cobranza: any) {
    
    this.navCtrl.push(CobranzaPage, { invoices: this.invoices, charge: cobranza, account_state: this.account_state });

  }

  goToClosedCharge(cobranza: any) {

    this.navCtrl.push(CobranzaClosedPage, { invoices: this.invoices, charge: cobranza, account_state: this.account_state });

  }

  goToSyncedPrintedCharge(cobranza: any) {

    this.navCtrl.push(CobranzaSyncedPrintedPage, { invoices: this.invoices, charge: cobranza, account_state: this.account_state });

  }

  /*isAmountInvoiceGE() {
    let counter = 0;

    this.account_state.lisfac.forEach((i)=>{

      if(i.seleccionada){
        counter += i.salfac;

      }

    });
    console.log('total: ' + counter);
    console.log('amount: ' + this.charge.charge_amount);
    return( counter >= this.charge.charge_amount );
  }*/

  isAmountInvoiceGE() {

    return (this.invoices_amount != this.charge.charge_amount);

  }

  getSelectedInvoices() {
    this.selected_invoices = [];
    /*this.account_state.lisfac.forEach((i) => {
      if (i.seleccionada) {
        this.selected_invoices.push(i);
      }
    });*/
  }

  goToCobranzaProcesar() {

    this.charge.cntesp = this.account_state.cntesp;
    this.charge.codcli = this.account_state.codcli;
    this.navCtrl.push(CobranzaProcesarPage, { invoices: this.selected_invoices, charge: this.charge });

  }

  goToComprobanteRetencion() {

    this.charge.cntesp = this.account_state.cntesp;
    this.charge.codcli = this.account_state.codcli;
    this.navCtrl.push(ComprobanteRetencionPage, { invoices: this.selected_invoices, charge: this.charge });

  }

  changeSegment() {

    //alert("changeSegment called");
    this.content.resize();

    this.translate.get(["charges.account_statement", "charges.title", "charges.invoices", "deposits.title"], null).subscribe(
      (localizedValues) => {

        if (this.type == "statement") {

          this.segment_title = localizedValues["charges.account_statement"];

        }

        if (this.type == "charges") {

          this.segment_title = localizedValues["charges.title"];


        }

        if (this.type == "deposits") {

          this.segment_title = localizedValues["deposits.title"];

        }


      });

  }

  getAccounStatement() {
    this.database.getAccountStatement(this.codigo_cliente).then(
      (account_statement) => {
        if (typeof account_statement != 'undefined') {
          this.account_state = account_statement;
        }   
      }
    )
      .catch((error) => {
        this.alertServ.showAlertError('error obteniendo el estado de cuenta');
      })
  }


  changeCobfas() {

  }



  openRemoveChargeConfirmation(charge: any) {
    this.translate.get(["charges.delete_charge", "common.choose_reason", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["charges.delete_charge"]);
        alert.setMessage(localizedValues["common.choose_reason"]);

        this.motivos.forEach((s) => {
          console.log('s.codmot: ' + s.codmot);
          alert.addInput({
            type: 'radio',
            label: s.desmot,
            value: s.codmot,
            checked: false
          });

        });

        alert.addButton(localizedValues["common.cancel"]);

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if (data !== '') {
              console.log('data: ' + data)
              this.deleteCharge(charge, data);


            }

          }
        });

        alert.present();
      });
  }

  getRemoveOrderReasons() {
    this.database.openDatabase().then(
      () => {
        this.database.getRemoveOrderReasons()
          .then(reasons => {
            this.motivos = reasons;
          })
      });
  }

  deleteCharge(charge: any, codmot: string) {

    this.translate.get(["charges.success_deleted", "charges.failure_deleted"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
          () => {
            this.database.updateChargePhaseStatus(charge.cobnum, 5, 3, charge)
              .then((data) => {

                this.database.deleteChargesxInvoices(charge.cobnum).then(
                  (res) => {
                    this.showMessage(localizedValues["charges.success_deleted"]);

                    switch (charge.cobfas) {
                      case 1:
                        this.getOpenCharges();
                        break;
                      case 2:
                        this.getOpenCharges();
                        break;
                      case 3:
                        this.getClosedCharges();
                        break;
                      case 4:
                        this.getSyncedCharges();
                        break;
                      case 6:
                        this.getPrintedCharges();
                        break;
                    }
                  },
                  (error) => {
                    this.alertServ.showAlertError('Las facturas de la cobranza no fueron removidas ');
                  });

              })
              .catch((error) => { this.showMessage(localizedValues["charges.failure_deleted"]); });
          });

      });


  }

  // printOrderSelectPrinter(){

  //     let context = this;

  //     this.translate.get(["basket.error_printers_not_found"], null).subscribe(
  //     (localizedValues) => {

  //         (<any>window).BTPrinter.list(function(data){
  //         //alert("Success Listing");
  //         context.showPrintersList(data);
  //         //alert(data); //list of printer in data array
  //         },function(err){
  //             //alert("Error Listing");
  //             context.showMessage(localizedValues["basket.error_printers_not_found"]);
  //         });     

  //     });      

  // }

}
