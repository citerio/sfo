import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { DbService } from "../../providers/db-service";
import { Diagnostic } from "@ionic-native/diagnostic";
import { TranslateService } from "ng2-translate";
import { FacturaPage } from "../factura/factura";
import { FacturaCrearPage } from "../factura-crear/factura-crear";
import { Geolocation } from '@ionic-native/geolocation';
import { AppUtils } from '../../providers/app-utils';
import { AlertsService } from '../../providers/alerts.service';
import { Storage } from '@ionic/storage';
/*
  Generated class for the ListaFacturas page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-lista-facturas',
  templateUrl: 'lista-facturas.html'
})
export class ListaFacturasPage {

  facturas:any[] = [];
  facturas_copia:any[] = [];
  filtro:string = '';
  searching: any = false;
  cliente:any;
  created_facturas:any[] = [];
  processing_facturas:any[] = [];
  closed_facturas:any[] = [];
  motivos: any[] = [];

  constructor(
    public navCtrl: NavController,
    public view: ViewController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public database: DbService,
    private diagnostic: Diagnostic,
    private translate: TranslateService,    
    public loadingCtrl: LoadingController,
    public geolocation: Geolocation,
    public alertsServ: AlertsService,
    private storage: Storage
  ) {

    if(this.navParams.data){

      /*if(this.navParams.data.facturas){      

        this.getInvoices(this.navParams.data.facturas);
        //this.getOrder(this.numero_actual_pedido);  

      }*/

      if(this.navParams.data.cliente){      

        this.cliente = this.navParams.data.cliente;
        //this.getOrder(this.numero_actual_pedido);  

      }         

    } 

  }

  getInvoices(){

    this.database.openDatabase().then(
      () => {
        this.database.getInvoices(this.cliente.codcli, 1, 1)
        .then(created_facturas => {
          this.created_facturas = created_facturas;
          //this.setFilteredItems();
        })
      });

      this.database.openDatabase().then(
      () => {
        this.database.getInvoices(this.cliente.codcli, 2, 1)
        .then(processing_facturas => {
          this.processing_facturas = this.created_facturas.concat(processing_facturas);
          //this.setFilteredItems();
        })
      });

    this.database.openDatabase().then(
      () => {
        this.database.getInvoices(this.cliente.codcli, 3, 1)
        .then(closed_facturas => {
          this.facturas = this.processing_facturas.concat(closed_facturas);
          //this.setFilteredItems();
        
        })
      });
      
  }

  ionViewWillEnter(){
    this.getInvoices();    
  }

  ionViewDidLoad() {
    this.getRemoveOrderReasons();
    console.log('ionViewDidLoad ListaFacturasPage');
  }  

  goToFactura(factura: any){

    let fase = factura.pedfas;

    if(fase == 3){

       this.navCtrl.push(FacturaPage, {cliente:factura});

    }

    if(fase == 1 || fase == 2){

       this.navCtrl.push(FacturaCrearPage, {cliente:factura});

    }   

  }

  BuscarCliente(){    
   
    return this.facturas.filter(item => item.codcli.includes(this.filtro) ||
        item.descli.includes(this.filtro)
    )

  }  

  filterItems(ev: any){

      this.facturas = this.facturas_copia;

      let val = ev.target.value;

    // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.facturas = this.facturas.filter((item) => {
          return (item.descli.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.codcli.includes(val));
        })
      }    
 
  }

  goToFacturaCrear(){
    this.diagnostic.isLocationEnabled().then(
      (setting) => {
          if(setting == true) {
            this.translate.get(["get_coord", "errors.not_in_peri_gps_invo"], null).subscribe(
              (localizedValues) => {
                let loading = this.loadingCtrl.create({
                  content: localizedValues["get_coord"]
                });
                loading.present();
                
                this.geolocation.getCurrentPosition({timeout: 4000}).then(
                  (position) => {
                    let geopcr = position.coords.latitude.toString() + "," + position.coords.longitude.toString();
                    this.database.getGpsobl()
                    .then((gpsobl) => {
                      console.log('gpsobl: ' + gpsobl);
                      /////////////gps temporary
                      gpsobl = 'false'
                      /////////////gps temporary
                      if(gpsobl == 'true') {
                        this.storage.get('gpsper')
                        .then((gpsper) => {
                          let latcli = this.cliente.coogeo.split(",")[0];
                          let loncli = this.cliente.coogeo.split(",")[1];
                          console.log('geopcr: ' + geopcr);
                          console.log('latcli: ' + latcli + " loncli: " + loncli);
                          console.log('gpsper: ' + gpsper);
                          console.log('distance: ' + AppUtils.distance(position.coords.latitude, position.coords.longitude, latcli, loncli));
                          if(AppUtils.distance(position.coords.latitude, position.coords.longitude, latcli, loncli) <= gpsper) {
                            console.log('geopcr: ' + geopcr);
                            this.database.openDatabase().then(
                              () => {
                                this.database.createInvoice(this.cliente, geopcr, false)
                                .then((data) => {
                                  this.navCtrl.push(FacturaCrearPage, {cliente:this.cliente});                          
                                });
                              });
                            loading.dismiss();
                          } else {
                            loading.dismiss().then(
                              () => {
                                this.alertsServ.showAlertError(localizedValues["errors.not_in_peri_gps_invo"]);
                              }
                            );
                          }
                        });
                      } else {
                        console.log('geopcr: ' + geopcr);
                        this.database.openDatabase().then(
                          () => {
                            this.database.createInvoice(this.cliente, geopcr, false)
                            .then((data) => {
                              this.navCtrl.push(FacturaCrearPage, {cliente:this.cliente});                          
                            });
                          });
                        loading.dismiss();
                      }
                    });
                  }).catch((error) => {
                    console.log('no geopcr: '+ error);
                    this.database.getGpsobl()
                    .then((gpsobl) => {
                      if(gpsobl == 'true') {
                        loading.dismiss().then(
                          () => {
                            this.alertsServ.showAlertError('No se pudo obtener sus coordenada GPS. Revise su red o configuración GPS');
                          }
                        );
                      } else {
                        this.database.openDatabase().then(
                          () => {
                            this.database.createInvoice(this.cliente, '', false)
                            .then((data) => {
                              this.navCtrl.push(FacturaCrearPage, {cliente:this.cliente});                          
                            });
                          });
                        loading.dismiss();
                      }
                    });
                  });
              });
          } else {
            this.database.openDatabase().then(
              () => {
                this.database.getGpsobl()
                .then((gpsobl) => {
                  if(gpsobl == 'true') {
                    console.log('gpsobl: ' + true);
                    this.showMessageTurnOnGPS();
                  } else {
                    console.log('gpsobl: ' + false);
                    this.showConfirmTurnOnGPS(this.cliente);
                  }
                });
              });
            // this.showAlert();
          }
      },
      (error) => {
        console.log('Entro en error buscando gps setting');
      });

    
  }

  showMessageTurnOnGPS() {
    this.translate.get(["gps_off", "gps_proc_acti", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["gps_proc_acti"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  showConfirmTurnOnGPS(cliente: any) {
    this.translate.get(["gps_off", "acti_gps", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["acti_gps"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.database.openDatabase().then(
                  () => {
                    this.database.createInvoice(cliente, '', false)
                    .then((data) => {
                      this.navCtrl.push(FacturaCrearPage, {cliente:cliente});                      
                    });
                  });
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  showAlert() {
    this.translate.get(["gps_off", "gps_proc_acti", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["gps_proc_acti"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  ///////////////////////////////////eliminacion factura///////////////////////////////////
  /*openRemoveOrderConfirmation(pedido: any){
    this.translate.get(["billing.delete_invoice", "common.choose_reason", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["billing.delete_invoice"]);
        alert.setMessage(localizedValues["common.choose_reason"]);

        this.motivos.forEach((s) => {
            console.log('s.codmot: ' + s.codmot);
            alert.addInput({
            type: 'radio',
            label: s.desmot,
            value: s.codmot,
            checked: false
          });

        });    

        alert.addButton(localizedValues["common.cancel"]);

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if(data !== ''){          
              console.log('data: ' + data)
              this.deleteProduct(pedido, data);
              

            }
            
          }
        });

        alert.present();
      });
  }*/

  getRemoveOrderReasons(){
    this.database.openDatabase().then(
      () => {
        this.database.getRemoveOrderReasons()
        .then(reasons => {
          this.motivos = reasons;
        })
      });
  } 
  
  /*deleteProduct(pedido: any, codmot: string){

    this.translate.get(["billing.success_deleted", "billing.failure_deleted"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
        () => {
          this.database.updateInvoicePhaseStatus(pedido.facnum, pedido.facnum, , 5, 3)
          .then((data) => {
            this.database.updateInvoiceLinesPhaseStatus(pedido.facnum, 5, 3)
            .then(() => {
              this.showMessage(localizedValues["billing.success_deleted"]);
              this.getInvoices();
              
            })
            .catch((error) => { this.showMessage(localizedValues["billing.failure_deleted"]); })
          })
          .catch((error) => { this.showMessage(localizedValues["billing.failure_deleted"]); });
        });
          
          
      });    
    

  }*/

  showMessage(msg: any){

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({      
        message: msg,
        buttons: [
          {
            text: localizedValues["common.acept"],
            handler: () =>{
              console.log('aceptar');
            }
          }
        ]
      });
      alert.present();        
          
      });   

  } 


}
