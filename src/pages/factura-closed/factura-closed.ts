import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { DbService } from "../../providers/db-service";
import { TranslateService } from "ng2-translate";
import { DetallarproductoFacturaPage } from "../detallarproducto-factura/detallarproducto-factura";
import { Storage } from '@ionic/storage';

/*
  Generated class for the Factura page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-factura-closed',
  templateUrl: 'factura-closed.html'
})
export class FacturaClosedPage {

  codigo_cliente: string = '';
  nombre_cliente: string = '';
  codigo_ordtra: string = '';
  cliente: any = {};
  productos_escogidos: any[] = [];
  productos_taxes: any[] = [];
  suma_total: any = { 'monto': 0 };
  importe_total: number = 0;
  numero_actual_factura_LN: string = '';
  numero_actual_control_LN:string = '';
  numero_actual_pedido: string = '';
  deshabilitar_cerrar: boolean = false;
  invoice: string = '';
  loader: any;
  printer_name = '';
  printed_copy = false;
  monped = 'true';
  serie_factura: any;
  nombre_usuario: any = '';
  seniat_serial: any = {};
  total_exento: number = 0;
  total_iva: number = 0;
  total_imponible: number = 0;
  divisa: any;
  printed_copies: number = 2;

  constructor(

    public navCtrl: NavController,
    public navParams: NavParams,
    public database: DbService,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private translate: TranslateService,
    private storage: Storage

  ) {


    if (this.navParams.data) {

      if (this.navParams.data.cliente.codcli) {

        this.codigo_cliente = this.navParams.data.cliente.clifac;

        this.database.openDatabase().then(
          () => {
            this.database.getClientName(this.navParams.data.cliente.codcli)
              .then((nombre) => {
                this.cliente = nombre;
                this.nombre_cliente = this.cliente.descli
              });
          });

        this.numero_actual_pedido = this.navParams.data.cliente.ordpes;
        this.codigo_ordtra = this.navParams.data.cliente.ordtra;
        //this.getOrder(this.numero_actual_pedido);  

        this.database.getInvoiceSeriesNum()
          .then((serie) => {

            this.serie_factura = serie;

          });

          this.database.getBadge().then(
            (badge: string) => {
              console.log('divisa: ' + badge);
              this.divisa = badge;
            });

      }

    }

  }

  ionViewWillEnter() {
    this.getInvoice(this.numero_actual_pedido, this.codigo_cliente);
  }

  getTaxByProduct(products: any) {
    this.total_imponible = 0;
    this.total_iva = 0;
    this.total_exento = 0;
    this.database.openDatabase()
      .then(
        () => {
          this.database.getTaxByProduct(products)
            .then(taxes => {
              this.productos_taxes = taxes;
              this.productos_taxes.forEach((pt) => {

                //alert("inside loop");

                let product = this.productos_escogidos.find((p) => p.codart.toString().trim() == pt.codpro.toString().trim());

                switch (pt.cdimpo.toString().trim()) {
                  case 'IVAEXE':
                    this.total_exento += Number(product.pedprt);
                    break;
                  case 'ALIGEN':
                    this.total_iva += ((Number(product.pedprt) * 16) / 100);
                    this.total_imponible += Number(product.pedprt);
                    break;
                }
              })

            })
            .catch((error) => alert("error on getting produc taxes"))
        });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FacturaPage');
    this.getSENIATSerial();
    this.getUserName();
  }

  getUserName() {
    this.storage.get('nombre')
      .then((name) => this.nombre_usuario = name)
      .catch((error) => alert("error on getting user name"))
  }

  /*getInvoice(pednum: any) {
    this.database.openDatabase().then(
      () => {
        this.database.getInvoiceLN(pednum, 3, 1)
          .then(pedido => {
            this.productos_escogidos = pedido;
            if (this.productos_escogidos.length == 0) {
              this.deshabilitar_cerrar = true;
            } else {
              this.deshabilitar_cerrar = false;
            }
            this.orderTotalSum();
          })
          .catch((error) => {

            alert('ocurrio un error: ' + JSON.stringify(error));
          }

          )
      });

  }*/

  getInvoice(pednum: any, codcli: any) {
    this.database.openDatabase().then(
      () => {
        this.database.getInvoice(pednum, 3, 1, codcli)
          .then(pedido => {
            this.productos_escogidos = pedido;
            if (this.productos_escogidos.length == 0) {
              this.deshabilitar_cerrar = true;
            } else {
              this.deshabilitar_cerrar = false;
            }
            this.getTaxByProduct(this.productos_escogidos);
            this.orderTotalSum();
          })
          .catch((error) => {

            alert('ocurrio un error: ' + JSON.stringify(error));
          }

          )
      });

  }

  getSENIATSerial() {
    this.database.openDatabase()
      .then(
        () => {
          this.database.getSENIATSerial()
            .then(serial => {
              this.seniat_serial = serial;
            })
            .catch((error) => alert("error on getting SENIAT Serial"))
        });
  }

  orderTotalSum() {

    this.importe_total = 0;
    this.productos_escogidos.forEach((s) => {
      this.importe_total = this.importe_total + s.pedprt;
    }
    );

  }

  editProduct(producto: any) {

    this.navCtrl.push(DetallarproductoFacturaPage, { numero_actual_pedido: this.numero_actual_pedido, producto_elegido: producto });

  }

  openSENIATSerialConfirmationNoEdit() {

    this.translate.get(["common.no", "common.yes", "common.confirm", "billing.print_confirm"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          subTitle: localizedValues["billing.print_confirm"],
          message: "<b>Consecutivo de documento<b><br/>" + this.seniat_serial.nfact + "<br/><b>Serie de Control<b><br/>" + this.seniat_serial.serie + "<br/><b>Numero de Control<b><br/>" + this.seniat_serial.ultimo,
          buttons: [
            {
              text: localizedValues["common.no"],
              handler: () => {
                //this.navCtrl.pop();
                this.openSENIATSerialConfirmation();
              }
            },
            {
              text: localizedValues["common.yes"],
              handler: () => {
                //this.createInvoiceLN();
                this.numero_actual_control_LN = this.seniat_serial.ultimo;
                this.printOrderSelectPrinter();
              }
            }
          ]
        });
        alert.present();

      });

  }

  openSENIATSerialConfirmation() {

    this.translate.get(["common.acept", "common.cancel", "common.confirm", "billing.print_confirm"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          subTitle: localizedValues["billing.print_confirm"],
          message: "<b>Consecutivo de documento<b><br/>" + this.seniat_serial.nfact + "<br/><b>Serie de Control<b><br/>" + this.seniat_serial.serie + "<br/><b>Numero de Control<b><br/>",
          inputs: [
            {
              placeholder: 'Numero de Control',
              name: 'numcon',
              label: 'Numero de Control',
              type: 'text',
              disabled: true,
              value: this.seniat_serial.ultimo
            }
          ],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.navCtrl.pop();
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: (data: any) => {                
                //this.createInvoiceLN();
                if(data.numcon != ''){
                  if(Number(data.numcon) < Number(this.seniat_serial.rainse) || Number(data.numcon) > Number(this.seniat_serial.rafise)){
                    let msg = "<b>Numero de Control fuera de rango<b><br/>" + "<b>Numero Inicial<b><br/>" + this.seniat_serial.rainse + "<br/><b>Numero Final<b><br/>" + this.seniat_serial.rafise;
                    this.showMessage(msg)
                  }else{
                    this.numero_actual_control_LN = data.numcon;
                    this.updateSeniatControlNumber(data.numcon);
                    this.printOrderSelectPrinter();
                  }
                }                
              }
            }
          ]
        });
        alert.present();

      });

  }

  /*openCloseConfirmation() {

    this.translate.get(["common.acept", "common.cancel", "common.confirm", "billing.close_confirm"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["billing.close_confirm"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                console.log('cancelar');
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.openPaymentConfirmation();
              }
            }
          ]
        });
        alert.present();

      });

  }*/

  /*openPaymentConfirmation() {

    this.translate.get(["common.yes", "common.no", "common.cancel", "common.confirm", "billing.payment_select"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["billing.payment_select"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                console.log('cancelar');
              }
            },
            {
              text: localizedValues["common.yes"],
              handler: () => {
                this.closeOrder();
              }
            },
            {
              text: localizedValues["common.no"],
              handler: () => {
                this.closeOrder();
              }
            }
          ]
        });
        alert.present();

      });

  }*/

  /*closeOrder() {

    this.translate.get(["billing.success_closed", "billing.failure_closed"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
          () => {
            this.database.updateInvoicePhaseStatus(this.numero_actual_pedido, 3, 1)
              .then((data) => {
                this.database.updateInvoiceLinesPhaseStatus(this.numero_actual_pedido, 3, 1)
                  .then((data) => { this.deshabilitar_cerrar = true; this.showMessageforPrinting(localizedValues["billing.success_closed"]); })
                  .catch((error) => { this.showMessage(localizedValues["billing.failure_closed"]); })
              })
              .catch((error) => { this.showMessage(localizedValues["billing.failure_closed"]); });
          });

      });

  }*/

  showMessageforPrinting(msg: any) {


    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.openPrintingConfirmation();
              }
            }
          ]
        });
        alert.present();

      });

  }

  openPrintingConfirmation() {

    this.translate.get(["common.acept", "common.cancel", "common.confirm", "billing.print_confirm"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["billing.print_confirm"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.createInvoiceLN()
                this.navCtrl.pop();
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                //this.openSENIATSerialConfirmationNoEdit();
                this.printOrderSelectPrinter();
              }
            }
          ]
        });
        alert.present();

      });

  }



  ///////////////////////////////////////////////////IMPRESION/////////////////////////////////////////////////
  printOrderSelectPrinter() {

    let context = this;

    this.translate.get(["basket.error_printers_not_found"], null).subscribe(
      (localizedValues) => {

        (<any>window).BTPrinter.list(function (data) {
          //alert("Success Listing");
          context.showPrintersList(data);
          //alert(data); //list of printer in data array
        }, function (err) {
          //alert("Error Listing");
          context.showMessage(localizedValues["basket.error_printers_not_found"]);
        });

      });

  }

  printOrderPrinting(printer_name: any) {

    let context = this;

    this.translate.get(["basket.error_printer_disconnection", "basket.error_printing_document", "basket.error_printer_connection"], null).subscribe(
      (localizedValues) => {

        (<any>window).BTPrinter.connect(function (data) {
          //alert("Success Connected");
          //alert(data);
          context.invoice = context.setInvoice();
          //this.printing();
          (<any>window).BTPrinter.printText(function (data) {
            //alert("Success printing");
            context.loader.dismiss();
            //context.PrintInvoice();
            if (context.printed_copies == 0) {
              context.createInvoiceLN();
            }
            context.printed_copies--;
            (<any>window).BTPrinter.disconnect(function (data) {
              //alert("Success disconnected");
              //alert(data)
            }, function (err) {
              context.showMessage(localizedValues["basket.error_printer_disconnection"]);
              //alert(err)
            }, printer_name);
          }, function (err) {
            context.loader.dismiss();
            context.showMessage(localizedValues["basket.error_printing_document"]);
          }, context.invoice);
        }, function (err) {
          //alert("Error");
          context.loader.dismiss();
          context.showMessage(localizedValues["basket.error_printer_connection"]);
          //alert(err)
        }, printer_name);

      });

  }

  showPrintersList(printers: any[]) {


    this.translate.get(["common.cancel", "common.acept", "basket.choose_bluetooth_printer"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["basket.choose_bluetooth_printer"]);

        printers.forEach((s) => {

          alert.addInput({
            type: 'radio',
            label: s,
            value: s,
            checked: false
          });

        });

        alert.addButton(localizedValues["common.cancel"]);

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if (data !== '') {

              this.printer_name = data;
              this.presentLoading(data);


            }

          }
        });

        alert.present();

      });


  }

  setInvoice() {

    let invoice_space = "";
    let max_lines = 13;

    if (this.printed_copies < 2) {      
      let num_prod = this.productos_escogidos.length;
      let space = max_lines - num_prod;
      for (let i = 1; i <= space; i++) {
        invoice_space += "\r\n";
      }
      invoice_space += '\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n';
    }

    let header = 'Av. Eugenio Mendoza Edif. Protinal, frente al stadium Jose\r\nBernardo Perez Valencia - Estado Carabobo Telefonos(0241)6138423\r\n';

    let order_number = 'Serie ' + this.serie_factura + '   Factura ' + this.seniat_serial.nfact + '     Fecha: ' + this.navParams.data.cliente.fechcr + '\r\n';

    let client_info = 'Cliente: ' + this.cliente.descli.toUpperCase() + '\r\n' + 'Codigo: ' + this.codigo_cliente.toUpperCase() + '\r\n' + 'RIF: ' + this.cliente.facrif.toUpperCase() + '\r\n' + 'Direccion: ' + this.cliente.facdir.toUpperCase() + '\r\n';

    let seller_info = 'Vendedor: ' +  this.navParams.data.cliente.codven + '\r\n' + 'Preventista: ' + this.nombre_usuario + '\r\n' + 'Condicion de pago: Contado' + '      Vence: ' + this.navParams.data.cliente.fechcr + '\r\n\n';

    let order_table_header = 'Nombre Producto         Cantidad    Und   Prec. Unit        Monto';

    //let order_table_body:string;

    let invoice = invoice_space /*+ '\r\n\r\n\r\n\r\n'*/ + order_number + client_info + seller_info + order_table_header;



    this.productos_escogidos.forEach((s) => {
      invoice += "\r\n";
      let space: number = 0;
      //article description
      let descar = s.descar.length;
      if (descar >= 23) {
        invoice += s.descar.substring(0, 23).toUpperCase();
        invoice += " ";
      } else {
        invoice += s.descar.toUpperCase();
        space = 24 - descar;
        for (let i = 1; i <= space; i++) {
          invoice += " ";
        }
      }

      //article pedcan      
      let pedcan = s.cancar.toFixed(2).length;
      space = 8 - pedcan;
      for (let i = 1; i <= space; i++) {
        invoice += " ";
      }
      invoice += s.cancar.toFixed(2);
      invoice += "    ";

      //article coduni    
      invoice += s.coduni;
      let coduni = s.coduni.toString().length;
      space = 6 - coduni;
      for (let i = 1; i <= space; i++) {
        invoice += " ";
      }

      //article preuprnt
      let preuprnt = 0;
      if (this.monped == 'true') {
        preuprnt = s.pedpru;
      }

      space = 10 - preuprnt.toFixed(2).length;

      for (let i = 1; i <= space; i++) {
        invoice += " ";
      }

      invoice += preuprnt.toFixed(2);
      invoice += "  ";


      //article pretprnt
      let pretprnt = 0;
      if (this.monped == 'true') {
        pretprnt = s.pedprt;
      }

      space = 11 - pretprnt.toFixed(2).length;
      // nesp = 10 - s.pedprt.toString().length;

      for (let i = 1; i <= space; i++) {
        invoice += " ";
      }

      invoice += pretprnt.toFixed(2);

    });

    //65 total white spaces

    let space: number = 0;

    let subtotal_order = '\r\n                        Sub total:';
    invoice += subtotal_order;
    space = 20 + (11 - this.importe_total.toFixed(2).length);
    for (let i = 1; i <= space; i++) {
      invoice += " ";
    }
    invoice += this.importe_total.toFixed(2);
    

    let no_tax = '\r\n                        Mon. Exento:';
    invoice += no_tax;
    space = 18 + (11 - this.total_exento.toFixed(2).length);
    for (let i = 1; i <= space; i++) {
      invoice += " ";
    }
    invoice += this.total_exento.toFixed(2);


    let taxable = '\r\n                        Base Imponible:';
    invoice += taxable;
    space = 15 + (11 - this.total_imponible.toFixed(2).length);
    for (let i = 1; i <= space; i++) {
      invoice += " ";
    }
    invoice += this.total_imponible.toFixed(2); 


    let tax = '\r\n                        IVA:';
    invoice += tax;
    space = 26 + (11 - this.total_iva.toFixed(2).length);
    for (let i = 1; i <= space; i++) {
      invoice += " ";
    }
    invoice += this.total_iva.toFixed(2); 


    let total = '\r\n                        Total:' + this.divisa;
    invoice += total;
    let total_sum = this.importe_total + this.total_iva;
    space = 21 + (11 - total_sum.toFixed(2).length);
    for (let i = 1; i <= space; i++) {
      invoice += " ";
    }
    invoice += total_sum.toFixed(2);  


    let footer_1 = '\r\n\r\n\r\nNO SE ACEPTAN DEVOLUCIONES\r\n* Este comprobante va sin enmiendas\r\nPRODUCTO ESPECIAL A SOLICITUD DEL CLIENTE\r\nFirma del Cliente o...............................\r\nRBF: AGRO2-000003942-8 \r\nCestas que contienen el producto vendido son de Proagro C.A. \n\n\n'

    let footer_2 = '\r\nSaldo Inicial    Reintegrado    Consignado    Saldo Final';

    let footer_3 = '\r\n0.0              0.0            0.0           0.0';

    //let invoice = header + order_number + client_info + seller_info + order_table_header + order_table_body + subtotal_order + total_order + footer;

    // let invoice = table;

    invoice += footer_1;/* + footer_2 + footer_3;*/
    return invoice;

  }

  PrintInvoice() {

    this.translate.get(["billing.error_printing_invoice"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
          () => {
            this.database.updateInvoiceLNPhaseStatus(this.numero_actual_factura_LN, 6, 1)
              .then((data) => {
                this.database.updateInvoiceLinesLNPhaseStatus(this.numero_actual_control_LN, 6, 1)
                  .then((data) => {
                    this.showMessageOnPrintingSuccess();
                    this.database.updateInvoicePhaseStatus(this.numero_actual_pedido, this.codigo_ordtra, this.codigo_cliente, 6, 1)
                      .then((data) => {
                        this.database.updateInvoiceLinesPhaseStatus(this.numero_actual_pedido, this.codigo_ordtra, this.codigo_cliente, 6, 1)
                          .then((data) => {
                            //this.deshabilitar_cerrar = true;
                            //this.showMessageforPrinting(localizedValues["billing.success_closed"]);
                            //this.createInvoiceLN();        
                          })
                          .catch((error) => { this.showMessage(localizedValues["billing.failure_closed"]); })
                      })
                      .catch((error) => { this.showMessage(localizedValues["billing.failure_closed"]); });
                  })
                  .catch((error) => { this.showMessage(localizedValues["billing.error_printing_invoice"]); })
              })
              .catch((error) => { this.showMessage(localizedValues["billing.error_printing_invoice"]); });
          });

      });


  }

  updateSeniatControlNumber(series: any){
    this.database.openDatabase().then(
      () => {
        this.database.updateSeniatControlNumberLN(series)
          .then((data) => {
            
          })
          .catch((error) => { this.showMessage("Error on updating Seniat Control Number"); });
      });  
  }

  showMessage(msg: any) {

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
              }
            }
          ]
        });
        alert.present();

      });

  }

  showMessageOnPrintingSuccessOrFailure(msg: any) {

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.navCtrl.pop();
              }
            }
          ]
        });
        alert.present();

      });

  }

  showMessageOnPrintingSuccess() {


    this.translate.get(["common.acept", "billing.success_printed"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: localizedValues["billing.success_printed"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                if (this.printed_copy === true) {
                  this.navCtrl.pop();
                } else {
                  this.database.openDatabase().then(
                    () => {
                      this.database.getPrncpy().then(
                        (print_copy) => {
                          if (print_copy == 'true') {
                            this.showMessagePrintCopy();
                          } else {
                            this.navCtrl.pop();
                          }
                        },
                        (error) => {
                        });
                    });
                }
              }
            }
          ]
        });
        alert.present();

      });
  }

  showMessagePrintCopy() {

    this.translate.get(["common.acept", "common.cancel", "common.confirm", "basket.print_copy_confirm"], null).subscribe(
      (localizedValues) => {

        this.printed_copy = true;
        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["basket.print_copy_confirm"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.navCtrl.pop();
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.presentLoading(this.printer_name);
              }
            }
          ]
        });

      });

  }

  presentLoading(data) {


    this.translate.get(["common.wait"], null).subscribe(
      (localizedValues) => {

        this.loader = this.loadingCtrl.create({
          content: localizedValues["common.wait"],
          spinner: "bubbles"
        });

        this.loader.present()
          .then(() => {
            for (let index = 0; index < 2; index++) {
              this.printOrderPrinting(data);
            }            
          });

      });

  }

  createInvoiceLN() {

    this.database.openDatabase().then(
      () => {
        this.database.createInvoiceLN(this.navParams.data.cliente)
          .then((data) => {
            this.numero_actual_factura_LN = this.database.getCurrentLNInvoiceNumber();
              //this.numero_actual_control_LN = this.database.getCurrentLNInvoiceControlNumber();
              //this.getSENIATSerial();
              this.database.createInvoiceLinesLN(this.numero_actual_factura_LN, this.numero_actual_control_LN, this.productos_escogidos)
              .then((data) => {
                this.PrintInvoice();
                if(Number(this.cliente.conpag) == 0){
                  this.createInvoiceToCharge();
                }
                //this.showMessageOnPrintingSuccess();
                //this.printOrderSelectPrinter();
              })
              .catch((error) => { this.showMessage("lineas factura LN no creadas"); })
          })
          .catch((error) => { this.showMessage("factura LN no creada"); });
      });

  }

  createInvoiceToCharge() {

    this.translate.get(["billing.success_closed", "billing.failure_closed"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
          () => {
            this.database.getInvoiceLNToCharge(this.numero_actual_factura_LN)
              .then((invoice_LN) => {
                invoice_LN.diasve = 0;
                invoice_LN.salfac = this.importe_total;
                this.database.createInvoiceToCharge(invoice_LN)
                  .then((data) => {
                    //this.showMessageforPrinting(localizedValues["billing.success_closed"]);
                  })
                  .catch((error) => { this.showMessage(localizedValues["billing.failure_closed"]); })
              })
              .catch((error) => { this.showMessage(localizedValues["billing.failure_closed"]); });
          });

      });

  }


}
