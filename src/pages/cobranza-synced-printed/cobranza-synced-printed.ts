import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { AlertsService } from '../../providers/alerts.service';
import { TranslateService } from 'ng2-translate';
import { Storage } from '@ionic/storage';

/*
  Generated class for the CobranzaSyncedPrinted page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cobranza-synced-printed',
  templateUrl: 'cobranza-synced-printed.html'
})
export class CobranzaSyncedPrintedPage {

  invoices: any[] = [];
  invoicesxcharge: any[] = [];
  charge: any = {};
  selected_bank: string = '';
  printer_name = '';
  invoice: string = '';
  loader: any;
  printed_copy = false;
  monped = 'false';
  indexes: any[] = [];
  total_amount: any;
  chargesxinvoices: any[] = [];
  chargedamountxinvoices: any[] = [];
  compania: any = {};
  nombre_usuario: any = '';
  cliente: any = {};
  order_cr_date = '';

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public view: ViewController,
    public alertCtrl: AlertController,
    public database: DbService,
    public alertServ: AlertsService,
    public loadingCtrl: LoadingController,
    private storage: Storage,
    private translate: TranslateService) {

    if (this.navParams.data.charge) {

      this.charge = this.navParams.data.charge;

      if (this.charge.metpag == '1' || this.charge.metpag == '3') {
        console.log('Bank Code: ' + this.charge.codban);
        this.database.getBankDescByCode(this.charge.codban).then(
          (bank) => {
            console.log('Bank Name: ' + bank);
            this.selected_bank = bank;
          });

      }

      this.database.getChargeCreationDate(this.charge.cobnum)
        .then((fecha) => { this.order_cr_date = fecha; });

      this.getInvoicesAlreadyCharged(this.charge.cobnum);
      this.getInvoicesXChargeUI(this.charge.cobnum);

    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CobranzaClosedPage');
    this.getUserName();
    this.getClientName();
  }

  ionViewWillEnter() {
    //this.getCharge(this.numero_actual_pedido);
  }

  getCharge(cobnum: any) {
    this.database.openDatabase().then(
      () => {
        this.database.getChargeToSync(cobnum)
          .then(detalles => {
            this.invoices = detalles;
            this.setInvoicesChargedAmount();
          })
          .catch((error) => {

            alert('ocurrio un error: ' + JSON.stringify(error));
          }

          )
      });

  }

  getInvoicesXChargeUI(cobnum: any) {
    this.database.getChargeInvoicesAllFields(cobnum).then(
      (invoices) => {
        this.invoicesxcharge = invoices;
      },
      error => {
        console.log('Error obteniendo las facturas ya asociadas a cobranza');

      });
  }

  getInvoicesAlreadyCharged(cobnum: any) {
    this.database.getChargeInvoices(cobnum).then(
      (invoices) => {
        this.database.getInvoicesXcharge(invoices).then(
          (invoices) => {
            this.invoices = invoices;
            this.setInvoicesChargedAmount(); 
            console.log('facturas asociadas a cobranzas obtenidas');
          },
          error => {
            console.log('Error obteniendo las facturas ya asociadas a cobranza');    
          });               
      },
      error => {
        console.log('Error obteniendo las facturas ya asociadas a cobranza');

      });
  }

  setInvoicesChargedAmount() {

    this.invoices.forEach((s) => {
      this.database.getAmountCharged(this.charge.cobnum, s.facnum).then(
        (montco) => {
          this.chargedamountxinvoices[s.facnum] = Number(montco).toFixed(2);
        },
        (error) => {
          alert("Error on getting the amount charged per invoice/charge")
        });
    });
  }

  getUserName() {
    this.storage.get('username')
      .then((username) => {
        this.nombre_usuario = username;
        this.getCompanyName(username);
      })
      .catch((error) => alert("error on getting username"))
  }

  getCompanyName(username: any) {
    this.database.openDatabase().then(
      () => {
        this.database.getCompanyName(username)
          .then((nombre) => {
            this.compania = nombre;
          });
      });
  }

  getClientName() {
    this.database.openDatabase().then(
      () => {
        this.database.getClientName(this.charge.codcli)
          .then((nombre) => {
            this.cliente = nombre;
          });
      });
  }

  confirmCharge() {
    this.translate.get(["charges.success_closed", "charges.failure_closed"], null).subscribe(
      (localizedValues) => {

        this.database.updateChargePhaseStatus(this.charge.cobnum, 3, 1, this.charge).then(
          (res) => {
            this.showMessageforPrinting(localizedValues["charges.success_closed"]);
          },
          (error) => {
            this.alertServ.showAlertError('La cobranza cabecera no se actualizo ');
          });

      });
  }

  goBack() {
    this.navCtrl.pop();
  }

  /*previewCharges() {
 
    this.total_amount = 0;
 
    this.invoices.forEach(invoice => {
 
      invoice.charges.forEach(charge => {
 
        if (charge.metpag == 1 || charge.metpag == 2 || charge.metpag == 3) {
          this.database.getBankDescByCode(charge.codban).then(
            (bank) => {
              charge.desban = bank;
            });
        }
 
        this.total_amount = this.total_amount + Number(charge.montco);
        this.chargesxinvoices.push(charge);
 
      });
 
    })
 
 
  }*/




  /////////////////////////////////////printing operations//////////////////////////////////////////////////////////
  showMessageforPrinting(msg: any) {


    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.openPrintingConfirmation();
              }
            }
          ]
        });
        alert.present();

      });

  }

  openPrintingConfirmation() {

    this.translate.get(["common.acept", "common.cancel", "common.confirm", "charges.print_confirm"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["charges.print_confirm"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.closeViews();
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.printOrderSelectPrinter();
              }
            }
          ]
        });
        alert.present();

      });

  }

  /*printOrderSelectPrinter() {
 
    let context = this;
 
    this.translate.get(["charges.error_printers_not_found"], null).subscribe(
      (localizedValues) => {
 
        (<any>window).BTPrinter.list(function (data) {
          //alert("Success Listing");
          context.showPrintersList(data);
          //alert(data); //list of printer in data array
        }, function (err) {
          //alert("Error Listing");
          context.showMessage(localizedValues["charges.error_printers_not_found"]);
        });
 
      });
 
    //this.testing(); 
    //alert('imgData ' + this.imgData);     
 
  }*/

  printOrderSelectPrinter() {

    let context = this;

    this.translate.get(["charges.error_printers_not_found"], null).subscribe(
      (localizedValues) => {

        (<any>window).BTPrinter.list(function (data) {
          //alert("Success Listing");
          context.showPrintersList(data);
          //alert(data); //list of printer in data array
        }, function (err) {
          //alert("Error Listing");
          context.showMessage(localizedValues["charges.error_printers_not_found"]);
        });

      });

    //this.testing(); 
    //alert('imgData ' + this.imgData);     

  }

  showMessage(msg: any) {

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
              }
            }
          ]
        });
        alert.present();

      });

  }

  printOrderPrinting(printer_name: any) {

    let context = this;

    this.translate.get(["basket.error_printer_disconnection", "basket.error_printing_document", "basket.error_printer_connection"], null).subscribe(
      (localizedValues) => {

        (<any>window).BTPrinter.connect(function (data) {
          //alert("Success Connected");
          //alert(data);
          context.invoice = context.setInvoice();
          //this.printing();
          (<any>window).BTPrinter.printText(function (data) {
            //alert("Success printing");
            context.loader.dismiss();
            context.PrintInvoice();

            (<any>window).BTPrinter.disconnect(function (data) {
              //alert("Success disconnected");
              //alert(data)
            }, function (err) {
              context.showMessageOnPrintingSuccessOrFailure(localizedValues["basket.error_printer_disconnection"]);
              //alert(err)
            }, printer_name);
          }, function (err) {
            context.loader.dismiss();
            context.showMessageOnPrintingSuccessOrFailure(localizedValues["basket.error_printing_document"]);
          }, context.invoice);
        }, function (err) {
          //alert("Error");
          context.loader.dismiss();
          context.showMessageOnPrintingSuccessOrFailure(localizedValues["basket.error_printer_connection"]);
          //alert(err)
        }, printer_name);

      });

  }

  syncOrder() {

  }

  showPrintersList(printers: any[]) {


    this.translate.get(["common.cancel", "common.acept", "basket.choose_bluetooth_printer"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["basket.choose_bluetooth_printer"]);

        printers.forEach((s) => {

          alert.addInput({
            type: 'radio',
            label: s,
            value: s,
            checked: false
          });

        });

        alert.addButton({
          text: localizedValues["common.cancel"],
          handler: data => {

            this.navCtrl.pop();

          }
        });

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if (data !== '') {

              this.printer_name = data;
              this.presentLoading(data);


            }

          }
        });

        alert.present();

      });


  }

  /*setInvoice() {
 
 
    let header = this.compania.despro + '\r\n';
 
    let address = this.compania.dirpro + '\r\n';
 
    let rif = this.compania.rifpro + '\r\n';
 
    let dealer = this.compania.dessuc + '\r\n';
 
    let client_info = 'Cliente: ' + this.cliente.codcli.toUpperCase() + ' ' + this.cliente.descli.toUpperCase() + '\r\n' + 'RIF: ' + this.cliente.facrif.toUpperCase() + '\r\n' + 'Direccion: ' + this.cliente.facdir.toUpperCase() + '\r\n';
 
    let charge_info = 'Fecha Cobranza: ' + this.charge.fechcr + '\r\n\n';
 
    let client_type = "";
    if(this.charge.cntesp){
      client_type = "CE \r\n";
    }else{
      client_type = "CN \r\n";
    }
 
    //let order_table_body:string;
 
    let receipt = header + address + rif + dealer + client_info + client_type + charge_info;
 
 
    this.invoices.forEach((s) => {
      receipt += "\r\n";
 
      let space: number = 0;
      let value_length: number = 0;
 
      receipt += "Factura Cobranza: ";
      value_length = s.header.facnum.toString().length;
      space = 12 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += s.header.facnum + '\r\n';
 
      receipt += "  Fecha Factura: ";
      value_length = s.header.fecfac.toString().length;
      space = 12 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += s.header.fecfac + '\r\n';
 
      receipt += "  Total Factura: ";
      value_length = s.header.salfac.toString().length;
      space = 12 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += s.header.salfac + '\r\n';
 
      //bi base imponible
      //tax iva
      //mt monto retenido
 
      let bi = ((s.header.salfac * 100) / 116);
      let tax = (bi * 16) / 100;
      let mt = tax * 0.75;
      //tax = tax - mt;
      //bi = bi + tax;
 
      receipt += "            IVA: ";
      value_length = tax.toString().length;
      space = 12 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += tax + '\r\n';
 
      receipt += "  Monto Factura: ";
      value_length = bi.toString().length;
      space = 12 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += bi + '\r\n';
 
      if (this.charge.cntesp) {
        receipt += " IVA Retenido: ";
        value_length = mt.toString().length;
        space = 12 - value_length;
        for (let i = 1; i <= space; i++) {
          receipt += " ";
        }
        receipt += mt + '\r\n';
      }
 
      receipt += "Total Cobranza: ";
      let total: number = 0;
      if(this.charge.cntesp){
        total = bi + mt;
      }else{
        total = bi;
      }
      value_length = total.toString().length;
      space = 12 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += total + '\r\n';
     
 
      //receipt += pretprnt;
 
    });
    
    return receipt;
 
  }*/

  setInvoice() {


    let header = this.compania.despro + '\r\n';

    let address = this.compania.dirpro + '\r\n';

    let rif = this.compania.rifpro + '\r\n';

    let cobnum = this.charge.cobnum + '\r\n';

    //let dealer = this.compania.dessuc + '\r\n';

    let client_info = 'Cliente: ' + this.cliente.codcli.toUpperCase() + ' ' + this.cliente.descli.toUpperCase() + '\r\n' + 'RIF: ' + this.cliente.facrif.toUpperCase() + '\r\n' + 'Direccion: ' + this.cliente.facdir.toUpperCase() + '\r\n';

    let charge_info = 'Fecha Cobranza: ' + this.order_cr_date + '\r\n\n';

    let client_type = "";
    if (Boolean(this.charge.cntesp)) {
      client_type = "CE \r\n";
    } else {
      client_type = "CN \r\n";
    }

    //let order_table_body:string;

    let order_table_header = 'Nro.  Factura      Importe        IVA        Total Factura\r\n';
    let order_table_subheader = '                                  Ret IVA    Valor Cobrado\r\n';

    let receipt = header + address + rif + cobnum + client_info + client_type + charge_info + order_table_header + order_table_subheader;


    this.invoices.forEach((s) => {

      let bi = s.salfac - s.ivafac;
      let tax = s.ivafac;
      let mt = tax * 0.75;
      receipt += "\r\n";

      let space: number = 0;
      let value_length: number = 0;

      value_length = s.facnum.toString().length;
      space = 13 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += s.facnum;
      receipt += "      ";

      value_length = bi.toFixed(2).length;
      space = 10 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += bi.toFixed(2);
      receipt += "   ";

      value_length = tax.toFixed(2).length;
      space = 10 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += tax.toFixed(2);
      receipt += "   ";


      value_length = s.salfac.toFixed(2).length;
      space = 13 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += s.salfac.toFixed(2);
      receipt += '\r\n';


      ///////////////////second line///////////////////


      value_length = s.facnum.toString().length;
      space = 13 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += s.facnum;
      receipt += "                   ";



      value_length = mt.toFixed(2).length;
      space = 10 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += mt.toFixed(2);
      receipt += "   ";


      value_length = s.salfac.toFixed(2).length;
      space = 13 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += this.chargedamountxinvoices[s.facnum];



      /*this.database.getAmountCharged(this.charge.cobnum, s.facnum).then(
        (res) => {
          receipt += res[0].montco.toFixed(2);
        },
        (error) => {
          alert("Error on getting the amount charged per invoice/charge")
        });*/

      /*this.database.getAmountCharged(this.charge.cobnum, s.facnum).then(
      (montco) => {
        receipt += montco.toFixed(2);
      },
      (error) => {
        alert("Error on getting the amount charged per invoice/charge")
      });*/



      //receipt += s.salfac.toFixed(2);
      //receipt += "  ";

      /*if (this.charge.cntesp) {
        receipt += " IVA Retenido: ";
        value_length = mt.toString().length;
        space = 12 - value_length;
        for (let i = 1; i <= space; i++) {
          receipt += " ";
        }
        receipt += mt + '\r\n';
      }*/

      /*receipt += "Total Cobranza: ";
      let total: number = 0;
      if(this.charge.cntesp){
        total = bi + mt;
      }else{
        total = bi;
      }*/
      /*value_length = total.toString().length;
      space = 12 - value_length;
      for (let i = 1; i <= space; i++) {
        receipt += " ";
      }
      receipt += total.toFixed(2) + '\r\n';*/


      //receipt += pretprnt;

    });

    receipt += '\r\n\r\n\r\n';
    let metpag = "";
    switch (this.charge.metpag) {
      case '0':
        metpag = "Efectivo";
        break;
      case '1':
        metpag = "Transferencia";
        break;
      case '2':
        metpag = "Cheque";
        break;
      case '3':
        metpag = "Deposito";
        break;
      default:
        metpag = "Efectivo";
        break;
    }
    receipt += "Metodo de Pago: " + metpag + "\r\n";
    if (this.charge.metpag == 2) {
      receipt += "Nro Cheque: " + this.charge.numchq + "\r\n";
      receipt += "Banco: " + this.charge.codban + "\r\n";
      receipt += "Nro Cuenta Emisora: " + this.charge.ctachq + "\r\n";
    }
    if (this.charge.metpag == 1 || this.charge.metpag == 3) {
      receipt += "Nro Referencia: " + this.charge.referc + "\r\n";
      receipt += "Banco: " + this.selected_bank + "\r\n";
    }

    receipt += "Monto Pagado: " + Number(this.charge.montco).toFixed(2) + " " +  this.charge.divisa + "\r\n";

    return receipt;

  }

  PrintInvoice() {

    this.translate.get(["charges.error_printing_charge"], null).subscribe(
      (localizedValues) => {

        this.showMessageOnPrintingSuccess();

      });

  }

  presentLoading(data) {


    this.translate.get(["common.wait"], null).subscribe(
      (localizedValues) => {

        this.loader = this.loadingCtrl.create({
          content: localizedValues["common.wait"],
          spinner: "bubbles"
        });

        this.loader.present()
          .then(() => {

            this.printOrderPrinting(data);

          });

      });

  }

  showMessageOnPrintingSuccessOrFailure(msg: any) {

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.closeViews();
              }
            }
          ]
        });
        alert.present();

      });
  }

  showMessageOnPrintingSuccess() {


    this.translate.get(["common.acept", "charges.success_printed"], null).subscribe(
      (localizedValues) => {

        console.log('Entro en mi metodo');
        let alert = this.alertCtrl.create({
          message: localizedValues["charges.success_printed"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                if (this.printed_copy === true) {
                  this.closeViews();
                } else {
                  this.database.openDatabase().then(
                    () => {
                      this.database.getPrncpy().then(
                        (print_copy) => {
                          if (print_copy == 'true') {
                            console.log('prncpy: ' + true);
                            this.showMessagePrintCopy();
                          } else {
                            console.log('prncpy: ' + false);
                            this.closeViews();
                          }
                        },
                        (error) => {
                        });
                    });
                }
              }
            }
          ]
        });
        alert.present();

      });
  }

  showMessagePrintCopy() {


    this.translate.get(["common.acept", "common.cancel", "common.confirm", "basket.print_copy_confirm"], null).subscribe(
      (localizedValues) => {

        this.printed_copy = true;
        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["basket.print_copy_confirm"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.closeViews();
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.presentLoading(this.printer_name);
              }
            }
          ]
        });
        alert.present();

      });

  }

  closeViews() {
    /*this.indexes.forEach((v) => {
      this.navCtrl.remove(v);
    })*/
    this.navCtrl.pop();
  }

}
