import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, App, ViewController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { DbChangesService } from '../../providers/db-changes-service';
import { TranslateService } from 'ng2-translate';
import { AlertsService } from '../../providers/alerts.service';
import { CobranzaProcesarPage } from '../cobranza-procesar/cobranza-procesar';
import { ComprobanteRetencionPage } from '../comprobante-retencion/comprobante-retencion';
import { isNumber } from 'ionic-angular/umd/util/util';
import { AppUtils } from '../../providers/app-utils';

/*
  Generated class for the Cobranza page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cobranza',
  templateUrl: 'cobranza.html'
})
export class CobranzaPage {

  selected_invoices: any[] = [];
  invoices: any[] = [];
  indexes: any[] = [];
  invoices_already_charged: any[] = [];
  type: string = 'statement';
  deshabilitar_cobrar: boolean = true;
  charge: any = {};
  show_banks_list: any = false;
  show_reference: any = false;
  show_check_a_number: any = false;
  show_check_number: any = false;
  segment_title: string;
  account_state: any = {};
  bank_list: any[] = [];
  currencies: any[] = [];
  cobfas: string = 'open';
  invoices_amount: any = 0;
  invoices_amount_shadow: any = 0;
  invoice_selected: any = false;
  numero_actual_cobranza: string = '';
  available_invoices: any[] = [];
  check_bank_list: any[] = [];
  invoices_count: number = 0;
  masks: any;
  date_1_3: any;
  max_date: any;
  montco_ui:number = 0;
  //cash_control_final: any[] = [{"name":1, "quantity":0}, {"name":2, "quantity":0}, {"name":5, "quantity":0}, {"name":10, "quantity":0}, {"name":20, "quantity":0}, {"name":50, "quantity":0}, {"name":100, "quantity":0}, {"name":200, "quantity":0}, {"name":500, "quantity":0}];
  cash_control: any[] = [];
  cash_denomination: any[] = [1, 2, 5, 10, 20, 50, 100, 200, 500];
  cash_control_final: any[] = [];
  special_tax: boolean = false;
  

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public app: App,
    public view: ViewController,
    public navParams: NavParams,
    public database: DbService,
    public dbChangesService: DbChangesService,
    private translate: TranslateService,
    private alertServ: AlertsService) {

    /*if(this.navParams.data.invoices){

      this.invoices = this.navParams.data.invoices;
 
    }*/

    //this.setUpMask();

    if (this.navParams.data.charge) {


      this.charge = this.navParams.data.charge;
      this.max_date = new Date().toISOString();

      this.getInvoicesToCharge();
      ///charge already created
      if (this.charge.cobnum) {
        //alert("charge already created: " + this.numero_actual_cobranza);
        //this.charge.montco = parseFloat(this.charge.montco);
        this.montco_ui = Number(this.charge.montco);
        this.numero_actual_cobranza = this.charge.cobnum;
        this.getChargeDate_1_3(this.numero_actual_cobranza);
        this.initExisting();
        if (this.charge.metpag == 0) {
          this.getCash(this.charge.cobnum);
        }
      } else {//charge just created
        this.date_1_3 = new Date().toISOString();
        this.numero_actual_cobranza = this.database.getCurrentChargeNumber();
        this.charge.cobnum = this.numero_actual_cobranza;
        this.montco_ui = this.charge.montco;
        //alert("current charge number: " + this.numero_actual_cobranza);
        this.init();
      }

    }

    if (this.navParams.data.account_state) {

      this.account_state = this.navParams.data.account_state;

    }

    this.indexes.push(this.view.index);

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CobranzaPage');
    //this.getInvoicesToCharge();
  }

  getCash(cobnum: any){

    this.database.getCash(cobnum).then(
      (cashies: any) => {
        //console.log('divisa: ' + badge);
        if(cashies.cant_1 > 0){
          this.cash_control.push({"name":1, "quantity":cashies.cant_1}); 
        }

        if(cashies.cant_2 > 0){
          this.cash_control.push({"name":2, "quantity":cashies.cant_2}); 
        }

        if(cashies.cant_5 > 0){
          this.cash_control.push({"name":5, "quantity":cashies.cant_5}); 
        }

        if(cashies.cant_10 > 0){
          this.cash_control.push({"name":10, "quantity":cashies.cant_10}); 
        }

        if(cashies.cant_20 > 0){
          this.cash_control.push({"name":20, "quantity":cashies.cant_20}); 
        }

        if(cashies.cant_50 > 0){
          this.cash_control.push({"name":50, "quantity":cashies.cant_50}); 
        }

        if(cashies.cant_100 > 0){
          this.cash_control.push({"name":100, "quantity":cashies.cant_100}); 
        }

        if(cashies.cant_200 > 0){
          this.cash_control.push({"name":200, "quantity":cashies.cant_200}); 
        }

        if(cashies.cant_500 > 0){
          this.cash_control.push({"name":500, "quantity":cashies.cant_500}); 
        }
           
      });

  }

  addCashie() {

    let quantity_empty = this.cash_control.find(c => c.quantity == 0);

    if (typeof quantity_empty == 'undefined') {

      let denominations = [];

      this.cash_control.forEach((d)=>{
        
        denominations.push(d.name);
  
  
      });
  
      let available_denomination = [];
  
      this.cash_denomination.forEach((d)=>{
  
        if (denominations.indexOf(d) < 0) {
  
          available_denomination.push(d);
  
        }
        
      });
  
      let cash_line = {"name":available_denomination[0], "quantity":0};
  
      this.cash_control.push(cash_line);
      
    }

  }

  validateCachie(denomination: any, i: any){

    let denominations = [];

    for (let index = 0; index < this.cash_control.length; index++) {
      
      if (index != i) {
        denominations.push(this.cash_control[index].name);
      }
      
    }


    let den_index = denominations.indexOf(denomination);

    if(den_index > -1 ){

      this.deleteCashie(i);

    }

  }


  deleteCashie(line: any) {
    this.cash_control.splice(line, 1);
    
  }

  initExisting() {

    this.database.getCurrencies().then(
      (curr: any) => {
        //console.log('divisa: ' + badge);
        this.currencies = curr;        
      });

    this.loadBanks();
    this.loadBanksForCheck();
    this.getInvoicesAlreadyCharged();
    //this.changePaymentMethod();

  }

  getChargeDate_1_3(cobnum: any){
    this.database.getChargeDate_1_3(cobnum).then(
      (fechcr) => {
        this.date_1_3 = new Date(fechcr.toString()).toISOString();
      },
      error => {
        console.log('Error obteniendo la fecha 1_3');

      });
  }

  setUpMask() {

    this.masks = {

      montco: [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]

    };


  }

  init() {

    /*this.invoices.forEach(
      (i) => {        
        i.seleccionada = false; 
      }
    )*/

    console.log('ionViewDidLoad CobranzaPage');
    this.charge.metpag = 0;
    this.charge.monret = 0;
    //this.charge.montco = Number(this.charge.montco);
    //this.account_state.cntesp = true;

    this.database.getCurrencies().then(
      (curr: any) => {
        //console.log('divisa: ' + badge);
        this.currencies = curr;
        this.charge.divisa = this.currencies[0].coddiv;
      });

    this.loadBanks();
    this.loadBanksForCheck();
    this.getInvoicesAlreadyCharged();

  }

  loadBanks() {

    this.database.getBanks().then(
      (banks) => {
        banks.forEach(
          (element) => {
            console.log('codban:' + element.codban);
            console.log('desban:' + element.desban);
          });
        this.bank_list = banks;
      });

  }

  loadBanksForCheck() {

    this.database.getBanksNEW().then(
      (banks) => {
        banks.forEach(
          (element) => {
            console.log('codban:' + element.nomban);
          });
        this.check_bank_list = banks;
      });

  }

  showMessage(msg: any) {

    this.translate.get(["common.acept", "common.cancel"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            /*{
              text: localizedValues["common.cancel"],
              handler: () => {
                console.log('cancelar');
              }
            },*/
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
                //this.goToCobranzaProcesar();
              }
            }
          ]
        });
        alert.present();

      });

  }

  formataNumero(e: any, separador: string = '.', decimais: number = 2) {
    let a: any = e.value.split('');
    let ns: string = '';
    a.forEach((c: any) => { if (!isNaN(c)) ns = ns + c; });
    ns = parseInt(ns).toString();
    if (ns.length < (decimais + 1)) { ns = ('0'.repeat(decimais + 1) + ns); ns = ns.slice((decimais + 1) * -1); }
    let ans = ns.split('');
    let r = '';
    for (let i = 0; i < ans.length; i++) if (i == ans.length - decimais) r = r + separador + ans[i]; else r = r + ans[i];
    //e.value = r;
    this.charge.montco = r;
  }

  showMessageSpecialTaxPayer(msg: any) {

    this.translate.get(["common.acept", "common.cancel"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                console.log('cancelar');
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
                this.goToComprobanteRetencion();
              }
            }
          ]
        });
        alert.present();

      });

  }

  changeInvoiceChecked(invoice: any) {
    

        this.database.getExchangeRate(invoice.coddiv, this.charge.divisa)
        .then((tasa) => {          

          if (Boolean(this.account_state.cntesp)) {

            if (invoice.seleccionada) {
  
              console.log("factura seleccionada true changeInvoiceChecked");
  
  
              let bi = invoice.salfac - invoice.ivafac;
              let tax = invoice.ivafac;
              let mt = tax * 0.75;
              tax = tax - mt;
              bi = bi + tax;
              let exchange_rate = ((bi - invoice.salcob)) * tasa;
              this.invoices_amount = this.invoices_amount + exchange_rate;
              this.charge.monret = this.charge.monret + (mt * tasa);
              this.charge.montco = this.invoices_amount.toFixed(2);
              this.montco_ui = Number(this.charge.montco);
              this.invoices_count++;
  
  
  
            } else {
  
              console.log("factura seleccionada false changeInvoiceChecked");
  
              let bi = invoice.salfac - invoice.ivafac;
              let tax = invoice.ivafac;
              let mt = tax * 0.75;
              tax = tax - mt;
              bi = bi + tax;
              let exchange_rate = ((bi - invoice.salcob)) * tasa;
              this.invoices_amount = this.invoices_amount - exchange_rate;
              this.charge.monret = this.charge.monret - (mt * tasa);
              this.charge.montco = this.invoices_amount.toFixed(2);
              this.montco_ui = Number(this.charge.montco);
              this.invoices_count--;
  
  
            }
  
  
          } else {
  
            if (invoice.seleccionada) {
  
              this.database.invoiceAlreadyCharged(invoice.facnum).then(
                (invoices) => {

                  let exchange_rate = invoice.salfac * tasa;  
                  this.invoices_amount = this.invoices_amount + exchange_rate;
                  this.charge.montco = this.invoices_amount.toFixed(2);
                  this.montco_ui = Number(this.charge.montco);
                  this.invoices_count++;
  
                  /*if (invoices.length > 0)  {
  
                    if (invoices[0].cobnum != this.numero_actual_cobranza){
                      invoice.seleccionada = false;
                      this.alertServ.showAlertError(localizedValues["charges.invoice_already_charged"]);
                    }                 
                    
                  }*/
  
                },
                error => {
                  alert('Error consultando la factura ya asociada a cobranza');
                });
  
  
  
            } else {
  
              let exchange_rate = invoice.salfac * tasa;
              this.invoices_amount = this.invoices_amount - exchange_rate;
              this.charge.montco = this.invoices_amount.toFixed(2);
              this.montco_ui = Number(this.charge.montco);
              this.invoices_count--;
  
            }
  
          }

        })
        .catch(() => {
          console.log('Error obteniendo la tasa de cambio de la factura ');
        })        

      

  }


  changeDivisa(){

    console.log('changeDivisa called');
    this.invoices_amount = 0;
    this.charge.monret = 0;
    this.charge.montco = 0;
    this.invoices_count = 0;

    this.available_invoices.forEach(
      (invoice) => {

        this.database.getExchangeRate(invoice.coddiv, this.charge.divisa)
        .then((tasa) => {
          

          if (Boolean(this.account_state.cntesp)) {

            if (invoice.seleccionada) {
  
              console.log("factura seleccionada true changeInvoiceChecked");
  
  
              let bi = invoice.salfac - invoice.ivafac;
              let tax = invoice.ivafac;
              let mt = tax * 0.75;
              tax = tax - mt;
              bi = bi + tax;
              let exchange_rate = ((bi - invoice.salcob)) * tasa;
              this.invoices_amount = this.invoices_amount + exchange_rate;
              this.charge.monret = this.charge.monret + (mt * tasa);
              this.charge.montco = this.invoices_amount.toFixed(2);
              this.montco_ui = Number(this.charge.montco);
              this.invoices_count++;
  
  
  
            } 
  
  
          } else {
  
            if (invoice.seleccionada) {
  
              this.database.invoiceAlreadyCharged(invoice.facnum).then(
                (invoices) => {

                  let exchange_rate = invoice.salfac * tasa;  
                  this.invoices_amount = this.invoices_amount + exchange_rate;
                  this.charge.montco = this.invoices_amount.toFixed(2);
                  this.montco_ui = Number(this.charge.montco);
                  this.invoices_count++;
  
                  /*if (invoices.length > 0)  {
  
                    if (invoices[0].cobnum != this.numero_actual_cobranza){
                      invoice.seleccionada = false;
                      this.alertServ.showAlertError(localizedValues["charges.invoice_already_charged"]);
                    }                 
                    
                  }*/
  
                },
                error => {
                  alert('Error consultando la factura ya asociada a cobranza');
                });
  
  
            } 
  
          }

        })
        .catch(() => {
          console.log('Error obteniendo la tasa de cambio de la factura ');
        })    

      }
    )

  }

  changePaymentMethod() {

    if (this.charge.metpag == 1 || this.charge.metpag == 3) {

      this.show_banks_list = true;
      this.show_reference = true;

      this.show_check_a_number = false;
      this.show_check_number = false;

    } else if (this.charge.metpag == 2) {
      this.show_banks_list = true;
      this.show_check_a_number = true;
      this.show_check_number = true;

      this.show_reference = false;
    } else {

      this.show_banks_list = false;
      this.show_check_a_number = false;
      this.show_check_number = false;
      this.show_reference = false;


    }

  }

  makeCharge() {
    this.translate.get(["errors.amount_mand", "errors.refer_mand", "errors.cash_not_equal", "errors.check_a_numb_mand", "errors.check_numb_mand", "errors.doc_numb_mand", "errors.rete_numb_mand", "errors.invoi_balan_less", "errors.bank_mand", "charges.no_selected_invoices", "charges.no_equal_amounts"], null).subscribe(
      (localizedValues) => {
        if (this.charge.montco == '' || this.charge.montco == null) {
          this.alertServ.showAlertError(localizedValues["errors.amount_mand"]);
        } else {
          let show_msg = false;
          switch (this.charge.metpag) {
            /**********CASH CONTROL*********/
            /*case 0:
              let cash_sum = 0;
              this.cash_control.forEach((d) => {
                cash_sum = cash_sum + (d.name * d.quantity);
              });

              console.log("Cash Sum: " + cash_sum + "  Total: " + this.montco_ui);  

              if (this.montco_ui < cash_sum) {
                this.alertServ.showAlertError(localizedValues["errors.cash_not_equal"]);
                show_msg = true;
              }
              break;*/
              /**********CASH CONTROL*********/
            case 3:
            case 1:
              if (this.charge.codban == '' || this.charge.codban == null) {
                this.alertServ.showAlertError(localizedValues["errors.bank_mand"]);
                show_msg = true;
              } else {
                if (this.charge.referc == '' || this.charge.referc == null) {
                  this.alertServ.showAlertError(localizedValues["errors.refer_mand"]);
                  show_msg = true;
                }
              }
              break;
            case 2:
              if (this.charge.codban == '' || this.charge.codban == null) {
                this.alertServ.showAlertError(localizedValues["errors.bank_mand"]);
                show_msg = true;
              } else {
                if (this.charge.ctachq == '' || this.charge.ctachq == null) {
                  this.alertServ.showAlertError(localizedValues["errors.check_a_numb_mand"]);
                  show_msg = true;
                } else {
                  if (this.charge.numchq == '' || this.charge.numchq == null) {
                    this.alertServ.showAlertError(localizedValues["errors.check_numb_mand"]);
                    show_msg = true;
                  }
                }
              }
              break;
          }
          if (show_msg == false) {
            this.getSelectedInvoices();
            this.setCashie();
            if (Boolean(this.account_state.cntesp)) {
              /*if(this.charge.document_number == '' || this.charge.document_number == null) {
                this.alertServ.showAlertError(localizedValues["errors.doc_numb_mand"]);
              } else {
                if(this.charge.retention_number == '' || this.charge.retention_number == null) {
                  this.alertServ.showAlertError(localizedValues["errors.rete_numb_mand"]);
                } else {
                  if(this.isAmountInvoiceGE()) {
                    this.alertServ.showAlertError(localizedValues["charges.no_equal_amounts"]);
                  } else {
                    this.charge.cntesp = this.account_state.cntesp;
                    this.charge.codcli = this.account_state.codcli;
                    this.navCtrl.push(CobranzaProcesarPage, {invoices:this.selected_invoices, charge:this.charge});
                  }
                }
              }*/

              if (!this.selectedInvoicesTaxesOk) {
                this.showMessage(localizedValues["charges.charge_less_tax"]);
              } else {
                this.goToComprobanteRetencion();
              }

            } else {
              /*if(!this.isAmountInvoiceGE()) {
                this.alertServ.showAlertError(localizedValues["errors.invoi_balan_less"]);
              } else {
                if(this.selected_invoices.length == 0){
                  this.showMessage(localizedValues["charges.no_selected_invoices"]);
                }          
                this.charge.cntesp = this.account_state.cntesp;
                this.charge.codcli = this.account_state.codcli;
                this.navCtrl.push(CobranzaProcesarPage, {invoices:this.selected_invoices, charge:this.charge});
              }*/
              if (!this.selectedInvoicesTaxesOk) {
                this.showMessage(localizedValues["charges.charge_less_tax"]);
              } else {
                this.goToCobranzaProcesar();
              }

            }
          }
        }
      });
  }

  isAmountInvoiceGE() {

    return (this.invoices_amount != this.charge.montco);

  }

  getSelectedInvoices() {
    this.selected_invoices = [];
    this.available_invoices.forEach((i) => {
      if (i.seleccionada) {
        this.selected_invoices.push(i);
      }
    });
  }

  setCashie(){

    let denominations = [];
    this.cash_control_final = []


      this.cash_control.forEach((d)=>{
        
        denominations.push(d.name);
        this.cash_control_final.push(d);
  
      });


      this.cash_denomination.forEach((d)=>{
  
        if (denominations.indexOf(d) < 0) {
  
          this.cash_control_final.push({"name":d, "quantity":0});
  
        }
        
      });

      this.cash_control_final.sort((a, b)=> a - b);

    
  }

  selectedInvoicesTaxesOk() {
    let sum = 0;
    this.selected_invoices.forEach((i) => {
      if (i.seleccionada) {
        sum += i.ivafac;
      }
    });
    return this.charge.montco >= sum && this.selected_invoices.length > 0;
  }

  /*goToCobranzaProcesar() {

    this.charge.cntesp = this.account_state.cntesp;
    //this.charge.codcli = this.account_state.codcli;
    this.database.updateChargePhaseStatus(this.numero_actual_cobranza, 1, 1, this.charge).then(
      (res) => {
        this.database.deleteChargeInvoices(this.numero_actual_cobranza).then(
          (res) => {
            //this.alertServ.showAlert('La cobranza fue almacenada con exito');
            this.database.createChargeInvoices(this.numero_actual_cobranza, this.selected_invoices).then(
              (res) => {
                //this.alertServ.showAlert('La cobranza fue almacenada con exito');                        
                this.navCtrl.push(CobranzaProcesarPage, { invoices: this.selected_invoices, charge: this.charge, indexes: this.indexes });
              },
              (error) => {
                this.alertServ.showAlertError('Las facturas de la cobranza no fueron insertadas ');
              });
          },
          (error) => {
            this.alertServ.showAlertError('Las facturas de la cobranza no fueron removidas ');
          });

      },
      (error) => {
        this.alertServ.showAlertError('La cobranza cabecera no se actualizo ');
      });

  }*/

  goToCobranzaProcesar() {

    this.charge.cntesp = this.account_state.cntesp;
    this.charge.montco = this.montco_ui;
    //this.charge.codcli = this.account_state.codcli;
    this.database.updateChargePhaseStatus(this.numero_actual_cobranza, 1, 1, this.charge).then(
      (res) => {
        this.database.deleteChargeInvoices(this.numero_actual_cobranza).then(
          (res) => {
            //this.alertServ.showAlert('La cobranza fue almacenada con exito');
            this.database.createChargesxInvoicesRenewed(this.charge, this.numero_actual_cobranza, this.selected_invoices, this.date_1_3).then(
              (res) => {                
                //this.alertServ.showAlert('La cobranza fue almacenada con exito'); 
                
                this.database.deleteCashControlxCharge(this.charge.cobnum).then(
                  (res) => {

                    this.database.saveCashControl(this.charge, this.cash_control_final).then(
                      (res) => {
                        this.database.updateInvoicesChargedAmount(this.charge, this.available_invoices).then(
                          (res) => {
        
                            //this.navCtrl.push(ComprobanteRetencionPage, { invoices: this.selected_invoices, charge: this.charge, indexes: this.indexes });
                            this.navCtrl.push(CobranzaProcesarPage, { invoices: this.selected_invoices, charge: this.charge, indexes: this.indexes });
                          },
                          (error) => {
                            this.alertServ.showAlertError('La cantidad cobrada de las facturas no fue actualizada exitosamente');
                          });
    
                      },
                      (error) => {
                        this.alertServ.showAlertError('El control de efectivo no pudo ser guardadao');
                      });
               
                  },
                  (error) => {
                    this.alertServ.showAlertError('El control de efectivo no pudo ser borrado');
                  });
 
              },
              (error) => {
                this.alertServ.showAlertError('Las facturas de la cobranza no fueron insertadas ');
              });
          },
          (error) => {
            this.alertServ.showAlertError('Las facturas de la cobranza no fueron removidas ');
          });
      },
      (error) => {
        this.alertServ.showAlertError('La cobranza cabecera no se actualizo ');
      });

  }

  goToComprobanteRetencion() {

    this.charge.cntesp = this.account_state.cntesp;
    this.charge.montco = this.montco_ui;
    //this.charge.codcli = this.account_state.codcli;
    this.database.updateChargePhaseStatus(this.numero_actual_cobranza, 1, 1, this.charge).then(
      (res) => {
        this.database.deleteChargeInvoices(this.numero_actual_cobranza).then(
          (res) => {
            //this.alertServ.showAlert('La cobranza fue almacenada con exito');
            this.database.createChargesxInvoicesRenewed(this.charge, this.numero_actual_cobranza, this.selected_invoices, this.date_1_3).then(
              (res) => {                
                //this.alertServ.showAlert('La cobranza fue almacenada con exito'); 
                
                this.database.deleteCashControlxCharge(this.charge.cobnum).then(
                  (res) => {

                    this.database.saveCashControl(this.charge, this.cash_control_final).then(
                      (res) => {
                        this.database.updateInvoicesChargedAmount(this.charge, this.available_invoices).then(
                          (res) => {
        
                            this.navCtrl.push(ComprobanteRetencionPage, { invoices: this.selected_invoices, charge: this.charge, indexes: this.indexes });
                          },
                          (error) => {
                            this.alertServ.showAlertError('La cantidad cobrada de las facturas no fue actualizada exitosamente');
                          });
    
                      },
                      (error) => {
                        this.alertServ.showAlertError('El control de efectivo no pudo ser guardadao');
                      });
               
                  },
                  (error) => {
                    this.alertServ.showAlertError('El control de efectivo no pudo ser borrado');
                  });
 
              },
              (error) => {
                this.alertServ.showAlertError('Las facturas de la cobranza no fueron insertadas ');
              });
          },
          (error) => {
            this.alertServ.showAlertError('Las facturas de la cobranza no fueron removidas ');
          });
      },
      (error) => {
        this.alertServ.showAlertError('La cobranza cabecera no se actualizo ');
      });

  }

  getInvoicesToCharge() {
    this.database.getInvoicesToCharge(this.charge.codcli)
      .then(
        (invoices) => {

          if (Boolean(this.account_state.cntesp)) {

            invoices.forEach((invoice) => {
              let bi = invoice.salfac - invoice.ivafac;
              let tax = invoice.ivafac;
              let mt = tax * 0.75;
              tax = tax - mt;
              bi = bi + tax;

              if (bi >= invoice.salcob) {
                invoice.salreal = bi.toFixed(2);
                invoice.diff = Number(invoice.salreal - invoice.salcob).toFixed(2);
                this.invoices.push(invoice);
                console.log('Invoice: ' + invoice.facnum + ' chargable: ' + invoice.salreal);
              }

            })

          } else {

            invoices.forEach((invoice) => {

              invoice.salreal = invoice.salfac;
              this.invoices.push(invoice);

            })

          }

        })
      .catch(() => {
        console.log('Error obteniendo las facturas por cobrar');
      })
  }

  getInvoicesAlreadyCharged() {
    this.database.getChargeInvoices(this.numero_actual_cobranza).then(
      (invoices) => {
        this.invoices_already_charged = invoices;
        console.log('facturas asociadas a cobranzas obtenidas');
        this.checkInvoices();
      },
      error => {
        console.log('Error obteniendo las facturas ya asociadas a cobranza');

      });
  }

  checkInvoices() {

    this.charge.monret = 0;

    /*this.invoices_already_charged.forEach(
      (f) => {
        console.log("factura asociada a cobranza: " + f.facnum);
      }
    )*/

    this.invoices.forEach(
      (i) => {
        if (this.invoices_already_charged.indexOf(i.facnum) > -1) {
          this.database.getInvoiceCharge(i.facnum, this.numero_actual_cobranza).then(
            (charges) => {
              //alert("current charge number inside loop: " + this.numero_actual_cobranza);
              if (charges.length > 0) {
                i.seleccionada = true;              
                i.diff = charges[0].montco;
                i.salcob = i.salcob - charges[0].montco;
                i.ivaret = i.ivaret - charges[0].monret;
                this.available_invoices.push(i);
              }
            },
            error => {
              alert('Error obteniendo el cobnum de la factura');
            });
          console.log("factura asociada a cobranza");
          console.log("factura seleccionada true");
          //this.invoices_amount = this.invoices_amount + i.salfac;
        } else {


          if (Number(i.diff) > 0) {
            console.log('Fac: '  + i.facnum + 'Diff: ' + i.diff);
            i.seleccionada = false;
            this.available_invoices.push(i);
          }
          /*this.database.getInvoiceCharge(i.facnum).then(
            (charge) => {
              if(charge == null){
                i.seleccionada = false;
                this.available_invoices.push(i); 
                console.log("factura no asociada a alguna cobranza");  
              }              
            },
            error => {
              alert('Error obteniendo el cobnum de la factura');
            }); */

        }
      }
    )

    //this.invoices_amount = this.invoices_amount - this.charge.monret;
    //this.available_invoices.sort((a, b) => a.fecfac - b.fecfac);

}

}
