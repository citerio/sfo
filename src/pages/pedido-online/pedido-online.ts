import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { CestaPage } from '../cesta/cesta';
import { Diagnostic } from '@ionic-native/diagnostic';
import {TranslateService} from 'ng2-translate/ng2-translate';
import { Geolocation } from '@ionic-native/geolocation';
import { AlertsService } from '../../providers/alerts.service';
import { Storage } from '@ionic/storage';

import { AppUtils } from '../../providers/app-utils';
import { GenericRestService } from "../../providers/generic-rest-service";
import { AppConstants } from "../../providers/app-constants";
/*
  Generated class for the Pedido page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-pedido-online',
  templateUrl: 'pedido-online.html'
})
export class PedidoOnline {

  clientes_vista:any[] = [];
  filter: string = "";
  offset: number = 0;

  constructor(
    public navCtrl: NavController,
    public view: ViewController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public database: DbService,
    private diagnostic: Diagnostic,
    private translate: TranslateService,
    public geolocation: Geolocation,
    public loadingCtrl: LoadingController,
    public alertsServ: AlertsService,
    public genericRestService: GenericRestService,
    private storage: Storage) {}

  
  ionViewDidLoad() {
    // this.getAllClients();
    //this.getAsocClients();
    //this.getAllClientsOffset();
  }

  getAllClientsOffset(){
    this.database.openDatabase().then(
      () => {
        this.database.getAllClientsOffset(this.offset)
        .then(response => {
          for(let i = 0; i < response.length; i++){
            this.clientes_vista.push(response[i]);
          }
        });
      });
  }

  // filterItems(ev: any){

  //     this.clientes = this.clientes_copia;

  //     let val = ev.target.value;

  //   // if the value is an empty string don't filter the items
  //     if (val && val.trim() != '') {
  //       this.clientes = this.clientes.filter((item) => {
  //         return (item.descli.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.codcli.includes(val));
  //       })
  //     }    
 
  // }

  filterItems(ev: any){
    this.filter = ev.target.value;
    if (this.filter) {
      if(this.filter.trim() != '') {
        this.clientes_vista = [];

        
        
                          this.genericRestService.getFromUrl(AppConstants.URL_GET_CLIENTS_ONLINE + this.filter).then(
                          (res: any) => {
                            if(res.status == 202) {
                              console.log('SERVER: Clientes online success');

                              try {

                                let body = JSON.parse(res._body);
                                console.log('APP: success lista clientes online');

                                for(let i = 0; i < body.length; i++){
                                  this.clientes_vista.push(body[i]);
                                }

                                
                              } catch(e) {
                                console.log('APP: failure lista clientes online');
                              }                             
                              

                            } else {
                              console.log('SERVER: Clientes online failure');
                              
                            }
                          },
                          (error: any) => {
                            console.log('API REST failure: ' + error);
                          });



      } 
    } 
  }

  goToCesta(cliente: any){
    this.diagnostic.isLocationEnabled().then(
      (setting) => {
          if(setting == true) {
            this.translate.get(["get_coord", "errors.not_in_peri_gps_orde"], null).subscribe(
              (localizedValues) => {
                let loading = this.loadingCtrl.create({
                  content: localizedValues["get_coord"]
                });
                loading.present();
                
                this.geolocation.getCurrentPosition({timeout: 12000}).then(
                  (position) => {
                    let geopcr = position.coords.latitude.toString() + "," + position.coords.longitude.toString();
                    this.database.getGpsobl()
                    .then((gpsobl) => {
                      console.log('gpsobl: ' + gpsobl);
                      if(gpsobl == 'true') {
                        this.storage.get('gpsper')
                        .then((gpsper) => {
                          //latitud y longitud
                          let latcli = cliente.coogeo.split(",")[0];
                          let loncli = cliente.coogeo.split(",")[1];
                          console.log('geopcr: ' + geopcr);
                          console.log('latcli: ' + latcli + " loncli: " + loncli);
                          console.log('gpsper: ' + gpsper);
                          console.log('distance: ' + AppUtils.distance(position.coords.latitude, position.coords.longitude, latcli, loncli));
                          if(AppUtils.distance(position.coords.latitude, position.coords.longitude, latcli, loncli) <= gpsper) {
                            this.database.createOrder(cliente, geopcr, false)
                            .then((data) => {
                              loading.dismiss().then(
                                () => {
                                  this.navCtrl.push(CestaPage, {cliente:cliente});
                                  this.navCtrl.remove(this.view.index);
                                }
                              );
                            });
                          } else {
                            loading.dismiss().then(
                              () => {
                                this.alertsServ.showAlertError(localizedValues["errors.not_in_peri_gps_orde"]);
                              }
                            );
                          }
                        });
                      } else {
                        console.log('geopcr: ' + geopcr);
                        this.database.openDatabase().then(
                          () => {
                            this.database.createOrder(cliente, geopcr, false)
                            .then((data) => {
                              this.navCtrl.push(CestaPage, {cliente:cliente});
                              this.navCtrl.remove(this.view.index);
                            });
                          });
                        loading.dismiss();
                      }
                    });
                  }).catch((error) => { //El Gps esta encendido pero no obtuvo las coordenadas
                    console.log('no geopcr: '+ error);
                    this.database.getGpsobl()
                    .then((gpsobl) => {
                      if(gpsobl == 'true') {
                        loading.dismiss().then(
                          () => {
                            this.alertsServ.showAlertError('No se pudo obtener sus coordenada GPS. Revise su red o configuración GPS');
                          }
                        );
                      } else {
                        this.database.openDatabase().then(
                          () => {
                            this.database.createOrder(cliente, '', false)
                            .then((data) => {
                              this.navCtrl.push(CestaPage, {cliente:cliente});
                              this.navCtrl.remove(this.view.index);
                            });
                          });
                        loading.dismiss();
                      }
                    });
                  });
              });
          } else {
            this.database.openDatabase().then(
              () => {
                this.database.getGpsobl()
                .then((gpsobl) => {
                  if(gpsobl == 'true') {
                    console.log('gpsobl: ' + true);
                    this.showMessageTurnOnGPS();
                  } else {
                    console.log('gpsobl: ' + false);
                    this.showConfirmTurnOnGPS(cliente);
                  }
                });
              });
            // this.showAlert();
          }
      },
      (error) => {
        console.log('Entro en error buscando gps setting');
      });

    
  }

  

  showMessageTurnOnGPS() {
    this.translate.get(["gps_off", "gps_proc_acti", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["gps_proc_acti"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  showConfirmTurnOnGPS(cliente: any) {
    this.translate.get(["gps_off", "acti_gps", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["acti_gps"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.database.openDatabase().then(
                  () => {
                    this.database.createOrder(cliente, '', false)
                    .then((data) => {
                      this.navCtrl.push(CestaPage, {cliente:cliente});
                      this.navCtrl.remove(this.view.index);
                    });
                  });
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  showAlert() {
    this.translate.get(["gps_off", "gps_proc_acti", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["gps_proc_acti"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  doInfinite(infiniteScroll) {
    this.database.openDatabase().then(
      () => {
        if(this.filter == null || this.filter == "") {
          this.database.getAllClientsOffset(this.clientes_vista.length)
          .then(response => {
            for(let i = 0; i < response.length; i++){
              this.clientes_vista.push(response[i]);
            }

            infiniteScroll.complete();
            // if(this.productos_vista.length >= this.allProdLength){
            //   infiniteScroll.enable(false);
            // }
          });
        } else {
          this.database.getAllClientsOffsetByfilter(this.clientes_vista.length, this.filter)
          .then(response => {
            for(let i = 0; i < response.length; i++){
              this.clientes_vista.push(response[i]);
            }

            infiniteScroll.complete();
            // if(this.productos_vista.length >= this.allProdLength){
            //   infiniteScroll.enable(false);
            // }
          });
        }
      });
  }
}
