import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, App, ViewController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { DbChangesService } from '../../providers/db-changes-service';
import { TranslateService } from 'ng2-translate';
import { AlertsService } from '../../providers/alerts.service';
import { CobranzaProcesarPage } from '../cobranza-procesar/cobranza-procesar';
import { ComprobanteRetencionPage } from '../comprobante-retencion/comprobante-retencion';

/*
  Generated class for the CrearCobranza page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-crear-cobranza',
  templateUrl: 'crear-cobranza.html'
})
export class CrearCobranzaPage {

  selected_invoices: any[] = [];
  invoices: any[] = [];
  //invoices: any = {};
  indexes: any[] = [];
  invoices_already_charged: any[] = [];
  type: string = 'statement';
  deshabilitar_cobrar: boolean = true;
  charge: any = {};
  show_banks_list: any = false;
  show_reference: any = false;
  show_check_a_number: any = false;
  show_check_number: any = false;
  segment_title: string;
  account_state: any = {};
  bank_list: any[] = [];
  check_bank_list: any[] = [];
  cobfas: string = 'open';
  invoices_amount: any = 0;
  invoice_selected: any = false;
  numero_actual_cobranza: string = '';
  available_invoices: any[] = [];
  //chargesxinvoice: any[] = [];
  divisa: any;



  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public app: App,
    public view: ViewController,
    public navParams: NavParams,
    public database: DbService,
    public dbChangesService: DbChangesService,
    private translate: TranslateService,
    private alertServ: AlertsService) {

    if (this.navParams.data.charge) {

      this.charge = this.navParams.data.charge;
      this.getInvoicesToCharge();
      ///charge already created
      if (this.charge.cobnum) {
        this.numero_actual_cobranza = this.charge.cobnum;
        this.getInvoicesAlreadyCharged();
      } else {//charge just created
        this.numero_actual_cobranza = this.database.getCurrentChargeNumber();
        this.charge.cobnum = this.numero_actual_cobranza;
        this.getInvoicesAlreadyCharged();
      }

    }

    if (this.navParams.data.account_state) {

      this.account_state = this.navParams.data.account_state;

    }

    this.indexes.push(this.view.index);

    this.database.getBadge().then(
      (badge: string) => {
        console.log('divisa: ' + badge);
        this.divisa = badge;
      });

    this.loadBanks();
    this.loadBanksForCheck();



  }

  loadBanks() {

    this.database.getBanks().then(
      (banks) => {
        banks.forEach(
          (element) => {
            console.log('codban:' + element.codban);
            console.log('desban:' + element.desban);
          });
        this.bank_list = banks;
      });

  }

  loadBanksForCheck() {

    this.database.getBanksNEW().then(
      (banks) => {
        banks.forEach(
          (element) => {
            console.log('codban:' + element.nomban);            
          });
        this.check_bank_list = banks;
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CrearCobranzaPage');
  }

  showMessage(msg: any) {

    this.translate.get(["common.acept", "common.cancel"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                console.log('cancelar');
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
                //this.goToCobranzaProcesar();
              }
            }
          ]
        });
        alert.present();

      });

  }

  showMessageSpecialTaxPayer(msg: any) {

    this.translate.get(["common.acept", "common.cancel"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                console.log('cancelar');
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
                this.goToComprobanteRetencion();
              }
            }
          ]
        });
        alert.present();

      });

  }

  showMessageMaximumAmountReached(amount: any) {

    this.translate.get(["common.acept", "common.cancel", "charges.max_charge_amount_reached"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          title: localizedValues["charges.max_charge_amount_reached"],
          message: this.divisa + " " + amount,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
              }
            }
          ]
        });
        alert.present();

      });

  }

  changePaymentMethod(i: any, j: any) {

    if (this.charge.metpag == 1 || this.charge.metpag == 3) {

      this.show_banks_list = true;
      this.show_reference = true;

      this.show_check_a_number = false;
      this.show_check_number = false;

    } else if (this.charge.metpag == 2) {
      this.show_banks_list = true;
      this.show_check_a_number = true;
      this.show_check_number = true;

      this.show_reference = false;
    } else {

      this.show_banks_list = false;
      this.show_check_a_number = false;
      this.show_check_number = false;
      this.show_reference = false;


    }

  }

  getInvoicesToCharge() {

    this.database.getInvoicesToCharge(this.charge.codcli)
      .then(
        (invoices) => {
          this.invoices = invoices;
        })
      .catch(() => {
        console.log('Error obteniendo las facturas por cobrar');
      })
  }

  getInvoicesAlreadyCharged() {
    this.database.getChargeInvoices(this.numero_actual_cobranza).then(
      (invoices) => {
        this.invoices_already_charged = invoices;
        console.log('facturas asociadas a cobranzas obtenidas');
        this.checkInvoices();
      },
      error => {
        console.log('Error obteniendo las facturas ya asociadas a cobranza');
      });
  }

  checkInvoices2() {

    this.charge.monret = 0;

    /*this.invoices_already_charged.forEach(
      (f) => {
        console.log("factura asociada a cobranza: " + f.facnum);
      }
    )*/

    this.invoices.forEach(

      
      (i) => {
        if (this.invoices_already_charged.indexOf(i.facnum) > -1) {
          this.database.getInvoiceCharge(i.facnum, this.numero_actual_cobranza).then(
            (cobnum) => {
              /*if (this.numero_actual_cobranza == cobnum) {
                this.available_invoices.push(i);
                i.seleccionada = true;
              }*/
            },
            error => {
              alert('Error obteniendo el cobnum de la factura');
            });
          console.log("factura asociada a cobranza");
          console.log("factura seleccionada true");
          //this.invoices_amount = this.invoices_amount + i.salfac;
        } else {
          this.available_invoices.push(i);
          console.log("factura no asociada a cobranza");
        }
      }
    )
    //this.invoices_amount = this.invoices_amount - this.charge.monret;

  }

  checkInvoices() {



    /*this.invoices_already_charged.forEach(
      (f) => {
        console.log("factura asociada a cobranza: " + f.facnum);
      }
    )*/


    this.invoices.forEach(
      (i) => {
        let chargesxinvoice = [];
        this.invoices_already_charged.forEach(
          (j) => {
            if (i.facnum == j.facnum) {
              chargesxinvoice.push(j);
            }
          })
        let invoice = { header: i, charges: chargesxinvoice };
        this.available_invoices.push(invoice);
      }
    )
    //this.invoices_amount = this.invoices_amount - this.charge.monret;

  }

  getAccounStatement() {
    this.database.getAccountStatement(this.charge.codcli).then(
      (account_statement) => {
        this.account_state = account_statement;
      }
    )
      .catch((error) => {
        this.alertServ.showAlertError('error obteniendo el estado de cuenta');
      })
  }

  sumChargedAmount(invoice: any, charge: any) {

    /*this.available_invoices[invoice].header.salcob = 0;
    this.available_invoices[invoice].charges.forEach(charge => {

      if (charge.montco != "") {
        this.available_invoices[invoice].header.salcob = this.available_invoices[invoice].header.salcob + Number(charge.montco);
      }
    });*/

    this.available_invoices[invoice].header.salcob = this.available_invoices[invoice].header.salcob + Number(charge.montco);

    if (this.account_state.cntesp) {

      //bi base imponible
      //tax iva
      //mt monto retenido

      let bi = (( this.available_invoices[invoice].header.salfac * 100) / 116);
      let tax = (bi * 16) / 100;
      let mt = tax * 0.75;
      tax = tax - mt;
      bi = bi + tax;

      if ( this.available_invoices[invoice].header.salcob > bi) {
        this.showMessageMaximumAmountReached(bi);
        //alert("Monto maximo cobrable alcanzado: " + bi);
        this.available_invoices[invoice].header.salcob = this.available_invoices[invoice].header.salcob - Number(charge.montco);
        this.available_invoices[invoice].charges[charge] = bi - this.available_invoices[invoice].header.salcob;
        this.available_invoices[invoice].header.salcob = this.available_invoices[invoice].header.salcob + (bi - this.available_invoices[invoice].header.salcob);
      }

    } else {

      if (this.available_invoices[invoice].header.salcob > this.available_invoices[invoice].header.salfac) {
        this.showMessageMaximumAmountReached(this.available_invoices[invoice].header.salfac);
        //alert("Monto maximo cobrable alcanzado: " + this.available_invoices[invoice].header.salfac);
        this.available_invoices[invoice].header.salcob = this.available_invoices[invoice].header.salcob - Number(charge.montco);
        this.available_invoices[invoice].charges[charge] = this.available_invoices[invoice].header.salfac - this.available_invoices[invoice].header.salcob;
        this.available_invoices[invoice].header.salcob = this.available_invoices[invoice].header.salcob + (this.available_invoices[invoice].header.salfac - this.available_invoices[invoice].header.salcob);
      }
    }
  }


  sumAfterDeleteChargedAmount(invoice: any) {

    this.available_invoices[invoice].header.salcob = 0;
    this.available_invoices[invoice].charges.forEach(charge => {

      if (charge.montco != "") {
        this.available_invoices[invoice].header.salcob = this.available_invoices[invoice].header.salcob + Number(charge.montco);
      }
    });
    
  }


  addChargeXInvoice(invoice: any, i: any) {

    if (this.account_state.cntesp) {

      //bi base imponible
      //tax iva
      //mt monto retenido

      let bi = ((invoice.salfac * 100) / 116);
      let tax = (bi * 16) / 100;
      let mt = tax * 0.75;
      tax = tax - mt;
      bi = bi + tax;
      if (invoice.salcob < bi) {
        let charge = { 'cobnum': this.numero_actual_cobranza, 'facnum': invoice.facnum, 'coblin': 0, 'montco': bi - invoice.salcob, 'metpag': 0, 'codban': 0, 'referc': '', 'ctachq': '', 'numchq': '', 'divisa': this.divisa, 'fechcr': '' };
        this.available_invoices[i].charges.push(charge);
        this.available_invoices[i].header.salcob = bi - invoice.salcob;        
      }{
        this.showMessageMaximumAmountReached(bi);
        //alert("Monto maximo cobrable alcanzado: " + bi);
      }

    } else {

      if (invoice.salcob < invoice.salfac) {
        let charge = { 'cobnum': this.numero_actual_cobranza, 'facnum': invoice.facnum, 'coblin': 0, 'montco': invoice.salfac - invoice.salcob, 'metpag': 0, 'codban': 0, 'referc': '', 'ctachq': '', 'numchq': '', 'divisa': this.divisa, 'fechcr': '' };
        this.available_invoices[i].charges.push(charge);
        this.available_invoices[i].header.salcob = invoice.salfac - invoice.salcob;
      }{
        this.showMessageMaximumAmountReached(invoice.salfac);
        //alert("Monto maximo cobrable alcanzado: " + invoice.salfac);

      }
    }

  }


  deleteChargeXInvoice(invoice: any, charge: any) {
    this.available_invoices[invoice].charges.splice(charge, 1);
    this.sumAfterDeleteChargedAmount(invoice);
  }

  goToComprobanteRetencion() {

    this.charge.cntesp = this.account_state.cntesp;
    //this.charge.codcli = this.account_state.codcli;
    this.database.updateChargePhaseStatus(this.numero_actual_cobranza, 1, 1, this.charge).then(
      (res) => {
        this.database.deleteChargesxInvoices(this.numero_actual_cobranza).then(
          (res) => {
            //this.alertServ.showAlert('La cobranza fue almacenada con exito');
            this.database.createChargesxInvoices(this.numero_actual_cobranza, this.available_invoices).then(
              (res) => {
                //this.alertServ.showAlert('La cobranza fue almacenada con exito');                          
                this.navCtrl.push(ComprobanteRetencionPage, { invoices: this.available_invoices, charge: this.charge, indexes: this.indexes });
              },
              (error) => {
                this.alertServ.showAlertError('Las facturas de la cobranza no fueron insertadas ');
              });
          },
          (error) => {
            this.alertServ.showAlertError('Las facturas de la cobranza no fueron removidas ');
          });
      },
      (error) => {
        this.alertServ.showAlertError('La cobranza cabecera no se actualizo ');
      });

  }

  makeCharge() {
    this.translate.get(["errors.amount_mand", "errors.refer_mand", "errors.check_a_numb_mand", "errors.check_numb_mand", "errors.doc_numb_mand", "errors.rete_numb_mand", "errors.invoi_balan_less", "errors.bank_mand", "charges.no_selected_invoices", "charges.no_equal_amounts"], null).subscribe(
      (localizedValues) => {

        let show_msg = false;

        for (let invoice of this.available_invoices) {

          for (let charge of invoice.charges) {

            if (charge.montco == '' || charge.montco == null) {
              this.alertServ.showAlertError(localizedValues["errors.amount_mand"]);
              show_msg = true;
            } else {

              switch (charge.metpag) {
                case '3':
                case '1':
                  if (charge.codban == '' || charge.codban == null) {
                    this.alertServ.showAlertError(localizedValues["errors.bank_mand"]);
                    show_msg = true;
                  } else {
                    if (charge.referc == '' || charge.referc == null) {
                      this.alertServ.showAlertError(localizedValues["errors.refer_mand"]);
                      show_msg = true;
                    }
                  }
                  break;
                case '2':
                  if (charge.codban == '' || charge.codban == null) {
                    this.alertServ.showAlertError(localizedValues["errors.bank_mand"]);
                    show_msg = true;
                  } else {
                    if (charge.ctachq == '' || charge.ctachq == null) {
                      this.alertServ.showAlertError(localizedValues["errors.check_a_numb_mand"]);
                      show_msg = true;
                    } else {
                      if (charge.numchq == '' || charge.numchq == null) {
                        this.alertServ.showAlertError(localizedValues["errors.check_numb_mand"]);
                        show_msg = true;
                      }
                    }
                  }
                  break;
              }

            }

            if (show_msg) {
              break;
            }

          }

          if (show_msg) {
            break;
          }

        }

        if (show_msg == false) {
          this.goToComprobanteRetencion();
        }

      });
  }

}
