import { Injectable }              from '@angular/core';
import { Http, Headers, Response }          from '@angular/http';
import { Storage } from '@ionic/storage';

import { Observable }     from 'rxjs/Observable';
import { DbService } from './db-service';

import { LoadingController } from 'ionic-angular';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
//import { Hero } from './hero';
@Injectable()
export class GenericRestService {

//   private app_url: string = config.apiUrl;

  constructor (private http: Http, public storage: Storage, public database: DbService, public loadingCtrl: LoadingController ) {}

  getFromUrl(rscUrl: string){
    return new Promise((resolve, reject) => {
        this.database.openDatabase().then(
            () => {
                this.database.getUrlServComp().then(
                (res: string) => {
                    this.storage.get('todo').then(
                        (value) => {
                            // this.todo = value;   
                            let url = res + rscUrl;
                            console.log('Url consultada: ' + url);
                            console.log('Token enviado: ' + value);
                            let headers = new Headers();
                            headers.append('Authorization-Route', 'route:'+value);

                            this.http.get(url, {headers: headers}).timeout(600000).map((res:Response) => res).subscribe(
                                (data:any) => {
                                    console.log('Consulta a la url realizada con exito');
                                    console.log('status: '+ data.status);
                                    resolve(data);
                                },
                                (error: any) => {
                                    console.log('Error en getFromUrl: ' + error);
                                    reject(error);
                                });

                            // return this.http.get(url, {headers: headers})./*timeout(10).*/map((res: Response) => res);
                        });
                },
                (error:any) => {
                    console.log('error leyendo el dato: '); console.log(error);
                    reject(error);
                } )
            })
    });
  }

//   getFromUrl(rscUrl: string, message: string){
//     let loading = this.loadingCtrl.create({
//         content: localizedValue,
//         spinner: "bubbles"
//     });
//     loading.present();

//     return new Promise((resolve, reject) => {
//         this.database.openDatabase().then(
//             () => {
//                 this.database.getUrlServComp().then(
//                 (res: string) => {
//                     this.storage.get('todo').then(
//                         (value) => {
//                             // this.todo = value;   
//                             let url = res + rscUrl;
//                             console.log('Url consultada: ' + url);
//                             console.log('Token enviado: ' + value);
//                             let headers = new Headers();
//                             headers.append('Authorization-Route', 'route:'+value);

//                             this.http.get(url, {headers: headers}).timeout(90000).map((res:Response) => res).subscribe(
//                                 (data:any) => {
//                                     console.log('status: '+ data.status);
//                                     resolve(data);
//                                 },
//                                 (error: any) => {
//                                     console.log('Error en getFromUrl');
//                                     reject(error);
//                                 });

//                             // return this.http.get(url, {headers: headers})./*timeout(10).*/map((res: Response) => res);
//                         });
//                 },
//                 (error:any) => {
//                     console.log('error leyendo el dato: '); console.log(error);
//                     reject(error);
//                 } )
//             })
//     });
//   }

  sendToUrl(rscUrl: string, obj: any){
    return new Promise((resolve, reject) => {
        this.database.openDatabase().then(
            () => {
                this.database.getUrlServComp().then(
                (res: string) => {
                    this.storage.get('todo').then(
                        (value) => {
                            // this.todo = value;   
                            let url = res + rscUrl;
                            let headers = new Headers();
                            headers.append('Authorization-Route', 'route:'+value);

                            this.http.post(url, obj, {headers: headers}).timeout(90000).map((res:Response) => res).subscribe(
                                (res:any) => {
                                    console.log('status: '+ res.status);
                                    resolve(res);
                                },
                                (error: any) => {
                                    console.log('Error en sendToUrl');
                                    reject(error);
                                });
                        });
                },
                (error:any) => {
                    console.log('error leyendo el dato: '); console.log(error);
                    reject(error);
                } )
            })
    });
  }

  getFromUrlExtern(rscUrl: string){
    return new Promise((resolve, reject) => {
        this.http.get(rscUrl)./*timeout(10).*/map(res => res).subscribe(
            (data:any) => {
                resolve(data);
            },
            (error: any) => {
                console.log('Error en getFromUrlExtern');
                reject(error);
            });
    });
  }

//   getFromUrl(rscUrl: string){
//     return new Promise((resolve, reject) => {
//        this.database.openDatabase().then(
//             () => {
//                 this.database.getUrlServComp().then(
//                 (res: string) => {
//                     this.storage.get('todo').then(
//                         (value) => {
//                             // this.todo = value;   
//                             let url = res + rscUrl;
//                             console.log('Url consultada: ' + url);
//                             console.log('Token enviado: ' + value);
//                             let headers = new Headers();
//                             headers.append('Authorization-Route', 'route:'+value);

//                             this.http.get(url, {headers: headers})./*timeout(10).*/map((res:Response) => res).subscribe(
//                                 (data:any) => {
//                                     console.log('status: '+ data.status);
//                                     resolve(data);
//                                 },
//                                 (error: any) => {
//                                     console.log('Error en getFromUrl');
//                                     reject(error);
//                                 });

//                             // return this.http.get(url, {headers: headers})./*timeout(10).*/map((res: Response) => res);
//                         });
//                 },
//                 (error:any) => {
//                     console.log('error leyendo el dato: '); console.log(error);
//                     reject(error);
//                 } )
//         })
//     });
//   }

}
