import { NgModule, ErrorHandler } from '@angular/core';
import {Camera} from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import {Http} from '@angular/http';
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from '@angular/http';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';

import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';

import {TranslateModule, TranslateStaticLoader, TranslateLoader} from 'ng2-translate/ng2-translate';

import { ConferenceApp } from './app.component';

import { LoginPage } from '../pages/login/login';
import { Profile } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { PruebaPage } from '../pages/prueba/prueba';
import { FacturacionPage } from '../pages/facturacion/facturacion';
import { CobranzasPage } from '../pages/cobranzas/cobranzas';
import { AdministracionPage } from '../pages/administracion/administracion';
import { PopoverPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { AgregarproductoPage } from '../pages/agregarproducto/agregarproducto';
import { CestaPage } from '../pages/cesta/cesta';
import { DetallarproductoPage } from '../pages/detallarproducto/detallarproducto';
import { PedidoPage } from '../pages/pedido/pedido';
import { PedidoMainPage } from '../pages/pedido-main/pedido-main';
import { DefaultHomePage } from '../pages/default-home/default-home';
import { VisitasPage } from '../pages/visitas/visitas';
import { FacturaPage } from '../pages/factura/factura';
import { ListaFacturasPage } from '../pages/lista-facturas/lista-facturas';
import { DetallarproductoFacturaPage } from '../pages/detallarproducto-factura/detallarproducto-factura';
import { CestaClosedPage } from '../pages/cesta-closed/cesta-closed';
import { CestaSyncedPrintedPage } from '../pages/cesta-synced-printed/cesta-synced-printed';
import { DataNoSyncPage } from '../pages/data-no-sync/data-no-sync';
import { CobranzaMainPage } from '../pages/cobranza-main/cobranza-main';
import { CobranzaProcesarPage } from '../pages/cobranza-procesar/cobranza-procesar';
import { FacturaCrearPage } from '../pages/factura-crear/factura-crear';
import { AgregarproductoFacturaPage } from '../pages/agregarproducto-factura/agregarproducto-factura';
import { FotoClientesPage } from '../pages/foto-clientes/foto-clientes';
import { FotoCapturaPage } from '../pages/foto-captura/foto-captura';
import { PedidoOnline } from '../pages/pedido-online/pedido-online';
import { ComprobanteRetencionPage } from '../pages/comprobante-retencion/comprobante-retencion';
import { CobranzaPage } from '../pages/cobranza/cobranza';
import { DepositoPage } from '../pages/deposito/deposito';
import { DepositoProcesarPage } from '../pages/deposito-procesar/deposito-procesar';
import { FacturaMainPage } from '../pages/factura-main/factura-main';
import { FacturaClosedPage } from '../pages/factura-closed/factura-closed';
import { FacturaSyncedPrintedPage } from '../pages/factura-synced-printed/factura-synced-printed';
import { CrearCobranzaPage } from '../pages/crear-cobranza/crear-cobranza';



import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';
import { GenericRestService } from '../providers/generic-rest-service';
import { LoginService } from '../pages/login/login.service';
import { AlertsService } from '../providers/alerts.service';
import { DataPagesService } from '../providers/data-pages-service';
import { SendDataService } from '../providers/send-data-service';
import { DbChangesService } from '../providers/db-changes-service';

import { DbService } from '../providers/db-service.ts';

import {NgIdleModule} from '@ng-idle/core';
import { Network } from '@ionic-native/network';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': 'd8ef82bb'
  },
  'push': {
    'sender_id': '114547964894',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#00ff0000'
      }
    }
  }
};



export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

@NgModule({
  declarations: [
    ConferenceApp,
    LoginPage,
    TabsPage,
    DashboardPage,
    PruebaPage,
    FacturacionPage,
    CobranzasPage,
    AdministracionPage,
    PopoverPage,
    HomePage,
    AgregarproductoPage,
    CestaPage,
    DetallarproductoPage,
    PedidoPage,
    PedidoMainPage,
    CestaClosedPage,
    DefaultHomePage,
    CestaSyncedPrintedPage,
    VisitasPage,
    DataNoSyncPage,
    Profile,
    FacturaPage,
    ListaFacturasPage,
    DetallarproductoFacturaPage,
    CobranzaMainPage,
    CobranzaProcesarPage,
    FacturaCrearPage,
    AgregarproductoFacturaPage,
    FotoClientesPage,
    FotoCapturaPage,
    PedidoOnline,
    ComprobanteRetencionPage,
    CobranzaPage,
    DepositoPage,
    DepositoProcesarPage,
    FacturaMainPage,
    FacturaClosedPage,
    FacturaSyncedPrintedPage,
    CrearCobranzaPage
  ],
  imports: [
    IonicModule.forRoot(ConferenceApp,{
      activator:'ripple'	
    },{}),
		IonicStorageModule.forRoot(),
    CloudModule.forRoot(cloudSettings),
    NgIdleModule.forRoot(),
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: createTranslateLoader,
      deps: [Http]
    })
  ],
  exports: [TranslateModule],
  bootstrap: [IonicApp],
  entryComponents: [
    ConferenceApp,
    LoginPage,
    TabsPage,
    DashboardPage,
    PruebaPage,
    FacturacionPage,
    CobranzasPage,
    AdministracionPage,
    PopoverPage,
    HomePage,
    AgregarproductoPage,
    CestaPage,
    DetallarproductoPage,
    PedidoPage,
    PedidoMainPage,
    CestaClosedPage,
    DefaultHomePage,
    CestaSyncedPrintedPage,
    VisitasPage,
    DataNoSyncPage,
    Profile,
    FacturaPage,
    ListaFacturasPage,
    DetallarproductoFacturaPage,
    CobranzaMainPage,
    CobranzaProcesarPage,
    FacturaCrearPage,
    AgregarproductoFacturaPage,
    FotoClientesPage,
    FotoCapturaPage,
    PedidoOnline,
    ComprobanteRetencionPage,
    CobranzaPage,
    DepositoPage,
    DepositoProcesarPage,
    FacturaMainPage,
    FacturaClosedPage,
    FacturaSyncedPrintedPage,
    CrearCobranzaPage
  ],
  providers: [
    ConferenceData,
    UserData,
    GenericRestService,
    LoginService,
    Diagnostic,
    DbService,
    AlertsService,
    DataPagesService,
    SendDataService,
    DbChangesService,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Network,
    Camera,
    File,
    FilePath
  ]
})
export class AppModule { }
