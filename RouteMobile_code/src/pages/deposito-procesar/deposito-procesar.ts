import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { AlertsService } from '../../providers/alerts.service';
import { TranslateService } from 'ng2-translate';

/*
  Generated class for the DepositoProcesar page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-deposito-procesar',
  templateUrl: 'deposito-procesar.html'
})
export class DepositoProcesarPage {

  invoices: any[] = [];
  charge: any = {};
  selected_bank: string = '';
  printer_name = '';
  invoice: string = '';
  loader: any;
  printed_copy = false;
  monped = 'false';
  indexes: any[] = [];

  /**********************cobranzas****************************/
  all_charges: any[] = [];
  charges: any[] = [];
  available_charges: any[] = [];
  printed_cobranzas: any[] = [];
  deposit: any = {};
  bank_account_number: string = '';
  cash_and_check: boolean = true;
  numero_actual_deposito: string = '';
  cod_cli: string = '';
  charges_already_deposited: any[] = [];
  selected_charges: any[] = [];
  other_bank_checks: any[] = [];
  same_bank_checks: any[] = [];
  bank_deposits: any[] = [];
  bank_transferences: any[] = [];
  total_cash: any = 0;
  total_other_bank_checks: any = 0;
  total_same_bank_checks: any = 0;
  total_deposits: any = 0;
  total_transferences: any = 0;
  sucursal_codban: any;
  /**********************cobranzas****************************/

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public view: ViewController,
    public alertCtrl: AlertController,
    public database: DbService,
    public alertServ: AlertsService,
    public loadingCtrl: LoadingController,
    private translate: TranslateService) {

    if (this.navParams.data.charges) {

      this.charges = this.navParams.data.charges;
      this.getSelectedSucxcomp();

    }

    if (this.navParams.data.deposit) {

      this.deposit = this.navParams.data.deposit;
      this.bank_account_number = this.navParams.data.bank_account_number;

    }

    if (this.navParams.data.indexes) {

      this.indexes = this.navParams.data.indexes;

    }



  }

  init() {

    console.log('deposito procesar charges size: ' + this.charges.length);

    this.charges.forEach((c) => {

      switch (c.metpag) {
        case 0:
          this.total_cash = this.total_cash + Number(c.montco);
          break;
        case 1:
          let transferencia: any = {};
          this.database.getBankDescAndCtaByCode(c.codban).then(
            (bank) => {
              transferencia.desban = bank.desban;
              transferencia.numcta = bank.numcue;
            });
          transferencia.monchq = c.montco;
          this.total_transferences = this.total_transferences + Number(c.montco);
          this.bank_transferences.push(transferencia);
          break;
        case 2:
          let cheque: any = {};
          this.database.getBankDescAndCtaByCode(c.codban).then(
            (bank) => {
              cheque.desban = bank.desban;
              cheque.numcta = bank.numcue;
            });
          cheque.monchq = c.montco;
          cheque.numchq = c.numchq;
          if (c.codban == this.sucursal_codban) {
            this.total_same_bank_checks = this.total_same_bank_checks + c.montco;
            this.same_bank_checks.push(cheque);
          } else {
            this.total_other_bank_checks = this.total_other_bank_checks + c.montco;
            this.other_bank_checks.push(cheque);
          }
          break;
        case 3:
          let deposito: any = {};
          this.database.getBankDescAndCtaByCode(c.codban).then(
            (bank) => {
              deposito.desban = bank.desban;
              deposito.numcta = bank.numcue;
            });
          deposito.monchq = c.montco;
          deposito.numchq = c.numchq;
          this.total_deposits = this.total_deposits + c.montco;
          this.bank_deposits.push(deposito);
          break;
      }

    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CobranzaProcesarPage');
  }

  confirmDeposit() {
    this.translate.get(["deposits.success_closed", "deposits.failure_closed"], null).subscribe(
      (localizedValues) => {

        this.database.updateDepositPhaseStatus(this.deposit.depnum, 3, 1, this.deposit).then(
          (res) => {
            this.showMessageforPrinting(localizedValues["deposits.success_closed"]);
          },
          (error) => {
            this.alertServ.showAlertError('La cobranza cabecera no se actualizo ');
          });

      });
  }

  goBack() {
    this.navCtrl.pop();
  }

  getSelectedSucxcomp() {
    this.database.openDatabase().then(
      () => {
        this.database.getSelectedSucxcomp().then(
          (res: string) => {
            console.log('sucxcom: ' + res);
            if (res != '' && res != null) {
              console.log('no es nula');
              let ids = res.split(" ");
              this.database.getDealerBankByCode(ids[1]).then(
                (codban) => {
                  this.sucursal_codban = codban;
                  this.init();
                }
              )
                .catch((error) => {
                  this.alertServ.showAlertError('error obteniendo el codigo de banco-distribuidora');
                })
            }
          });
      });
  }


  /////////////////////////////////////printing operations//////////////////////////////////////////////////////////
  showMessageforPrinting(msg: any) {


    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.openPrintingConfirmation();
              }
            }
          ]
        });
        alert.present();

      });

  }

  openPrintingConfirmation() {

    this.translate.get(["common.acept", "common.cancel", "common.confirm", "deposits.print_confirm"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["deposits.print_confirm"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.closeViews();
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.printOrderSelectPrinter();
              }
            }
          ]
        });
        alert.present();

      });

  }

  printOrderSelectPrinter() {

    let context = this;

    this.translate.get(["basket.error_printers_not_found"], null).subscribe(
      (localizedValues) => {

        let data = ["200164"];

        this.showPrintersList(data);

        /*(<any>window).BTPrinter.list(function(data){
        //alert("Success Listing");
        context.showPrintersList(data);
        //alert(data); //list of printer in data array
        },function(err){
            //alert("Error Listing");
            context.showMessage(localizedValues["basket.error_printers_not_found"]);
        });   */

      });

    //this.testing(); 
    //alert('imgData ' + this.imgData);     

  }

  showMessage(msg: any) {

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
              }
            }
          ]
        });
        alert.present();

      });

  }

  printOrderPrinting(printer_name: any) {

    let context = this;

    this.translate.get(["basket.error_printer_disconnection", "basket.error_printing_document", "basket.error_printer_connection"], null).subscribe(
      (localizedValues) => {

        (<any>window).BTPrinter.connect(function (data) {
          //alert("Success Connected");
          //alert(data);
          context.invoice = context.setInvoice();
          //this.printing();
          (<any>window).BTPrinter.printText(function (data) {
            //alert("Success printing");
            context.loader.dismiss();
            context.PrintInvoice();

            (<any>window).BTPrinter.disconnect(function (data) {
              //alert("Success disconnected");
              //alert(data)
            }, function (err) {
              context.showMessageOnPrintingSuccessOrFailure(localizedValues["basket.error_printer_disconnection"]);
              //alert(err)
            }, printer_name);
          }, function (err) {
            context.loader.dismiss();
            context.showMessageOnPrintingSuccessOrFailure(localizedValues["basket.error_printing_document"]);
          }, context.invoice);
        }, function (err) {
          //alert("Error");
          context.loader.dismiss();
          context.showMessageOnPrintingSuccessOrFailure(localizedValues["basket.error_printer_connection"]);
          //alert(err)
        }, printer_name);

      });

  }

  syncOrder() {

  }

  showPrintersList(printers: any[]) {


    this.translate.get(["common.cancel", "common.acept", "basket.choose_bluetooth_printer"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["basket.choose_bluetooth_printer"]);

        printers.forEach((s) => {

          alert.addInput({
            type: 'radio',
            label: s,
            value: s,
            checked: false
          });

        });

        alert.addButton({
          text: localizedValues["common.cancel"],
          handler: data => {

            this.navCtrl.pop();

          }
        });

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if (data !== '') {

              this.printer_name = data;
              this.presentLoading(data);


            }

          }
        });

        alert.present();

      });


  }

  setInvoice() {


    let header = 'RECIBO DE DEPOSITO\r\n';

    let order_number = 'Proagro, C.A.\r\n';

    let client_info = 'Cedula Juridica:\r\n';

    let invoice = header + order_number + client_info;

    return invoice;

  }

  PrintInvoice() {

    this.translate.get(["deposits.error_printing_deposit"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
          () => {
            this.database.updateDepositPhaseStatus(this.deposit.depnum, 6, 1, this.deposit)
              .then((data) => {
                this.showMessageOnPrintingSuccess();
              })
              .catch((error) => { this.showMessage(localizedValues["deposits.error_printing_deposit"]); });
          });

      });

  }

  presentLoading(data) {

    this.translate.get(["common.wait"], null).subscribe(
      (localizedValues) => {

        this.loader = this.loadingCtrl.create({
          content: localizedValues["common.wait"],
          spinner: "bubbles"
        });

        this.loader.present()
          .then(() => { 
            //this.printOrderPrinting(data);
            setTimeout(()=>{ 
              this.loader.dismiss();
              this.PrintInvoice(); }, 4000);
           });

      });

  }

  showMessageOnPrintingSuccessOrFailure(msg: any) {

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.closeViews();
              }
            }
          ]
        });
        alert.present();

      });
  }

  showMessageOnPrintingSuccess() {


    this.translate.get(["common.acept", "deposits.success_printed"], null).subscribe(
      (localizedValues) => {

        console.log('Entro en mi metodo');
        let alert = this.alertCtrl.create({
          message: localizedValues["deposits.success_printed"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                if (this.printed_copy === true) {
                  this.closeViews();
                } else {
                  this.database.openDatabase().then(
                    () => {
                      this.database.getPrncpy().then(
                        (print_copy) => {
                          if (print_copy == 'true') {
                            console.log('prncpy: ' + true);
                            this.showMessagePrintCopy();
                          } else {
                            console.log('prncpy: ' + false);
                            this.closeViews();
                          }
                        },
                        (error) => {
                        });
                    });
                }
              }
            }
          ]
        });
        alert.present();

      });
  }

  showMessagePrintCopy() {


    this.translate.get(["common.acept", "common.cancel", "common.confirm", "basket.print_copy_confirm"], null).subscribe(
      (localizedValues) => {

        this.printed_copy = true;
        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["basket.print_copy_confirm"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.closeViews();
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.presentLoading(this.printer_name);
              }
            }
          ]
        });
        alert.present();

      });

  }

  closeViews() {
    this.indexes.forEach((v) => {
      this.navCtrl.remove(v);
    })
    this.navCtrl.pop();
  }

}
