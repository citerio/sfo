import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DbService } from "../../providers/db-service";
import {Camera, CameraOptions} from '@ionic-native/camera';
import { TranslateService } from "ng2-translate";
import { File } from '@ionic-native/file';


/*
  Generated class for the FotoCaptura page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-foto-captura',
  templateUrl: 'foto-captura.html'
})
export class FotoCapturaPage {

  cliente: any;
  public photos: any[] = [];
  public base64Image : string;
  public imageUrl : string;
  motivos: any[] = [];
  
  

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public database: DbService,
    public alertCtrl: AlertController,
    private translate: TranslateService,
    private camera : Camera,
    private file: File
    ) {
      

    if(this.navParams.data){      

      if(this.navParams.data.cliente){      

        this.cliente = this.navParams.data.cliente;        

      }         

    } 


  }

  ionViewDidLoad() {
    this.getRemoveOrderReasons();
    console.log('ionViewDidLoad ListaFacturasPage');
  }  

  ionViewWillEnter(){
    this.getPhotos();    
  }

  getPhotos(){

    this.database.openDatabase().then(
      () => {
        this.database.getPhotosByClient(this.cliente.codcli)
        .then(photos => {
          this.photos = photos;
          //alert("path on db: " + this.photos.toString());
          this.photos.reverse();          
          //this.setFilteredItems();
        })
      });     
      
  }

  deletePhoto(index) {
   console.log("Delete Photo");
  }

  /*takePhoto() {
    const options : CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.photos.push(this.base64Image);
        this.photos.reverse();
      }, (err) => {
        console.log(err);
      });
  }*/

  takePhoto() {
    const options : CameraOptions = {
      quality: 50, // picture quality
      destinationType: this.camera.DestinationType.FILE_URI,
      saveToPhotoAlbum: false      
    }
    this.camera.getPicture(options).then((imageData) => {
        this.imageUrl = imageData;
        //alert("cache directory: " + this.file.externalCacheDirectory);
        //alert("path on take: " + this.imageUrl);
        this.database.insertPhotoByClient(this.cliente.codcli, this.imageUrl)
        .then(()=>{

          //this.photos.push(this.imageUrl);
          //this.photos.reverse();
          this.getPhotos();

        })
        .catch(()=>{

          alert("ERROR on saving pic");

        });
        
      }, (err) => {
        console.log(err);
      });
  }
  

  ///////////////////////////////////eliminacion factura///////////////////////////////////
  openRemovePhotoConfirmation(photo: any, id: any){
    this.translate.get(["photo.delete_photo", "common.choose_reason", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["photo.delete_photo"]);
        alert.setMessage(localizedValues["common.choose_reason"]);

        this.motivos.forEach((s) => {
            console.log('s.codmot: ' + s.codmot);
            alert.addInput({
            type: 'radio',
            label: s.desmot,
            value: s.codmot,
            checked: false
          });

        });    

        alert.addButton(localizedValues["common.cancel"]);

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if(data !== ''){          
              console.log('data: ' + data)
              this.deleteProduct(photo, id, data);
              

            }
            
          }
        });

        alert.present();
      });
  }

  getRemoveOrderReasons(){
    this.database.openDatabase().then(
      () => {
        this.database.getRemoveOrderReasons()
        .then(reasons => {
          this.motivos = reasons;
        })
      });
  } 
  
  deleteProduct(photo: any, id: any, codmot: string){

    this.translate.get(["photo.success_deleted", "photo.failure_deleted"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
        () => {
          this.database.deletePhotoByClient(photo.codcli, photo.fotnum)
            .then(() => {
              this.showMessage(localizedValues["photo.success_deleted"]);
              this.getPhotos();              
              let file_name:string = photo.destino;
              file_name = file_name.replace(this.file.externalCacheDirectory, '')
              //alert("file name deleted: " + file_name);
              this.file.removeFile(this.file.externalCacheDirectory, file_name)
              .then(_ => console.log('File deleted'))
              .catch(err => console.log('File couldnt be deleted'));
              
            })
            .catch((error) => { this.showMessage(localizedValues["photo.failure_deleted"]); })
        });
          
          
      });    
    

  }

  showMessage(msg: any){

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({      
        message: msg,
        buttons: [
          {
            text: localizedValues["common.acept"],
            handler: () =>{
              console.log('aceptar');
            }
          }
        ]
      });
      alert.present();        
          
      });   

  }  

  

  

}
