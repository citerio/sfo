import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { TranslateService } from "ng2-translate";


/*
  Generated class for the CestaClosed page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cesta-closed',
  templateUrl: 'cesta-closed.html'
})
export class CestaClosedPage {

  codigo_cliente: string = '';
  nombre_cliente: string = '';
  productos_escogidos: any[] = [];
  suma_total: any = { 'monto': 0 };
  importe_total: number = 0;
  numero_actual_pedido: string = '';
  deshabilitar_cerrar: boolean = false;
  invoice: string = '';
  loader: any;
  printer_name = '';
  printed_copy = false;
  monped = 'false';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public database: DbService,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private translate: TranslateService
  ) {

    this.getMonped();
    if (this.navParams.data) {

      if (this.navParams.data.cliente.codcli) {

        this.codigo_cliente = this.navParams.data.cliente.codcli;

        this.database.openDatabase().then(
          () => {
            this.database.getClientName(this.navParams.data.cliente.codcli)
              .then((nombre) => { this.nombre_cliente = nombre; })
          });

        this.numero_actual_pedido = this.navParams.data.cliente.pednum;
        //this.getOrder(this.numero_actual_pedido);  

      }

      if (this.navParams.data.cliente.codigo) {

        this.codigo_cliente = this.navParams.data.cliente.codigo;

        this.database.openDatabase().then(
          () => {
            this.database.getClientName(this.navParams.data.cliente.codigo)
              .then((nombre) => { this.nombre_cliente = nombre; })
          });


        this.numero_actual_pedido = this.database.getCurrentOrderNumber();
        //this.getOrder(this.numero_actual_pedido); 

      }

    }


  }

  ionViewWillEnter() {
    this.getOrder(this.numero_actual_pedido);
  }

  getMonped() {
    this.database.openDatabase().then(
      () => {
        this.database.getMonped()
          .then(monped => {
            this.monped = monped;
          })
          .catch((error) => {
            alert('ocurrio un error: ' + JSON.stringify(error));
          });
      });
  }

  getOrder(pednum: any) {
    this.database.openDatabase().then(
      () => {
        this.database.getOrder(pednum, 3, 1)
          .then(pedido => {
            this.productos_escogidos = pedido;
            if (this.productos_escogidos.length == 0) {
              this.deshabilitar_cerrar = true;
            } else {
              this.deshabilitar_cerrar = false;
            }
            this.orderTotalSum();
          })
          .catch((error) => {

            alert('ocurrio un error: ' + JSON.stringify(error));
          }

          )
      });

  }



  orderTotalSum() {

    this.importe_total = 0;
    this.productos_escogidos.forEach((s) => {
      this.importe_total = this.importe_total + s.pedprt;
    }
    );

  }


  printing() {

    (<any>window).BTPrinter.printText(function (data) {
      alert("Success printing");
    }, function (err) {
      alert("Error printing");
    }, "testing");


  }

  testing() {

    (<any>window).pdf.htmlToPDF({
      data: "<html> <h1>  Hello World  </h1> </html>",
      documentSize: "A4",
      landscape: "portrait",
      type: "base64"
    },
      (sucess) => console.log('sucess: ', sucess),
      (error) => console.log('error:', error));



  }

  //lo nuevo+++++++++++++++++++++++++++++++++++++

  printOrderSelectPrinter() {

    let context = this;

    this.translate.get(["basket.error_printers_not_found"], null).subscribe(
      (localizedValues) => {        

        //////////original code
         (<any>window).BTPrinter.list(function(data){
          //alert("Success Listing");
          context.showPrintersList(data);
          //alert(data); //list of printer in data array
          },function(err){
              //alert("Error Listing");
              context.showMessage(localizedValues["basket.error_printers_not_found"]);
          });  

      });

  }

  printOrderPrinting(printer_name: any) {

    let context = this;

    this.translate.get(["basket.error_printer_disconnection", "basket.error_printing_document", "basket.error_printer_connection"], null).subscribe(
      (localizedValues) => {

        (<any>window).BTPrinter.connect(function (data) {
          //alert("Success Connected");
          //alert(data);
          context.invoice = context.setInvoice();
          //this.printing();
          (<any>window).BTPrinter.printText(function (data) {
            //alert("Success printing");
            context.loader.dismiss();
            context.PrintOrder();
            (<any>window).BTPrinter.disconnect(function (data) {
              //alert("Success disconnected");
              //alert(data)
            }, function (err) {
              context.showMessage(localizedValues["basket.error_printer_disconnection"]);
              //alert(err)
            }, printer_name);
          }, function (err) {
            context.loader.dismiss();
            context.showMessage(localizedValues["basket.error_printing_document"]);
          }, context.invoice);
        }, function (err) {
          //alert("Error");
          context.loader.dismiss();
          context.showMessage(localizedValues["basket.error_printer_connection"]);
          //alert(err)
        }, printer_name);

      });

  }

  showPrintersList(printers: any[]) {


    this.translate.get(["common.cancel", "common.acept", "basket.choose_bluetooth_printer"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["basket.choose_bluetooth_printer"]);

        printers.forEach((s) => {

          alert.addInput({
            type: 'radio',
            label: s,
            value: s,
            checked: false
          });

        });

        alert.addButton(localizedValues["common.cancel"]);

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if (data !== '') {

              this.printer_name = data;
              this.presentLoading(data);


            }

          }
        });

        alert.present();

      });

  }

  setInvoice() {


    let header = 'Distribuidora Proagro Valencia\r\nAv. Eugenio Mendoza Edif. Protinal, frente al stadium Jose\r\nBernardo Perez Valencia - Estado Carabobo Telefonos(0241)6138423\r\n';

    let order_number = '\r\nPedido No.' + this.numero_actual_pedido + '     Fecha: ' + this.navParams.data.cliente.fechcr + '\r\n';

    let client_info = 'Nombre Cliente: ' + this.nombre_cliente.toUpperCase() + '\r\n' + 'RIF: ' + '\r\n' + 'Direccion: ' + '\r\n';

    let seller_info = 'Vendedor: ' + '\r\n' + 'Preventista: ' + '\r\n' + 'Condicion de pago: Contado' + '      Fecha Entrega: ' + this.navParams.data.cliente.fechcr + '\r\n\n';

    let order_table_header = 'Nombre Producto         Cantidad    Und   Prec.Unit    Monto';

    //let order_table_body:string;

    let invoice = header + order_number + client_info + seller_info + order_table_header;



    this.productos_escogidos.forEach((s) => {
      invoice += "\r\n";
      invoice += s.descar.toUpperCase();
      let nesp: number = 30 - (s.descar.toUpperCase().length + s.pedcan.toString().length)
      for (let i = 1; i <= nesp; i++) {
        invoice += " ";
      }
      invoice += s.pedcan;
      invoice += "      " + s.coduni;

      let preuprnt = 0;
      if (this.monped == 'true') {
        preuprnt = s.pedpru;
      }

      nesp = 11 - preuprnt.toString().length;
      // nesp = 11 - s.pedpru.toString().length;

      for (let i = 1; i <= nesp; i++) {
        invoice += " ";
      }

      invoice += preuprnt;
      // invoice += s.pedpru;

      let pretprnt = 0;
      if (this.monped == 'true') {
        pretprnt = s.pedprt;
      }

      nesp = 10 - pretprnt.toString().length
      // nesp = 10 - s.pedprt.toString().length;

      for (let i = 1; i <= nesp; i++) {
        invoice += " ";
      }

      invoice += pretprnt;
      // invoice += s.pedprt;

      //  order_table_body + s.descart.toUpperCase() + '\t\t' + s.pedcan + '\t' + s.peduni + '\t' + s.pedprun + '\t' + s.pedprto + '\n';
    });

    let subtotal_order = '\t\t\t\t\t' + this.importe_total + '\n';

    let total_order = '\t\t\t\t\t\t' + this.importe_total + '\n';

    let footer = '\r\nNO SE ACEPTAN DEVOLUCIONES\r\n * Este comprobante va sin enmiendas\r\n PRODUCTO ESPECIAL A SOLICITUD DEL CLIENTE\r\n Firma del Cliente o...............................\r\n RBF: AGRO2-000003942-8 \r\n Cestas que contienen el producto vendido son de Proagro C.A. \n\n\n'


    //let invoice = header + order_number + client_info + seller_info + order_table_header + order_table_body + subtotal_order + total_order + footer;

    // let invoice = table;

    invoice += footer;
    return invoice;

  }

  PrintOrder() {

    this.translate.get(["basket.error_printing_order"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
          () => {
            this.database.updateOrderPhaseStatus(this.numero_actual_pedido, 6, 1)
              .then((data) => {
                this.database.updateOrderLinesPhaseStatus(this.numero_actual_pedido, 6, 1)
                  .then((data) => { this.showMessageOnPrintingSuccess(); })
                  .catch((error) => { this.showMessage(localizedValues["basket.error_printing_order"]); })
              })
              .catch((error) => { this.showMessage(localizedValues["basket.error_printing_order"]); });
          });

      });


  }

  showMessage(msg: any) {

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
              }
            }
          ]
        });
        alert.present();

      });

  }

  showMessageOnPrintingSuccessOrFailure(msg: any) {

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.navCtrl.pop();
              }
            }
          ]
        });
        alert.present();

      });

  }

  showMessageOnPrintingSuccess() {


    this.translate.get(["common.acept", "basket.success_printed"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: localizedValues["basket.success_printed"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                if (this.printed_copy === true) {
                  this.navCtrl.pop();
                } else {
                  this.database.openDatabase().then(
                    () => {
                      this.database.getPrncpy().then(
                        (print_copy) => {
                          if (print_copy == 'true') {
                            this.showMessagePrintCopy();
                          } else {
                            this.navCtrl.pop();
                          }
                        },
                        (error) => {
                        });
                    });
                }
              }
            }
          ]
        });
        alert.present();

      });
  }

  showMessagePrintCopy() {

    this.translate.get(["common.acept", "common.cancel", "common.confirm", "basket.print_copy_confirm"], null).subscribe(
      (localizedValues) => {

        this.printed_copy = true;
        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["basket.print_copy_confirm"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.navCtrl.pop();
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.presentLoading(this.printer_name);
              }
            }
          ]
        });

      });

  }

  presentLoading(data) {


    this.translate.get(["common.wait"], null).subscribe(
      (localizedValues) => {

        this.loader = this.loadingCtrl.create({
          content: localizedValues["common.wait"],
          spinner: "bubbles"
        });

        this.loader.present()
          .then(() => {            
            //original code 
            this.printOrderPrinting(data);
          });

      });

  }
}
