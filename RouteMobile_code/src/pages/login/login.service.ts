import { Injectable } from '@angular/core';

import { Login } from './login.model';
import { Coordenadas } from './coord.model';
import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {
    constructor (private http: Http) {}
    login: Login;
    
    // private _url: string = "http://localhost:8081/sfoWeb/login"; // Local 
    //private _url: string = "http://186.167.17.146:8081/sfoWeb/login"; // public
    // private _url: string = "http://localhost:8081/RouteWeb/api/loginRest"; // outsourcing
private _url: string = "http://172.20.85.162:8088/RouteWeb/mlogin";
    sendData(login: Login, coord: Coordenadas, url: string): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        // let headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
        //headers.append('accept ', 'application/json; charset=utf-8')
        let options = new RequestOptions({ headers: headers });


        let urlSearchParams = new URLSearchParams();
        urlSearchParams.append('musername', login.username);
        urlSearchParams.append('mpassword', login.password);
        urlSearchParams.append('lat', coord.lat);
        urlSearchParams.append('lon', coord.lon);
        urlSearchParams.append('imei', login.imei);
        let body = urlSearchParams.toString()

        return this.http.post(url, body, options).timeout(60000)
                        .map((res: Response) => res);
    }

}