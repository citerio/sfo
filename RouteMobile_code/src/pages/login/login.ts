import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NavController, ModalController, AlertController, ToastController, LoadingController, PopoverController, NavParams, Popover, Platform, ActionSheetController, ViewController } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
// import { DashboardPage } from '../dashboard/dashboard';

import { DashboardPage } from '../dashboard/dashboard';
import { ModalUrl } from './modal/modalUrl';
import { Observable } from 'rxjs/Observable';

// import { PopoverPage } from './popover/popover';
import { LoginService } from './login.service';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';

import {TranslateService} from 'ng2-translate/ng2-translate';

import { Diagnostic } from '@ionic-native/diagnostic';
import { Http, Response } from '@angular/http';
import {AppConstants} from '../../providers/app-constants';
import { Sim } from 'ionic-native';
import { DbService } from '../../providers/db-service';
import { AlertsService } from '../../providers/alerts.service';

declare var cordova;

@Component({
  template: `
    <ion-header>
      <ion-navbar>
        <ion-buttons start>
          <button ion-button (click)="dismiss()">
            <span ion-text color="primary" showWhen="ios">Cancel</span>
            <ion-icon name="md-close" showWhen="android,windows"></ion-icon>
          </button>
        </ion-buttons>
        <ion-title>{{"acercaDe.title"|translate}}</ion-title>
      </ion-navbar>
    </ion-header>
    <ion-content padding>
      <div class="logo">
        <img src="assets/img/Logo_Transparente_ASC_200_200.png" alt="Ionic logo">
      </div>
      <p>{{"acercaDe.soft_info"|translate}}</p>
      <p>{{"app_version"|translate}}</p>
      <p>{{"acercaDe.date"|translate}}</p>
      <p>{{"acercaDe.comp_data"|translate}}</p>
      <p>{{"comp_name"|translate}}</p>
      <p>{{"acercaDe.contact"|translate}}</p>
    </ion-content>
  `,
  styles: [`
    .logo {
      padding: 20px 0;
      min-height: 200px;
      text-align: center;

      img {
        max-width: 150px;
      }
    }
  `]
})
export class Profile {

  constructor(params: NavParams, public viewCtrl: ViewController) {
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}

@Component({
  template: `
    <ion-list radio-group [(ngModel)]="language" (ionChange)="changeLanguage()" class="popover-page">
      <ion-item class="text-athelas">
        <ion-label>Español</ion-label>
        <ion-radio value="es"></ion-radio>
      </ion-item>
      <ion-item class="text-charter">
        <ion-label>English</ion-label>
        <ion-radio value="en"></ion-radio>
      </ion-item>
    </ion-list>
  `
})
export class PopoverPage {
  
  language: any;

  constructor(private navParams: NavParams,
  private translate: TranslateService) {

  }

  ngOnInit() {
    if (this.navParams.data) {
      this.language = this.navParams.data.lang;
    }
  }

  changeLanguage() {
    this.translate.use(this.language);
  }
}

@Component({
  selector: 'page-user',
  templateUrl: 'login.html'
})
export class LoginPage {
  login: {username?: string, password?: string} = {};
  submitted = false;
  noError = true;
  errorMessagge = '';
  url: string;
  popover: Popover;
  titleClickCount: number;
  confirmAlert: any;
  showedAlert: boolean;
  constructor(
      public navCtrl: NavController,
      public userData: UserData,
      public modalCntrl: ModalController,
      public alertCtrl: AlertController,
      public loginService: LoginService,
      public storage: Storage,
      public http: Http,
      private diagnostic: Diagnostic,
      public geolocation: Geolocation,
      public toastCtrl: ToastController,
      public loadingCtrl: LoadingController,
      private translate: TranslateService,
      private popoverCtrl: PopoverController,
      public platform: Platform,
      public database: DbService,
      private navParams: NavParams,
      private alertServ: AlertsService,
      public modalCtrl: ModalController,
      public actionSheetCtrl: ActionSheetController
    ) {
      // cordova.plugins.certificates.trustUnsecureCerts(true);
      // (<any>window).certificates.trustUnsecureCerts(true);
      this.platformReady();
    }    

  ionViewDidLoad() {
    this.titleClickCount = 0;
  }

  ngOnInit() {
    if (this.navParams.data) {
      if(this.navParams.data.lang){
        this.translate.use(this.navParams.data.lang);
      }
    }
  }

  platformReady() {
    // Call any initial plugins when ready
    this.platform.ready().then(() => {
      // console.log("plugin: "+ (<any>window).certificate);
      console.log("plugin: "+ cordova.plugins.certificates);

      this.hasReadPermission();

      this.requestReadPermission();
      
      cordova.plugins.certificates.trustUnsecureCerts(true);
      this.showedAlert = false;
      // Confirm exit
      this.platform.registerBackButtonAction(() => {
        if (!this.showedAlert) {
            this.confirmExitApp();
        } else {
            this.showedAlert = false;
            this.confirmAlert.dismiss();
        }
      });


    });
  }

  confirmExitApp() {
    this.showedAlert = true;
    this.translate.get(["common.cancel", "common.acept", "common.sure_close", "common.exit"], null).subscribe(
      (localizedValues) => {
        this.confirmAlert = this.alertCtrl.create({
          title: localizedValues["common.exit"],
          message: localizedValues["common.sure_close"],
          buttons: [
              {
                  text: localizedValues["common.cancel"],
                  handler: () => {
                      this.showedAlert = false;
                      return;
                  }
              },
              {
                  text: localizedValues["common.acept"],
                  handler: () => {
                      this.platform.exitApp();
                  }
              }
          ]
        });
        this.confirmAlert.present();
      });
}

  titleClick() {
    this.titleClickCount++;

    if(this.titleClickCount == 5){
      this.database.openDatabase().then(
        () => {
          this.database.getUrlServ().then(
            (res: string) => {
              this.showAlertUrl(false, res);
            },
            (error:any) => {
              this.showAlertUrl(false);
            })
        });
      this.titleClickCount = 0;
    }
  }

  onLogin(form: NgForm) {
    this.submitted = true;
    if (form.valid) {
      // this.navCtrl.push(DashboardPage);
      this.database.openDatabase().then(
        () => {
          console.log('Exito abrinendo bd');
          this.database.getUrlServComp().then(
            (res: string) => {
              console.log('Exito obteniendo url');
              this.url = res;
              this.doLogin(this.url);
            },
            (error:any) => {
              console.log('error leyendo el dato: '); console.log(error);
              this.showAlertUrl(true);
            } );
        },
        error => {
           console.log('Error abrinendo bd');
        });
      
      // this.doLogin('172.20.85.46:8080');
    }   

    
  }

  doLogin(url: string){
    url += AppConstants.URL_LOGIN;

    console.log(url);

    this.diagnostic.isLocationEnabled().then(
      (setting) => {
        Sim.getSimInfo().then(
          (info) => {
            if(setting == true) {
              this.translate.get("get_coord", null).subscribe(
                (localizedValue) => {
                  let loading = this.loadingCtrl.create({
                    content: localizedValue,
                    spinner: "bubbles"
                  });
                  loading.present();
                  
                  this.geolocation.getCurrentPosition({timeout: 4000}).then(
                    (position) => {
                      loading.dismiss();
                      //login
                      this.sendLoginData(url, info.deviceId, position.coords.latitude.toString(), position.coords.longitude.toString());
                    }).catch((error) => {
                      loading.dismiss();
                      this.sendLoginData(url, info.deviceId);
                    });
                });
            } else {
              this.showConfirm(url, info.deviceId);
            }
        })
      },
      (error) => {
        console.log('Entro en error buscando gps setting');
      });
  }

  sendLoginData(url: string, imei: string, lat?: string, lon?: string){
    if(!lat) {
      lat = '';
    }
    if(!lon) {
      lon = '';
    }
    
    this.translate.get(["send_serv", "errors.serv_error", "errors.serv_not_resp"], null).subscribe(
      (localizedValues) => {
        let loading = this.loadingCtrl.create({
          content: localizedValues["send_serv"],
          spinner: "bubbles"
        });
        loading.present();

        this.loginService.sendData({'username': this.login.username, 'password': this.login.password, 'imei' : imei}, 
          {'lat': lat, 'lon': lon}, url).subscribe(
            (res: any) => {
                var data = res;
                console.log('response login: '+ data._body);
                this.storage.set('todo', data._body);
                this.storage.set('username', this.login.username);
                this.navCtrl.push(DashboardPage, {fromLogin: true});
                loading.dismiss();
            }, // success
            (error: any) => {
                let msg;

                if(error.status === 0 || error.status == undefined){
                  msg = localizedValues["errors.serv_not_resp"];
                } else {
                  if(error.headers.get('sfo_error')){
                    msg = error.headers.get('sfo_error');
                  } else {
                    msg = localizedValues["errors.serv_error"];
                  }
                }

                loading.dismiss().then(
                  res => {
                    this.showAlertError(msg);
                  });
            },
            () => console.log('hola paso'))     // complete
      });
  }

  changeLanguage(lang: string) {
    this.translate.use(lang);
  }

  showAlertUrl(optLogin: Boolean, url?: String) {
    this.translate.get(["url_serv", "enter_servadd", "common.address", "common.cancel", "common.save"], null).subscribe(
      (localizedValues) => {
        let myInputs: any[] = [];
        if(url){
          myInputs = [
            {
              name: 'url',
              value: url
            },
          ];
        } else {
          myInputs = [
            {
              name: 'url',
              placeholder: localizedValues["common.address"]
            },
          ];
        }

        let prompt = this.alertCtrl.create({
          title: localizedValues["url_serv"],
          message: localizedValues["enter_servadd"],
          inputs: myInputs,
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: (data: any) => {
                console.log('Cancel clicked');
              }
            },
            {
              text: localizedValues["common.save"],
              handler: (data: any) => {
                if(data.url != '') {
                  this.translate.get(["vali_endp", "errors.serv_not_resp", "endp_regi", "errors.serv_error"], null).subscribe(
                    (localizedValues) => {
                      let loading = this.loadingCtrl.create({
                        content: localizedValues["vali_endp"],
                        spinner: "bubbles"
                      });
                      loading.present();

                      Sim.getSimInfo().then(
                        (info) => {
                          console.log('Imei: '+info.deviceId);
                          this.database.openDatabase().then(
                            () => {
                              this.database.getFieldOfTable("config", "tokenPush", "id = ?", [1]).then(
                                (tokenPush) => {
                                  console.log("token obtenido: " +tokenPush);
                                  let object = {
                                    "cdimei": info.deviceId,
                                    "carnam" : info.carrierName,
                                    "swvers" : info.deviceSoftwareVersion,
                                    "dispna" : "",
                                    "phonum" : "",
                                    "simser" : info.simSerialNumber,
                                    "ctoken" : tokenPush
                                  };
                                  
                                  this.http.post('https://'+data.url+'/RouteWeb/checkEndPoint', object).timeout(15000).map((res: Response) => res).subscribe(
                                    (res: any/*Response*/) => {
                                      if(res.status == 202) {
                                        
                                        this.database.openDatabase().then(
                                          () => {
                                            let urlComp = /*"http://"+*/data.url/*+"/RouteWeb"*/;
                                            this.database.insertOrUpdateUrlServ(urlComp).then(
                                              (res: string) => {
                                                // this.alertServ.alertCtrl();
                                                this.showAlertEndpRegistered(optLogin, "https://"+urlComp+"/RouteWeb");

                                                // this.alertServ.showAlert(localizedValues["endp_regi"]);
                                                // if(optLogin == true){
                                                //   this.doLogin("https://"+urlComp+"/RouteWeb");
                                                // }
                                              },
                                              (error:any) => {
                                                console.log('error insertando el dato: '); console.log(error);
                                              } );
                                          });
                                        loading.dismiss();
                                      } else {
                                        let msg = '';
                                        msg = localizedValues["errors.serv_error"];
                                        loading.dismiss().then(
                                          res => {
                                            this.showAlertError(msg);
                                          });
                                      }

                                      
                                    },
                                    (err: any) => {
                                      let msg = '';
                                      console.log(err);
                                      if( (err.status === 0) || (err.status === undefined) ) {
                                        msg = localizedValues["errors.serv_not_resp"];
                                      } else {
                                        msg = localizedValues["errors.serv_error"];
                                        if( (err.headers != undefined) && (err.headers.get('sfo_error') != undefined) ){
                                          msg = err.headers.get('sfo_error');
                                        }
                                      }

                                      loading.dismiss().then(
                                        res => {
                                          this.showAlertError(msg);
                                        });
                                    });
                                },
                                error => {
                                  loading.dismiss().then(
                                    () => {
                                      this.alertServ.showAlertError("No se pudo obtener el token push");
                                    }
                                  );
                                });
                            });
                        },
                        (err) =>  {
                          console.log("Unable to get sim info: " + err);

                          loading.dismiss().then(
                            res => {
                              this.showAlertError("Unable to get sim info ");
                            });
                        });
                    });
                }
              }
            }
          ]
        });
        prompt.present();
      });
  }

  showAlertEndpRegistered(optLogin: Boolean, url: string) {
    this.translate.get(["common.acept", "endp_regi"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          message: localizedValues["endp_regi"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                if(optLogin == true) {
                  this.doLogin(url);
                }
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  showConfirm(url: string, imei: string) {
    this.translate.get(["gps_off", "acti_gps", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["acti_gps"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.sendLoginData(url, imei);
                console.log('Disagree clicked');
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
                console.log('Agree clicked');
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  showAlertError(errorMsg: string) {
    this.translate.get(["common.error", "common.acept"], null).subscribe(
      (localizedValues) => {
        let alert = this.alertCtrl.create({
          title: localizedValues["common.error"],
          subTitle: '',
          buttons: [localizedValues["common.acept"]],
          cssClass: "messageDanger"
        });
        alert.setSubTitle(errorMsg);
        alert.present();
      });
    
  }

  presentPopover(ev: any) {

    let popover = this.popoverCtrl.create(PopoverPage, {
      lang: this.translate.currentLang
    });

    popover.present({
      ev: ev
    });
  }

  acercaDe() {
    let profileModal = this.modalCtrl.create(Profile);
    profileModal.present();
  }

  hasReadPermission() {
    Sim.hasReadPermission()
    .then(()=>{

      console.log("success read permission");

    })
    .catch(()=>{

      console.log("error read permission");

    });
  }

// Android only: request permission
 requestReadPermission() {

    Sim.requestReadPermission()
    .then(()=>{

      console.log("success request permission");

    })
    .catch(()=>{

      console.log("error request permission");


    });

 }


}