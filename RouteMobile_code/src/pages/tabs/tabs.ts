import { Component } from '@angular/core';

import { NavParams } from 'ionic-angular';

import { PruebaPage } from '../prueba/prueba';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // set the root pages for each tab
  tab1Root: any = PruebaPage;
  tab2Root: any = PruebaPage;
  tab3Root: any = PruebaPage;
  mySelectedIndex: number;

  constructor(navParams: NavParams) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;
  }

}
