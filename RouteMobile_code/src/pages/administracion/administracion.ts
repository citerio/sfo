import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { Http, Response } from '@angular/http';
import { DbService } from '../../providers/db-service';

import { GenericRestService } from '../../providers/generic-rest-service';
import {TranslateService} from 'ng2-translate/ng2-translate';

import {AppConstants} from '../../providers/app-constants';
import {AlertsService} from '../../providers/alerts.service';

/*
  Generated class for the Administracion page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-administracion',
  templateUrl: 'administracion.html'
})
export class AdministracionPage {

  isSync: string = 'false';
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http: Http,
              public database: DbService,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              private genericRestService: GenericRestService,
              private translate: TranslateService,
              private alertServ: AlertsService
  ) {
    this.setIsSync();
  }

  setIsSync() {
    this.database.openDatabase().then(
      () => {
        this.database.getFieldOfTable("config", "inic_sync", "id = ?", [1]).then(
          (data) => {
            console.log('inic_sync '+ data);
            this.isSync = data;
            console.log('isSync '+ data);
          });
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdministracionPage');
  }

  inicConfig(){
    this.translate.get(["errors.inner_error"], null).subscribe(
      (localizedValues) => {
        this.database.getFieldOfTable("config", "inic_config", "id = ?", [1]).then(
          (data) => {
            console.log('data: '+data);
            if(data == 'true') {
              this.showAlertConfirm();
            } else {
              this.getTables();
            }
          },
          error => {
            this.alertServ.showAlertError(localizedValues["errors.inner_error"]);
            console.log("Error en consulta de inicConfig: "+ error);
          });
      });
  }

  showAlertConfirm() {
    this.translate.get(["management.sure_init_conf", "common.acept", "common.cancel"], null).subscribe(
      (localizedValues) => {
        let alert = this.alertCtrl.create({
          message: localizedValues["management.sure_init_conf"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () =>{
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () =>{
                this.getTables();
              }
            }
          ]
        });
        alert.present();
      });
  }

  getTables(){

    /*//////////////////////////////////////////////////facturacion testing/////////////////////////////////////////////////
    this.http.get("https://my-json-server.typicode.com/citerio/jsonapi/db").map(res => res.json()).subscribe(
      (res: any) => {
        this.database.openDatabase().then(
        () => {
          for(let index = 0; index < res.tables.length; index++){
            this.database.deleteTable(res.tables[index].drop);
            this.database.createTable2(res.tables[index].create);
          }
        });
      });
    //////////////////////////////////////////////////facturacion testing/////////////////////////////////////////////////*/
    
    this.translate.get(["common.get_data", "common.creating_tables", "errors.creating_tables", "errors.serv_error", "errors.serv_not_resp"], null).subscribe(
      (localizedValues) => {
        let loading = this.loadingCtrl.create({
          content: localizedValues["common.get_data"],
          spinner: "bubbles"
        });
        loading.present();

        
        this.genericRestService.getFromUrl(AppConstants.URL_CREATE_TABLES).then(
          (res: any) => {
            if(res.status == 202) {
              loading.setContent(localizedValues["common.creating_tables"]);
              this.database.openDatabase().then(
                () => {
                  let body = JSON.parse(res._body);
                  let promises: any[] = [];
                  for(let index = 0; index < body.tables.length; index++){
                    promises.push(this.database.deleteTable(body.tables[index].drop).then(
                      () => {
                        return this.database.createTable2(body.tables[index].create).then(
                          () => {
                            return Promise.resolve();
                          });
                      }));
                  }
                  Promise.all(promises).then(
                    () => {
                      loading.dismiss().then(
                        ()=> {
                          this.database.updateInic_config(1, true);
                          this.showAlert();
                        });
                    },
                    (error) => {
                      loading.dismiss().then(
                        ()=> {
                            this.showAlertError(localizedValues["errors.creating_tables"]);
                        });
                    });
                });
            } else {
              loading.dismiss().then(
                ()=> {
                  let msg: string;
                    if( (res.headers != undefined) && (res.headers.get('sfo_error') != undefined) ){
                      msg = res.headers.get('sfo_error');
                    } else {
                      msg = localizedValues["errors.serv_error"];
                    }
                  this.showAlertError(msg);
                });
            }
          },
          (err: any) => {
              loading.dismiss().then(
                ()=> {
                  console.log('errorget: ' + err._body);
                  let msg: string;
                  if( (err.status !== undefined) && (err.status !== 0) ) {
                    if(err.headers.get('sfo_error')){
                      msg = err.headers.get('sfo_error');
                    } else {
                      msg = localizedValues["errors.serv_error"];
                    }
                  } else {
                    msg = localizedValues["errors.serv_not_resp"];
                  }
                  this.showAlertError(msg);
                });
          });
      });
  }

  showAlert(){
    this.translate.get(["common.acept", "management.init_conf_succ"], null).subscribe(
      (localizedValues) => {
        let alert = this.alertCtrl.create({
          message: localizedValues["management.init_conf_succ"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                
              }
            }
          ]
        });
        alert.present();
      });
  }

  showAlertError(errorMsg: string) {
    this.translate.get(["common.error", "common.acept"], null).subscribe(
      (localizedValues) => {
        let alert = this.alertCtrl.create({
          title: localizedValues["common.error"],
          message: '',
          buttons: [localizedValues["common.acept"]]
        });
        alert.setMessage(errorMsg);
        alert.present();
      });
  }

  resetClientsOrProducts(opt: number) {//0 = clients, 1 = Products
    this.translate.get(["common.get_data", "errors.serv_error", "errors.serv_not_resp", "common.del_data", "common.inserting_data",
      "management.refr_client_table_succ", "management.refr_prod_table_succ", "errors.del_data_error", "errors.insert_data_error"], null).subscribe(
      (localizedValues) => {
        let loading = this.loadingCtrl.create({
          content: localizedValues["common.get_data"],
          spinner: "bubbles"
        });
        loading.present();
        
        let url;
        switch(opt) {
          case 0:
            url = AppConstants.URL_GET_CLIENTS;
            break;
          case 1:
            url = AppConstants.URL_GET_PRODUCTS;
            break;
        }
        this.genericRestService.getFromUrl(url).then(
          (res: any) => {
            if(res.status == 202) {
              let body = JSON.parse(res._body);
              loading.setContent(localizedValues["common.del_data"]);

              this.database.openDatabase().then(
                () => {
                  this.database.deleteClientsOrProductTable(opt).then(
                    () => {
                      loading.setContent(localizedValues["common.inserting_data"]);
                      this.database.insertDatos(body.data).then(
                        () => {
                          loading.dismiss().then(
                            () => {
                              if(opt == 0) {
                                this.alertServ.showAlert(localizedValues["management.refr_client_table_succ"]);
                              } else {
                                this.alertServ.showAlert(localizedValues["management.refr_prod_table_succ"]);
                              }
                            });
                        },
                        error => {
                          loading.dismiss().then(
                            () => {
                              this.alertServ.showAlertError(localizedValues["errors.insert_data_error"]);
                            });
                        });
                    },
                    error => {
                      loading.dismiss().then(
                        () => {
                          this.alertServ.showAlertError(localizedValues["errors.del_data_error"]);
                        });
                    });
                },
                error => {
                  loading.dismiss().then(
                    () => {
                      this.alertServ.showAlertError(localizedValues["errors.del_data_error"]);
                    });
                });
            } else {
              loading.dismiss().then(
                ()=> {
                  let msg: string;
                  console.log('no 202');
                  if( (res.headers != undefined) && (res.headers.get('sfo_error') != undefined) ){
                    msg = res.headers.get('sfo_error');
                  } else {
                    msg = localizedValues["errors.serv_error"];
                  }
                  this.alertServ.showAlertError(msg);
                });
            }
          },
          (err: any) => {
            loading.dismiss().then(
              ()=> {
                console.log('errorget: ' + err._body);
                let msg: string;
                if( (err.status !== undefined) && (err.status !== 0) ) {
                  if(err.headers.get('sfo_error')){
                    msg = err.headers.get('sfo_error');
                  } else {
                    msg = localizedValues["errors.serv_error"];
                  }
                } else {
                  msg = localizedValues["errors.serv_not_resp"];
                }
                this.alertServ.showAlertError(msg);
              });
          });
      });
  }
}
