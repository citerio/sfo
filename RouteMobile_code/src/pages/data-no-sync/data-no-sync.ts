import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DbService } from '../../providers/db-service';

/*
  Generated class for the DataNoSync page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-data-no-sync',
  templateUrl: 'data-no-sync.html'
})
export class DataNoSyncPage {

  ordersWhSync: Boolean = false;
  cxpWhSync: Boolean = false;
  vpWhSync: Boolean = false;

  constructor(public navCtrl: NavController, private database: DbService, public navParams: NavParams) {
    this.getOrdersWhSync();
    this.getCxpWhSync();
    this.getVisitingPlansWhSync();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DataNoSyncPage');
  }

  getOrdersWhSync() {
    this.database.OrdersWhSync().then(
      (data) => {
        if(data) {
          this.ordersWhSync = true;
          console.log('si hay ordenes sin sincronizar');
        } else {
          this.ordersWhSync = false;
          console.log('no hay ordenes sin sincronizar');
        }
      },
      error => {
        console.log('error OrdersWhSync');
      });
  }

  getCxpWhSync() {
    this.database.cxpWhSync().then(
      (data) => {
        if(data) {
          this.cxpWhSync = true;
          console.log('hay clientes por planes sin sincronizar');
        } else {
          this.cxpWhSync = false;
          console.log('no hay clientes por planes sin sincronizar');
        }
      },
      error => {
        console.log('error OrdersWhSync');
      });
  }

  getVisitingPlansWhSync() {
    this.database.vpWhSync().then(
      (data) => {
        if(data) {
          this.vpWhSync = true;
          console.log('hay planes sin sincronizar');
        } else {
          this.vpWhSync = false;
          console.log('no hay planes sin sincronizar');
        }
      },
      error => {
        console.log('error OrdersWhSync');
      });
  }

}
