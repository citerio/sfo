import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
/*
  Generated class for the CestaSyncedPrinted page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cesta-synced-printed',
  templateUrl: 'cesta-synced-printed.html'
})
export class CestaSyncedPrintedPage {

  codigo_cliente:string = '';
  nombre_cliente:string = '';
  productos_escogidos:any[] = [];
  suma_total:any = {'monto':0};
  importe_total:number = 0;
  numero_actual_pedido:string = '';
  deshabilitar_cerrar:boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public database: DbService,
    public alertCtrl: AlertController,
    public platform: Platform) {

    if(this.navParams.data){

      if(this.navParams.data.cliente.codcli){

        this.codigo_cliente = this.navParams.data.cliente.codcli;

        this.database.openDatabase().then(
        () => {
          this.database.getClientName(this.navParams.data.cliente.codcli)
          .then((nombre) => {this.nombre_cliente = nombre;})
        });

        this.numero_actual_pedido = this.navParams.data.cliente.pednum;
        //this.getOrder(this.numero_actual_pedido);  

      }

      if(this.navParams.data.cliente.codigo){

        this.codigo_cliente = this.navParams.data.cliente.codigo;

        this.database.openDatabase().then(
        () => {
          this.database.getClientName(this.navParams.data.cliente.codigo)
          .then((nombre) => {this.nombre_cliente = nombre;})
        });
        

        this.numero_actual_pedido = this.database.getCurrentOrderNumber();
        //this.getOrder(this.numero_actual_pedido); 

      }

      //platform.ready().then(() => {alert(DatecsPrinter);});            

    }
  }

  ionViewWillEnter(){
    if(this.navParams.data.synced_or_printed){

      this.getOrder(this.numero_actual_pedido); 

    }          
  }

  getOrder(pednum: any){
    this.database.openDatabase().then(
        () => {
          this.database.getOrder(pednum, this.navParams.data.synced_or_printed, 1)
          .then(pedido => {      
            this.productos_escogidos = pedido;
            if(this.productos_escogidos.length == 0){
              this.deshabilitar_cerrar = true;
            }else{
              this.deshabilitar_cerrar = false;
            }
            this.orderTotalSum();
          })
          .catch((error) => {

            alert('ocurrio un error: '+ JSON.stringify(error));}

          )
        });
    
  } 

  

  orderTotalSum(){

    this.importe_total = 0;
    this.productos_escogidos.forEach((s) => 

        {          
          this.importe_total = this.importe_total + s.pedprt;
        }
    );

  }

}
