import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController, ViewController } from 'ionic-angular';
import { DbService } from "../../providers/db-service";
import { AlertsService } from "../../providers/alerts.service";
import { TranslateService } from 'ng2-translate';
import { isNumber } from 'ionic-angular/umd/util/util';

/*
  Generated class for the CobranzaProcesar page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cobranza-procesar',
  templateUrl: 'cobranza-procesar.html'
})
export class CobranzaProcesarPage {

  invoices: any[] = [];
  charge: any = {};
  selected_bank: string = '';
  printer_name = '';
  invoice: string = '';
  loader: any;
  printed_copy = false;
  monped = 'false';
  indexes: any[] = [];
  total_amount: any;
  chargesxinvoices: any[] = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public view: ViewController,
    public alertCtrl: AlertController,
    public database: DbService,
    public alertServ: AlertsService,
    public loadingCtrl: LoadingController,
    private translate: TranslateService) {

    if (this.navParams.data.invoices) {

      this.invoices = this.navParams.data.invoices;

    }

    if (this.navParams.data.charge) {

      this.charge = this.navParams.data.charge;

      if (this.charge.metpag == 1 || this.charge.metpag == 2 || this.charge.metpag == 3) {
        this.database.getBankDescByCode(this.charge.codban).then(
          (bank) => {
            this.selected_bank = bank;
          });
      }

    }

    if (this.navParams.data.indexes) {

      this.indexes = this.navParams.data.indexes;

    }

    this.previewCharges();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CobranzaProcesarPage');
  }

  confirmCharge() {
    this.translate.get(["charges.success_closed", "charges.failure_closed"], null).subscribe(
      (localizedValues) => {

        this.database.updateChargePhaseStatus(this.charge.cobnum, 3, 1, this.charge).then(
          (res) => {
            this.showMessageforPrinting(localizedValues["charges.success_closed"]);
          },
          (error) => {
            this.alertServ.showAlertError('La cobranza cabecera no se actualizo ');
          });

      });
  }

  goBack() {
    this.navCtrl.pop();
  }

  previewCharges() {
   
    this.total_amount = 0;

    this.invoices.forEach(invoice => {

      invoice.charges.forEach(charge => {

        if (charge.metpag == 1 || charge.metpag == 2 || charge.metpag == 3) {
          this.database.getBankDescByCode(charge.codban).then(
            (bank) => {
              charge.desban = bank;
            });
        }       

        this.total_amount = this.total_amount + Number(charge.montco);
        this.chargesxinvoices.push(charge);

      });

    })


  }


  /////////////////////////////////////printing operations//////////////////////////////////////////////////////////
  showMessageforPrinting(msg: any) {


    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.openPrintingConfirmation();
              }
            }
          ]
        });
        alert.present();

      });

  }

  openPrintingConfirmation() {

    this.translate.get(["common.acept", "common.cancel", "common.confirm", "charges.print_confirm"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["charges.print_confirm"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.closeViews();
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.printOrderSelectPrinter();
              }
            }
          ]
        });
        alert.present();

      });

  }

  /*printOrderSelectPrinter() {

    let context = this;

    this.translate.get(["charges.error_printers_not_found"], null).subscribe(
      (localizedValues) => {

        (<any>window).BTPrinter.list(function (data) {
          //alert("Success Listing");
          context.showPrintersList(data);
          //alert(data); //list of printer in data array
        }, function (err) {
          //alert("Error Listing");
          context.showMessage(localizedValues["charges.error_printers_not_found"]);
        });

      });

    //this.testing(); 
    //alert('imgData ' + this.imgData);     

  }*/

  printOrderSelectPrinter() {

    let context = this;

    this.translate.get(["charges.error_printers_not_found"], null).subscribe(
      (localizedValues) => {

        let data = ["200164"];

        this.showPrintersList(data);

        /*(<any>window).BTPrinter.list(function (data) {
          //alert("Success Listing");
          context.showPrintersList(data);
          //alert(data); //list of printer in data array
        }, function (err) {
          //alert("Error Listing");
          context.showMessage(localizedValues["charges.error_printers_not_found"]);
        });*/

      });

    //this.testing(); 
    //alert('imgData ' + this.imgData);     

  }

  showMessage(msg: any) {

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                console.log('aceptar');
              }
            }
          ]
        });
        alert.present();

      });

  }

  printOrderPrinting(printer_name: any) {

    let context = this;

    this.translate.get(["basket.error_printer_disconnection", "basket.error_printing_document", "basket.error_printer_connection"], null).subscribe(
      (localizedValues) => {

        (<any>window).BTPrinter.connect(function (data) {
          //alert("Success Connected");
          //alert(data);
          context.invoice = context.setInvoice();
          //this.printing();
          (<any>window).BTPrinter.printText(function (data) {
            //alert("Success printing");
            context.loader.dismiss();
            context.PrintInvoice();

            (<any>window).BTPrinter.disconnect(function (data) {
              //alert("Success disconnected");
              //alert(data)
            }, function (err) {
              context.showMessageOnPrintingSuccessOrFailure(localizedValues["basket.error_printer_disconnection"]);
              //alert(err)
            }, printer_name);
          }, function (err) {
            context.loader.dismiss();
            context.showMessageOnPrintingSuccessOrFailure(localizedValues["basket.error_printing_document"]);
          }, context.invoice);
        }, function (err) {
          //alert("Error");
          context.loader.dismiss();
          context.showMessageOnPrintingSuccessOrFailure(localizedValues["basket.error_printer_connection"]);
          //alert(err)
        }, printer_name);

      });

  }

  syncOrder() {

  }

  showPrintersList(printers: any[]) {


    this.translate.get(["common.cancel", "common.acept", "basket.choose_bluetooth_printer"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create();
        alert.setTitle(localizedValues["basket.choose_bluetooth_printer"]);

        printers.forEach((s) => {

          alert.addInput({
            type: 'radio',
            label: s,
            value: s,
            checked: false
          });

        });

        alert.addButton({
          text: localizedValues["common.cancel"],
          handler: data => {

            this.navCtrl.pop();

          }
        });

        alert.addButton({
          text: localizedValues["common.acept"],
          handler: data => {
            if (data !== '') {

              this.printer_name = data;
              this.presentLoading(data);


            }

          }
        });

        alert.present();

      });


  }

  setInvoice() {


    let header = 'RECIBO DE DINERO\r\n';

    let order_number = 'Proagro, C.A.\r\n';

    let client_info = 'Cedula Juridica:\r\n';

    let invoice = header + order_number + client_info;

    return invoice;

  }

  PrintInvoice() {

    this.translate.get(["charges.error_printing_charge"], null).subscribe(
      (localizedValues) => {

        this.database.openDatabase().then(
          () => {
            this.database.updateChargePhaseStatus(this.charge.cobnum, 6, 1, this.charge)
              .then((data) => {
                this.showMessageOnPrintingSuccess();
              })
              .catch((error) => { this.showMessage(localizedValues["charges.error_printing_charge"]); });
          });

      });

  }

  presentLoading(data) {

    
    this.translate.get(["common.wait"], null).subscribe(
      (localizedValues) => {

        this.loader = this.loadingCtrl.create({
          content: localizedValues["common.wait"],
          spinner: "bubbles"
        });

        this.loader.present()
          .then(() => { 
            //original call this.printOrderPrinting(data);
            setTimeout(()=>{ 
              this.loader.dismiss();
              this.PrintInvoice(); }, 4000);
             
          });

      });

  }

  showMessageOnPrintingSuccessOrFailure(msg: any) {

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.closeViews();
              }
            }
          ]
        });
        alert.present();

      });
  }

  showMessageOnPrintingSuccess() {


    this.translate.get(["common.acept", "charges.success_printed"], null).subscribe(
      (localizedValues) => {

        console.log('Entro en mi metodo');
        let alert = this.alertCtrl.create({
          message: localizedValues["charges.success_printed"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                if (this.printed_copy === true) {
                  this.closeViews();
                } else {
                  this.database.openDatabase().then(
                    () => {
                      this.database.getPrncpy().then(
                        (print_copy) => {
                          if (print_copy == 'true') {
                            console.log('prncpy: ' + true);
                            this.showMessagePrintCopy();
                          } else {
                            console.log('prncpy: ' + false);
                            this.closeViews();
                          }
                        },
                        (error) => {
                        });
                    });
                }
              }
            }
          ]
        });
        alert.present();

      });
  }

  showMessagePrintCopy() {


    this.translate.get(["common.acept", "common.cancel", "common.confirm", "basket.print_copy_confirm"], null).subscribe(
      (localizedValues) => {

        this.printed_copy = true;
        let alert = this.alertCtrl.create({
          title: localizedValues["common.confirm"],
          message: localizedValues["basket.print_copy_confirm"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.closeViews();
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.presentLoading(this.printer_name);
              }
            }
          ]
        });
        alert.present();

      });

  }

  closeViews() {
    this.indexes.forEach((v) => {
      this.navCtrl.remove(v);
    })
    this.navCtrl.pop();
  }



}
