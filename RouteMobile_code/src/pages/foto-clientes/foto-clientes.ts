import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { DbService } from "../../providers/db-service";
import { Diagnostic } from "@ionic-native/diagnostic";
import { TranslateService } from "ng2-translate";
import { FacturaPage } from "../factura/factura";
import { FotoCapturaPage } from "../foto-captura/foto-captura";

/*
  Generated class for the FotoClientes page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-foto-clientes',
  templateUrl: 'foto-clientes.html'
})
export class FotoClientesPage {

  clientes:any[] = [];
  clientes_copia:any[] = [];
  filtro:string = '';
  searching: any = false;
  clientes_vista:any[] = [];
  filter: string = "";
  offset: number = 0;

  constructor(

    public navCtrl: NavController,
    public view: ViewController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public database: DbService,
    private diagnostic: Diagnostic,
    private translate: TranslateService,   
    public loadingCtrl: LoadingController


  ) {}

  ionViewDidLoad() {
    //this.getAllClients();
    this.getAllClientsOffset();
  }

  getAllClientsOffset(){
    this.database.openDatabase().then(
      () => {
        this.database.getAllClientsOffset(this.offset)
        .then(response => {
          for(let i = 0; i < response.length; i++){
            this.clientes_vista.push(response[i]);
          }
        });
      });
  }

  getAllClients(){
     this.database.openDatabase().then(
      () => {
        this.database.getAllClients()
        .then(clientes => {
          this.clientes_copia = clientes;      
          this.clientes = this.clientes_copia;
          //this.setFilteredItems();
        })
      });
    
  }

  getAsocClients(){
    this.database.openDatabase().then(
      () => {
        this.database.getAsocClients()
        .then(clientes => {
          if(clientes.length > 0){
            console.log('La lista no esta vacia');
          } else {
            console.log('La lista si esta vacia');
          }
          this.clientes_copia = clientes;      
          this.clientes = this.clientes_copia;
          //this.setFilteredItems();
        })
      });
    
  }

  BuscarCliente(){    
   
    return this.clientes.filter(item => item.codcli.includes(this.filtro) ||
        item.descli.includes(this.filtro)
    )

  }  

  /*filterItems(ev: any){

      this.clientes = this.clientes_copia;

      let val = ev.target.value;

    // if the value is an empty string don't filter the items
      if (val && val.trim() != '') {
        this.clientes = this.clientes.filter((item) => {
          return (item.descli.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.codcli.includes(val));
        })
      }    
 
  }*/

  filterItems(ev: any){
    this.filter = ev.target.value;
    if (this.filter) {
      if(this.filter.trim() != '') {
        this.clientes_vista = [];
        this.database.openDatabase().then(
          () => {
            this.database.getAllClientsOffsetByfilter(this.clientes_vista.length, this.filter)
            .then(response => {
              for(let i = 0; i < response.length; i++){
                this.clientes_vista.push(response[i]);
              }
            });
          });
      } else {
        this.clientes_vista = [];
        this.database.openDatabase().then(
          () => {
            this.database.getAllClientsOffset(this.clientes_vista.length)
            .then(response => {
              for(let i = 0; i < response.length; i++){
                this.clientes_vista.push(response[i]);
              }
            });
          });
      }
    } else {
      this.clientes_vista = [];
      this.database.openDatabase().then(
        () => {
          this.database.getAllClientsOffset(this.clientes_vista.length)
          .then(response => {
            for(let i = 0; i < response.length; i++){
              this.clientes_vista.push(response[i]);
            }
          });
        });
    }
  }

  /*goToCesta(cliente: any){
    this.diagnostic.isLocationEnabled().then(
      (setting) => {
          if(setting == true) {
            this.translate.get("get_coord", null).subscribe(
              (localizedValue) => {
                let loading = this.loadingCtrl.create({
                  content: localizedValue
                });
                loading.present();
                
                this.geolocation.getCurrentPosition({timeout: 4000}).then(
                  (position) => {
                    let geopcr = position.coords.latitude.toString() + "," + position.coords.longitude.toString();
                    this.database.openDatabase().then(
                      () => {
                        this.database.createOrder(cliente, geopcr, false)
                        .then((data) => {
                          this.navCtrl.push(CestaPage, {cliente:cliente});
                          this.navCtrl.remove(this.view.index);
                        });
                      });
                    loading.dismiss();
                  }).catch((error) => {
                    this.database.openDatabase().then(
                      () => {
                        this.database.createOrder(cliente, '', false)
                        .then((data) => {
                          this.navCtrl.push(CestaPage, {cliente:cliente});
                          this.navCtrl.remove(this.view.index);
                        });
                      });
                    loading.dismiss();
                  });
              });
          } else {
            this.database.openDatabase().then(
              () => {
                this.database.getGpsobl()
                .then((gpsobl) => {
                  if(gpsobl == 'true') {
                    console.log('gpsobl: ' + true);
                    this.showMessageTurnOnGPS();
                  } else {
                    console.log('gpsobl: ' + false);
                    this.showConfirmTurnOnGPS(cliente);
                  }
                });
              });
            // this.showAlert();
          }
      },
      (error) => {
        console.log('Entro en error buscando gps setting');
      });

    
  }

  showMessageTurnOnGPS() {
    this.translate.get(["gps_off", "gps_proc_acti", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["gps_proc_acti"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  showConfirmTurnOnGPS(cliente: any) {
    this.translate.get(["gps_off", "acti_gps", "common.cancel", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["acti_gps"],
          buttons: [
            {
              text: localizedValues["common.cancel"],
              handler: () => {
                this.database.openDatabase().then(
                  () => {
                    this.database.createOrder(cliente, '', false)
                    .then((data) => {
                      this.navCtrl.push(CestaPage, {cliente:cliente});
                      this.navCtrl.remove(this.view.index);
                    });
                  });
              }
            },
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }*/

  showAlert() {
    this.translate.get(["gps_off", "gps_proc_acti", "common.acept"], null).subscribe(
      (localizedValues) => {
        let confirm = this.alertCtrl.create({
          title: localizedValues["gps_off"],
          message: localizedValues["gps_proc_acti"],
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () => {
                this.diagnostic.switchToLocationSettings();
              }
            }
          ],
          enableBackdropDismiss: false
        });
        confirm.present();
      });
  }

  ////////////////////////////////////////////////////FACTURACION/////////////////////////////////////////////////////
  checkClientInvoices(cliente: any){    
    /*this.database.openDatabase().then(
      () => {
        this.database.checkClientInvoices(cliente.codcli, 1, 1)
        .then((response) => {            
          
          /*if(response.rows.length == 0){

            this.showMessage("No Invoices");

          }else if(response.rows.length > 1){
            
            this.goToListaFacturas(response.rows, cliente);

          }else{

            this.goToFactura(response.rows.item(0));

          }
          

        
        });
      });*/
      //this.goToListaFacturas(cliente);
  }


  goToFactura(cliente: any){

    this.navCtrl.push(FacturaPage, {cliente:cliente});

  }

  goToFotoCaptura(cliente: any){

    this.navCtrl.push(FotoCapturaPage, {cliente:cliente});

  }

  showMessage(msg: any){

    this.translate.get(["common.acept"], null).subscribe(
      (localizedValues) => {

        let alert = this.alertCtrl.create({
          message: msg,
          buttons: [
            {
              text: localizedValues["common.acept"],
              handler: () =>{
                console.log('aceptar');
              }
            }
          ]
        });
        alert.present();          
      });   

  }

  doInfinite(infiniteScroll) {
    this.database.openDatabase().then(
      () => {
        if(this.filter == null || this.filter == "") {
          this.database.getAllClientsOffset(this.clientes_vista.length)
          .then(response => {
            for(let i = 0; i < response.length; i++){
              this.clientes_vista.push(response[i]);
            }

            infiniteScroll.complete();
            // if(this.productos_vista.length >= this.allProdLength){
            //   infiniteScroll.enable(false);
            // }
          });
        } else {
          this.database.getAllClientsOffsetByfilter(this.clientes_vista.length, this.filter)
          .then(response => {
            for(let i = 0; i < response.length; i++){
              this.clientes_vista.push(response[i]);
            }

            infiniteScroll.complete();
            // if(this.productos_vista.length >= this.allProdLength){
            //   infiniteScroll.enable(false);
            // }
          });
        }
      });
  }

}
