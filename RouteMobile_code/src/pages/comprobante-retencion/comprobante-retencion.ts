import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';
import { TranslateService } from 'ng2-translate';
import { AlertsService } from '../../providers/alerts.service';
import { CobranzaProcesarPage } from '../cobranza-procesar/cobranza-procesar';

/*
  Generated class for the ComprobanteRetencion page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-comprobante-retencion',
  templateUrl: 'comprobante-retencion.html'
})
export class ComprobanteRetencionPage {

  invoices:any[] = [];
  comprobantes:any[] = [];  
  charge:any={};
  selected_bank:string = '';
  retained_amount:any;
  retentions_already_charged:any[]=[];
  indexes:any[]=[];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public database: DbService,
    public view: ViewController,
    private translate: TranslateService,
    private alertServ: AlertsService) {

      if(this.navParams.data.invoices){

        this.invoices = this.navParams.data.invoices;
  
      }
  
      if(this.navParams.data.charge){
  
        this.charge = this.navParams.data.charge;
        this.getRetentionsAlreadyCharged();        
  
      }
      
      if(this.navParams.data.indexes){
  
        this.indexes = this.navParams.data.indexes;
        this.indexes.push(this.view.index);             
  
      }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComprobanteRetencionPage');    

  }

  createComprobante() {
    let comprobante = {'compnum' : '', 'cobnum' : this.charge.cobnum, 'montcomp' : 0};
    this.comprobantes.push(comprobante);   

  }

  confirmCharge(){
    this.database.deleteChargeRetentions(this.charge.cobnum).then(
      (res) => {
        //this.alertServ.showAlert('La cobranza fue almacenada con exito');
        this.database.createChargeRetentions(this.charge.cobnum, this.comprobantes).then(
          (res) => {
            //this.alertServ.showAlert('La cobranza fue almacenada con exito');
            this.navCtrl.push(CobranzaProcesarPage, {invoices:this.invoices, charge:this.charge, indexes:this.indexes}); 
          },
          (error) => {
            this.alertServ.showAlertError('Las comprobantes de la cobranza no fueron insertadas ');
        }); 
      },
      (error) => {
        this.alertServ.showAlertError('Las comprobantes de la cobranza no fueron insertadas ');
    });        
  }

  goBack() {
    this.navCtrl.pop();
  }

  getRetentionsAlreadyCharged() {
    this.database.getChargeRetentions(this.charge.cobnum, 1, 1).then(
      (retenciones) => {
        this.comprobantes = retenciones;        
      },
      error => {
        alert('Error obteniendo las retenciones ya asociadas a cobranza');
      });
  }

  deleteRetention(comprobante: any){
    let index = this.comprobantes.indexOf(comprobante);
    if(index > -1){
      this.comprobantes.splice(index, 1);
    }
  }

}
