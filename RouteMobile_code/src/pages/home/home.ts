import { Component } from '@angular/core';
import { App, NavController, NavParams, LoadingController, AlertController, Platform } from 'ionic-angular';

import { AdministracionPage } from '../administracion/administracion';
import { PedidoMainPage } from '../pedido-main/pedido-main';

import { DataPagesService } from '../../providers/data-pages-service';
import { GenericRestService } from '../../providers/generic-rest-service';
import { ConfigService } from '../../providers/config.service';
import { DbService } from '../../providers/db-service';
import {AppConstants} from '../../providers/app-constants';
import {TranslateService} from 'ng2-translate/ng2-translate';
import { AlertsService } from '../../providers/alerts.service';
import { VisitasPage } from "../visitas/visitas";
import { Storage } from '@ionic/storage';
import { Http, Response, Headers } from '@angular/http';
import { LoginPage } from "../login/login";
import { SendDataService } from "../../providers/send-data-service";
import { FacturacionPage } from "../facturacion/facturacion";
import { CobranzasPage } from "../cobranzas/cobranzas";
import { DashboardPage } from "../dashboard/dashboard";
import { FotoClientesPage } from "../foto-clientes/foto-clientes";
import { PedidoOnline } from "../pedido-online/pedido-online";

/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

export interface PageInterface {
  title?: string;
  component: any;
  icon: string;
  visible?: boolean;
  estilo: string;
  titleMsg: string;
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  params: any;
  nombre : string;
  gotMask: Boolean;
  setMask: string;

  appPages: PageInterface[] = [
      { component: AdministracionPage, icon: 'settings', visible: false, estilo: 'cell-0', titleMsg: 'management.menu_opt' },
      { component: VisitasPage, icon: 'calendar', visible: false, estilo: 'cell-1', titleMsg: 'visits.menu_opt' },
      { component: PedidoMainPage, icon: 'cart', visible: false, estilo: 'cell-2', titleMsg: 'take_orders.menu_opt' },
      { component: FacturacionPage, icon: 'paper', visible: false, estilo: 'cell-3', titleMsg: 'billing.menu_opt' },
      { component: CobranzasPage, icon: 'cash', visible: false, estilo: 'cell-4', titleMsg: 'charges.menu_opt' }
  ];

  menus: PageInterface[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public genericRestServ: GenericRestService,
    public database: DbService,
    public dataPagesService: DataPagesService,
    private translate: TranslateService,
    private alertServ: AlertsService,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private genericRestService: GenericRestService,
    private storage: Storage,
    private http: Http,
    public platform: Platform,
    private sendDataService: SendDataService,
    public appCtrl: App
  ) {
    // this.params = navParams.get('params');
    // this.gotMask = false;

    this.platformReady();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }
  
  platformReady() {
    // Call any initial plugins when ready
    
    this.platform.ready().then(
      () => {
        this.gotMask = false;
        this.dataPagesService.getMask().subscribe(
          (mask: string) => {
            console.log("Lega la mascara:" + mask);
            if( (this.gotMask === false) && (mask !== '') )  {
              console.log('entro al if mask: '+ mask);
              this.gotMask = true;
              this.setMask = mask;
              for(var i = 2; i < mask.length; i++){
                if(mask[i] === '1'){
                  this.menus.push(this.appPages[i-2]);
                }
              }
            } else if( (this.gotMask === true) && (mask !== '') && (this.setMask !== mask) ) {
              this.setMask = mask;
              this.menus = [];
              for(var i = 2; i < mask.length; i++){
                if(mask[i] === '1'){
                  this.menus.push(this.appPages[i-2]);
                }
              }
            }
          },
          (error: any) => {

          });
      });

  }

  // ionViewWillEnter() {
  //   this.navCtrl.parent.setRoot(DashboardPage);
  // }

  openPage(menu: PageInterface/*opt: string*/){
    this.navCtrl.push(menu.component);
  }

  ////////////////////////////facturacion//////////////////////////////
  goToFacturacion(){

    this.navCtrl.push(FacturacionPage);
    //this.navCtrl.push(CobranzasPage);

  }

  openPhotos(){

    this.navCtrl.push(FotoClientesPage);
    //this.navCtrl.push(CobranzasPage);

  }

  openOnlineClients(){

    this.navCtrl.push(PedidoOnline);
    //this.navCtrl.push(CobranzasPage);

  }

  goToCobranzas(){

    //this.navCtrl.push(FacturacionPage);
    this.navCtrl.push(CobranzasPage);

  }

  openSucursales() {
    this.translate.get(["common.cancel", "common.acept", "common.choose_branch_office"], null).subscribe(
      (localizedValues) => {
        this.database.getSucxcomp().then(
          (sucxcomp) => {
            let alert = this.alertCtrl.create();
            alert.setTitle(localizedValues["common.choose_branch_office"]);

            sucxcomp.forEach((s) => {
                alert.addInput({
                type: 'radio',
                label: s.dessuc + '-'+s.despro,
                value: s.codusr+'-'+s.codsuc+'-'+s.codpro,
                checked: false
              });
            });    

            alert.addButton(localizedValues["common.cancel"]);

            alert.addButton({
              text: localizedValues["common.acept"],
              handler: data => {
                if(data !== ''){
                  
                  // this.printer_name = data;
                  // this.presentLoading(data);
                  

                }
                
              }
            });
            alert.present();
          });
      });
  }
}
