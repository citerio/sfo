import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DbService } from '../../providers/db-service';

/*
  Generated class for the CestaClosed page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cesta-closed',
  templateUrl: 'cesta-closed.html'
})
export class CestaClosedPage {

codigo_cliente:string = '';
  nombre_cliente:string = '';
  productos_escogidos:any[] = [];
  suma_total:any = {'monto':0};
  importe_total:number = 0;
  numero_actual_pedido:string = '';
  deshabilitar_cerrar:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public database: DbService, public alertCtrl: AlertController) {

    if(this.navParams.data){

      if(this.navParams.data.cliente.codcli){

        this.codigo_cliente = this.navParams.data.cliente.codcli;

        this.database.openDatabase().then(
        () => {
          this.database.getClientName(this.navParams.data.cliente.codcli)
          .then((nombre) => {this.nombre_cliente = nombre;})
        });

        this.numero_actual_pedido = this.navParams.data.cliente.pednum;
        //this.getOrder(this.numero_actual_pedido);  

      }

      if(this.navParams.data.cliente.codigo){

        this.codigo_cliente = this.navParams.data.cliente.codigo;

        this.database.openDatabase().then(
        () => {
          this.database.getClientName(this.navParams.data.cliente.codigo)
          .then((nombre) => {this.nombre_cliente = nombre;})
        });
        

        this.numero_actual_pedido = this.database.getCurrentOrderNumber();
        //this.getOrder(this.numero_actual_pedido); 

      }            

    }   
           

  }

  ionViewWillEnter(){
    this.getOrder(this.numero_actual_pedido);    
  }

  getOrder(pednum: any){
    this.database.openDatabase().then(
        () => {
          this.database.getOrder(pednum, 3, 1)
          .then(pedido => {      
            this.productos_escogidos = pedido;
            if(this.productos_escogidos.length == 0){
              this.deshabilitar_cerrar = true;
            }else{
              this.deshabilitar_cerrar = false;
            }
            this.orderTotalSum();
          })
          .catch((error) => {

            alert('ocurrio un error: '+ JSON.stringify(error));}

          )
        });
    
  } 

  

  orderTotalSum(){

    this.importe_total = 0;
    this.productos_escogidos.forEach((s) => 

        {          
          this.importe_total = this.importe_total + s.pedprt;
        }
    );

  }

  printOrderSelectPrinter(){

      let context = this;

      (<any>window).BTPrinter.list(function(data){
        alert("Success Listing");
        context.showPrintersList(data);
        //alert(data); //list of printer in data array
      },function(err){
          alert("Error Listing");
          alert(err);
      });      

  }

  printOrderPrinting(printer_name:any){

    (<any>window).BTPrinter.connect(function(data){
        alert("Success Connected");
        alert(data);
        //this.printing();
        (<any>window).BTPrinter.printText(function(data){
            alert("Success printing");
            (<any>window).BTPrinter.disconnect(function(data){
                alert("Success disconnected");
                alert(data)
                },function(err){
                  alert("Error disconnecting!");
                  alert(err)
                }, printer_name);        
            },function(err){
                alert("Error printing");        
        }, "testing");
      },function(err){
        alert("Error");
        alert(err)
      }, printer_name);

  }

  showPrintersList(printers:any[]){

    let alert = this.alertCtrl.create();
    alert.setTitle('Seleccione Impresora Bluetooh');

    printers.forEach((s) => {

        alert.addInput({
        type: 'radio',
        label: s,
        value: s,
        checked: false
      });

    });    

    alert.addButton('Cancelar');

    alert.addButton({
      text: 'Aceptar',
      handler: data => {
        if(data !== ''){

          this.printOrderPrinting(data);

        }
        
      }
    });

    alert.present();


  }


  printing(){

    (<any>window).BTPrinter.printText(function(data){
        alert("Success printing");        
    },function(err){
        alert("Error printing");        
    }, "testing");


  }  


  setInvoice(){

    


  }
  

}
